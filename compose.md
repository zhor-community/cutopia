📂 ton-projet
├── 📄 docker-compose.yml
├── 📂 ipfs
│ ├── 📄 config (si nécessaire)
├── 📂 tahoe
│ ├── 📂 introducer
│ ├── 📂 storage
│ ├── 📂 client
├── 📂 hyperledger
│ ├── 📂 peer
│ ├── 📂 certs
│ ├── 📂 configtx
│ ├── 📂 crypto-config
│ ├── 📄 core.yaml (si nécessaire)
├── 📂 couchdb
│ ├── 📄 local.ini (si nécessaire)
├── 📂 cockroachdb
│ ├── 📂 data
├── 📂 mariadb
│ ├── 📂 data
│ ├── 📄 my.cnf (si besoin de config)
├── 📂 postgresql
│ ├── 📂 data
│ ├── 📄 postgresql.conf (si besoin de config)
├── 📂 redis
│ ├── 📂 data
│ ├── 📄 redis.conf (si besoin de config)
├── 📂 nextcloud
│ ├── 📂 data
│ ├── 📄 config.php (si besoin de config)
├── 📂 netdata
│ ├── 📂 config
│ ├── 📂 db
│ ├── 📂 health
├── 📂 moodle
│ ├── 📂 data
│ ├── 📂 config
│ ├── 📂 moodledata
│ ├── 📄 config.php (si besoin de config)
├── 📂 opencart
│ ├── 📂 data
│ ├── 📄 config.php (si besoin de config)
├── 📂 dolibarr
│ ├── 📂 documents
│ ├── 📂 conf
│ ├── 📄 conf.php (si besoin de config)
├── 📂 mailchain
│ ├── 📂 config
│ ├── 📄 mailchain.yaml (si besoin de config)
├── 📂 traefik
│ ├── 📄 traefik.yml
│ ├── 📂 certs
│ ├── 📂 logs
