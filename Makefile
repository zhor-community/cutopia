# Variables
REPO=git@gitlab.com:zhor-community/cutopia.git
DEST_DIR=cutopia
BRANCH=main

# Commande par défaut
.PHONY: all
all: clone install run

# Clonage du dépôt
.PHONY: clone
clone:
	@git clone --branch $(BRANCH) $(REPO) $(DEST_DIR) || { echo "❌ Échec du clonage"; exit 1; }
	@cd $(DEST_DIR) || { echo "❌ Échec d'accès au répertoire"; exit 1; }

# Installation des dépendances (multi-langages)
.PHONY: install
install:
	@cd $(DEST_DIR) && { \
		if [ -f "package.json" ]; then \
			echo "📦 Installation des dépendances Node.js..."; \
			npm install; \
		fi; \
		if [ -f "requirements.txt" ]; then \
			echo "🐍 Installation des dépendances Python..."; \
			pip install -r requirements.txt; \
		fi; \
		if [ -f "Cargo.toml" ]; then \
			echo "🦀 Compilation du projet Rust..."; \
			cargo build; \
		fi; \
		if [ -f "go.mod" ]; then \
			echo "🐹 Installation des dépendances Go..."; \
			go mod tidy; go build; \
		fi; \
		if [ -f "composer.json" ]; then \
			echo "🐘 Installation des dépendances PHP..."; \
			composer install; \
			if [ -f "artisan" ]; then \
				echo "🌟 Configuration Laravel..."; \
				cp .env.example .env; \
				php artisan key:generate; \
				php artisan migrate --seed; \
			fi; \
		fi; \
	}

# Lancement des services (multi-langages)
.PHONY: run
run:
	@cd $(DEST_DIR) && { \
		if [ -f "package.json" ]; then \
			echo "🚀 Démarrage de Node.js..."; \
			npm start & \
		fi; \
		if [ -f "manage.py" ]; then \
			echo "🚀 Démarrage du serveur Django..."; \
			python manage.py runserver & \
		fi; \
		if [ -f "Cargo.toml" ]; then \
			echo "🚀 Exécution du programme Rust..."; \
			cargo run & \
		fi; \
		if [ -f "go.mod" ]; then \
			echo "🚀 Exécution du programme Go..."; \
			./$$(basename $$(go list -m)) & \
		fi; \
		if [ -f "artisan" ]; then \
			echo "🚀 Démarrage du serveur Laravel..."; \
			php artisan serve & \
		elif [ -f "index.php" ]; then \
			echo "🚀 Démarrage du serveur PHP..."; \
			php -S localhost:8000 & \
		fi; \
		wait; \
	}

# Nettoyage du projet
.PHONY: clean
clean:
	@rm -rf $(DEST_DIR)
	@echo "🧹 Nettoyage terminé."

