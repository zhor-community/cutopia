frontend/src/store/
├── index.ts # Fichier central pour importer et organiser les stores
├── authStore.ts # Gestion de l'authentification (login, token, utilisateur)
├── clusterStore.ts # Gestion des nœuds du cluster
├── vmStore.ts # Gestion des machines virtuelles
├── storageStore.ts # Gestion des pools de stockage et disques
├── networkStore.ts # Gestion du réseau (bridges, interfaces, IPs)
├── userStore.ts # Gestion des utilisateurs et rôles
├── notificationStore.ts # Gestion des notifications globales
