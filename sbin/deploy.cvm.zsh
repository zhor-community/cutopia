#!/bin/zsh

# Définition des couleurs pour l'affichage
green='\e[32m'
red='\e[31m'
yellow='\e[33m'
blue='\e[34m'
reset='\e[0m'

# Fonction d'affichage formaté
log() {
    echo -e "${blue}[$(date '+%Y-%m-%d %H:%M:%S')] ${1}${reset}"
}

# Vérifier la présence des dépendances
check_dependencies() {
    for cmd in qemu-img qemu-system-x86_64 virsh virt-install; do
        if ! command -v $cmd &> /dev/null; then
            echo "${red}❌ Erreur: $cmd n'est pas installé.${reset}"
            exit 1
        fi
    done
}

# Créer une image disque
create_image() {
    if [ -z "$1" ] || [ -z "$2" ]; then
        echo "${yellow}❗ Usage: $0 create_image <nom> <taille (ex: 20G)>${reset}"
        exit 1
    fi
    log "📦 Création de l'image disque $1 de taille $2..."
    qemu-img create -f qcow2 "$1.qcow2" "$2"
    log "✅ Image disque $1 créée."
}

# Démarrer une machine virtuelle
start_vm() {
    if [ -z "$1" ]; then
        echo "${yellow}❗ Usage: $0 start <nom_vm>${reset}"
        exit 1
    fi
    log "🚀 Démarrage de la VM $1..."
    virsh start "$1"
    log "✅ VM $1 démarrée."
}

# Arrêter une machine virtuelle
stop_vm() {
    if [ -z "$1" ]; then
        echo "${yellow}❗ Usage: $0 stop <nom_vm>${reset}"
        exit 1
    fi
    log "🛑 Arrêt de la VM $1..."
    virsh shutdown "$1"
    log "✅ VM $1 arrêtée."
}

# Supprimer une machine virtuelle
remove_vm() {
    if [ -z "$1" ]; then
        echo "${yellow}❗ Usage: $0 remove <nom_vm>${reset}"
        exit 1
    fi
    log "🗑️ Suppression de la VM $1..."
    virsh destroy "$1"
    virsh undefine "$1"
    log "✅ VM $1 supprimée."
}

# Lister les machines virtuelles
list_vms() {
    log "📊 Liste des VMs :"
    virsh list --all
}

# Vérification des arguments et exécution
check_dependencies
case "$1" in
    create_image) create_image "$2" "$3" ;;
    start) start_vm "$2" ;;
    stop) stop_vm "$2" ;;
    remove) remove_vm "$2" ;;
    list) list_vms ;;
    *)
        echo "${yellow}❗ Usage: $0 {create_image|start|stop|remove|list} <arguments>${reset}"
        exit 1
        ;;
esac

