#!/bin/zsh

# Définition des couleurs pour l'affichage
green='\e[32m'
red='\e[31m'
yellow='\e[33m'
blue='\e[34m'
reset='\e[0m'

# Fonction d'affichage formaté
log() {
    echo -e "${blue}[$(date '+%Y-%m-%d %H:%M:%S')] ${1}${reset}"
}

# Vérifier la présence des dépendances
check_dependencies() {
    for cmd in lxc-create lxc-start lxc-stop lxc-destroy lxc-ls lxc-attach lxc-info; do
        if ! command -v $cmd &> /dev/null; then
            echo "${red}❌ Erreur: $cmd n'est pas installé.${reset}"
            exit 1
        fi
    done
}

# Créer un conteneur LXC
create_container() {
    if [ -z "$1" ]; then
        echo "${yellow}❗ Nom du conteneur requis.${reset}"
        exit 1
    fi
    log "📦 Création du conteneur $1..."
    lxc-create -n "$1" -t ubuntu -- -r focal
    log "✅ Conteneur $1 créé avec succès."
}

# Démarrer un conteneur
start_container() {
    if [ -z "$1" ]; then
        echo "${yellow}❗ Nom du conteneur requis.${reset}"
        exit 1
    fi
    log "🚀 Démarrage du conteneur $1..."
    lxc-start -n "$1" -d
    log "✅ Conteneur $1 démarré."
}

# Arrêter un conteneur
stop_container() {
    if [ -z "$1" ]; then
        echo "${yellow}❗ Nom du conteneur requis.${reset}"
        exit 1
    fi
    log "🛑 Arrêt du conteneur $1..."
    lxc-stop -n "$1"
    log "✅ Conteneur $1 arrêté."
}

# Supprimer un conteneur
remove_container() {
    if [ -z "$1" ]; then
        echo "${yellow}❗ Nom du conteneur requis.${reset}"
        exit 1
    fi
    log "🗑️ Suppression du conteneur $1..."
    lxc-destroy -n "$1"
    log "✅ Conteneur $1 supprimé."
}

# Afficher l'état des conteneurs
list_containers() {
    log "📊 Liste des conteneurs :"
    lxc-ls --fancy
}

# Attacher un terminal au conteneur
attach_container() {
    if [ -z "$1" ]; then
        echo "${yellow}❗ Nom du conteneur requis.${reset}"
        exit 1
    fi
    log "🔗 Attachement au conteneur $1..."
    lxc-attach -n "$1"
}

# Afficher les informations d'un conteneur
container_info() {
    if [ -z "$1" ]; then
        echo "${yellow}❗ Nom du conteneur requis.${reset}"
        exit 1
    fi
    log "ℹ️ Informations sur le conteneur $1 :"
    lxc-info -n "$1"
}

# Vérification des arguments et exécution
check_dependencies
case "$1" in
    create) create_container "$2" ;;
    start) start_container "$2" ;;
    stop) stop_container "$2" ;;
    remove) remove_container "$2" ;;
    list) list_containers ;;
    attach) attach_container "$2" ;;
    info) container_info "$2" ;;
    *)
        echo "${yellow}❗ Usage: $0 {create|start|stop|remove|list|attach|info} <container_name>${reset}"
        exit 1
        ;;
esac

