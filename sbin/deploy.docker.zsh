#!/bin/zsh

# Définition des couleurs pour l'affichage
green='\e[32m'
red='\e[31m'
yellow='\e[33m'
blue='\e[34m'
reset='\e[0m'

# Fichier de configuration
docker_compose_file="docker-compose.yml"
stack_name="my_stack"

# Fonction d'affichage formaté
log() {
    echo -e "${blue}[$(date '+%Y-%m-%d %H:%M:%S')] ${1}${reset}"
}

# Vérifier la présence des dépendances
check_dependencies() {
    for cmd in docker docker-compose cuinit; do
        if ! command -v $cmd &> /dev/null; then
            echo "${red}❌ Erreur: $cmd n'est pas installé.${reset}"
            exit 1
        fi
    done
}

# Initialisation du projet avec cuinit
initialize_project() {
    log "⚙️ Initialisation du projet avec cuinit..."
    cuinit || log "⚠️ Échec de l'initialisation avec cuinit."
    log "✅ Initialisation terminée."
}

# Générer le fichier docker-compose.yml
generate_compose() {
    log "🔄 Génération de $docker_compose_file..."
    {
        echo "# Auto-generated docker-compose.yml"
        cat compose.template.yml
        cat compose.network.yml
    } > "$docker_compose_file"
    log "✅ Génération terminée."
}

# Construire les images Docker
build_services() {
    log "🔨 Construction des images Docker..."
    docker-compose -f "$docker_compose_file" build
    log "✅ Construction terminée."
}

# Démarrer les services
start_services() {
    log "🚀 Démarrage des services..."
    docker-compose -f "$docker_compose_file" up -d --remove-orphans
    log "✅ Services en cours d'exécution."
}

# Arrêter les services
stop_services() {
    log "🛑 Arrêt des services..."
    docker-compose -f "$docker_compose_file" down
    log "✅ Services arrêtés."
}

# Redémarrer les services
restart_services() {
    stop_services
    start_services
}

# Afficher l'état des services
status_services() {
    log "📊 État des services :"
    docker-compose -f "$docker_compose_file" ps
}

# Nettoyer les ressources inutilisées
cleanup() {
    log "🧹 Nettoyage des ressources Docker..."
    docker system prune -af --volumes
    log "✅ Nettoyage terminé."
}

# Initialiser Docker Swarm
init_swarm() {
    log "⚙️ Initialisation de Docker Swarm..."
    docker swarm init || log "⚠️ Swarm déjà initialisé."
    log "✅ Swarm initialisé."
}

# Déployer les services avec Swarm
deploy_stack() {
    log "🚀 Déploiement de la stack $stack_name..."
    docker stack deploy -c "$docker_compose_file" "$stack_name"
    log "✅ Stack déployée."
}

# Supprimer la stack Swarm
remove_stack() {
    log "🛑 Suppression de la stack $stack_name..."
    docker stack rm "$stack_name"
    log "✅ Stack supprimée."
}

# Vérification des arguments et exécution
check_dependencies
case "$1" in
    init) initialize_project ;;
    generate) generate_compose ;;
    build) generate_compose && build_services ;;
    start) generate_compose && start_services ;;
    stop) stop_services ;;
    restart) restart_services ;;
    status) status_services ;;
    cleanup) cleanup ;;
    swarm-init) init_swarm ;;
    deploy) generate_compose && deploy_stack ;;
    remove) remove_stack ;;
    *)
        echo "${yellow}❗ Usage: $0 {init|generate|build|start|stop|restart|status|cleanup|swarm-init|deploy|remove}${reset}"
        exit 1
        ;;
esac

