#!/bin/zsh

# Définition des couleurs pour l'affichage
GREEN='\e[32m'
RED='\e[31m'
YELLOW='\e[33m'
BLUE='\e[34m'
RESET='\e[0m'

# Fonction d'affichage formaté
log() {
    echo -e "${BLUE}[$(date '+%Y-%m-%d %H:%M:%S')] ${1}${RESET}"
}

# Fonction d'affichage de l'usage
usage() {
    echo -e "${YELLOW}Usage: cunet -a <create|remove|list> -n <nom_network> -t <type>${RESET}"
    echo -e "${YELLOW}Types supportés: docker, podman, lxc, systemd, kvm, xen, openvz.${RESET}"
    exit 1
}

# Vérifier la présence des dépendances
check_dependencies() {
    local dependencies=(docker podman lxc networkctl virsh xenstore xen-network-setup vzctl)
    for cmd in "${dependencies[@]}"; do
        if ! command -v "$cmd" &> /dev/null; then
            echo -e "${RED}❌ Erreur: $cmd n'est pas installé.${RESET}"
            exit 1
        fi
    done
}

# Fonction de gestion des réseaux
cunet() {
    local action="" name="" type=""

    while getopts "a:n:t:" opt; do
        case "$opt" in
            a) action="$OPTARG" ;;
            n) name="$OPTARG" ;;
            t) type="$OPTARG" ;;
            *) usage ;;
        esac
    done

    [[ -z "$action" ]] && { echo -e "${RED}❌ Erreur: Action requise (-a).${RESET}"; usage; }

    case "$action" in
        create)
            [[ -z "$name" || -z "$type" ]] && usage
            log "🌐 Création du réseau $type : $name..."
            case "$type" in
                docker) docker network create "$name" ;;
                podman) podman network create "$name" ;;
                lxc) lxc network create "$name" ;;
                systemd) networkctl add "$name" ;;
                kvm) virsh net-define "$name.xml" && virsh net-start "$name" ;;
                xen) xenstore-write /local/domain/0/backend/vif/$name "bridge=xenbr0" && xen-network-setup "$name" ;;
                openvz) vzctl set 0 --netif_add "$name" --save ;;
                *) echo -e "${RED}❌ Type inconnu: $type.${RESET}"; usage ;;
            esac
            log "✅ Réseau $name créé."
            ;;
        remove)
            [[ -z "$name" ]] && usage
            log "🗑️ Suppression du réseau : $name..."
            case "$type" in
                docker) docker network rm "$name" ;;
                podman) podman network rm "$name" ;;
                lxc) lxc network delete "$name" ;;
                systemd) networkctl delete "$name" ;;
                kvm) virsh net-destroy "$name" && virsh net-undefine "$name" ;;
                xen) xenstore-rm /local/domain/0/backend/vif/$name ;;
                openvz) vzctl set 0 --netif_del "$name" --save ;;
                *) echo -e "${RED}❌ Type inconnu: $type.${RESET}"; usage ;;
            esac
            log "✅ Réseau $name supprimé."
            ;;
        list)
            log "📊 Liste des réseaux disponibles :"
            docker network ls 2>/dev/null
            podman network ls 2>/dev/null
            lxc network list 2>/dev/null
            networkctl list 2>/dev/null
            virsh net-list --all 2>/dev/null
            xenstore-ls /local/domain/0/backend/vif 2>/dev/null
            vzctl exec 0 ifconfig -a 2>/dev/null
            ;;
        *)
            echo -e "${RED}❌ Action inconnue: $action.${RESET}"
            usage
            ;;
    esac
}

# Exécution de la fonction avec les arguments fournis
#check_dependencies
#cunet "$@"

