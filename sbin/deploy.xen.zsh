#!/bin/zsh

# Définition des couleurs pour l'affichage
green='\e[32m'
red='\e[31m'
yellow='\e[33m'
blue='\e[34m'
reset='\e[0m'

# Fonction d'affichage formaté
log() {
    echo -e "${blue}[$(date '+%Y-%m-%d %H:%M:%S')] ${1}${reset}"
}

# Vérifier la présence des dépendances
check_dependencies() {
    for cmd in xl xen-create-image xen-delete-image; do
        if ! command -v $cmd &> /dev/null; then
            echo "${red}❌ Erreur: $cmd n'est pas installé.${reset}"
            exit 1
        fi
    done
}

# Créer une machine virtuelle Xen
create_vm() {
    if [ -z "$1" ] || [ -z "$2" ]; then
        echo "${yellow}❗ Usage: $0 create <nom_vm> <distribution>${reset}"
        exit 1
    fi
    log "📦 Création de la VM $1 avec la distribution $2..."
    xen-create-image --hostname="$1" --lvm=vg0 --dist="$2"
    log "✅ VM $1 créée."
}

# Démarrer une machine virtuelle Xen
start_vm() {
    if [ -z "$1" ]; then
        echo "${yellow}❗ Usage: $0 start <nom_vm>${reset}"
        exit 1
    fi
    log "🚀 Démarrage de la VM $1..."
    xl create "/etc/xen/$1.cfg"
    log "✅ VM $1 démarrée."
}

# Arrêter une machine virtuelle Xen
stop_vm() {
    if [ -z "$1" ]; then
        echo "${yellow}❗ Usage: $0 stop <nom_vm>${reset}"
        exit 1
    fi
    log "🛑 Arrêt de la VM $1..."
    xl shutdown "$1"
    log "✅ VM $1 arrêtée."
}

# Supprimer une machine virtuelle Xen
remove_vm() {
    if [ -z "$1" ]; then
        echo "${yellow}❗ Usage: $0 remove <nom_vm>${reset}"
        exit 1
    fi
    log "🗑️ Suppression de la VM $1..."
    xl destroy "$1"
    xen-delete-image --hostname="$1"
    log "✅ VM $1 supprimée."
}

# Lister les machines virtuelles Xen
list_vms() {
    log "📊 Liste des VMs :"
    xl list
}

# Vérification des arguments et exécution
check_dependencies
case "$1" in
    create) create_vm "$2" "$3" ;;
    start) start_vm "$2" ;;
    stop) stop_vm "$2" ;;
    remove) remove_vm "$2" ;;
    list) list_vms ;;
    *)
        echo "${yellow}❗ Usage: $0 {create|start|stop|remove|list} <arguments>${reset}"
        exit 1
        ;;
esac

