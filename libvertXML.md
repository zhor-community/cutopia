libvirtXML/
├── kvm/
│ ├── openwrt.xml # Configuration OpenWrt (KVM)
│ ├── pfsense.xml # Configuration pfSense (KVM)
│ ├── android.xml # Configuration Android (KVM)
│ ├── vps.xml # Configuration VPS basique (KVM)
│ ├── proxmox.xml # Configuration Proxmox (KVM)
│ ├── windows.xml # Configuration Windows (KVM)
│ ├── freenas.xml # Configuration FreeNAS (KVM)
│ ├── unraid.xml # Configuration Unraid (KVM)
│ ├── ipfs.xml # Configuration IPFS (KVM)
│ ├── raspberry-pi.xml # Configuration Raspberry Pi OS (ARM, KVM)
│ ├── balena-os.xml # Configuration BalenaOS (ARM, KVM)
│ ├── openwrt-arm.xml # Configuration OpenWrt (ARM, KVM)
│ └── coreelec.xml # Configuration CoreELEC (ARM, KVM)
├── xen/
│ ├── openwrt.cfg # Configuration OpenWrt (Xen)
│ └── windows.cfg # Configuration Windows (Xen)
├── lxc/
│ ├── openwrt.conf # Configuration OpenWrt (LXC)
│ ├── freenas.conf # Configuration FreeNAS (LXC)
│ └── ipfs.conf # Configuration IPFS (LXC)
├── openvz/
│ ├── openwrt.conf # Configuration OpenWrt (OpenVZ)
│ └── ipfs.conf # Configuration IPFS (OpenVZ)
├── docker/
│ ├── docker-compose.yml # Configuration IPFS avec Docker Compose
│ └── README.txt # Instructions pour Docker
├── proxmox/
│ └── README.txt # Instructions pour Proxmox
├── kubernetes/
│ └── minikube.xml # Configuration Minikube (Kubernetes, KVM)
└── openstack/
└── openstack-instance.xml # Configuration instance OpenStack (KVM)
