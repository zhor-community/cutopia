frontend/src/components/
├── common/ # Composants réutilisables (boutons, modals, tables...)
│ ├── DataTable.vue # Tableaux génériques avec tri & filtres
│ ├── LoadingSpinner.vue # Animation de chargement
│ ├── ModalDialog.vue # Fenêtre modale réutilisable
│ ├── StatusBadge.vue # Badge d'état pour VMs, nœuds, etc.
│ ├── CardInfo.vue # Carte d'affichage des infos principales
│ ├── Notification.vue # Notifications globales
│
├── dashboard/ # Composants du tableau de bord
│ ├── ClusterStatus.vue # Affichage global du cluster
│ ├── ResourceUsage.vue # Graphiques de consommation CPU/RAM/Disque
│ ├── RecentActivity.vue # Dernières actions & logs système
│
├── virtualmachines/ # Composants liés aux machines virtuelles
│ ├── VMList.vue # Liste des VMs
│ ├── VMCard.vue # Affichage résumé d'une VM
│ ├── VMDetailsPanel.vue # Panneau de détails d'une VM
│ ├── VMControls.vue # Boutons d'action (démarrer, arrêter, etc.)
│ ├── VMConsole.vue # Console VNC/SPICE intégrée
│
├── storage/ # Composants pour la gestion du stockage
│ ├── StorageList.vue # Liste des pools de stockage
│ ├── StorageDetails.vue # Détails d'un pool de stockage
│ ├── VolumeManager.vue # Gestion des disques et snapshots
│
├── network/ # Composants pour la gestion réseau
│ ├── NetworkList.vue # Liste des réseaux configurés
│ ├── NetworkDetails.vue # Paramètres d'un réseau spécifique
│ ├── InterfaceList.vue # Interfaces réseau des VMs
│
├── cluster/ # Composants pour la gestion des nœuds
│ ├── NodeList.vue # Liste des nœuds du cluster
│ ├── NodeStatus.vue # Statut détaillé d’un nœud
│ ├── NodeMetrics.vue # Graphiques et statistiques d’un nœud
│ ├── NodeActions.vue # Boutons d'action pour gérer un nœud
│
├── users/ # Composants pour la gestion des utilisateurs
│ ├── UserList.vue # Liste des utilisateurs
│ ├── UserForm.vue # Formulaire de gestion d’un utilisateur
│ ├── RoleManager.vue # Gestion des rôles et permissions
│
├── auth/ # Composants pour l'authentification
│ ├── LoginForm.vue # Formulaire de connexion
│ ├── RegisterForm.vue # Formulaire d'inscription
│ ├── PasswordReset.vue # Réinitialisation de mot de passe
