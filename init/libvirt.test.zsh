#sudo systemctl status libvirtd
#!/bin/bash

# Déclaration des variables globales
LOG_FILE="/var/log/fix_libvirt.log" # Fichier de log pour le script
ISO_DIR="/var/www/files/isos"       # Répertoire pour les images ISO

# Fonction de journalisation
log_message() {
    local message="$1"
    echo "$(date '+%Y-%m-%d %H:%M:%S') - $message" | tee -a "$LOG_FILE"
}

# Gestion des erreurs
handle_error() {
    local error_message="$1"
    log_message "ERREUR : $error_message"
    exit 1
}

# Vérification et activation de KVM
check_and_enable_kvm() {
    log_message "Vérification et activation de KVM..."

    # Vérifier si la virtualisation est activée dans le BIOS/UEFI
    if ! grep -q -E '(vmx|svm)' /proc/cpuinfo; then
        handle_error "La virtualisation n'est pas activée dans le BIOS/UEFI. Veuillez activer la virtualisation et redémarrer."
    fi

    # Installer les modules KVM
    log_message "Installation et chargement des modules KVM..."
    if ! dpkg -l | grep -q qemu-kvm; then
        sudo apt update && sudo apt install -y qemu-kvm >> "$LOG_FILE" 2>&1 || handle_error "Échec de l'installation de qemu-kvm."
    fi

    # Charger les modules KVM
    if ! lsmod | grep -q kvm; then
        sudo modprobe kvm >> "$LOG_FILE" 2>&1 || handle_error "Échec du chargement du module kvm."
        if [ $(uname -m) = "x86_64" ]; then
            sudo modprobe kvm_intel >> "$LOG_FILE" 2>&1 || handle_error "Échec du chargement du module kvm_intel."
        elif [ $(uname -m) = "aarch64" ]; then
            sudo modprobe kvm_arm >> "$LOG_FILE" 2>&1 || handle_error "Échec du chargement du module kvm_arm."
        else
            handle_error "Architecture non prise en charge pour KVM."
        fi
    fi

    # Ajouter l'utilisateur au groupe kvm
    log_message "Ajout de l'utilisateur au groupe kvm..."
    if ! groups "$(whoami)" | grep -q kvm; then
        sudo usermod -aG kvm "$(whoami)" >> "$LOG_FILE" 2>&1 || handle_error "Échec de l'ajout à kvm."
        log_message "Redémarrez votre session pour appliquer les changements de groupe kvm."
    fi
}

# Création du répertoire pour les ISOs
create_iso_directory() {
    log_message "Création du répertoire pour les ISOs..."

    if [ ! -d "$ISO_DIR" ]; then
        sudo mkdir -p "$ISO_DIR" >> "$LOG_FILE" 2>&1 || handle_error "Échec de la création du répertoire $ISO_DIR."
        sudo chmod 755 "$ISO_DIR" >> "$LOG_FILE" 2>&1 || handle_error "Échec de la modification des permissions de $ISO_DIR."
        sudo chown root:root "$ISO_DIR" >> "$LOG_FILE" 2>&1 || handle_error "Échec de la modification du propriétaire de $ISO_DIR."
        log_message "Répertoire $ISO_DIR créé avec succès."
    else
        log_message "Le répertoire $ISO_DIR existe déjà."
    fi
}

# Redémarrage du service libvirtd
restart_libvirtd() {
    log_message "Redémarrage du service libvirtd..."
    sudo systemctl restart libvirtd >> "$LOG_FILE" 2>&1 || handle_error "Échec du redémarrage du service libvirtd."
    log_message "Service libvirtd redémarré avec succès."
}

# Vérification finale du statut de libvirtd
check_libvirtd_status() {
    log_message "Vérification du statut de libvirtd..."
    if sudo systemctl is-active --quiet libvirtd; then
        log_message "Le service libvirtd est actif (running)."
    else
        handle_error "Le service libvirtd n'est pas actif. Vérifiez les logs pour plus de détails."
    fi
}

# Fonction principale
main() {
    log_message "Démarrage du script de correction pour libvirt..."
    check_and_enable_kvm
    create_iso_directory
    restart_libvirtd
    check_libvirtd_status
    log_message "Correction terminée avec succès."
}

# Exécution du script
main
