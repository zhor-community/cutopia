#!/bin/bash

# Déclaration des variables globales
LOG_FILE="/var/log/install_hypervisor.log" # Fichier de log pour le script
#ISO_PATH="$CUTOPIAPATH/var/www/files/isos/debian.iso"            # Chemin vers l'image ISO (à modifier selon vos besoins)
#ISO_PATH="$CUTOPIAPATH/var/www/files/isos/cutopia.iso"            # Chemin vers l'image ISO (à modifier selon vos besoins)
ISO_PATH="$CUTOPIAPATH/var/www/files/isos/ticenergy.iso"            # Chemin vers l'image ISO (à modifier selon vos besoins)
HYPERVISOR="libvirt"                       # Hyperviseur par défaut (libvirt ou xen)

# Fonction principale qui gère toutes les opérations
main_function() {
    local action="$1"  # Action à effectuer (basée sur les options)

    case "$action" in
        "install")
            install_hypervisor
            ;;
        "configure")
            configure_hypervisor
            ;;
        "test")
            test_hypervisor
            ;;
        "all")
            install_hypervisor
            configure_hypervisor
            test_hypervisor
            ;;
        *)
            echo "Option non reconnue." | tee -a "$LOG_FILE"
            usage
            exit 1
            ;;
    esac
}

# Installation de l'hyperviseur sélectionné (libvirt ou Xen)
install_hypervisor() {
    log_message "Début de l'installation de $HYPERVISOR..."

    if [ "$HYPERVISOR" = "libvirt" ]; then
        log_message "Installation de libvirt et ses dépendances..."
        sudo apt update && sudo apt upgrade -y >> "$LOG_FILE" 2>&1 || handle_error "Échec de la mise à jour du système."
        sudo apt install -y libvirt-daemon-system libvirt-clients qemu-kvm virt-manager bridge-utils >> "$LOG_FILE" 2>&1 || handle_error "Échec de l'installation des paquets libvirt."

        log_message "Ajout de l'utilisateur actuel au groupe libvirt..."
        sudo usermod -aG libvirt "$(whoami)" >> "$LOG_FILE" 2>&1 || handle_error "Échec de l'ajout à libvirt."
        log_message "Redémarrez votre session pour que les changements prennent effet."
    elif [ "$HYPERVISOR" = "xen" ]; then
        log_message "Installation de Xen..."
        sudo apt update && sudo apt upgrade -y >> "$LOG_FILE" 2>&1 || handle_error "Échec de la mise à jour du système."
        sudo apt install -y xen-hypervisor-amd64 xen-tools >> "$LOG_FILE" 2>&1 || handle_error "Échec de l'installation des paquets Xen."

        log_message "Configuration du noyau pour Xen..."
        sudo sed -i 's/^GRUB_DEFAULT=.*$/GRUB_DEFAULT="Xen 4.16-amd64"/' /etc/default/grub >> "$LOG_FILE" 2>&1 || handle_error "Échec de la configuration GRUB."
        sudo update-grub >> "$LOG_FILE" 2>&1 || handle_error "Échec de la mise à jour de GRUB."
        log_message "Redémarrez le système pour activer Xen."
    else
        handle_error "Hyperviseur non pris en charge : $HYPERVISOR"
    fi
}

# Configuration de l'hyperviseur sélectionné (libvirt ou Xen)
configure_hypervisor() {
    log_message "Début de la configuration de $HYPERVISOR..."

    if [ "$HYPERVISOR" = "libvirt" ]; then
        log_message "Activation et démarrage du service libvirt..."
        sudo systemctl enable --now libvirtd >> "$LOG_FILE" 2>&1 || handle_error "Échec du démarrage du service libvirt."
        sudo systemctl status libvirtd >> "$LOG_FILE" 2>&1

        log_message "Configuration du réseau NAT par défaut..."
        virsh net-start default >> "$LOG_FILE" 2>&1 || handle_error "Échec du démarrage du réseau NAT."
        virsh net-autostart default >> "$LOG_FILE" 2>&1 || handle_error "Échec de l'autodémarrage du réseau NAT."
    elif [ "$HYPERVISOR" = "xen" ]; then
        log_message "Chargement du module xen-netback..."
        sudo modprobe xen-netback >> "$LOG_FILE" 2>&1 || handle_error "Échec de la charge du module xen-netback."

        log_message "Création du pont réseau pour Xen..."
        sudo tee /etc/network/interfaces.d/xen-br0 <<EOF > /dev/null
auto xenbr0
iface xenbr0 inet static
    bridge_ports eth0
    bridge_stp off
    bridge_fd 0
    address 192.168.1.100
    netmask 255.255.255.0
    gateway 192.168.1.1
    dns-nameservers 8.8.8.8 8.8.4.4
EOF

        log_message "Redémarrage du service réseau..."
        sudo systemctl restart networking >> "$LOG_FILE" 2>&1 || handle_error "Échec du redémarrage du service réseau."
    else
        handle_error "Hyperviseur non pris en charge : $HYPERVISOR"
    fi

    log_message "Configuration terminée avec succès."
}

# Tests de l'hyperviseur sélectionné (libvirt ou Xen)
test_hypervisor() {
    log_message "Début des tests de $HYPERVISOR..."

    if [ "$HYPERVISOR" = "libvirt" ]; then
        log_message "Liste des domaines disponibles..."
        virsh list --all >> "$LOG_FILE" 2>&1

        log_message "Création d'une machine virtuelle de test..."
        virt-install \
            --name=testvm \
            --memory=1024 \
            --vcpus=1 \
            --os-type=linux \
            --os-variant=debian11 \
            --disk path=/var/lib/libvirt/images/testvm.qcow2,size=5 \
            --cdrom="$ISO_PATH" \
            --network network=default \
            --graphics vnc,listen=0.0.0.0 \
            --noautoconsole >> "$LOG_FILE" 2>&1 || handle_error "Échec de la création de la machine virtuelle."

        log_message "Test de connectivité réseau pour la machine virtuelle..."
        virsh domifaddr testvm >> "$LOG_FILE" 2>&1
    elif [ "$HYPERVISOR" = "xen" ]; then
        log_message "Liste des domaines Xen disponibles..."
        xm list >> "$LOG_FILE" 2>&1

        log_message "Création d'une machine virtuelle Xen de test..."
        xen-create-image --hostname=testvm-xen --memory=1024mb --vcpus=1 --dist=debian11 --role=udev >> "$LOG_FILE" 2>&1 || handle_error "Échec de la création de la machine virtuelle Xen."

        log_message "Démarrage de la machine virtuelle Xen..."
        xm create /etc/xen/testvm-xen.cfg >> "$LOG_FILE" 2>&1 || handle_error "Échec du démarrage de la machine virtuelle Xen."
    else
        handle_error "Hyperviseur non pris en charge : $HYPERVISOR"
    fi

    log_message "Tests terminés avec succès."
}

# Fonction de journalisation
log_message() {
    local message="$1"
    echo "$message" | tee -a "$LOG_FILE"
}

# Gestion des erreurs
handle_error() {
    local error_message="$1"
    log_message "ERREUR : $error_message"
    exit 1
}

# Affiche l'aide/utilisation du script
usage() {
    echo "Usage: $0 -i | -c | -t | -a [-x]"
    echo "Options:"
    echo "  -i : Installer l'hyperviseur (libvirt ou Xen)."
    echo "  -c : Configurer l'hyperviseur."
    echo "  -t : Effectuer des tests sur l'hyperviseur."
    echo "  -a : Exécuter toutes les étapes (installation, configuration et tests)."
    echo "  -x : Utiliser Xen comme hyperviseur (par défaut : libvirt)."
    echo "Logs sont enregistrés dans : $LOG_FILE"
}

# Parsing des options avec getopts
while getopts ":ictax" opt; do
    case "${opt}" in
        i)  # Installation de l'hyperviseur
            main_function "install"
            ;;
        c)  # Configuration de l'hyperviseur
            main_function "configure"
            ;;
        t)  # Tests de l'hyperviseur
            main_function "test"
            ;;
        a)  # Toutes les étapes
            main_function "all"
            ;;
        x)  # Utiliser Xen comme hyperviseur
            HYPERVISOR="xen"
            ;;
        *)  # Gestion des options incorrectes
            usage
            exit 1
            ;;
    esac
done

# Si aucune option n'est spécifiée, affiche l'aide
if [ "$OPTIND" -eq 1 ]; then
    usage
    exit 1
fi

# Fin du script
exit 0
