#!/bin/zsh
# ==========================================
# 🎯 CUMOTO - Surveillance système & services
# Auteur : Vous
# Version : 1.0
# Description : Surveille CPU, RAM, Disque, Réseau et Services
# ==========================================

cumoto() {
  local config_file="cumoto.json"
  
  # Vérifier si jq est installé
  if ! command -v jq &>/dev/null; then
    echo "❌ Erreur : jq n'est pas installé. Installez-le avec 'sudo apt install jq' ou 'brew install jq'."
    return 1
  fi
  
  # Vérifier si le fichier de config existe
  if [[ ! -f "$config_file" ]]; then
    echo "❌ Erreur : Fichier $config_file introuvable !"
    return 1
  fi

  # Charger la configuration
  local cpu_threshold mem_threshold disk_threshold network_interface
  local services log_files alert_methods webhook_url
  local email_recipient smtp_server smtp_user smtp_password
  
  cpu_threshold=$(jq '.cpu_threshold' "$config_file")
  mem_threshold=$(jq '.mem_threshold' "$config_file")
  disk_threshold=$(jq '.disk_threshold' "$config_file")
  network_interface=$(jq -r '.network_interface' "$config_file")
  services=($(jq -r '.services[]' "$config_file"))
  log_files=($(jq -r '.log_files[]' "$config_file"))
  alert_methods=$(jq -c '.alert_methods' "$config_file")
  webhook_url=$(jq -r '.webhook_url' "$config_file")
  
  email_recipient=$(jq -r '.email_settings.recipient' "$config_file")
  smtp_server=$(jq -r '.email_settings.smtp_server' "$config_file")
  smtp_user=$(jq -r '.email_settings.smtp_user' "$config_file")
  smtp_password=$(jq -r '.email_settings.smtp_password' "$config_file")

  echo "📊 Surveillance activée avec configuration de $config_file"

  # Fonction d'alerte
  send_alert() {
    local message="$1"
    echo "⚠️ Alerte : $message"

    # Notification de bureau
    if [[ $(echo "$alert_methods" | jq '.desktop') == "true" ]]; then
      notify-send "⚠️ Cumoto Alert" "$message"
    fi

    # Envoi d'un e-mail
    if [[ $(echo "$alert_methods" | jq '.email') == "true" ]]; then
      echo "$message" | mail -s "Cumoto Alert" "$email_recipient"
    fi

    # Envoi vers un webhook (ex : Slack, Discord)
    if [[ $(echo "$alert_methods" | jq '.webhook') == "true" ]]; then
      curl -X POST -H "Content-Type: application/json" -d "{\"text\": \"$message\"}" "$webhook_url"
    fi
  }

  # Vérification CPU
  local cpu_usage=$(top -bn1 | grep "Cpu(s)" | awk '{print $2+$4}')
  echo "🖥️ CPU: $cpu_usage%"
  if (( ${cpu_usage%.*} > cpu_threshold )); then
    send_alert "CPU élevé : $cpu_usage%"
  fi

  # Vérification Mémoire
  local mem_usage=$(free | awk '/Mem:/ {printf("%.0f", $3/$2 * 100)}')
  echo "🧠 Mémoire: $mem_usage%"
  if (( mem_usage > mem_threshold )); then
    send_alert "Mémoire élevée : $mem_usage%"
  fi

  # Vérification Disque
  local disk_usage=$(df / | awk 'NR==2 {print $5}' | tr -d '%')
  echo "💾 Disque: $disk_usage%"
  if (( disk_usage > disk_threshold )); then
    send_alert "Espace disque faible : $disk_usage%"
  fi

  # Vérification Réseau
  local net_rx=$(cat /sys/class/net/$network_interface/statistics/rx_bytes)
  local net_tx=$(cat /sys/class/net/$network_interface/statistics/tx_bytes)
  echo "📡 Réseau: ${network_interface} ⬇️ $net_rx octets ⬆️ $net_tx octets"

  # Vérification Services
  for service in "${services[@]}"; do
    if systemctl is-active --quiet "$service"; then
      echo "✅ Service '$service' est actif."
    else
      echo "❌ Service '$service' est arrêté !"
      send_alert "Service $service arrêté !"
    fi
  done

  # Surveillance des logs
  for log_file in "${log_files[@]}"; do
    if [[ -f "$log_file" ]]; then
      echo "📜 Vérification des erreurs dans $log_file..."
      if grep -i "error" "$log_file" | tail -n 5; then
        send_alert "Erreurs trouvées dans $log_file"
      else
        echo "✅ Aucun problème détecté."
      fi
    fi
  done
}

# Exécuter la fonction
#cumoto


