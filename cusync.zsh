#!/bin/bash

# Liste des serveurs SSH (remplace par tes serveurs)
SERVERS=("user@server1" "user@server2" "user@server3")

# Répertoire du dépôt Git sur les serveurs
REPO_DIR="/path/to/repo"

# Branche à synchroniser
BRANCH="main"

# Fonction pour synchroniser un serveur
sync_server() {
    SERVER=$1
    echo "🔄 Synchronisation avec $SERVER..."
    
    ssh "$SERVER" <<EOF
        cd "$REPO_DIR" || exit 1
        git fetch origin
        git reset --hard origin/$BRANCH
        git pull origin $BRANCH
        echo "✅ Synchronisation terminée sur $SERVER"
EOF
}

# Boucle sur tous les serveurs
for SERVER in "${SERVERS[@]}"; do
    sync_server "$SERVER"
done

