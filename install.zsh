#!/bin/zsh
set -e  # Stoppe le script en cas d'erreur
function cuinit(){
# 🔹 Configuration
REPO="git@gitlab.com:zhor-community/cutopia.git"
DEST_DIR="/opt/cutopia"
BRANCH="main"
PIDS=()

# 🔄 Fonction de clonage du dépôt
clone_repo() {
    rm -rf /opt/cutopia
    echo "🔄 Clonage du dépôt..."
    git clone --branch $BRANCH $REPO $DEST_DIR || { echo "❌ Échec du clonage"; exit 1; }
    cd $DEST_DIR || { echo "❌ Impossible d'accéder au répertoire"; exit 1; }
    export CUTOPIAPATH=/opt/cutopia
    
    source $DEST_DIR/src/zsh/cusource.zsh
}

# 📦 Fonction d'installation des dépendances
install_dependencies() {
    echo "🔍 Détection et installation des dépendances..."

    # CuAPK
    [[ -f "cuapks.json" ]] && echo "📦 CuAPK..." && export CUREQUIREMENT=("curl" "tree") && cuapk add $CUREQUIREMENT
    # Cupmose
    [[ -f "cupmose.json" ]] && echo "🧩 Cupmose..." && cupmose install
    # CuCLI
    [[ -f "cuclis.json" ]] && echo "🖥️ CuCLI..." && cucli install
    # CuVM
    [[ -f "cuvms.json" ]] && echo "💾 CuVM..." && cuvm setup
    # CuNet
    [[ -f "cunets.json" ]] && echo "🌐 CuNet..." && cunet apply

    # PHP (Composer + Laravel)
    if [[ -f "composer.json" ]]; then
        echo "🐘 PHP (Composer)..."
        composer install
        [[ -f "artisan" ]] && echo "🌟 Laravel détecté..." && cp .env.example .env && php artisan key:generate && php artisan migrate --seed
    fi

    # Node.js
    [[ -f "package.json" ]] && echo "📦 Node.js..." && npm install

    # Python
    [[ -f "requirements.txt" ]] && echo "🐍 Python..." && pip install -r requirements.txt

    # Rust
    if [[ -f "Cargo.toml" ]]; then
        echo "🦀 Rust..."
        cargo build
        grep -q '\[dependencies\].*wasm-bindgen' Cargo.toml && echo "🌐 WebAssembly..." && wasm-pack build
    fi

    # Go
    [[ -f "go.mod" ]] && echo "🐹 Go..." && go mod tidy && go build

    # Java (Maven + Spring Boot)
    if [[ -f "pom.xml" ]]; then
        echo "☕ Java (Maven)..."
        mvn clean install
        grep -q "spring-boot" pom.xml && echo "🚀 Spring Boot..." && mvn spring-boot:run &
    fi

    # Java/Kotlin (Gradle)
    [[ -f "build.gradle" || -f "build.gradle.kts" ]] && echo "🚀 Gradle..." && ./gradlew build

    # Dart/Flutter
    [[ -f "pubspec.yaml" ]] && echo "🦄 Flutter..." && flutter pub get

    # Ruby (Bundler)
    [[ -f "Gemfile" ]] && echo "💎 Ruby..." && bundle install

    # Elixir (Mix)
    [[ -f "mix.exs" ]] && echo "🚀 Elixir..." && mix deps.get

    # C/C++ (Makefile)
    #[[ -f "Makefile" ]] && echo "⚙️ Make..." && make

    # C/C++ (CMake)
    if [[ -f "CMakeLists.txt" ]]; then
        echo "🔧 CMake..."
        mkdir -p build && cd build && cmake .. && make && cd ..
    fi

    # .NET Core (C#)
    [[ -f "dotnet.csproj" ]] && echo "🖥️ .NET Core..." && dotnet restore && dotnet build

    # Perl (CPAN)
    [[ -f "cpanfile" ]] && echo "🐪 Perl..." && cpanm --installdeps .

    # Haskell (Stack & Cabal)
    [[ -f "stack.yaml" ]] && echo "📚 Haskell (Stack)..." && stack setup && stack build
    [[ -f "cabal.project" || -f "*.cabal" ]] && echo "📦 Haskell (Cabal)..." && cabal update && cabal install --only-dependencies

    # Scala (SBT)
    [[ -f "build.sbt" ]] && echo "🎭 Scala (SBT)..." && sbt update && sbt compile

    # Terraform
    find . -name "*.tf" | grep -q . && echo "🌍 Terraform..." && terraform init

    # Ansible
    [[ -f "ansible.cfg" || -d "roles" ]] && echo "📜 Ansible..." && ansible-galaxy install -r requirements.yml

    # Docker Compose
    [[ -f "docker-compose.yml" ]] && echo "🐳 Docker Compose..." && docker-compose up -d
}

# 🚀 Fonction de lancement des applications
start_applications() {
    echo "🚀 Lancement des services..."

    [[ -f "package.json" ]] && echo "🚀 Node.js..." && npm start & PIDS+=($!)
    [[ -f "manage.py" ]] && echo "🚀 Django..." && python manage.py runserver & PIDS+=($!)
    [[ -f "main.py" && $(grep -i "fastapi" main.py) ]] && echo "🚀 FastAPI..." && uvicorn main:app --reload & PIDS+=($!)
    [[ -f "Cargo.toml" ]] && echo "🚀 Rust..." && cargo run & PIDS+=($!)
    [[ -f "go.mod" ]] && echo "🚀 Go..." && ./$(basename $(go list -m)) & PIDS+=($!)
    [[ -f "artisan" ]] && echo "🚀 Laravel..." && php artisan serve & PIDS+=($!)
    [[ -f "index.php" ]] && echo "🚀 PHP..." && php -S localhost:8000 & PIDS+=($!)
}

# 🔄 Fonction pour redémarrer les services système
restart_services() {
    echo "🔄 Redémarrage des services..."
    [[ -f "/etc/systemd/system/myapp.service" ]] && systemctl restart myapp.service
}

# 📌 Exécution des étapes
clone_repo
install_dependencies
start_applications

# ⏳ Attente de la fin des processus
for pid in "${PIDS[@]}"; do
    wait $pid
done

restart_services

echo "✅ Installation et exécution terminées !"
}
cuinit
