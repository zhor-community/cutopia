projet-libvirt-quasar
├── backend/ # Backend en Rust
│ ├── src/
│ │ ├── config.rs # Configuration du backend
│ │ ├── libvirt.rs # Gestion des interactions avec Libvirt
│ │ ├── handlers.rs # Gestion des requêtes API
│ │ ├── models.rs # Définition des structures de données
│ │ ├── routes.rs # Définition des routes API
│ │ ├── main.rs # Point d'entrée du backend
│ ├── Cargo.toml # Dépendances et configuration Rust
│ ├── config.yaml # Fichier de configuration
│ ├── Dockerfile # Conteneurisation du backend
│ ├── .env # Variables d'environnement
│ ├── README.md # Documentation du backend
│
├── frontend/ # Frontend en QuasarJS
│ ├── src/
│ │ ├── components/ # Composants UI
│ │ ├── pages/ # Pages principales
│ │ ├── layouts/ # Layouts globaux
│ │ ├── router/ # Configuration du router
│ │ ├── store/ # Gestion de l'état (Pinia/Vuex)
│ │ ├── boot/ # Initialisation (auth, API, etc.)
│ │ ├── App.vue # Composant principal
│ │ ├── main.js # Point d'entrée de l'application
│ ├── public/ # Fichiers statiques
│ ├── quasar.config.js # Configuration de Quasar
│ ├── package.json # Dépendances frontend
│ ├── README.md # Documentation frontend
│
├── infra/ # Infrastructure et déploiement
│ ├── docker-compose.yml # Déploiement Docker
│ ├── k8s/ # Configuration Kubernetes (optionnel)
│ ├── ansible/ # Scripts d'automatisation (optionnel)
│ ├── README.md # Documentation infrastructure
│
├── docs/ # Documentation du projet
│ ├── architecture.md # Explication de l'architecture
│ ├── api.md # Documentation de l'API backend
│ ├── frontend.md # Guide d'utilisation du frontend
│
├── scripts/ # Scripts utilitaires
│ ├── init_db.sh # Initialisation de la base de données (si nécessaire)
│ ├── deploy.sh # Script de déploiement
│
├── .gitignore # Fichiers à ignorer pour Git
├── README.md # Documentation principale
