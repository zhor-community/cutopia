#!/bin/zsh

# === Configuration ===
LOG_DIR="./logs"
LOG_FILE="$LOG_DIR/install.log"
ENV_FILE=".env"

# Créer le dossier de logs
mkdir -p "$LOG_DIR"
touch "$LOG_FILE"

# Fonction pour logger les messages
log() {
    local level=$1
    local message=$2
    echo "[$(date +'%Y-%m-%d %H:%M:%S')] [$level] $message" | tee -a "$LOG_FILE"
}

# Charger les variables d'environnement
load_env() {
    if [[ -f "$ENV_FILE" ]]; then
        source "$ENV_FILE"
        log "INFO" "Fichier .env chargé."
    else
        log "ERROR" "Fichier .env non trouvé. Créez un fichier .env avec les configurations nécessaires."
        exit 1
    fi
}

# Fonction pour vérifier les dépendances système
check_dependencies() {
    local dependencies=("curl" "wget" "tar" "sudo")
    for dep in "${dependencies[@]}"; do
        if ! command -v "$dep" &> /dev/null; then
            log "ERROR" "Dépendance manquante : $dep"
            exit 1
        fi
    done
    log "INFO" "Toutes les dépendances système sont installées."
}

# === Fonctions d'installation ===

# Mettre à jour le système
update_system() {
    log "INFO" "Mise à jour du système..."
    sudo apt update && sudo apt upgrade -y || {
        log "ERROR" "Échec de la mise à jour du système."
        exit 1
    }
}

# Installer Rust
install_rust() {
    log "INFO" "Installation de Rust..."
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y || {
        log "ERROR" "Échec de l'installation de Rust."
        exit 1
    }
    source "$HOME/.cargo/env"
    rustup component add clippy rustfmt || {
        log "ERROR" "Échec de l'ajout des composants Rust."
        exit 1
    }
}

# Installer et configurer VPN
install_vpn() {
    log "INFO" "Installation d'OpenVPN et WireGuard..."
    sudo apt install openvpn wireguard -y || {
        log "ERROR" "Échec de l'installation des outils VPN."
        exit 1
    }

    # Configurer OpenVPN
    if [[ -f "$OPENVPN_CONFIG_PATH" ]]; then
        sudo cp "$OPENVPN_CONFIG_PATH" /etc/openvpn/client.conf
        sudo systemctl enable openvpn@client
        sudo systemctl start openvpn@client
        log "INFO" "OpenVPN configuré et démarré."
    else
        log "ERROR" "Fichier de configuration OpenVPN introuvable : $OPENVPN_CONFIG_PATH"
    fi

    # Configurer WireGuard
    if [[ -f "$WIREGUARD_CONFIG_PATH" ]]; then
        sudo wg-quick up "$WIREGUARD_INTERFACE"
        sudo systemctl enable wg-quick@"$WIREGUARD_INTERFACE"
        log "INFO" "WireGuard configuré et démarré."
    else
        log "ERROR" "Fichier de configuration WireGuard introuvable : $WIREGUARD_CONFIG_PATH"
    fi
}

# Installer et configurer IPFS
install_ipfs() {
    log "INFO" "Installation d'IPFS..."
    wget https://dist.ipfs.tech/kubo/v0.23.0/kubo_v0.23.0_linux-amd64.tar.gz || {
        log "ERROR" "Échec du téléchargement d'IPFS."
        exit 1
    }
    tar -xvzf kubo_v0.23.0_linux-amd64.tar.gz || {
        log "ERROR" "Échec de l'extraction d'IPFS."
        exit 1
    }
    sudo mv kubo/ipfs /usr/local/bin/ipfs || {
        log "ERROR" "Échec de l'installation d'IPFS."
        exit 1
    }
    rm -rf kubo kubo_v0.23.0_linux-amd64.tar.gz

    log "INFO" "Configuration d'IPFS..."
    ipfs init || {
        log "ERROR" "Échec de l'initialisation d'IPFS."
        exit 1
    }
    ipfs config Datastore.StorageMax "$IPFS_STORAGE_MAX" || {
        log "ERROR" "Échec de la configuration d'IPFS."
        exit 1
    }
}

# Installer et configurer GPG
install_gpg() {
    log "INFO" "Installation de GPG..."
    sudo apt install gnupg -y || {
        log "ERROR" "Échec de l'installation de GPG."
        exit 1
    }

    log "INFO" "Configuration de GPG..."
    gpg --full-generate-key || {
        log "ERROR" "Échec de la génération de la clé GPG."
        exit 1
    }
}

# Installer et configurer SSH
install_ssh() {
    log "INFO" "Installation d'OpenSSH..."
    sudo apt install openssh-client openssh-server -y || {
        log "ERROR" "Échec de l'installation d'OpenSSH."
        exit 1
    }

    log "INFO" "Configuration d'OpenSSH..."
    ssh-keygen -t "$SSH_KEY_TYPE" -C "$SSH_KEY_COMMENT" -f "$SSH_KEY_PATH" -N "" || {
        log "ERROR" "Échec de la génération de la clé SSH."
        exit 1
    }
}

# Installer et configurer Git
install_git() {
    log "INFO" "Installation de Git..."
    sudo apt install git -y || {
        log "ERROR" "Échec de l'installation de Git."
        exit 1
    }

    log "INFO" "Configuration de Git..."
    git config --global user.name "$GIT_USER_NAME" || {
        log "ERROR" "Échec de la configuration du nom Git."
        exit 1
    }
    git config --global user.email "$GIT_USER_EMAIL" || {
        log "ERROR" "Échec de la configuration de l'email Git."
        exit 1
    }
}

# Installer et configurer Docker
install_docker() {
    log "INFO" "Installation de Docker..."
    sudo apt install docker.io -y || {
        log "ERROR" "Échec de l'installation de Docker."
        exit 1
    }
    sudo systemctl enable --now docker || {
        log "ERROR" "Échec du démarrage de Docker."
        exit 1
    }

    log "INFO" "Configuration de Docker..."
    sudo usermod -aG docker "$DOCKER_USER" || {
        log "ERROR" "Échec de l'ajout de l'utilisateur au groupe Docker."
        exit 1
    }
}

# Installer et configurer UFW
install_ufw() {
    log "INFO" "Installation d'UFW..."
    sudo apt install ufw -y || {
        log "ERROR" "Échec de l'installation d'UFW."
        exit 1
    }

    log "INFO" "Configuration d'UFW..."
    for port in $(echo "$UFW_ALLOW_PORTS" | tr "," " "); do
        sudo ufw allow "$port" || {
            log "ERROR" "Échec de l'ouverture du port $port."
            exit 1
        }
    done
    sudo ufw enable || {
        log "ERROR" "Échec de l'activation d'UFW."
        exit 1
    }
}

# Installer et configurer Fail2Ban
install_fail2ban() {
    log "INFO" "Installation de Fail2Ban..."
    sudo apt install fail2ban -y || {
        log "ERROR" "Échec de l'installation de Fail2Ban."
        exit 1
    }

    log "INFO" "Configuration de Fail2Ban..."
    sudo cp /etc/fail2ban/jail.conf "$FAIL2BAN_JAIL_LOCAL" || {
        log "ERROR" "Échec de la configuration de Fail2Ban."
        exit 1
    }
    sudo systemctl restart fail2ban || {
        log "ERROR" "Échec du redémarrage de Fail2Ban."
        exit 1
    }
}

# Installer mdBook
install_mdbook() {
    log "INFO" "Installation de mdBook..."
    cargo install mdbook || {
        log "ERROR" "Échec de l'installation de mdBook."
        exit 1
    }
}

# === Fonction principale ===
sdfos() {
    load_env
    check_dependencies
    update_system

    # Installer toutes les dépendances
    install_rust
    install_vpn
    install_ipfs
    install_gpg
    install_ssh
    install_git
    install_docker
    install_ufw
    install_fail2ban
    install_mdbook

    log "INFO" "Installation terminée avec succès !"
    echo "Redémarrez votre session pour appliquer les changements (notamment pour Docker)."
}

# Exécuter la fonction principale
sdfos
