cudeploy() {
    local config_file="cudeploy.json"

    if [[ ! -f "$config_file" ]]; then
        echo "❌ Erreur : Fichier '$config_file' introuvable !" >&2
        return 1
    fi

    echo "🚀 Initialisation du déploiement..."
    
    # Charger la configuration
    local repo branch target_path backup_enabled backup_location backup_retention
    repo=$(jq -r '.repository' "$config_file")
    branch=$(jq -r '.branch' "$config_file")
    target_path=$(jq -r '.deployment.target_path' "$config_file")
    backup_enabled=$(jq -r '.deployment.backup.enabled' "$config_file")
    backup_location=$(jq -r '.deployment.backup.location' "$config_file")
    backup_retention=$(jq -r '.deployment.backup.retention' "$config_file")

    echo "📂 Dépôt : $repo"
    echo "🌿 Branche : $branch"
    echo "📌 Chemin cible : $target_path"

    # Vérifier si le répertoire cible existe
    if [[ ! -d "$target_path" ]]; then
        echo "❌ Erreur : Le dossier '$target_path' n'existe pas !" >&2
        return 1
    fi

    # Sauvegarde si activée
    if [[ "$backup_enabled" == "true" ]]; then
        echo "🛑 Sauvegarde en cours..."
        mkdir -p "$backup_location"
        tar -czf "$backup_location/backup_$(date +%F_%T).tar.gz" "$target_path"
        echo "✅ Sauvegarde terminée !"

        # Supprimer les anciens backups (rétention)
        find "$backup_location" -type f -name "backup_*.tar.gz" | sort -r | tail -n +"$((backup_retention + 1))" | xargs rm -f
    fi

    # Mise à jour du dépôt Git
    echo "🔄 Mise à jour du code..."
    git -C "$target_path" fetch origin "$branch"
    git -C "$target_path" reset --hard "origin/$branch"

    # Exécuter les commandes de build
    echo "⚙️ Exécution des builds..."
    jq -c '.commands | to_entries[]' "$config_file" | while read -r entry; do
        local name enabled execution
        name=$(echo "$entry" | jq -r '.key')
        enabled=$(echo "$entry" | jq -r '.value.enabled')
        execution=$(echo "$entry" | jq -r '.value.execution')

        if [[ "$enabled" == "true" ]]; then
            echo "🚀 Exécution de $name..."
            eval "$execution"
        fi
    done

    # Relancer les services nécessaires
    echo "♻️ Redémarrage des services..."
    jq -r '.services.restart[]' "$config_file" | while read -r service; do
        echo "🔄 Restarting $service..."
        sudo systemctl restart "$service"
    done

    # Gestion de Docker
    if [[ "$(jq -r '.services.docker.enabled' "$config_file")" == "true" ]]; then
        local docker_cmd
        docker_cmd=$(jq -r '.services.docker.command' "$config_file")
        echo "🐳 Docker en cours d'exécution..."
        eval "$docker_cmd"
    fi

    # Gestion de PM2
    if [[ "$(jq -r '.services.pm2.enabled' "$config_file")" == "true" ]]; then
        local pm2_cmd
        pm2_cmd=$(jq -r '.services.pm2.command' "$config_file")
        echo "🔄 Redémarrage PM2..."
        eval "$pm2_cmd"
    fi

    # Gestion de Proxmox
    if [[ "$(jq -r '.services.proxmox.enabled' "$config_file")" == "true" ]]; then
        local proxmox_host proxmox_backup_cmd
        proxmox_host=$(jq -r '.services.proxmox.host' "$config_file")
        proxmox_backup_cmd=$(jq -r '.services.proxmox.backup_command' "$config_file")

        echo "💾 Sauvegarde Proxmox en cours sur $proxmox_host..."
        ssh "root@$proxmox_host" "$proxmox_backup_cmd"
    fi

    # Vérifier l'état du noyau
    echo "🖥️ Informations sur le noyau..."
    uname -a
    if [[ -f /sys/kernel/realtime ]]; then
        echo "⏳ Kernel Temps Réel : ACTIVÉ"
    else
        echo "⏳ Kernel Temps Réel : NON ACTIVÉ"
    fi

    # Afficher les logs si activés
    if [[ "$(jq -r '.logging.enabled' "$config_file")" == "true" ]]; then
        local log_file
        log_file=$(jq -r '.logging.log_file' "$config_file")
        echo "📜 Logs disponibles ici : $log_file"
    fi

    echo "✅ Déploiement terminé avec succès !"
}

