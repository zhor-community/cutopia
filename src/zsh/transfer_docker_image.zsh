#!/bin/zsh

# Function to display usage
usage() {
    echo "Usage: $0 -s <source_machine> -d <destination_machine> -i <image_name>"
    exit 1
}

# Parse command line arguments
while getopts ":s:d:i:" opt; do
    case ${opt} in
        s )
            SOURCE_MACHINE=$OPTARG
            ;;
        d )
            DESTINATION_MACHINE=$OPTARG
            ;;
        i )
            IMAGE_NAME=$OPTARG
            ;;
        \? )
            usage
            ;;
    esac
done

# Check if all arguments are provided
if [ -z "$SOURCE_MACHINE" ] || [ -z "$DESTINATION_MACHINE" ] || [ -z "$IMAGE_NAME" ]; then
    usage
fi

# Variables
IMAGE_FILE="docker_image.tar"
DESTINATION_PATH="/home/$(whoami)"

# Function to check SSH connection
check_ssh_connection() {
    local machine=$1
    ssh -q $machine exit
    if [ $? -ne 0 ]; then
        echo "Failed to connect to $machine via SSH."
        exit 1
    fi
}

# Check SSH connections
echo "Checking SSH connection to source machine..."
check_ssh_connection $SOURCE_MACHINE

echo "Checking SSH connection to destination machine..."
check_ssh_connection $DESTINATION_MACHINE

# Export the image from the source machine
echo "Exporting Docker image from source machine..."
ssh $SOURCE_MACHINE "docker save -o ${IMAGE_FILE} ${IMAGE_NAME}" || { echo "Failed to save image on source machine"; exit 1; }

# Transfer the tar file to the destination machine
echo "Transferring Docker image to destination machine..."
scp $SOURCE_MACHINE:${IMAGE_FILE} $DESTINATION_MACHINE:${DESTINATION_PATH} || { echo "Failed to transfer image"; exit 1; }

# Import the image on the destination machine
echo "Importing Docker image on destination machine..."
ssh $DESTINATION_MACHINE "docker load -i ${DESTINATION_PATH}/${IMAGE_FILE}" || { echo "Failed to load image on destination machine"; exit 1; }

# Cleanup (optional)
echo "Cleaning up temporary files..."
ssh $SOURCE_MACHINE "rm -f ${IMAGE_FILE}"
ssh $DESTINATION_MACHINE "rm -f ${DESTINATION_PATH}/${IMAGE_FILE}"

echo "Docker image transfer complete!"

