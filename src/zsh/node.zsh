# installs nvm (Node Version Manager)
#cuapk add build-base gcc g++ python3 make nodejs npm yarn
#curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash
#curl -L https://bit.ly/n-install | bash
source $CUTOPIAPATH/bin/zshrc
n lts
# download and install Node.js (you may need to restart the terminal)
nvm install 20

# verifies the right Node.js version is in the environment
node -v # should print `v20.16.0`

# verifies the right npm version is in the environment
npm -v # should print `10.8.1`



