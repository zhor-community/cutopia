#!/bin/zsh

# ==========================================
# CUCLI - Command Line Interface for Cutopia
# ==========================================
# Description:
#   A modular command-line interface for managing Cutopia modules efficiently.
# 
# ==========================================

# ========================================================================================
# Cutopia Command Line Interface (cucli) - Enterprise Grade Module Management System
# Version: 2.0.0
# SPDX-License-Identifier: AGPL-3.0
# Copyright (c) 2024 Cutopia Ecosystem - AbdElHakim ZOUAÏ
# Email : abdelhakimzouai@gmail.com
# ========================================================================================

# ██████╗ ██╗   ██╗██████╗ ██╗      █████╗ ██████╗ 
# ██╔══██╗██║   ██║██╔══██╗██║     ██╔══██╗██╔══██╗
# ██████╔╝██║   ██║██████╔╝██║     ███████║██████╔╝
# ██╔═══╝ ██║   ██║██╔═══╝ ██║     ██╔══██║██╔═══╝ 
# ██║     ╚██████╔╝██║     ███████╗██║  ██║██║     
# ╚═╝      ╚═════╝ ╚═╝     ╚══════╝╚═╝  ╚═╝╚═╝     

# ----------------------------------------------------------------------------------------
# Environment Configuration
# ----------------------------------------------------------------------------------------

# Enable strict mode
set -euo pipefail
emulate -L zsh

# ----------------------------------------------------------------------------------------
# Global Constants
# ----------------------------------------------------------------------------------------

readonly CLI_NAME="cucli"
readonly CLI_VERSION="2.0.0"
readonly CONFIG_DIR="${CALPINEPATH}/sec/conf"
readonly SCHEMA_FILE="${CONFIG_DIR}/vps.schema.json"
readonly MODULES_FILE="${CONFIG_DIR}/vps.json"
readonly LOCK_FILE="/tmp/${CLI_NAME}.lock"
readonly LOG_DIR="${CALPINEPATH}/var/log/${CLI_NAME}"
readonly CACHE_DIR="${CALPINEPATH}/var/cache/${CLI_NAME}"

# ----------------------------------------------------------------------------------------
# Module Management Functions
# ----------------------------------------------------------------------------------------

function __cucli::validate_config() {
    # Validate JSON against schema with ajv
    if ! ajv validate -s "${SCHEMA_FILE}" -d "${MODULES_FILE}" --strict=true; then
        __cucli::log error "Config validation failed"
        return 1
    fi
}

function __cucli::load_modules() {
    typeset -gA modules
    local module_data

    __cucli::validate_config || return 1

    module_data=$(jq -c '.modules[]' "${MODULES_FILE}") || {
        __cucli::log error "Failed to parse modules file"
        return 1
    }

    while IFS= read -r line; do
        local name=$(jq -r '.name' <<< "${line}")
        modules[${name}]="${line}"
    done <<< "${module_data}"
}

function __cucli::find_module() {
    local module_name=$1
    if [[ -z "${modules[$module_name]:-}" ]]; then
        __cucli::log error "Module not found: ${module_name}"
        return 1
    fi
    echo "${modules[$module_name]}"
}

# ----------------------------------------------------------------------------------------
# Core Operations
# ----------------------------------------------------------------------------------------

function __cucli::install_module() {
    local module_name=$1
    local module_data
    local install_cmd
    local -a dependencies

    module_data=$(__cucli::find_module "${module_name}") || return 1

    install_cmd=$(jq -r '.install_command' <<< "${module_data}")
    dependencies=($(jq -r '.dependencies[]?' <<< "${module_data}"))

    __cucli::log info "Installing dependencies: ${dependencies[*]}"
    sudo apt-get install -qy "${dependencies[@]}" || {
        __cucli::log error "Dependency installation failed"
        return 1
    }

    __cucli::log info "Executing install command: ${install_cmd}"
    zsh -c "${install_cmd}" || {
        __cucli::log error "Installation failed for ${module_name}"
        return 1
    }
}

function __cucli::toggle_module() {
    local module_name=$1
    local state=$2
    local temp_file="${MODULES_FILE}.tmp"

    jq --arg name "${module_name}" \
       --argjson state "${state}" \
       '( .modules[] | select(.name == $name) ).included |= $state' \
       "${MODULES_FILE}" > "${temp_file}" && mv "${temp_file}" "${MODULES_FILE}"

    __cucli::log info "Module ${module_name} ${state?string}"
}

# ----------------------------------------------------------------------------------------
# Utility Functions
# ----------------------------------------------------------------------------------------

function __cucli::log() {
    local level=$1
    local message=$2
    local timestamp=$(date -Iseconds)
    local log_entry="[${timestamp}] [${level^^}] ${message}"
    
    # Définition des couleurs et icônes
    local reset="\033[0m"
    local green="\033[1;32m"
    local yellow="\033[1;33m"
    local red="\033[1;31m"
    local blue="\033[1;34m"
    local gray="\033[1;90m"

    case "${level}" in
        debug) echo -e "${gray}🐞 ${log_entry}${reset}" ;;
        info) echo -e "${blue}ℹ️  ${log_entry}${reset}" ;;
        warn) echo -e "${yellow}⚠️  ${log_entry}${reset}" >&2 ;;
        error) echo -e "${red}❌ ${log_entry}${reset}" >&2 ;;
        success) echo -e "${green}✅ ${log_entry}${reset}" ;;
        *) echo "${log_entry}" ;;
    esac

    # Sauvegarde dans le fichier de logs
    echo "${log_entry}" >> "${LOG_DIR}/${CLI_NAME}.log"
}


function __cucli::cleanup() {
    rm -f "${LOCK_FILE}"
    __cucli::log debug "Cleanup completed"
}

function __cucli::check_dependencies() {
    local -a required_tools=(jq zsh ajv)
    local missing=()

    for tool in "${required_tools[@]}"; do
        if ! command -v "${tool}" >/dev/null; then
            missing+=("${tool}")
        fi
    done

    (( ${#missing[@]} > 0 )) && {
        __cucli::log error "Missing dependencies: ${missing[*]}"
        return 1
    }
}

# ----------------------------------------------------------------------------------------
# User Interface
# ----------------------------------------------------------------------------------------

function __cucli::show_help() {
    cat << EOF
${CLI_NAME} - Cutopia Module Manager v${CLI_VERSION}

Usage:
  ${CLI_NAME} [COMMAND] [ARGS...]

Commands:
  list                     List available modules
  install <module|--all>   Install specific module or all enabled modules
  enable <module>          Enable module in configuration
  disable <module>         Disable module in configuration
  info <module>            Show detailed module information
  status                   Show system status
  version                  Show version information
  help                     Display this help message

Options:
  --dry-run                Simulate operation without changes
  --verbose                Enable verbose output
  --quiet                  Suppress non-error output

Environment:
  CALPINEPATH              Base installation path [${CALPINEPATH}]
  CUCLI_LOG_LEVEL          Logging verbosity [INFO]

EOF
}

function __cucli::show_version() {
    cat << EOF
${CLI_NAME} version ${CLI_VERSION}
Copyright (c) 2024 Cutopia Ecosystem
License AGPL-3.0: GNU Affero General Public License v3.0
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
EOF
}

# ----------------------------------------------------------------------------------------
# Main Entry Point
# ----------------------------------------------------------------------------------------

function cucli() {
    # Initialize environment
    trap __cucli::cleanup EXIT INT TERM
    mkdir -p "${LOG_DIR}" "${CACHE_DIR}"
    touch "${LOCK_FILE}"

    # Parse arguments
    local command=$1
    shift

    # Pre-flight checks
    __cucli::check_dependencies || return 1
    __cucli::load_modules || return 1

    case "${command}" in
        list)
            jq -r '.modules[] | "\(.name)\t\(.description)\t\(.version)\t\(.included)"' "${MODULES_FILE}" | 
            column -t -s $'\t' -N "Module,Description,Version,Enabled"
            ;;
            
        install)
            local module=$1
            case "${module}" in
                --all) 
                    for m in $(jq -r '.modules[] | select(.included == true) | .name' "${MODULES_FILE}"); do
                        __cucli::install_module "${m}"
                    done
                    ;;
                *) 
                    __cucli::install_module "${module}"
                    ;;
            esac
            ;;
            
        enable|disable)
            local module=$1
            local state=$([[ "${command}" == "enable" ]] && echo "true" || echo "false")
            __cucli::toggle_module "${module}" "${state}"
            ;;
            
        version)
            __cucli::show_version
            ;;
            
        help|*)
            __cucli::show_help
            ;;
    esac
}

# ----------------------------------------------------------------------------------------
# Execution Guard
# ----------------------------------------------------------------------------------------

if [[ "${ZSH_EVAL_CONTEXT}" == "toplevel" ]]; then
    cucli "$@"
fi
