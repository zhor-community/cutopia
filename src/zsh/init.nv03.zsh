#!/usr/bin/env zsh

# ========================================================================================
# Cutopia Initialization System - Enterprise Grade Environment Bootstrap
# Version: 3.0.0
# SPDX-License-Identifier: AGPL-3.0
# ========================================================================================

# Enable enterprise-grade execution controls
set -o errexit          # Exit immediately on error
set -o nounset          # Treat unset variables as errors
set -o pipefail         # Capture pipeline failures
emulate -R zsh          # ZSH compatibility mode

# ███████╗██╗  ██╗██████╗ ███████╗██████╗ 
# ██╔════╝╚██╗██╔╝██╔══██╗██╔════╝██╔══██╗
# █████╗   ╚███╔╝ ██████╔╝█████╗  ██████╔╝
# ██╔══╝   ██╔██╗ ██╔═══╝ ██╔══╝  ██╔══██╗
# ███████╗██╔╝ ██╗██║     ███████╗██║  ██║
# ╚══════╝╚═╝  ╚═╝╚═╝     ╚══════╝╚═╝  ╚═╝

# ----------------------------------------------------------------------------------------
# Global Configuration
# ----------------------------------------------------------------------------------------

declare -gr APP_BASE="/opt/cutopia"
declare -gr LOCK_DIR="/var/lock/cutopia"
declare -gr LOG_DIR="/var/log/cutopia"
declare -gr CONFIG_DIR="${APP_BASE}/sec/conf"
declare -gr ENV_FILE="${CONFIG_DIR}/cutopia.env"

# ----------------------------------------------------------------------------------------
# Advanced Logging System
# ----------------------------------------------------------------------------------------

function __cisys::log() {
    local -r level="${1:?}"
    local -r message="${2:?}"
    local -r timestamp="$(date '+%Y-%m-%dT%H:%M:%S%z')"
    local -r log_entry="[${timestamp}] [${level}] ${message}"
    
    # ANSI Color Codes
    local -rA colors=(
        DEBUG "\033[38;5;244m"   # Grey
        INFO "\033[38;5;39m"     # Blue
        WARN "\033[38;5;226m"    # Yellow
        ERROR "\033[38;5;196m"   # Red
        SUCCESS "\033[38;5;46m"  # Green
        RESET "\033[0m"
    )

    case "${level}" in
        DEBUG) echo -e "${colors.DEBUG}${log_entry}${colors.RESET}" ;;
        INFO) echo -e "${colors.INFO}${log_entry}${colors.RESET}" ;;
        WARN) echo -e "${colors.WARN}${log_entry}${colors.RESET}" >&2 ;;
        ERROR) echo -e "${colors.ERROR}${log_entry}${colors.RESET}" >&2 ;;
        SUCCESS) echo -e "${colors.SUCCESS}${log_entry}${colors.RESET}" ;;
    esac

    echo "${log_entry}" >> "${LOG_DIR}/system-init.log"
}

# ----------------------------------------------------------------------------------------
# Security Enforcement Functions
# ----------------------------------------------------------------------------------------

function __cisys::secure_path() {
    local -r path="${1:?}"
    
    if [[ ! -d "${path}" ]]; then
        __cisys::log ERROR "Path validation failed: ${path}"
        return 1
    fi

    chmod 0750 "${path}"
    chown root:root "${path}"
    __cisys::log DEBUG "Secured directory: ${path}"
}

function __cisys::validate_integrity() {
    local -r checksum_file="${CONFIG_DIR}/integrity.sha256"
    
    if ! sha256sum --quiet --check "${checksum_file}"; then
        __cisys::log ERROR "Integrity check failed"
        return 1
    fi
    __cisys::log INFO "System integrity validated"
}

# ----------------------------------------------------------------------------------------
# Main Initialization Sequence
# ----------------------------------------------------------------------------------------

function __cisys::initialize() {
    __cisys::log INFO "🚀 Starting Cutopia Enterprise Initialization"
    
    # Security First Phase
    __cisys::secure_path "${APP_BASE}"
    __cisys::validate_integrity

    # Environment Setup
    [[ -f "${ENV_FILE}" ]] && source "${ENV_FILE}"
    __cisys::log DEBUG "Environment loaded from ${ENV_FILE}"

    # Core System Bootstrap
    zsh "${APP_BASE}/src/zsh/cuapk.zsh" install \
        zsh curl bash jq git sudo ca-certificates
    
    # Module System Initialization
    if ! zsh "${APP_BASE}/src/zsh/cucli.zsh" system bootstrap; then
        __cisys::log ERROR "Module system bootstrap failed"
        return 1
    fi

    __cisys::log SUCCESS "✅ System Initialization Complete"
}

# ----------------------------------------------------------------------------------------
# Execution Controller
# ----------------------------------------------------------------------------------------

if [[ "${ZSH_EVAL_CONTEXT}" == "toplevel" ]]; then
    mkdir -p "${LOCK_DIR}"
    exec {lock_fd}>"${LOCK_DIR}/init.lock" || exit 1
    
    if flock -n "${lock_fd}"; then
        __cisys::initialize
        flock -u "${lock_fd}"
    else
        __cisys::log ERROR "Initialization already running"
        exit 1
    fi
fi
