#!/bin/zsh

# ---------------------------------------------------------------------
# Script pour créer un modèle à partir d'une image Alpine dans LXD
# ---------------------------------------------------------------------

########################################################################
#                                                                      #
#                                                                      #
#                            container env                             #
#                                                                      #
#                                                                      #
########################################################################
# Configuration
IMAGE_ALIAS="alpine/3.20/cloud"
TEMPLATE_NAME="talpine"
TEMP_CONTAINER_NAME="calpine"
WAIT_TIME=10  # Temps d'attente pour que le conteneur soit prêt (en secondes)

########################################################################
#                                                                      #
#                                                                      #
#                          container error                            #
#                                                                      #
#                                                                      #
########################################################################
# Fonction pour afficher un message d'erreur et quitter le script
error_exit() {
    echo "$1" 1>&2
    # Ajout d'informations de diagnostic
    echo "Diagnostics supplémentaires :"
    lxc info "$TEMP_CONTAINER_NAME" 1>&2
    exit 1
}

########################################################################
#                                                                      #
#                                                                      #
#                      container check command                         #
#                                                                      #
#                                                                      #
########################################################################
# Fonction pour vérifier si une commande est disponible
check_command() {
    command -v "$1" >/dev/null 2>&1 || error_exit "$1 n'est pas installé. Veuillez l'installer avant d'exécuter ce script."
}

########################################################################
#                                                                      #
#                                                                      #
#                               image check                            #
#                                                                      #
#                                                                      #
########################################################################
# Fonction pour vérifier l'existence de l'image localement ou dans le dépôt officiel
check_image_exists() {
    local clean_image_alias=$(echo "$IMAGE_ALIAS" | tr -d '[:space:]')

    local local_image_id=$(lxc image list | grep -F "$clean_image_alias" | awk '{print $2}' | head -n 1)
    if [ -n "$local_image_id" ]; then
        echo "Image locale trouvée : $local_image_id"
        IMAGE_ID="$local_image_id"
    else
        echo "Image locale non trouvée. Recherche dans le dépôt officiel..."
        local remote_image_id=$(lxc image list images: | grep -F "$clean_image_alias" | awk '{print $2}' | head -n 1)
        if [ -n "$remote_image_id" ]; then
            echo "Image trouvée dans le dépôt officiel : $remote_image_id"
            echo "Téléchargement de l'image..."
            lxc image copy images:"$remote_image_id" local: --alias "$clean_image_alias" || error_exit "Échec du téléchargement de l'image depuis le dépôt officiel."
            IMAGE_ID="$remote_image_id"
        else
            error_exit "Image $clean_image_alias non trouvée ni localement ni dans le dépôt officiel."
        fi
    fi
}

########################################################################
#                                                                      #
#                                                                      #
#                           template check                             #
#                                                                      #
#                                                                      #
########################################################################
# Fonction pour vérifier si le modèle existe déjà et gérer les options via getopt
check_template_exists() {
    local overwrite_flag=0
    local use_existing_flag=0
    local abort_flag=0

    local options=$(getopt -o oua --long overwrite,use,abort -- "$@")
    [ $? -ne 0 ] && error_exit "Erreur dans l'analyse des options."

    eval set -- "$options"
    while true; do
        case "$1" in
            -o | --overwrite) overwrite_flag=1; shift ;;
            -u | --use) use_existing_flag=1; shift ;;
            -a | --abort) abort_flag=1; shift ;;
            --) shift; break ;;
            *) error_exit "Option invalide." ;;
        esac
    done

    local template_exists=$(lxc image list | grep -F "$TEMPLATE_NAME" | awk '{print $2}' | head -n 1)
    if [ -n "$template_exists" ]; then
        echo "Le modèle $TEMPLATE_NAME existe déjà."
        if [ "$overwrite_flag" -eq 1 ]; then
            echo "Écrasement du modèle existant..."
            lxc image delete "$TEMPLATE_NAME" || error_exit "Échec de la suppression de l'ancien modèle."
        elif [ "$use_existing_flag" -eq 1 ]; then
            echo "Utilisation du modèle existant."
            return 1
        elif [ "$abort_flag" -eq 1 ]; then
            echo "Opération annulée."
            exit 0
        else
            error_exit "Aucune action spécifiée pour un modèle existant. Utilisez -o, -u ou -a."
        fi
    fi
}

########################################################################
#                                                                      #
#                                                                      #
#                         container lancher                            #
#                                                                      #
#                                                                      #
########################################################################
#uu 
# Fonction pour lancer un conteneur temporaire
launch_temp_container() {
    if lxc list --format csv -c n | grep -q "^$TEMP_CONTAINER_NAME$"; then
        echo "Le conteneur $TEMP_CONTAINER_NAME existe déjà. Suppression en cours..."
        lxc stop "$TEMP_CONTAINER_NAME" --force || error_exit "Échec de stoper le conteneur existant."
        lxc delete "$TEMP_CONTAINER_NAME" --force || error_exit "Échec de la suppression du conteneur existant."
    fi
    echo "Lancement du conteneur temporaire $TEMP_CONTAINER_NAME..."
    lxc launch "$IMAGE_ID" "$TEMP_CONTAINER_NAME"  || error_exit "Échec du lancement du conteneur temporaire."

    # Vérifier si le conteneur a démarré correctement
    echo "Vérification de l'état du conteneur après lancement..."
    sleep 5  # Attendre un court instant pour permettre au conteneur de démarrer
}

########################################################################
#                                                                      #
#                                                                      #
#                           container wait                             #
#                                                                      #
#                                                                      #
########################################################################
# Fonction pour attendre que le conteneur soit prêt
wait_for_container() {
    echo "Attente de $WAIT_TIME secondes pour que le conteneur soit prêt..."
    sleep $WAIT_TIME
}

########################################################################
#                                                                      #
#                                                                      #
#                     container check network                          #
#                                                                      #
#                                                                      #
########################################################################
# Fonction pour tester la connexion et la configuration réseau du conteneur
check_network() {
    echo "Test de la connexion et de la configuration réseau du conteneur..."

    #local container_status=$(lxc list | grep "$TEMP_CONTAINER_NAME" | awk '{print $2}')
    #[ "$container_status" != "RUNNING" ] && error_exit "Le conteneur $TEMP_CONTAINER_NAME n'est pas en cours d'exécution."
    #container_status=$(lxc info "$TEMP_CONTAINER_NAME" | grep -i "Status" | awk '{print $2}')
    #[ "$container_status" != "Running" ] && error_exit "Le conteneur $TEMP_CONTAINER_NAME n'a pas démarré correctement."
    echo "Test de la connexion à 8.8.8.8..."
    lxc exec "$TEMP_CONTAINER_NAME" -- ping -c 4 8.8.8.8 || error_exit "Échec du test de connexion à 8.8.8.8."

    echo "Test de la connexion à google.com..."
    lxc exec "$TEMP_CONTAINER_NAME" -- ping -c 4 google.com || error_exit "Échec du test de connexion à google.com."

    echo "Affichage des interfaces réseau..."
    lxc exec "$TEMP_CONTAINER_NAME" -- ip a || error_exit "Échec de la commande ip a."

    echo "Affichage des routes réseau..."
    lxc exec "$TEMP_CONTAINER_NAME" -- ip r || error_exit "Échec de la commande ip r."

    echo "Affichage des paramètres DNS..."
    lxc exec "$TEMP_CONTAINER_NAME" -- cat /etc/resolv.conf || error_exit "Échec de la commande cat /etc/resolv.conf."

    echo "Test réseau terminé avec succès."
}

# Fonction pour configurer le conteneur, incluant la copie de fichiers et la configuration avancée
########################################################################
#                                                                      #
#                                                                      #
#                          container config                            #
#                                                                      #
#                                                                      #
########################################################################

# Utilisation de la fonction
# configure_container "nom_du_conteneur" "/chemin/vers/fichier.txt" "/chemin/destination/fichier.txt" "512MB" "2" "vim curl" "myuser" "password"
#configure_container --cname calpine --ifile .cutopia/ --ofile /root/monfichier --mem 1GB --cpu 4 --apk "nginx curl" --user admin --pass admin123


configure_container() {
    # Initialisation des variables avec des valeurs par défaut
    local container_name="calpine"
    local memory_limit="512MB"
    local cpu_limit="2"
    # Analyse des options avec getopts
    while [[ "$#" -gt 0 ]]; do
        case "$1" in
            --cname) container_name="$2"; shift 2;;  # Nom du conteneur
            --mem) memory_limit="$2"; shift 2;;      # Limite mémoire
            --cpu) cpu_limit="$2"; shift 2;;         # Limite CPU
            *) echo "Option inconnue: $1"; return 1;; # Gestion des erreurs d'option
        esac
    done

    # Vérification des paramètres obligatoires
    if [ -z "$container_name" ] || [ -z "$memory_limit" ] || [ -z "$memory_limit" ] || [ -z "$cpu_limit" ]; then
        echo "Erreur : --cname et --username sont obligatoires."
        return 1
    fi

    # Vérifier si le fichier source existe
    #if [ ! -f "$source_file" ]; then
    #    echo "Erreur : Le fichier source $source_file n'existe pas."
    #    return 1
    #fi

    # Vérifier si le conteneur existe
    echo "Vérification de l'état du conteneur $container_name..."
    if ! lxc list --format csv | grep -q "$container_name"; then
        echo "Erreur : Le conteneur $container_name n'existe pas."
        return 1
    fi

    # S'assurer que le conteneur est démarré
    if ! lxc info "$container_name" | grep -q "Status: RUNNING"; then
        echo "Le conteneur $container_name n'est pas en cours d'exécution. Démarrage..."
        lxc start "$container_name"
    fi

    # Configuration des ressources (mémoire, CPU)
    echo "Configuration des ressources pour le conteneur $container_name..."
    lxc config set "$container_name" limits.memory "$memory_limit"
    lxc config set "$container_name" limits.cpu "$cpu_limit"


    echo "Configuration du conteneur $container_name terminée."
}
#configure_container --cname calpine --mem 1GB --cpu 1 --apk "nginx curl" --user admin --pass admin123

#configure_container --cname calpine --ifile /home/hermit/.cutopia --ofile /home/admin --mem 1GB --cpu 4 --apk "bash zsh git sudo curl wget tree" --user admin --pass admin123

########################################################################
#                                                                      #
#                                                                      #
#                     copy from host to container                      #
#                                                                      #
#                                                                      #
########################################################################
copy_to_lxc() {
  # Paramètres : $1 = nom du conteneur, $2 = utilisateur cible dans le conteneur, $3 = dossier source (facultatif), $4 = dossier destination (facultatif)
  CONTAINER="$1"
  USER="$2"
  SRC_DIR="${3:-$HOME/.cutopia}"  # Utilisation du dossier par défaut si non spécifié
  DEST_DIR="${4:-/home/$USER/.cutopia}"  # Utilisation du dossier par défaut si non spécifié

  # Vérification si le dossier source existe
  if [ ! -d "$SRC_DIR" ]; then
    echo "Erreur : Le dossier source $SRC_DIR n'existe pas."
    return 1
  fi

  # Vérification si le conteneur LXC existe
  if ! lxc info "$CONTAINER" > /dev/null 2>&1; then
    echo "Erreur : Le conteneur LXC nommé $CONTAINER n'existe pas."
    return 1
  fi

  # Vérification si le conteneur est en cours d'exécution
  if ! lxc list "$CONTAINER" | grep -q "RUNNING"; then
    echo "Erreur : Le conteneur $CONTAINER n'est pas en cours d'exécution."
    echo "Démarrage du conteneur..."
    lxc start "$CONTAINER"
    if [ $? -ne 0 ]; then
      echo "Erreur : Impossible de démarrer le conteneur."
      return 1
    fi
  fi

  # Vérification si le répertoire de destination existe dans le conteneur
  if ! lxc exec "$CONTAINER" -- test -d "$DEST_DIR"; then
    echo "Création du répertoire de destination $DEST_DIR dans le conteneur."
    lxc exec "$CONTAINER" -- mkdir -p "$DEST_DIR"
    if [ $? -ne 0 ]; then
      echo "Erreur : Impossible de créer le répertoire de destination dans le conteneur."
      return 1
    fi
  fi

  # Copier le dossier source vers le conteneur
  echo "Copie des fichiers du dossier $SRC_DIR vers le conteneur $CONTAINER dans $DEST_DIR..."
  lxc file push "$SRC_DIR"/* "$CONTAINER$DEST_DIR" --recursive
  if [ $? -ne 0 ]; then
    echo "Erreur : Échec de la copie des fichiers."
    return 1
  fi

  # Vérifier les permissions dans le conteneur et les ajuster si nécessaire
  echo "Vérification et ajustement des permissions dans le conteneur..."
  lxc exec "$CONTAINER" -- chown -R "$USER:$USER" "$DEST_DIR"
  lxc exec "$CONTAINER" -- chmod -R 755 "$DEST_DIR"
  lxc exec "$CONTAINER" -- ls -lahr /root/
  lxc exec "$CONTAINER" -- ls -lahr /home/
  if [ $? -ne 0 ]; then
    echo "Erreur : Impossible de modifier les permissions du répertoire dans le conteneur."
    return 1
  fi

  echo "Copie terminée avec succès vers $CONTAINER pour l'utilisateur $USER !"
  return 0
}
########################################################################
#                                                                      #
#                                                                      #
#                       container check image                          #
#                                                                      #
#                                                                      #
########################################################################

########################################################################
#                                                                      #
#                                                                      #
#                       container check image                          #
#                                                                      #
#                                                                      #
########################################################################
# Fonction pour arrêter le conteneur
stop_container() {
    echo "Arrêt du conteneur $TEMP_CONTAINER_NAME..."
    lxc stop "$TEMP_CONTAINER_NAME" || error_exit "Échec de l'arrêt du conteneur."
}
########################################################################
#                                                                      #
#                                                                      #
#                       container check image                          #
#                                                                      #
#                                                                      #
########################################################################
# Fonction pour créer un modèle à partir du conteneur
create_model() {
    echo "Création du modèle $TEMPLATE_NAME..."
    lxc publish "$TEMP_CONTAINER_NAME" --alias "$TEMPLATE_NAME" || error_exit "Échec de la création du modèle."
}

########################################################################
#                                                                      #
#                                                                      #
#                       container check image                          #
#                                                                      #
#                                                                      #
########################################################################
# Fonction pour supprimer le conteneur temporaire
cleanup_temp_container() {
    echo "Suppression du conteneur temporaire $TEMP_CONTAINER_NAME..."
    lxc delete "$TEMP_CONTAINER_NAME" || error_exit "Échec de la suppression du conteneur temporaire."
}

########################################################################
#                                                                      #
#                                                                      #
#                       container check image                          #
#                                                                      #
#                                                                      #
########################################################################
# Fonction pour vérifier que le modèle a été créé
verify_model() {
    echo "Vérification du modèle créé..."
    lxc image list | grep "$TEMPLATE_NAME" || error_exit "Le modèle $TEMPLATE_NAME n'a pas été trouvé."
}

########################################################################
#                                                                      #
#                                                                      #
#                       container check image                          #
#                                                                      #
#                                                                      #
########################################################################
# Fonction principale
main() {
    check_command lxc
    check_image_exists
    check_template_exists --overwrite
    launch_temp_container
    wait_for_container
    check_network > network.log
    #configure_container
    
    # Utilisation de la fonction
    # configure_container "nom_du_conteneur" "/chemin/vers/fichier.txt" "/chemin/destination/fichier.txt"

    #configure_container --cname calpine --ifile /home/hermit/.cutopia --ofile ~/ --mem 1GB --cpu 4 --apk "nginx curl" --user admin --pass admin123
    configure_container --cname calpine --mem 1GB --cpu 2
    lxc exec calpine -- useradd -m admin
    copy_to_lxc  calpine admin 
    lxc exec calpine -- sh /home/admin/.cutopia/init.zsh
    stop_container
    create_model
    cleanup_temp_container
    verify_model
    echo "Processus terminé avec succès."
}

# Exécuter la fonction principale
main

lxc stop khededja
lxc delete khededja
lxc launch fppShell khededja
lxc exec khededja -- sh
