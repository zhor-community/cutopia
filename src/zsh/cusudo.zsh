#!/bin/zsh

##############################################################################
#                                                                            #
#                                 cusudo                                     #
#                                                                            #
##############################################################################

# Fonction pour créer un utilisateur et configurer ses privilèges sudo
cusudo() {
    # Variables par défaut
    local userName=""
    local userPassword=""
    local force_nopasswd=false
    local sudoGroup=""

    # Vérifier que le script est exécuté avec les privilèges root
    if [[ $EUID -ne 0 ]]; then
        echo "Erreur : ce script doit être exécuté en tant que root." >&2
        return 1
    fi

    # Vérifier la présence des commandes essentielles
    for cmd in useradd usermod passwd sudo; do
        if ! command -v "$cmd" &>/dev/null; then
            echo "Erreur : commande '$cmd' introuvable. Vérifiez votre installation." >&2
            return 1
        fi
    done

    # Traitement des arguments
    while getopts "u:p:y" opt; do
        case "$opt" in
            u) userName="$OPTARG" ;;  # Nom d'utilisateur
            p) userPassword="$OPTARG" ;;  # Mot de passe
            y) force_nopasswd=true ;;  # Désactiver la demande de mot de passe pour sudo
            *) echo "Usage: cusudo -u <username> -p <password> [-y]" >&2 ; return 1 ;;
        esac
    done

    # Vérifier que le nom d'utilisateur est défini
    if [[ -z "$userName" ]]; then
        echo "Erreur : un nom d'utilisateur doit être spécifié avec -u." >&2
        return 1
    fi

    # Déterminer le groupe sudo en fonction du système d'exploitation
    if [[ -f /etc/os-release ]]; then
        source /etc/os-release
        case "$ID" in
            ubuntu|debian) sudoGroup="sudo" ;;
            centos|rhel|fedora) sudoGroup="wheel" ;;
            alpine) sudoGroup="wheel" ;;
            *) echo "Erreur : Système d'exploitation non supporté." >&2; return 1 ;;
        esac
    else
        echo "Erreur : Impossible de détecter le système d'exploitation." >&2
        return 1
    fi

    # Vérifier si l'utilisateur existe
    if id -u "$userName" &>/dev/null; then
        echo "Utilisateur '$userName' déjà existant."
    else
        echo "Création de l'utilisateur '$userName'..."
        if [[ -z "$userPassword" ]]; then
            echo "Erreur : Aucun mot de passe spécifié avec -p." >&2
            return 1
        fi
        useradd -m -s /bin/bash "$userName"
        echo "$userName:$userPassword" | chpasswd
        if [[ $? -ne 0 ]]; then
            echo "Erreur : Échec de la création de l'utilisateur '$userName'." >&2
            return 1
        fi
        echo "Utilisateur '$userName' créé avec succès."
    fi

    # Ajouter l'utilisateur au groupe sudo
    echo "Ajout de '$userName' au groupe '$sudoGroup'..."
    usermod -aG "$sudoGroup" "$userName"
    echo "Utilisateur '$userName' ajouté au groupe sudo ($sudoGroup)."

    # Configuration sudo sans mot de passe si demandé
    if $force_nopasswd; then
        echo "$userName ALL=(ALL) NOPASSWD:ALL" | tee "/etc/sudoers.d/$userName" > /dev/null
        chmod 0440 "/etc/sudoers.d/$userName"
        echo "Sudo configuré sans mot de passe pour '$userName'."
    else
        read -p "Configurer sudo sans mot de passe pour '$userName' ? (y/n) : " response
        if [[ "$response" =~ ^[Yy]$ ]]; then
            echo "$userName ALL=(ALL) NOPASSWD:ALL" | tee "/etc/sudoers.d/$userName" > /dev/null
            chmod 0440 "/etc/sudoers.d/$userName"
            echo "Sudo configuré sans mot de passe pour '$userName'."
        else
            echo "Sudo configuré avec mot de passe pour '$userName'."
        fi
    fi

    return 0
}


