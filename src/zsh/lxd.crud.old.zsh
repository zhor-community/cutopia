#!/bin/zsh
#!/bin/bash

culxd (){

echo "Updating the system..."
cuapk update

# Install LXD via APT
echo "Installing LXD..."
cuapk add lxd lxd-client

# Add the user to the lxd group
echo "Adding the user to the lxd group..."
usermod -aG lxd $SUDO_USER

lxd init

}
# Update the system

#!/bin/zsh

# Script name: culxd.zsh
# Description: Script to manage LXD containers using 'culxd' commands.

# Initialize LXD with a basic configuration
culxd_init() {
  echo "Initialisation de LXD avec la configuration par défaut..."
  cat <<EOF | lxd init --preseed
config: {}
networks:
- config:
    ipv4.address: auto
    ipv4.nat: "true"
    ipv6.address: none
  description: ""
  managed: true
  name: lxdbr0
  type: bridge
storage_pools:
- config: {}
  description: ""
  name: default
  driver: dir
profiles:
- config: {}
  description: ""
  devices:
    root:
      path: /
      pool: default
      type: disk
  name: default
cluster: null
EOF
  echo "LXD initialisé."
}

# List all containers
culxd_list() {
  echo "Liste de tous les conteneurs..."
  lxc list
}

# Update a container (rename)
culxd_update() {
  if [[ $# -ne 2 ]]; then
    echo "Usage: culxd update <ancien_nom> <nouveau_nom>"
    return 1
  fi

  local ancien_nom=$1
  local nouveau_nom=$2

  echo "Renommage du conteneur '$ancien_nom' en '$nouveau_nom'..."
  lxc rename "$ancien_nom" "$nouveau_nom"
  echo "Conteneur renommé en '$nouveau_nom'."
}

# Delete a container
culxd_delete() {
  if [[ $# -ne 1 ]]; then
    echo "Usage: culxd delete <nom_du_conteneur>"
    return 1
  fi

  local nom_du_conteneur=$1

  echo "Arrêt du conteneur '$nom_du_conteneur'..."
  lxc stop "$nom_du_conteneur"

  echo "Suppression du conteneur '$nom_du_conteneur'..."
  lxc delete "$nom_du_conteneur"
  echo "Conteneur '$nom_du_conteneur' supprimé."
}

# Start a container
culxd_start() {
  if [[ $# -ne 1 ]]; then
    echo "Usage: culxd start <nom_du_conteneur>"
    return 1
  fi

  local nom_du_conteneur=$1

  echo "Démarrage du conteneur '$nom_du_conteneur'..."
  lxc start "$nom_du_conteneur"
  echo "Conteneur '$nom_du_conteneur' démarré."
}

# Stop a container
culxd_stop() {
  if [[ $# -ne 1 ]]; then
    echo "Usage: culxd stop <nom_du_conteneur>"
    return 1
  fi

  local nom_du_conteneur=$1

  echo "Arrêt du conteneur '$nom_du_conteneur'..."
  lxc stop "$nom_du_conteneur"
  echo "Conteneur '$nom_du_conteneur' arrêté."
}

# Show help
culxd_help() {
  echo "Usage:"
  echo "  culxd init                             - Initialiser LXD avec une configuration par défaut"
  echo "  culxd list                             - Lister tous les conteneurs"
  echo "  culxd update <ancien_nom> <nouveau_nom> - Renommer un conteneur existant"
  echo "  culxd delete <nom_du_conteneur>        - Supprimer un conteneur"
  echo "  culxd start <nom_du_conteneur>         - Démarrer un conteneur"
  echo "  culxd stop <nom_du_conteneur>          - Arrêter un conteneur"
  echo "  culxd help                             - Afficher ce message d'aide"
}

# Main function to parse command
culxd() {
  case $1 in
    init)
      culxd_init
      ;;
    list)
      culxd_list
      ;;
    update)
      shift
      culxd_update "$@"
      ;;
    delete)
      shift
      culxd_delete "$@"
      ;;
    start)
      shift
      culxd_start "$@"
      ;;
    stop)
      shift
      culxd_stop "$@"
      ;;
    help | *)
      culxd_help
      ;;
  esac
}

# Allow calling the script with 'culxd' as the command
if [[ $0 == *culxd.zsh ]]; then
  culxd "$@"
fi

