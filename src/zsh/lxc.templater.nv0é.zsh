#!/bin/zsh

# ---------------------------------------------------------------------
# Script pour créer un modèle LXD à partir d'une image Alpine
# Version optimisée, normalisée et standardisée
# ---------------------------------------------------------------------

# ------------------------------
# Configuration globale (avec variables d'environnement)
# ------------------------------

# Image source (Alpine Linux)
#IMAGE_ALIAS="${LXC_IMAGE_ALIAS:-alpine/3.20/cloud}"   # Variable d'environnement : LXC_IMAGE_ALIAS
#IMAGE_ALIAS="${LXC_IMAGE_ALIAS:-alpine/edge/cloud}"   # Variable d'environnement : LXC_IMAGE_ALIAS
IMAGE_ALIAS="${LXC_IMAGE_ALIAS:-alpine/edge}"   # Variable d'environnement : LXC_IMAGE_ALIAS

# Nom du modèle final
TEMPLATE_NAME="${LXC_TEMPLATE_NAME:-talpine}"        # Variable d'environnement : LXC_TEMPLATE_NAME

# Nom du conteneur temporaire
TEMP_CONTAINER_NAME="${LXC_TEMP_CONTAINER_NAME:-calpine}"  # Variable d'environnement : LXC_TEMP_CONTAINER_NAME

# Limite mémoire pour le conteneur
MEMORY_LIMIT="${LXC_MEMORY_LIMIT:-1GB}"              # Variable d'environnement : LXC_MEMORY_LIMIT

# Limite CPU pour le conteneur
CPU_LIMIT="${LXC_CPU_LIMIT:-2}"                      # Variable d'environnement : LXC_CPU_LIMIT

# Timeout global pour les attentes (en secondes)
WAIT_TIMEOUT="${LXC_WAIT_TIMEOUT:-60}"               # Variable d'environnement : LXC_WAIT_TIMEOUT

# Chemin du script initialisateur dans le conteneur
INIT_SCRIPT_PATH="${LXC_INIT_SCRIPT_PATH:-/opt/cutopia/zsh/init.zsh}"  # Variable d'environnement : LXC_INIT_SCRIPT_PATH

# Utilisateur administratif dans le conteneur
CONTAINER_USER="${LXC_CONTAINER_USER:-admin}"        # Variable d'environnement : LXC_CONTAINER_USER

# Dossier source local pour copie vers le conteneur
SOURCE_DIR="${LXC_SOURCE_DIR:-/opt/cutopia}"       # Variable d'environnement : LXC_SOURCE_DIR

# Dossier destination dans le conteneur
DESTINATION_DIR="${LXC_DESTINATION_DIR:-/opt/cutopia}"  # Variable d'environnement : LXC_DESTINATION_DIR

# Fichier log pour les diagnostics
LOG_FILE="${LXC_LOG_FILE:-lxc_templater.log}"        # Variable d'environnement : LXC_LOG_FILE

# ------------------------------
# Fonction principale
# ------------------------------
culxc() {
    set -euo pipefail  # Activation des options strictes

    # Journalisation initiale
    log_info "Début du processus de création du modèle LXD."

    # Vérification des prérequis
    check_prerequisites

    # Vérification de l'image source
    local IMAGE_ID
    IMAGE_ID=$(check_image_exists "$IMAGE_ALIAS")

    # Gestion des modèles existants
    handle_existing_template "$TEMPLATE_NAME"

    # Création et configuration du conteneur temporaire
    launch_and_configure_container "$IMAGE_ID" "$TEMP_CONTAINER_NAME" "$MEMORY_LIMIT" "$CPU_LIMIT"

    # Tests de connectivité réseau
    if ! check_network "$TEMP_CONTAINER_NAME"; then
        error_exit "Échec des tests de connectivité réseau."
    fi

    # Copie des fichiers dans le conteneur
    copy_files_to_container "$TEMP_CONTAINER_NAME" "$CONTAINER_USER" "$SOURCE_DIR" "$DESTINATION_DIR"

    # Exécution du script initialisateur
    execute_init_script "$TEMP_CONTAINER_NAME" "$INIT_SCRIPT_PATH"

    # Publication du modèle
    publish_template "$TEMP_CONTAINER_NAME" "$TEMPLATE_NAME"

    # Nettoyage du conteneur temporaire
    cleanup_container "$TEMP_CONTAINER_NAME"

    # Vérification finale du modèle
    verify_template "$TEMPLATE_NAME"

    log_info "Processus terminé avec succès."
}

# ------------------------------
# Fonction : Vérifier les prérequis
# ------------------------------
check_prerequisites() {
    local required_commands=("lxc" "grep" "awk" "head" "ping")
    for cmd in "${required_commands[@]}"; do
        if ! command -v "$cmd" >/dev/null; then
            error_exit "$cmd n'est pas installé. Veuillez l'installer avant de continuer."
        fi
    done
}

# ------------------------------
# Fonction : Vérifier l'existence de l'image
# ------------------------------
check_image_exists() {
    local alias="$1"
    local clean_alias=$(echo "$alias" | tr -d '[:space:]')

    local local_image_id=$(lxc image list | grep -F "$clean_alias" | awk '{print $2}' | head -n 1)
    if [ -n "$local_image_id" ]; then
        log_info "Image locale trouvée : $local_image_id"
        echo "$local_image_id"  # Utiliser echo au lieu de return
        return 0
    fi

    log_info "Recherche de l'image dans le dépôt officiel..."
    local remote_image_id=$(lxc image list images: | grep -F "$clean_alias" | awk '{print $2}' | head -n 1)
    if [ -n "$remote_image_id" ]; then
        log_info "Image trouvée dans le dépôt officiel : $remote_image_id"
        log_info "Téléchargement de l'image..."
        lxc image copy images:"$remote_image_id" local: --alias "$clean_alias" || error_exit "Échec du téléchargement de l'image."
        echo "$remote_image_id"
        return 0
    fi

    error_exit "Image $clean_alias non trouvée ni localement ni dans le dépôt officiel."
}

# ------------------------------
# Fonction : Gérer les modèles existants
# ------------------------------
handle_existing_template() {
    local template_name="$1"
    local existing_template=$(lxc image list | grep -F "$template_name" | awk '{print $2}' | head -n 1)

    if [ -n "$existing_template" ]; then
        log_warning "Le modèle $template_name existe déjà."

        local overwrite_flag=false
        local use_existing_flag=false
        local abort_flag=false

        while getopts ":oua" opt; do
            case $opt in
                o) overwrite_flag=true ;;
                u) use_existing_flag=true ;;
                a) abort_flag=true ;;
                *) error_exit "Option invalide." ;;
            esac
        done

        if $overwrite_flag; then
            log_info "Écrasement du modèle existant..."
            lxc image delete "$template_name" || error_exit "Échec de la suppression de l'ancien modèle."
        elif $use_existing_flag; then
            log_info "Utilisation du modèle existant."
            exit 0
        elif $abort_flag; then
            log_info "Opération annulée."
            exit 0
        else
            error_exit "Aucune action spécifiée pour un modèle existant. Utilisez -o (écraser), -u (utiliser), ou -a (annuler)."
        fi
    fi
}

# ------------------------------
# Fonction : Lancer et configurer le conteneur temporaire
# ------------------------------
launch_and_configure_container() {
    local image_id="$1"
    local container_name="$2"
    local memory_limit="$3"
    local cpu_limit="$4"

    if lxc list --format csv -c n | grep -q "^$container_name$"; then
        log_warning "Le conteneur $container_name existe déjà. Suppression en cours..."
        lxc stop "$container_name" --force || error_exit "Échec de l'arrêt du conteneur existant."
        lxc delete "$container_name" --force || error_exit "Échec de la suppression du conteneur existant."
    fi

    log_info "Lancement du conteneur temporaire $container_name..."
    lxc launch "$image_id" "$container_name" || error_exit "Échec du lancement du conteneur temporaire."

    log_info "Attente que le conteneur soit prêt..."
    wait_for_container_ready "$container_name"

    log_info "Configuration des ressources pour le conteneur $container_name..."
    lxc config set "$container_name" limits.memory "$memory_limit" || error_exit "Échec de la configuration de la mémoire."
    lxc config set "$container_name" limits.cpu "$cpu_limit" || error_exit "Échec de la configuration du CPU."

    log_info "Conteneur configuré avec succès."
}

# ------------------------------
# Fonction : Attendre que le conteneur soit prêt
# ------------------------------
wait_for_container_ready() {
    local container_name="$1"
    local timeout=$WAIT_TIMEOUT

    for ((i = 0; i < timeout; i++)); do
        if lxc list --format csv -c ns | grep -q "^$container_name,RUNNING"; then
            log_info "Conteneur prêt."
            return 0
        fi
        sleep 1
    done

    error_exit "Le conteneur $container_name n'a pas démarré dans le délai imparti ($timeout secondes)."
}

# ------------------------------
# Fonction : Tester la connectivité réseau
# ------------------------------
check_network() {
    local container_name="$1"
    local log_file="$LOG_FILE"

    log_info "Test de la connexion réseau du conteneur..." > "$log_file"

    log_info "eping 8.8.8.8..." >> "$log_file"
    if ! lxc exec "$container_name" -- ping -c 4 8.8.8.8 >> "$log_file" 2>&1; then
        log_error "Échec du test de connexion à 8.8.8.8. Voir $log_file pour plus de détails."
        return 1
    fi

    log_info "eping google.com..." >> "$log_file"
    if ! lxc exec "$container_name" -- ping -c 4 google.com >> "$log_file" 2>&1; then
        log_error "Échec du test de connexion à google.com. Voir $log_file pour plus de détails."
        return 1
    fi

    log_info "Affichage des interfaces réseau..." >> "$log_file"
    lxc exec "$container_name" -- ip a >> "$log_file" 2>&1 || return 1

    log_info "Affichage des routes réseau..." >> "$log_file"
    lxc exec "$container_name" -- ip r >> "$log_file" 2>&1 || return 1

    log_info "Affichage des paramètres DNS..." >> "$log_file"
    lxc exec "$container_name" -- cat /etc/resolv.conf >> "$log_file" 2>&1 || return 1

    log_info "Test réseau terminé avec succès."
    return 0
}

# ------------------------------
# Fonction : Copier des fichiers vers le conteneur
# ------------------------------
copy_files_to_container() {
    local container="$1"
    local user="$2"
    local src_dir="$3"
    local dest_dir="$4"

    if [ ! -d "$src_dir" ]; then
        error_exit "Le dossier source $src_dir n'existe pas."
    fi

    if ! lxc info "$container" >/dev/null 2>&1; then
        error_exit "Le conteneur $container n'existe pas."
    fi

    if ! lxc list "$container" | grep -q "RUNNING"; then
        log_warning "Le conteneur $container n'est pas en cours d'exécution. Démarrage..."
        lxc start "$container" || error_exit "Impossible de démarrer le conteneur."
    fi

    if ! lxc exec "$container" -- test -d "$dest_dir"; then
        log_warning "Création du répertoire de destination $dest_dir dans le conteneur..."
        lxc exec "$container" -- mkdir -p "$dest_dir" || error_exit "Impossible de créer le répertoire de destination."
    fi

    log_info "Copie des fichiers du dossier $src_dir vers $container:$dest_dir..."
    lxc file push "$src_dir"/* "$container$dest_dir" --recursive || error_exit "Échec de la copie des fichiers."

    log_info "Ajustement des permissions..."
    lxc exec "$container" -- chown -R "$user:$user" "$dest_dir" || error_exit "Impossible de modifier les propriétaires."
    lxc exec "$container" -- chmod -R 755 "$dest_dir" || error_exit "Impossible de modifier les permissions."

    log_info "Copie terminée avec succès."
}

# ------------------------------
# Fonction : Exécuter le script initialisateur
# ------------------------------
execute_init_script() {
    local container="$1"
    local script_path="$2"

    if ! lxc exec "$container" -- test -f "$script_path"; then
        error_exit "Le script initialisateur $script_path n'existe pas dans le conteneur."
    fi

    log_info "Exécution du script initialisateur $script_path..."
    lxc exec "$container" -- sh "$script_path" || error_exit "Échec de l'exécution du script initialisateur."

    log_info "Script initialisateur exécuté avec succès."
}

# ------------------------------
# Fonction : Publier le modèle
# ------------------------------
publish_template() {
    local container_name="$1"
    local template_name="$2"

    log_info "Publication du modèle $template_name..."
    lxc publish "$container_name" --alias "$template_name" || error_exit "Échec de la publication du modèle."

    log_info "Modèle publié avec succès."
}

# ------------------------------
# Fonction : Nettoyer le conteneur temporaire
# ------------------------------
cleanup_container() {
    local container_name="$1"

    log_info "Nettoyage du conteneur temporaire $container_name..."
    lxc stop "$container_name" --force || true
    lxc delete "$container_name" --force || true

    log_info "Conteneur supprimé avec succès."
}

# ------------------------------
# Fonction : Vérifier le modèle créé
# ------------------------------
verify_template() {
    local template_name="$1"

    log_info "Vérification du modèle créé..."
    if ! lxc image list | grep -q "$template_name"; then
        error_exit "Le modèle $template_name n'a pas été trouvé."
    fi

    log_info "Modèle vérifié avec succès."
}

# ------------------------------
# Fonction : Afficher un message d'erreur et quitter
# ------------------------------
error_exit() {
    local message="$1"
    log_error "❌ ERREUR : $message"
    exit 1
}

# ------------------------------
# Fonctions de journalisation
# ------------------------------
log_info() {
    echo "$(date '+%Y-%m-%d %H:%M:%S') [INFO] $*" | tee -a "$LOG_FILE"
}

log_warning() {
    echo "$(date '+%Y-%m-%d %H:%M:%S') [WARNING] $*" | tee -a "$LOG_FILE"
}

log_error() {
    echo "$(date '+%Y-%m-%d %H:%M:%S') [ERROR] $*" | tee -a "$LOG_FILE"
}

# Exécution de la fonction principale
#culxc "$@"
culxc
