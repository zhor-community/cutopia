#!/bin/zsh

# CUIPFS: IPFS Automation Script

# IPFS Commands
IPFS_CMD="ipfs"
IPFS_CLUSTER_CMD="ipfs-cluster-service"

cuipfs
# Helper function for output
echo_message() {
    echo "[$(date)] $1"
}

# Install IPFS
install_ipfs() {
    echo_message "Installing the latest IPFS release..."
    if command -v ipfs &> /dev/null; then
        echo_message "IPFS is already installed."
    else
        curl -O https://dist.ipfs.io/go-ipfs/latest/go-ipfs.tar.gz
        tar -xvzf go-ipfs.tar.gz
        cd go-ipfs
        sudo bash install.sh
        echo_message "IPFS installed successfully."
    fi
}

# Initialize IPFS Node
init_ipfs() {
    profile=$1
    echo_message "Initializing IPFS node with profile: $profile..."
    if [ ! -d "$HOME/.ipfs" ]; then
        ipfs init --profile=$profile
        echo_message "IPFS node initialized."
    else
        echo_message "IPFS node already initialized."
    fi
}

# Start IPFS Daemon
start_ipfs() {
    background=$1
    echo_message "Starting IPFS daemon..."
    if [ "$background" = "--background" ]; then
        ipfs daemon &> /dev/null &
        echo_message "IPFS daemon started in background."
    else
        ipfs daemon
    fi
}

# Stop IPFS Daemon
stop_ipfs() {
    echo_message "Stopping IPFS daemon..."
    pkill ipfs
    echo_message "IPFS daemon stopped."
}

# Add a file to IPFS
add_file() {
    file=$1
    pin=$2
    if [ "$pin" = "--pin" ]; then
        ipfs add -p "$file"
    else
        ipfs add "$file"
    fi
    echo_message "File added to IPFS."
}

# Retrieve a file from IPFS
get_file() {
    cid=$1
    output_path=$2
    ipfs get "$cid" -o "$output_path"
    echo_message "File retrieved from IPFS."
}

# List pinned files
list_pinned() {
    ipfs pin ls
}

# Connect to a peer
connect_peer() {
    peer_address=$1
    ipfs swarm connect "$peer_address"
    echo_message "Connected to peer: $peer_address"
}

# Show IPFS Node status
status_ipfs() {
    ipfs id
}

# Cluster add-peer (optional)
cluster_add_peer() {
    peer_id=$1
    ipfs-cluster-service peers add "$peer_id"
    echo_message "Cluster peer added: $peer_id"
}

# Main script logic
case "$1" in
    install)
        install_ipfs
        ;;
    init)
        init_ipfs "$2"
        ;;
    start)
        start_ipfs "$2"
        ;;
    stop)
        stop_ipfs
        ;;
    add)
        add_file "$2" "$3"
        ;;
    get)
        get_file "$2" "$3"
        ;;
    list)
        list_pinned
        ;;
    connect)
        connect_peer "$2"
        ;;
    status)
        status_ipfs
        ;;
    cluster)
        cluster_add_peer "$2"
        ;;
    *)
        echo "Usage: $0 {install|init|start|stop|add|get|list|connect|status|cluster}"
        exit 1
        ;;
esac
}
