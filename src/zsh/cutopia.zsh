#!/usr/bin/env zsh
# -*- mode: zsh; sh-indentation: 2; -*-

# Cutopia Core Module Manager
# License: AGPL-3.0
# Version: 2.0.0
# Author: AbdElHakim ZOUAÏ

# Enable strict mode
setopt errexit
setopt nounset
setopt pipefail

# Configuration
typeset -A CUTOPIA_CONFIG=(
  [CONFIG_DIR]="/etc/cutopia"
  [USER_CONFIG]="${HOME}/.config/cutopia"
  [MODULE_DIR]="/usr/local/share/cutopia/modules"
  [CACHE_DIR]="/var/cache/cutopia"
  [LOCK_DIR]="/var/lock/cutopia"
  [LOG_FILE]="/var/log/cutopia.log"
)

# Internationalization
export TEXTDOMAIN="cutopia"
source "${CUTOPIA_CONFIG[CONFIG_DIR]}/i18n.zsh"

# Load libs
source "${CUTOPIA_CONFIG[CONFIG_DIR]}/lib/logging.zsh"
source "${CUTOPIA_CONFIG[CONFIG_DIR]}/lib/crypto.zsh"
source "${CUTOPIA_CONFIG[CONFIG_DIR]}/lib/dependency.zsh"

# Logging subsystem
cutopia::log::init() {
  zmodload zsh/datetime
  autoload -U colors && colors

  typeset -gA LOG_LEVELS=(
    [DEBUG]=0
    [INFO]=1
    [WARN]=2
    [ERROR]=3
    [CRITICAL]=4
  )

  typeset -gA LOG_COLORS=(
    [DEBUG]="${fg[blue]}"
    [INFO]="${fg[green]}"
    [WARN]="${fg[yellow]}"
    [ERROR]="${fg[red]}"
    [CRITICAL]="${bg[red]}${fg[white]}"
  )

  cutopia::log::set_level "${LOG_LEVELS[INFO]}"
}

# Dependency management
cutopia::deps::verify() {
  local required=(
    "jq:jq"
    "curl:curl"
    "gpg:gnupg"
    "sha256sum:coreutils"
  )

  for dep in "${required[@]}"; do
    local cmd=${dep%%:*}
    local pkg=${dep#*:}
    
    if ! command -v "${cmd}" >/dev/null; then
      cutopia::log::error "$(gettext "Missing dependency: %s (package: %s")" "${cmd}" "${pkg}"
      return 1
    fi
  done
}

# Configuration loader
cutopia::config::load() {
  local config_files=(
    "${CUTOPIA_CONFIG[CONFIG_DIR]}/cutopia.conf"
    "${CUTOPIA_CONFIG[USER_CONFIG]}/cutopia.conf"
  )

  for config in "${config_files[@]}"; do
    if [[ -r "${config}" ]]; then
      source "${config}" || cutopia::log::error "Failed to load config: ${config}"
    fi
  done

  # Set defaults
  : ${CUTOPIA_MODULE_SOURCES:="https://modules.cutopia.io"}
  : ${CUTOPIA_SIGNING_KEY:="0xABCDEF1234567890"}
  : ${CUTOPIA_ENABLE_SANDBOX:=true}
}

# Module management core
cutopia::module::install() {
  local module="${1:?Module name required}"
  local version="${2:-latest}"
  local -a flags=("${@:3}")

  cutopia::validate::module_name "${module}" || return 1

  local tmpdir=$(mktemp -d)
  trap "rm -rf '${tmpdir}'" EXIT

  cutopia::log::info "$(gettext "Installing module: %s version %s")" "${module}" "${version}"
  
  # Download and verify
  cutopia::module::fetch "${module}" "${version}" "${tmpdir}" || return 1
  cutopia::module::verify "${tmpdir}/${module}.cutpkg" || return 1

  # Install
  cutopia::module::extract "${tmpdir}/${module}.cutpkg" || return 1
  cutopia::module::run_hooks "post_install" || return 1

  cutopia::log::success "$(gettext "Module %s installed successfully")" "${module}"
}

cutopia::module::fetch() {
  local module="$1"
  local version="$2"
  local dest="$3"

  local url="${CUTOPIA_MODULE_SOURCES}/${module}/${version}/${module}.cutpkg"
  
  if ! curl -sfL -o "${dest}/${module}.cutpkg" "${url}"; then
    cutopia::log::error "$(gettext "Failed to download module: %s")" "${module}"
    return 1
  fi

  cutopia::log::debug "Downloaded module to: ${dest}/${module}.cutpkg"
}

cutopia::module::verify() {
  local pkg="$1"
  
  if ! cutopia::crypto::verify_signature "${pkg}" "${CUTOPIA_SIGNING_KEY}"; then
    cutopia::log::critical "$(gettext "Invalid package signature for: %s")" "${pkg}"
    return 1
  fi

  if ! cutopia::crypto::verify_checksum "${pkg}"; then
    cutopia::log::critical "$(gettext "Checksum verification failed for: %s")" "${pkg}"
    return 1
  fi
}

# Security subsystem
cutopia::security::sandbox() {
  if [[ "${CUTOPIA_ENABLE_SANDBOX}" == true ]]; then
    cutopia::log::debug "Entering security sandbox"
    
    # Create namespaced environment
    unshare --user --map-root-user --pid --fork --mount-proc \
      --net --ipc --uts --cgroup --mount zsh -c "$@"
  else
    eval "$@"
  fi
}

# Main function
cutopia::main() {
  cutopia::log::init
  cutopia::config::load
  cutopia::deps::verify || exit 1

  local command="${1:-help}"
  shift

  case "${command}" in
    install|remove|update)
      cutopia::security::sandbox "cutopia::module::${command} $@"
      ;;
    list|search|info)
      cutopia::module::"${command}" "$@"
      ;;
    config)
      cutopia::config::"${@}"
      ;;
    help|--help|-h)
      cutopia::help
      ;;
    *)
      cutopia::log::error "$(gettext "Unknown command: %s")" "${command}"
      cutopia::help
      return 1
      ;;
  esac
}

# Help system
cutopia::help() {
  cat <<EOF
Cutopia Modular System Manager ${CUTOPIA_CONFIG[VERSION]}

Usage:
  cutopia [command] [options]

Commands:
  install <module> [version]  Install a module
  remove <module>            Remove a module
  update [module]            Update module(s)
  list                        List installed modules
  search <query>              Search modules
  info <module>               Show module info
  config                      Manage configuration
  help                        Show this help

Options:
  --no-sandbox               Disable security sandbox
  --verbose                  Enable debug output
  --registry <url>           Set custom module registry

Examples:
  cutopia install security --verbose
  cutopia config set CUTOPIA_MODULE_SOURCES https://private.registry.io
EOF
}

# Entry point
if [[ "${ZSH_EVAL_CONTEXT}" == "toplevel" ]]; then
  cutopia::main "$@"
fi
