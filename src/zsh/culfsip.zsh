#!/bin/bash

# ───────────────────────────────────────────────────────────────────
# Linux From Scratch (LFS) Installation Script
# ───────────────────────────────────────────────────────────────────

# -------------------------
# Variables de configuration
# -------------------------

LFS=/mnt/lfs
ARCHIVE_IPFS_HASH="Qm..."  # Remplacer par le hash IPFS des sources LFS
TAHOE_LAFS_URL="http://localhost:3456/uri/"  # Remplacer par l'URL Tahoe-LAFS

USE_IPFS=false
USE_TAHOE=false
INSTALL_GRUB=true
INSTALL_KERNEL=true

LOGFILE="/var/log/install-lfs.log"
TIMESTAMP=$(date '+%Y-%m-%d %H:%M:%S')

# Fonction de journalisation
log() {
    echo "[$TIMESTAMP] $1" | tee -a "$LOGFILE"
}

# Fonction d'aide (usage)
usage() {
    echo "Usage: $0 [-i] [-t] [-g] [-k] [-l <link>]"
    echo "Options:"
    echo "  -i  Télécharger les sources via IPFS"
    echo "  -t  Télécharger les sources via Tahoe-LAFS"
    echo "  -g  Désactiver l'installation de GRUB"
    echo "  -k  Désactiver l'installation du noyau Linux"
    echo "  -l  Spécifier un lien externe pour LFS (IPFS ou Tahoe-LAFS)"
    exit 1
}

# -------------------------
# Lecture des options
# -------------------------
while getopts "itgl:" opt; do
    case $opt in
        i) USE_IPFS=true ;;
        t) USE_TAHOE=true ;;
        g) INSTALL_GRUB=false ;;
        k) INSTALL_KERNEL=false ;;
        l) LFS=$OPTARG ;;  # Lien personnalisé pour LFS
        *) usage ;;
    esac
done

# -------------------------
# Validation des prérequis
# -------------------------

# Vérifier les permissions root
if [[ $(id -u) -ne 0 ]]; then
    log "Erreur: Ce script doit être exécuté en tant que root (utilisateur root)."
    exit 1
fi

# -------------------------
# Préparation de l'environnement LFS
# -------------------------
prepare_lfs() {
    log "🔧 Préparation de l'environnement LFS..."

    # Créer les répertoires nécessaires
    mkdir -pv $LFS/{bin,etc,lib,sbin,usr,var,sources,tools}
    ln -sv $LFS/tools /

    # Définir la variable d'environnement LFS
    export LFS
}

# -------------------------
# Téléchargement des sources
# -------------------------
download_sources() {
    log "🌍 Téléchargement des sources LFS..."

    cd $LFS/sources

    if [ "$USE_IPFS" = true ]; then
        log "📥 Téléchargement via IPFS..."
        ipfs get $ARCHIVE_IPFS_HASH -o sources.tar.gz
        tar -xvf sources.tar.gz
    elif [ "$USE_TAHOE" = true ]; then
        log "📥 Téléchargement via Tahoe-LAFS..."
        wget -O sources.tar.gz "$TAHOE_LAFS_URL"
        tar -xvf sources.tar.gz
    elif [[ $LFS == http* ]]; then
        log "📥 Téléchargement depuis l'URL personnalisée..."
        wget -O sources.tar.gz "$LFS"
        tar -xvf sources.tar.gz
    else
        log "📥 Téléchargement des sources depuis le répertoire local..."
        wget http://www.linuxfromscratch.org/lfs/download.html
    fi
}

# -------------------------
# Compilation des outils de base
# -------------------------
compile_binutils() {
    log "⚙️ Compilation de Binutils..."
    cd $LFS/sources
    tar -xvf binutils-*.tar.xz
    cd binutils-*
    mkdir build
    cd build
    ../configure --prefix=$LFS/tools --target=$(uname -m)-lfs-linux-gnu --disable-nls --disable-werror
    make -j$(nproc)
    make install
    cd ../..
    rm -rf binutils-*
}

compile_gcc() {
    log "⚙️ Compilation de GCC..."
    cd $LFS/sources
    tar -xvf gcc-*.tar.xz
    cd gcc-*
    mkdir build
    cd build
    ../configure --prefix=$LFS/tools --target=$(uname -m)-lfs-linux-gnu --disable-nls --enable-languages=c,c++
    make -j$(nproc)
    make install
    cd ../..
    rm -rf gcc-*
}

compile_glibc() {
    log "⚙️ Compilation de Glibc..."
    cd $LFS/sources
    tar -xvf glibc-*.tar.xz
    cd glibc-*
    mkdir build
    cd build
    ../configure --prefix=$LFS/tools --host=$(uname -m)-lfs-linux-gnu --build=$(../scripts/config.guess) --enable-kernel=3.2
    make -j$(nproc)
    make install
    cd ../..
    rm -rf glibc-*
}

# -------------------------
# Installation du noyau Linux
# -------------------------
install_kernel() {
    if [ "$INSTALL_KERNEL" = true ]; then
        log "🛠 Installation du noyau Linux..."
        cd $LFS/sources
        wget https://cdn.kernel.org/pub/linux/kernel/v6.x/linux-6.6.tar.xz
        tar -xvf linux-6.6.tar.xz
        cd linux-6.6
        make menuconfig  # Personnalisation facultative
        make -j$(nproc)
        make modules_install
        cp -v arch/x86/boot/bzImage /boot/vmlinuz-lfs
    else
        log "⏩ Installation du noyau désactivée."
    fi
}

# -------------------------
# Installation de GRUB
# -------------------------
install_grub() {
    if [ "$INSTALL_GRUB" = true ]; then
        log "📦 Installation de GRUB..."
        apt install grub -y
        grub-install --target=i386-pc /dev/sdX
        grub-mkconfig -o /boot/grub/grub.cfg
    else
        log "⏩ Installation de GRUB désactivée."
    fi
}

# -------------------------
# Configuration de GRUB
# -------------------------
configure_grub() {
    log "📜 Configuration de GRUB..."
    cat <<EOF > /boot/grub/grub.cfg
menuentry "Linux From Scratch" {
    set root=(hd0,1)
    linux /boot/vmlinuz-lfs root=/dev/sdX1 ro
}
EOF
}

# -------------------------
# Finalisation et redémarrage
# -------------------------
finalize_system() {
    log "📦 Finalisation du système..."
    umount -v $LFS
    log "🔄 Redémarrage du système..."
    reboot
}

# -------------------------
# Exécution du processus
# -------------------------
main() {
    prepare_lfs
    download_sources
    compile_binutils
    compile_gcc
    compile_glibc
    install_kernel
    install_grub
    configure_grub
    finalize_system
}

# -------------------------
# Démarrage de l'installation
# -------------------------
main

