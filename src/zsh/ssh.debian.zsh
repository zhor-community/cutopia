#!/bin/bash

# This script installs and configures OpenSSH Server on Ubuntu/Debian systems.

# Update package list
echo "Updating package list..."
sudo apt-get update

# Install OpenSSH Server
echo "Installing OpenSSH Server..."
sudo apt-get install -y openssh-server

# Start and enable SSH service
echo "Starting and enabling SSH service..."
sudo systemctl start ssh
sudo systemctl enable ssh

# Backup the original SSH configuration
echo "Backing up the original SSH configuration file..."
sudo cp /etc/ssh/sshd_config /etc/ssh/sshd_config.backup

# Configure SSH
echo "Configuring SSH settings..."
sudo tee /etc/ssh/sshd_config << EOF
# OpenSSH Server Configuration
Port 22
PermitRootLogin no
PasswordAuthentication yes
AllowUsers yourusername
EOF

# Reload SSH service to apply changes
echo "Reloading SSH service..."
sudo systemctl reload ssh

# Set up a firewall rule to allow SSH (if UFW is used)
if command -v ufw > /dev/null; then
    echo "Configuring firewall to allow SSH..."
    sudo ufw allow ssh
else
    echo "UFW is not installed. You may need to configure your firewall manually."
fi

echo "SSH Server installation and configuration completed."

