#!/bin/zsh

# ==========================================
# CUCLI - Command Line Interface for Cutopia
# ==========================================
# Description:
#   A modular command-line interface for managing Cutopia modules efficiently.
# 
# Author: AbdElHakim ZOUAÏ
# Email : abdelhakimzouai@gmail.com
# ==========================================

source $CALPINEPATH/src/env/calpine.env
#source $CALPINEPATH/src/env/cutopia.env
echo "$CALPINEPATH"
rm -rf /home/$userName/.zshrc
ln -s $CALPINEPATH/src/zsh/zshrc /home/$userName/.zshrc
chown -R $userName:$userName /home/$userName/.zshrc
