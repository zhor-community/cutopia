function setup_sshserver() {
    local username="$1"
    local port="$2"

    if [ -z "$username" ] || [ -z "$port" ]; then
        echo "Usage: setup_ssh_server <username> <port>"
        return 1
    fi

    if ! [[ "$port" =~ ^[0-9]+$ ]] || [ "$port" -lt 1 ] || [ "$port" -gt 65535 ]; then
        echo "Error: Port must be a number between 1 and 65535."
        return 1
    fi

    local log_file="/var/log/setup_ssh_server.log"
    local service_manager=""

    # Function to log errors
    log_error() {
        echo "[$(date)] ERROR: $1" | tee -a "$log_file"
        return 1
    }

    # Function to determine the service manager
    determine_service_manager() {
        if command -v rc-update > /dev/null; then
            service_manager="rc-update"
        elif command -v service > /dev/null; then
            service_manager="service"
        else
            log_error "No supported service manager found. Please install rc-update or service."
            exit 1
        fi
    }

    determine_service_manager

    echo "[$(date)] Starting SSH server installation and configuration..." | tee -a "$log_file"

    # Update package list
    echo "[$(date)] Updating package list..." | tee -a "$log_file"
    sudo apk update || log_error "Failed to update package list"

    # Install OpenSSH Server
    echo "[$(date)] Installing OpenSSH Server..." | tee -a "$log_file"
    sudo apk add openssh || log_error "Failed to install OpenSSH Server"

    # Start and enable SSH service
    echo "[$(date)] Configuring SSH service..." | tee -a "$log_file"
    if [ "$service_manager" = "rc-update" ]; then
        sudo rc-update add sshd || log_error "Failed to enable SSH service"
        sudo service sshd start || log_error "Failed to start SSH service"
    elif [ "$service_manager" = "service" ]; then
        sudo service sshd start || log_error "Failed to start SSH service"
        sudo rc-update add sshd || log_error "Failed to enable SSH service"
    fi

    # Backup the original SSH configuration
    echo "[$(date)] Backing up the original SSH configuration file..." | tee -a "$log_file"
    sudo cp /etc/ssh/sshd_config /etc/ssh/sshd_config.backup || log_error "Failed to back up SSH configuration"

    # Configure SSH using a temporary file
    echo "[$(date)] Configuring SSH settings on port $port..." | tee -a "$log_file"
    temp_file=$(mktemp)
    cat << EOF | sudo tee "$temp_file" > /dev/null
# OpenSSH Server Configuration
Port $port
PermitRootLogin no
PasswordAuthentication yes
AllowUsers $username
EOF

    sudo mv "$temp_file" /etc/ssh/sshd_config || log_error "Failed to move temporary configuration file"
    
    # Restart SSH service to apply changes
    echo "[$(date)] Restarting SSH service..." | tee -a "$log_file"
    if [ "$service_manager" = "rc-update" ]; then
        sudo service sshd restart || log_error "Failed to restart SSH service"
    elif [ "$service_manager" = "service" ]; then
        sudo service sshd restart || log_error "Failed to restart SSH service"
    fi

    # Configure firewall (if iptables is used)
    if command -v iptables > /dev/null; then
        echo "[$(date)] Configuring firewall to allow SSH on port $port..." | tee -a "$log_file"
        sudo iptables -A INPUT -p tcp --dport "$port" -j ACCEPT || log_error "Failed to configure firewall for SSH"
    else
        echo "[$(date)] iptables not found. Please configure your firewall manually." | tee -a "$log_file"
    fi

    echo "[$(date)] SSH Server installation and configuration completed successfully." | tee -a "$log_file"
}

