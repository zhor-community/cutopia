
IMAGE_ALIAS="${LXC_IMAGE_ALIAS:-alpine/edge/cloud}"   # Variable d'environnement : LXC_IMAGE_ALIAS
# ------------------------------
# Fonction : Vérifier l'existence de l'image
# ------------------------------
check_image_exists() {
    local alias="$1"
    local clean_alias=$(echo "$alias" | tr -d '[:space:]')

    local local_image_id=$(lxc image list | grep -F "$clean_alias" | awk '{print $2}' | head -n 1)
    if [ -n "$local_image_id" ]; then
        log_info "Image locale trouvée : $local_image_id"
        echo "$local_image_id"  # Utiliser echo au lieu de return
        return 0
    fi

    log_info "Recherche de l'image dans le dépôt officiel..."
    local remote_image_id=$(lxc image list images: | grep -F "$clean_alias" | awk '{print $2}' | head -n 1)
    if [ -n "$remote_image_id" ]; then
        log_info "Image trouvée dans le dépôt officiel : $remote_image_id"
        log_info "Téléchargement de l'image..."
        lxc image copy images:"$remote_image_id" local: --alias "$clean_alias" || error_exit "Échec du téléchargement de l'image."
        echo "$remote_image_id"
        return 0
    fi

    error_exit "Image $clean_alias non trouvée ni localement ni dans le dépôt officiel."
}


log_info() {
    echo "$(date '+%Y-%m-%d %H:%M:%S') [INFO] $*" | tee -a "$LOG_FILE"
}
IMAGE_ID=$(check_image_exists "$IMAGE_ALIAS")
echo "IMAGE_ID=$IMAGE_ID"
