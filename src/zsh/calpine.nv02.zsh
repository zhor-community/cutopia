#!/bin/zsh

# Valeurs par défaut
DEFAULT_PATH="~/.cutopia"
DEFAULT_GROUP="cutopia"
DEFAULT_IMAGE="calpine"
DEFAULT_OPERATION="start"
DEFAULT_CONTAINER="calpine"
DEFAULT_ENV_FILE="calpine"  # Valeurs autorisées : "calpine", "cutopia", "yats"
DEFAULT_PROFILE="admin"     # Valeurs autorisées : "guest", "user", "admin", "hermit", "root"

# Fonction pour tester les dépendances et diagnostiquer les problèmes
diagnose_dependencies() {
    echo "\n[INFO] Vérification des dépendances système..."

    # Vérifier Docker
    if ! command -v docker &>/dev/null; then
        echo "[ERREUR] Docker n'est pas installé. Veuillez installer Docker avant d'exécuter ce script."
        exit 1
    else
        echo "[OK] Docker est installé."
    fi

    # Vérifier les autorisations Docker
    if ! groups | grep -q "docker"; then
        echo "[ERREUR] L'utilisateur actuel n'est pas dans le groupe 'docker'. Ajoutez l'utilisateur au groupe avec : sudo usermod -aG docker $USER"
        exit 1
    else
        echo "[OK] L'utilisateur appartient au groupe 'docker'."
    fi

    # Vérifier le répertoire par défaut
    if [[ ! -d "$DEFAULT_PATH" ]]; then
        echo "[ERREUR] Le chemin par défaut '$DEFAULT_PATH' n'existe pas. Veuillez le créer ou spécifier un chemin valide."
        exit 1
    else
        echo "[OK] Le chemin par défaut '$DEFAULT_PATH' existe."
    fi

    # Vérifier le fichier d'environnement
    if [[ ! -f "$DEFAULT_PATH/src/env/$DEFAULT_ENV_FILE.env" ]]; then
        echo "[ERREUR] Le fichier d'environnement '$DEFAULT_ENV_FILE.env' est manquant dans '$DEFAULT_PATH/src/env/'."
        echo "[INFO] Création d'un fichier d'environnement par défaut..."
        mkdir -p "$DEFAULT_PATH/src/env"
        echo "# Fichier d'environnement par défaut" > "$DEFAULT_PATH/src/env/$DEFAULT_ENV_FILE.env"
        echo "userName='default'" >> "$DEFAULT_PATH/src/env/$DEFAULT_ENV_FILE.env"
        echo "userPassword='defaultPassword'" >> "$DEFAULT_PATH/src/env/$DEFAULT_ENV_FILE.env"
        echo "sudoUser='admin'" >> "$DEFAULT_PATH/src/env/$DEFAULT_ENV_FILE.env"
        echo "CALPINEPATH='/tmp'" >> "$DEFAULT_PATH/src/env/$DEFAULT_ENV_FILE.env"
        echo "[OK] Fichier d'environnement créé avec succès."
    else
        echo "[OK] Le fichier d'environnement '$DEFAULT_ENV_FILE.env' existe."
    fi

    echo "\n[INFO] Toutes les dépendances ont été vérifiées avec succès."
}

# Fonction principale : Gérer les conteneurs Docker et les images
calpine() {
    diagnose_dependencies

    # Initialiser les variables avec les valeurs par défaut
    local myPath="$DEFAULT_PATH"
    local myGroup="$DEFAULT_GROUP"
    local myImage="$DEFAULT_IMAGE"
    local myOperation="$DEFAULT_OPERATION"
    local myContainer="$DEFAULT_CONTAINER"
    local myEnvFile="$DEFAULT_ENV_FILE"
    local myProfile="$DEFAULT_PROFILE"

    # Analyser les options de la ligne de commande
    while getopts ":p:g:i:o:c:e:r:" opt; do
        case $opt in
            p) myPath="$OPTARG" ;;
            g) myGroup="$OPTARG" ;;
            i) myImage="$OPTARG" ;;
            o) myOperation="$OPTARG" ;;
            c) myContainer="$OPTARG" ;;
            e) myEnvFile="$OPTARG" ;;
            r) myProfile="$OPTARG" ;;
            \?)
                echo "[ERREUR] Option invalide -$OPTARG" >&2
                exit 1
                ;;
            :)
                echo "[ERREUR] L'option -$OPTARG nécessite un argument." >&2
                exit 1
                ;;
        esac
    done

    # Validation du fichier d'environnement
    if [[ ! "$myEnvFile" =~ ^(calpine|cutopia|yats)$ ]]; then
        echo "[ERREUR] Fichier d'environnement invalide '$myEnvFile'. Valeurs autorisées : calpine, cutopia, yats."
        exit 1
    fi

    # Validation du profil utilisateur
    if [[ ! "$myProfile" =~ ^(guest|user|admin|hermit|root)$ ]]; then
        echo "[ERREUR] Profil invalide '$myProfile'. Valeurs autorisées : guest, user, admin, hermit, root."
        exit 1
    fi

    # Afficher le résumé de l'opération
    echo "Opération : $myOperation"
    echo "Chemin : $myPath | Groupe : $myGroup | Image : $myImage | Conteneur : $myContainer"
    echo "Fichier d'environnement : $myEnvFile | Profil : $myProfile"

    # Exécution de l'opération demandée
    case $myOperation in
        "start")
            echo "[INFO] Démarrage du conteneur Docker : $myContainer"
            if docker ps -a --format '{{.Names}}' | grep -w "$myContainer" &>/dev/null; then
                if docker ps --format '{{.Names}}' | grep -w "$myContainer" &>/dev/null; then
                    echo "[INFO] Le conteneur '$myContainer' est déjà en cours d'exécution. Connexion..."
                    docker exec -it "$myContainer" sh
                else
                    echo "[INFO] Le conteneur '$myContainer' existe mais n'est pas en cours d'exécution. Démarrage..."
                    docker start "$myContainer" && docker exec -it "$myContainer" sh
                fi
            else
                echo "[INFO] Création et démarrage du conteneur '$myContainer'..."
                docker run -it -d --name "$myContainer" --env-file "$DEFAULT_PATH/src/env/$myEnvFile.env" "$myGroup/$myImage"
                docker exec -it "$myContainer" sh
            fi
            ;;
        "create")
            echo "[INFO] Création du conteneur Docker : $myContainer"
            docker run -it -d --name "$myContainer" --env-file "$DEFAULT_PATH/src/env/$myEnvFile.env" "$myGroup/$myImage"
            ;;
        "update")
            echo "[INFO] Mise à jour du conteneur Docker : $myContainer"
            docker stop "$myContainer" && docker rm "$myContainer"
            docker run -it -d --name "$myContainer" --env-file "$DEFAULT_PATH/src/env/$myEnvFile.env" "$myGroup/$myImage"
            ;;
        "upgrade")
            echo "[INFO] Mise à jour de l'image Docker et du conteneur : $myContainer"
            docker stop "$myContainer" && docker rm "$myContainer" && docker rmi "$myGroup/$myImage"
            docker build -t "$myGroup/$myImage:latest" "$myPath"
            docker run -it -d --name "$myContainer" --env-file "$DEFAULT_PATH/src/env/$myEnvFile.env" "$myGroup/$myImage"
            ;;
        "delete")
            echo "[INFO] Suppression du conteneur et de l'image Docker : $myContainer"
            docker stop "$myContainer" && docker rm "$myContainer" && docker rmi "$myGroup/$myImage"
            ;;
        *)
            echo "[ERREUR] Opération invalide '$myOperation'."
            echo "Opérations autorisées : create, start, update, upgrade, delete."
            exit 1
            ;;
    esac
}

# Lancer la fonction principale
calpine "$@"
