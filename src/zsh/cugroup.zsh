#!/bin/zsh

# ==============================
# setup_cutopia - Configuration automatisée de répertoires, groupes et permissions
# Version : 1.0
# Auteur : [Votre nom]
# Description : Configure un répertoire avec un groupe propriétaire spécifique.
#               Permet d'ajouter des utilisateurs au groupe et d'accorder des droits sudo.
# ==============================

# Variables par défaut
DEFAULT_GROUP="cutopia"
DEFAULT_DIR="/opt/cutopia"
DEFAULT_OWNER="root"

# Fonction principale
setup_cutopia() {
    local group_name="$DEFAULT_GROUP"
    local directory="$DEFAULT_DIR"
    local owner="$DEFAULT_OWNER"
    local add_users=0
    local add_sudo=0

    # Gestion des options avec getopts
    while getopts ":d:g:o:asu:h" opt; do
        case $opt in
            d)  # Répertoire personnalisé
                directory=$OPTARG
                ;;
            g)  # Groupe personnalisé
                group_name=$OPTARG
                ;;
            o)  # Propriétaire personnalisé
                owner=$OPTARG
                ;;
            a)  # Activer l'ajout interactif d'utilisateurs
                add_users=1
                ;;
            s)  # Ajouter le groupe à la liste sudo
                add_sudo=1
                ;;
            u)  # Ajouter un utilisateur spécifique au groupe
                if id "$OPTARG" > /dev/null 2>&1; then
                    usermod -aG "$group_name" "$OPTARG" && echo "L'utilisateur $OPTARG a été ajouté au groupe $group_name."
                else
                    echo "Erreur : L'utilisateur $OPTARG n'existe pas." >&2
                    return 1
                fi
                ;;
            h)  # Afficher l'aide
                display_help
                return 0
                ;;
            \?) # Option invalide
                echo "Erreur : Option invalide -$OPTARG." >&2
                display_help
                return 1
                ;;
        esac
    done

    # Vérifier les privilèges root
    if [[ $EUID -ne 0 ]]; then
        echo "Erreur : Ce script doit être exécuté en tant que root ou avec sudo." >&2
        return 1
    fi

    # Étape 1 : Créer le groupe si nécessaire
    if ! getent group "$group_name" > /dev/null; then
        echo "Création du groupe $group_name..."
        groupadd "$group_name" || { echo "Échec de la création du groupe $group_name." >&2; return 1; }
    else
        echo "Le groupe $group_name existe déjà."
    fi

    # Étape 2 : Vérifier ou créer le répertoire
    if [[ ! -d "$directory" ]]; then
        echo "Création du répertoire $directory..."
        mkdir -p "$directory" || { echo "Échec de la création du répertoire $directory." >&2; return 1; }
    fi

    # Étape 3 : Attribuer le propriétaire et le groupe
    echo "Attribution du propriétaire $owner et du groupe $group_name au répertoire $directory..."
    chown "$owner:$group_name" "$directory" || { echo "Échec de l'attribution du propriétaire et du groupe." >&2; return 1; }

    # Étape 4 : Définir les permissions (rwx pour propriétaire et groupe, rx pour autres)
    echo "Définition des permissions sur $directory..."
    chmod 775 "$directory" || { echo "Échec de la définition des permissions." >&2; return 1; }

    # Étape 5 : Ajouter des utilisateurs interactivement (si option -a activée)
    if [[ $add_users -eq 1 ]]; then
        echo "Ajout interactif d'utilisateurs au groupe $group_name (laissez vide pour terminer) :"
        while true; do
            read -p "Nom d'utilisateur : " username
            if [[ -z $username ]]; then
                break
            fi
            if id "$username" > /dev/null 2>&1; then
                usermod -aG "$group_name" "$username" && echo "L'utilisateur $username a été ajouté au groupe $group_name."
            else
                echo "Erreur : L'utilisateur $username n'existe pas."
            fi
        done
    fi

    # Étape 6 : Ajouter le groupe à la liste sudo (si option -s activée)
    if [[ $add_sudo -eq 1 ]]; then
        echo "Ajout du groupe $group_name à la liste sudo..."
        if grep -q "%$group_name ALL=(ALL:ALL) ALL" /etc/sudoers; then
            echo "Le groupe $group_name est déjà configuré dans sudoers."
        else
            echo "%$group_name ALL=(ALL:ALL) ALL" | EDITOR='tee -a' visudo && \
            echo "Le groupe $group_name a été ajouté à la liste sudo." || \
            { echo "Échec de l'ajout du groupe $group_name à la liste sudo." >&2; return 1; }
        fi
    fi

    # Vérification finale
    echo "Vérification finale des permissions, propriétaire et groupe..."
    ls -ld "$directory"

    echo "Configuration terminée avec succès."
}

# Fonction d'affichage de l'aide
display_help() {
    cat <<EOF
Usage: setup_cutopia [-d directory] [-g group_name] [-o owner] [-a] [-s] [-u username] [-h]

Options :
  -d <répertoire>   Spécifie un répertoire personnalisé (par défaut : $DEFAULT_DIR).
  -g <groupe>       Spécifie un groupe personnalisé (par défaut : $DEFAULT_GROUP).
  -o <propriétaire> Spécifie un propriétaire personnalisé (par défaut : $DEFAULT_OWNER).
  -a                Active l'ajout interactif d'utilisateurs au groupe.
  -s                Ajoute le groupe spécifié à la liste des groupes sudo.
  -u <utilisateur>  Ajoute un utilisateur spécifique au groupe.
  -h                Affiche cette aide.

Exemples :
  setup_cutopia -d /var/data -g mygroup -o myuser -a -s
EOF
}

# Exécution du script
if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
    setup_cutopia "$@"
fi
