function search_in_pdfs() {
    local search_word="$1"  # Le mot à rechercher est passé comme argument à la fonction
    for pdf in *.pdf; do
        if pdftotext "$pdf" - | grep -q "$search_word"; then
            echo "Mot trouvé dans : $pdf"
        fi
    done
}

search_in_pdfs "البطل" 
