#!/bin/zsh

# ======================================================
# Golang Installation and Configuration Script (zsh)
# ======================================================
# This script automates the installation, configuration,
# and validation of Golang on a Linux system.
# Author: AbdElHakim ZOUAÏ
# Email : abdelhakimzouai@gmail.com
# === CUGO ENVIRONMENT SETUP SCRIPT ===
# Description: A comprehensive script for configuring, managing, and deploying
#              Go-based projects for Frontend, Backend, and Microservices.
# ======================================================


source ~/.cutopia/src/env/cucli.env
# Load environment variables from cugo.env
if [[ -f "$CALPINEPATH/src/env/cugo.env" ]]; then
  source "$CALPINEPATH/src/env/cugo.env"
else
  echo "[ERROR] cugo.env not found at $CALPINEPATH/src/env/. Ensure the file exists."
  exit 1
fi

# === Constants ===
SUCCESS_COLOR="\033[1;32m"
INFO_COLOR="\033[1;34m"
WARN_COLOR="\033[1;33m"
ERROR_COLOR="\033[1;31m"
RESET_COLOR="\033[0m"

function cugo() {
# === Helper Functions ===
function log_msg() {
  local type="$1"
  local msg="$2"
  case "$type" in
    INFO) echo -e "${INFO_COLOR}[INFO]${RESET_COLOR} $msg" ;;
    SUCCESS) echo -e "${SUCCESS_COLOR}[SUCCESS]${RESET_COLOR} $msg" ;;
    WARN) echo -e "${WARN_COLOR}[WARN]${RESET_COLOR} $msg" ;;
    ERROR) echo -e "${ERROR_COLOR}[ERROR]${RESET_COLOR} $msg" ;;
    *) echo "$msg" ;;
  esac
}

function validate_env() {
  if [[ -z "$GO_VERSION" || -z "$INSTALL_DIR" || -z "$WORKSPACE_DIR" ]]; then
    log_msg ERROR "Required environment variables (GO_VERSION, INSTALL_DIR, WORKSPACE_DIR) are missing in cugo.env."
    return 1
  fi
}

# === Core Functions ===

# Validate system dependencies
function validate_dependencies() {
  log_msg INFO "Validating system dependencies..."
  local missing=false
  for dep in git wget tar; do
    if ! command -v "$dep" &> /dev/null; then
      log_msg ERROR "Dependency '$dep' is not installed. Please install it."
      missing=true
    fi
  done
  if $missing; then
    log_msg ERROR "Some dependencies are missing. Aborting."
    exit 1
  else
    log_msg SUCCESS "All dependencies are installed."
  fi
}

# Install Go
function install_go() {
  if [[ -d "$INSTALL_DIR/go" ]]; then
    log_msg WARN "Go is already installed at $INSTALL_DIR/go. Skipping installation."
    return 0
  fi
  log_msg INFO "Installing Go $GO_VERSION..."
  wget -q -O "$TEMP_ARCHIVE" "https://golang.org/dl/go${GO_VERSION}.linux-amd64.tar.gz"
  sudo tar -C "$INSTALL_DIR" -xzf "$TEMP_ARCHIVE"
  if [[ $? -eq 0 ]]; then
    log_msg SUCCESS "Go installed successfully at $INSTALL_DIR/go."
  else
    log_msg ERROR "Go installation failed."
    exit 1
  fi
}

# Configure environment variables
function configure_environment() {
  log_msg INFO "Configuring environment variables in $PROFILE_FILE..."
  {
    echo ""
    echo "# === Go Environment Variables ==="
    echo "export GOROOT=$INSTALL_DIR/go"
    echo "export GOPATH=$WORKSPACE_DIR"
    echo "export PATH=\$PATH:\$GOROOT/bin:\$GOPATH/bin"
  } >> "$PROFILE_FILE"
  source "$PROFILE_FILE"
  log_msg SUCCESS "Environment variables configured successfully."
}

# Initialize workspace directories
function init_workspace() {
  log_msg INFO "Initializing Go workspace directories..."
  mkdir -p "$WORKSPACE_DIR/src" "$WORKSPACE_DIR/pkg" "$WORKSPACE_DIR/bin"
  mkdir -p "$WORKSPACE_DIR/frontend" "$WORKSPACE_DIR/backend" "$WORKSPACE_DIR/microservices"
  log_msg SUCCESS "Workspace initialized at $WORKSPACE_DIR."
}

# Frontend project setup
function setup_frontend() {
  log_msg INFO "Setting up Frontend project using $FRONTEND_FRAMEWORK..."
  cd "$WORKSPACE_DIR/frontend" || return 1
  go mod init frontend
  go get github.com/vugu/vugu
  log_msg SUCCESS "Frontend project initialized."
}

# Backend project setup
function setup_backend() {
  log_msg INFO "Setting up Backend project using $BACKEND_FRAMEWORK..."
  cd "$WORKSPACE_DIR/backend" || return 1
  go mod init backend
  go get github.com/gin-gonic/gin
  log_msg SUCCESS "Backend project initialized."
}

# Microservices project setup
function setup_microservices() {
  log_msg INFO "Setting up Microservices project using $MICROSERVICES_FRAMEWORK..."
  cd "$WORKSPACE_DIR/microservices" || return 1
  go mod init microservices
  go get github.com/go-kit/kit
  log_msg SUCCESS "Microservices project initialized."
}

# Full setup process
function full_setup() {
  validate_env
  validate_dependencies
  install_go
  configure_environment
  init_workspace
  setup_frontend
  setup_backend
  setup_microservices
  log_msg SUCCESS "All components have been successfully set up!"
}

# Display help menu
function cugo_help() {
  echo "Usage: cugo <command>"
  echo ""
  echo "Commands:"
  echo "  validate      Validate system dependencies."
  echo "  install-go    Install Go language runtime."
  echo "  configure-env Configure environment variables."
  echo "  init-ws       Initialize Go workspace."
  echo "  setup-fe      Set up Frontend project."
  echo "  setup-be      Set up Backend project."
  echo "  setup-ms      Set up Microservices project."
  echo "  full-setup    Perform all setup steps."
  echo "  help          Display this help menu."
}

# Command-line interface
case "$1" in
  validate) validate_dependencies ;;
  install-go) install_go ;;
  configure-env) configure_environment ;;
  init-ws) init_workspace ;;
  setup-fe) setup_frontend ;;
  setup-be) setup_backend ;;
  setup-ms) setup_microservices ;;
  full-setup) full_setup ;;
  help | *) cugo_help ;;
esac
}
cugo full-setup
