#!/bin/bash

# Configuration
ROOTFS_DEFAULT="/var/www/files/imgs/alpine-minirootfs-3.21.3-x86_64.img"
MOUNTED=0

# Affichage de l'aide
usage() {
    echo "Usage: $0 [-r ROOTFS_PATH] [-h]"
    echo ""
    echo "Options :"
    echo "  -r ROOTFS_PATH  Spécifie le chemin du rootfs Alpine (par défaut: $ROOTFS_DEFAULT)"
    echo "  -h              Affiche ce message d'aide"
    exit 1
}

# Fonction principale
run_alpine() {
    local ROOTFS_PATH="$1"

    # Vérification de l'existence du répertoire rootfs
    if [ ! -d "$ROOTFS_PATH" ]; then
        echo "❌ Erreur: Le répertoire $ROOTFS_PATH n'existe pas."
        exit 1
    fi

    echo "✅ Montage des systèmes de fichiers..."
    mount -t proc /proc "$ROOTFS_PATH/proc"
    mount -t sysfs /sys "$ROOTFS_PATH/sys"
    mount --rbind /dev "$ROOTFS_PATH/dev"
    MOUNTED=1

    # Configuration réseau
    echo "✅ Configuration de la résolution DNS..."
    echo "nameserver 8.8.8.8" > "$ROOTFS_PATH/etc/resolv.conf"

    # Lancement du chroot
    echo "✅ Démarrage d'Alpine Linux..."
    chroot "$ROOTFS_PATH" /bin/sh

    # Nettoyage après sortie
    echo "✅ Nettoyage des montages..."
    umount -l "$ROOTFS_PATH/proc"
    umount -l "$ROOTFS_PATH/sys"
    umount -l "$ROOTFS_PATH/dev"
    MOUNTED=0

    echo "✅ Alpine Linux s'est arrêté proprement."
}

# Gestion des options avec getopts
ROOTFS_PATH="$ROOTFS_DEFAULT"
while getopts "r:h" opt; do
    case "$opt" in
        r) ROOTFS_PATH="$OPTARG" ;;
        h) usage ;;
        *) usage ;;
    esac
done

# Capture des interruptions pour un démontage propre
trap 'if [ $MOUNTED -eq 1 ]; then umount -l "$ROOTFS_PATH/proc" "$ROOTFS_PATH/sys" "$ROOTFS_PATH/dev"; fi; exit 1' INT TERM EXIT

# Exécution de la fonction principale
run_alpine "$ROOTFS_PATH"

