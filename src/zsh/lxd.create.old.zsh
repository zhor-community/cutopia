#!/bin/zsh

# Charger le script contenant les commandes LXD personnalisées
source src/zsh/lxd.crud.zsh

# Liste des conteneurs à créer
CONTAINERS=("cutopia" "ticenergy" "tassili" "zerozone" "zhor" "octomatics")

# Vérifier si LXD est initialisé
if ! lxc info > /dev/null 2>&1; then
    echo "LXD n'est pas initialisé. Initialisation en cours..."
    culxd init
fi

# Boucle pour créer les conteneurs
for CONTAINER in "${CONTAINERS[@]}"; do
    echo "Création du conteneur $CONTAINER..."

    # Vérifier si le conteneur existe déjà
    if lxc list --format csv -c n | grep -q "^$CONTAINER$"; then
        echo "Le conteneur $CONTAINER existe déjà. Suppression..."
        culxd delete "$CONTAINER"
    fi

    # Lancer le conteneur à partir de l’image par défaut
    lxc launch images:ubuntu/22.04 "$CONTAINER"

    # Ajouter un utilisateur 'admin' et exécuter un script d'init si nécessaire
    lxc exec "$CONTAINER" -- useradd -m admin
    lxc exec "$CONTAINER" -- sh -c "echo 'admin:password' | chpasswd"

    echo "Conteneur $CONTAINER créé avec succès !"
done

echo "Tous les conteneurs ont été créés avec succès."

