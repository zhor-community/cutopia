#!/bin/zsh

# Source NGINX setup script
#source $CALPINEPATH/src/zsh/cunginx.zsh
#cunginx -a -d cutopia.org -r /var/www/cutopia.org

# Setup OpenRC for service management
#ln -s /etc/init.d/nginx /etc/rc.d/
#rc-update add nginx default

# Start NGINX in the background
#nginx -g "daemon off;" &

# --- Add cuipfs installation and initialization ---
# Source the cuipfs setup script
#source $CALPINEPATH/src/zsh/cuipfs.zsh

# Install and initialize IPFS using cuipfs
#cuipfs install
#cuipfs init --profile=server

# Start the IPFS daemon in the background
#cuipfs start --background

# Add a test file to IPFS
#echo "Bienvenue dans Cutopia via IPFS!" > /tmp/testfile.txt
#cuipfs add /tmp/testfile.txt --pin

# Log the CID of the test file
#CID=$(cuipfs get-cid /tmp/testfile.txt)
#echo "File added to IPFS with CID: $CID"

# --- Deploy Quasar JS App ---
#echo "Starting Quasar JS app deployment..."

# Ensure Node.js and npm are installed
if ! command -v node &> /dev/null; then
    cuapk add --no-cache nodejs npm
fi

# Set working directory for Quasar app
QUASAR_APP_DIR=/opt/cutopia/src/fontend
BUILD_DIR=/var/www/cutopia.org

# Copy the Quasar app source code into the container (already mounted)
if [ -d "$QUASAR_APP_DIR" ]; then
    echo "Quasar app source found in $QUASAR_APP_DIR. Installing dependencies..."
    cd $QUASAR_APP_DIR

    # Install dependencies (using yarn or npm)
    if command -v yarn &> /dev/null; then
        yarn install
    else
        npm install
    fi

    # Build the app for production
    echo "Building Quasar app for production..."
    if command -v yarn &> /dev/null; then
        yarn build
    else
        npm run build
    fi

    # Copy the build output to the nginx directory
    echo "Copying build files to $BUILD_DIR..."
    cp -r dist/spa/* $BUILD_DIR/

    echo "Quasar JS app deployed successfully!"
else
    echo "Quasar app source not found in $QUASAR_APP_DIR. Skipping deployment."
    yarn global add @quasar/cli
    yarn create Quasar

fi
