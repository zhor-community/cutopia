#!/bin/zsh

# Variables globales
MODULES_JSON="/resources/json/modules.json"

# Fonction : Charger les modules depuis le fichier JSON
function load_modules() {
    if [[ ! -f $MODULES_JSON ]]; then
        echo "Erreur : Fichier des modules introuvable ($MODULES_JSON)"
        exit 1
    fi
    MODULES=$(jq -c '.modules[]' "$MODULES_JSON")
}

# Fonction : Afficher l'aide
function show_help() {
    echo "Usage : cucli [commande] [options]"
    echo ""
    echo "Commandes disponibles :"
    echo "  list               Liste tous les modules disponibles"
    echo "  install <module>   Installe un module spécifique"
    echo "  enable <module>    Active un module (modifie 'included' à true)"
    echo "  disable <module>   Désactive un module (modifie 'included' à false)"
    echo "  run <module>       Exécute le script d'installation d'un module"
    echo "  help               Affiche cette aide"
}

# Fonction : Lister les modules
function list_modules() {
    echo "Modules disponibles :"
    echo "$MODULES" | jq -r '. | "\(.name) - \(.description) [Included: \(.included)]"'
}

# Fonction : Modifier le champ 'included' d'un module
function modify_inclusion() {
    local module_name="$1"
    local new_status="$2"

    jq --arg name "$module_name" --argjson status $new_status '
        .modules |= map(if .name == $name then .included = $status else . end)
    ' "$MODULES_JSON" > "$MODULES_JSON.tmp" && mv "$MODULES_JSON.tmp" "$MODULES_JSON"

    echo "Module '$module_name' mis à jour : included=$new_status"
}

# Fonction : Activer un module
function enable_module() {
    modify_inclusion "$1" true
}

# Fonction : Désactiver un module
function disable_module() {
    modify_inclusion "$1" false
}

# Fonction : Installer un module
function install_module() {
    local module_name="$1"
    local module_command=$(echo "$MODULES" | jq -r --arg name "$module_name" '. | select(.name == $name) | .install_command')

    if [[ -z "$module_command" || "$module_command" == "null" ]]; then
        echo "Erreur : Module '$module_name' introuvable."
        exit 1
    fi

    echo "Installation du module '$module_name'..."
    eval "$module_command"
    echo "Module '$module_name' installé avec succès."
}

# Fonction principale : Analyser les commandes
function main() {
    local command="$1"
    shift

    case "$command" in
        list)
            list_modules
            ;;
        install)
            [[ -z "$1" ]] && { echo "Erreur : Nom du module requis pour 'install'."; exit 1; }
            install_module "$1"
            ;;
        enable)
            [[ -z "$1" ]] && { echo "Erreur : Nom du module requis pour 'enable'."; exit 1; }
            enable_module "$1"
            ;;
        disable)
            [[ -z "$1" ]] && { echo "Erreur : Nom du module requis pour 'disable'."; exit 1; }
            disable_module "$1"
            ;;
        run)
            [[ -z "$1" ]] && { echo "Erreur : Nom du module requis pour 'run'."; exit 1; }
            install_module "$1"
            ;;
        help|*)
            show_help
            ;;
    esac
}

# Charger les modules
load_modules
}
# Lancer la CLI
#main "$@"
