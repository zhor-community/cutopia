#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

# Configuration centralisée
readonly REMOTE_USER="deploy-user"
readonly REMOTE_HOST="cuipfs"
readonly REMOTE_APP_ROOT="/opt/cutopia"
readonly REMOTE_SRC_PATH="${REMOTE_APP_ROOT}/src"
readonly LOCAL_SRC_PATH="${HOME}/.cutopia/src"
readonly DEPLOY_SCRIPT="$CUTUPIAPATH/bin/deploy.sh"
readonly INIT_SCRIPT="$CUTUPIAPATH/bin/init.zsh"
readonly SSH_KEY="${HOME}/.ssh/id_rsa"
readonly SSH_OPTS=(-i "$SSH_KEY" -o LogLevel=ERROR -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null)

# Fonctions utilitaires
log() {
    local level=$1
    shift
    printf "[%s][%s] %s\n" "$(date +%Y-%m-%dT%H:%M:%S)" "${level^^}" "$*" >&2
}

validate_environment() {
    local required_cmds=(ssh rsync)
    for cmd in "${required_cmds[@]}"; do
        if ! command -v "$cmd" >/dev/null 2>&1; then
            log error "Commande manquante: $cmd"
            exit 1
        fi
    done

    [[ ! -f "$SSH_KEY" ]] && { log error "Clé SSH introuvable: $SSH_KEY"; exit 1; }
    [[ ! -d "$LOCAL_SRC_PATH" ]] && { log error "Répertoire source introuvable: $LOCAL_SRC_PATH"; exit 1; }
}

deploy_files() {
    log info "Déploiement des fichiers avec rsync"
    rsync -avz --delete \
        --exclude='.git' \
        --exclude='*.swp' \
        -e "ssh ${SSH_OPTS[*]}" \
        "$LOCAL_SRC_PATH/" \
        "${REMOTE_USER}@${REMOTE_HOST}:${REMOTE_SRC_PATH}/"

    log info "Copie des fichiers de configuration"
    scp "${SSH_OPTS[@]}" \
        "${LOCAL_SRC_PATH}/../.env" \
        "${LOCAL_SRC_PATH}/../docker-compose.yml" \
        "${REMOTE_USER}@${REMOTE_HOST}:${REMOTE_APP_ROOT}/"
}

setup_remote() {
    log info "Exécution des commandes à distance"
    ssh "${SSH_OPTS[@]}" "${REMOTE_USER}@${REMOTE_HOST}" << EOF
        set -euo pipefail
        
        # Vérification des dépendances
        if ! command -v docker &>/dev/null; then
            sudo apt-get update
            sudo apt-get install -y docker.io docker-compose
        fi

        # Construction et déploiement
        cd $REMOTE_APP_ROOT
        docker-compose build --no-cache
        docker-compose up -d --force-recreate

        # Exécution du script d'initialisation
        zsh $REMOTE_SRC_PATH/$INIT_SCRIPT
EOF
}

main() {
    validate_environment
    deploy_files
    setup_remote
    log success "Déploiement terminé avec succès"
}

main "$@"
