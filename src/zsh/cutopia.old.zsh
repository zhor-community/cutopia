history > h.zsh
#GPG
#SSL
#SSH
#DNS
# Cutopia:
#CUSER=hermit
IRepo=.cutopia
#############################################################################
#                                                                           #
#                                                                           #
#                          cutopia/calpine/zshrc                            #
#                                                                           #
#                                                                           #
#############################################################################
#mkdir -p /home/$USER/.antigen
#curl -L git.io/antigen > /home/$USER/.antigen/antigen.zsh
#chown -R $USER:$USER /home/$USER/.antigen
#chown -R $USER:$CUSER /home/$USER/$IRepo
#ln -s /home/$USER/$IRepo/zsh/zshrc /home/$USER/.zshrc
#chown -R $USER:$USER /home/$USER/.zshrc

#############################################################################
#                                                                           #
#                                                                           #
#                          cutopia/calpine/cadit                            #
#                                                                           #
#                                                                           #
#############################################################################
sudo apt-get update 
sudo apt-get upgrade
sudo apt-get install tree sudo neovim ranger wget curl go gnupg nodejs yarn npm #openrc 
chsh -s $(which zsh)
git clone git@gitlab.com:zhor-community/cudit.git ~/.cudit
cd ~/.cudit
zsh init.zsh
