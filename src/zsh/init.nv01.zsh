#!/bin/zsh
source /tmp/src/env/calpine
source /tmp/src/zsh/cuapk.zsh
source /tmp/src/zsh/cucli.zsh
source /tmp/src/zsh/cutool.zsh
#source /tmp/src/zsh/cusources.zsh
# Colors for output
RED='\033[0;31m'
GREEN='\033[0;32m'
CYAN='\033[0;36m'
RESET='\033[0m'

SUCCESS="${GREEN}✔${RESET}"
ERROR="${RED}✖${RESET}"
INFO="${CYAN}ℹ${RESET}"

# Check if CALPINEPATH is set
if [[ -z "$CALPINEPATH" ]]; then
    echo -e "${ERROR} CALPINEPATH is not set. Please configure it before running this script."
    exit 1
fi

# Variables
container_name="my_container"
image_name="debian:latest"
#tools=("ssh" "sudo" "ansible" "docker" "xen" "lxc" "qemu") # List of tools to install
tools=("ssh" "sudo" "ansible" "zsh") # List of tools to install


# Step 2: Install tools using addTool
for tool in "${tools[@]}"; do
    echo -e "${INFO} Installing ${CYAN}${tool}${RESET} in container ${CYAN}${container_name}${RESET}."
    #docker exec "$container_name" zsh -c "CALPINEPATH=$CALPINEPATH; addTool -t $tool -e dockerContainer"
    addTool -t $tool -e dockerContainer"
    if [[ $? -ne 0 ]]; then
        echo -e "${ERROR} Failed to install ${CYAN}${tool}${RESET}."
    else
        echo -e "${SUCCESS} ${CYAN}${tool}${RESET} installed successfully."
    fi
done

# Step 3: Display completion message
echo -e "${SUCCESS} Container ${CYAN}${container_name}${RESET} initialized successfully."
