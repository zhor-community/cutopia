#!/bin/zsh

cunginx() {
    # Vérifie si l'utilisateur est root
    if [[ $EUID -ne 0 ]]; then
        echo "Ce script doit être exécuté en tant que root."
        exit 1
    fi

    # Détection du système d'init (systemd, openrc, runit, init.d, ...)
    detect_init_system() {
        if [[ -d /run/systemd/system ]]; then
            echo "systemd"
        elif [[ -f /etc/init.d/rc-status ]]; then
            echo "openrc"
        elif [[ -d /etc/init.d ]]; then
            echo "init.d"
        elif command -v runsv &>/dev/null; then
            echo "runit"
        else
            echo "unsupported"
        fi
    }

    INIT_SYSTEM=$(detect_init_system)
    echo "Système d'init détecté : $INIT_SYSTEM"

    # Chemins de configuration Nginx
    NGINX_SITES_AVAILABLE="/etc/nginx/sites-available"
    NGINX_SITES_ENABLED="/etc/nginx/sites-enabled"
    NGINX_CONF="/etc/nginx/nginx.conf"
    WEB_ROOT="/var/www"
    DOMAIN="cutopia.org"
    ROOT="/var/www/$DOMAIN"

    # Fonction : Installer Nginx
    install_nginx() {
        echo "Installation de Nginx..."
        if [[ -f /etc/alpine-release ]]; then
            apk update && apk add nginx
        else
            echo "Distribution non supportée pour cette fonction."
            exit 1
        fi

        if [[ $INIT_SYSTEM == "systemd" ]]; then
            systemctl enable nginx
            systemctl start nginx
        elif [[ $INIT_SYSTEM == "openrc" ]]; then
            rc-update add nginx default
            service nginx start
            rc-service nginx restart
        elif [[ $INIT_SYSTEM == "init.d" ]]; then
            /etc/init.d/nginx start
        elif [[ $INIT_SYSTEM == "runit" ]]; then
            sv start nginx
        else
            echo "Système d'init non supporté. Veuillez utiliser systemd, openrc, init.d ou runit."
            exit 1
        fi
        echo "Nginx installé et démarré."
    }

    # Fonction : Créer un nouveau site
    create_site() {
        DOMAIN=$1
        SITE_CONF="$NGINX_SITES_AVAILABLE/$DOMAIN"
        SITE_ROOT="$WEB_ROOT/$DOMAIN"

        if [[ -f $SITE_CONF ]]; then
            echo "Le site $DOMAIN existe déjà."
            return
        fi

        mkdir -p $SITE_ROOT
        cat > $SITE_CONF <<EOF
server {
    listen 80;
    server_name $DOMAIN www.$DOMAIN;

    root $SITE_ROOT;
    index index.html index.php index.htm;

    location / {
        try_files \$uri \$uri/ =404;
    }
    location ~ \.php$ {
        include fastcgi_params;
        fastcgi_pass 127.0.0.1:9000; # ou unix:/run/php/php8.1-fpm.sock selon votre configuration
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    }
}
EOF

        ln -s $SITE_CONF $NGINX_SITES_ENABLED/
        echo "Bienvenue sur $DOMAIN !" > $SITE_ROOT/index.html

cat > /var/www/$DOMAIN/info.php <<EOF
<?php
echo "Bienvenue sur votre serveur PHP !";
phpinfo();
?>
EOF

        nginx -t
        if [[ $INIT_SYSTEM == "systemd" ]]; then
            systemctl reload nginx
        elif [[ $INIT_SYSTEM == "openrc" ]]; then
            service nginx reload
        elif [[ $INIT_SYSTEM == "init.d" ]]; then
            /etc/init.d/nginx reload
        elif [[ $INIT_SYSTEM == "runit" ]]; then
            sv reload nginx
        fi
        echo "Site $DOMAIN configuré et activé."
    }

    # Fonction : Redémarrer Nginx
    restart_nginx() {
        echo "Redémarrage de Nginx..."
        if [[ $INIT_SYSTEM == "systemd" ]]; then
            systemctl restart nginx
        elif [[ $INIT_SYSTEM == "openrc" ]]; then
            service nginx restart
        elif [[ $INIT_SYSTEM == "init.d" ]]; then
            /etc/init.d/nginx restart
        elif [[ $INIT_SYSTEM == "runit" ]]; then
            sv restart nginx
        else
            echo "Système d'init non supporté."
            exit 1
        fi
        echo "Nginx redémarré."
    }

    # Fonction : Configurer SSL avec Certbot
    setup_ssl() {
        DOMAIN=$1

        if ! command -v certbot &> /dev/null; then
            echo "Installation de Certbot..."
            if [[ -f /etc/alpine-release ]]; then
                apk add certbot certbot-nginx
            else
                echo "Distribution non supportée pour Certbot."
                exit 1
            fi
        fi

        certbot --nginx -d $DOMAIN -d www.$DOMAIN
        if [[ $INIT_SYSTEM == "systemd" ]]; then
            systemctl reload nginx
        elif [[ $INIT_SYSTEM == "openrc" ]]; then
            service nginx reload
        elif [[ $INIT_SYSTEM == "init.d" ]]; then
            /etc/init.d/nginx reload
        elif [[ $INIT_SYSTEM == "runit" ]]; then
            sv reload nginx
        fi
        echo "SSL configuré pour $DOMAIN."
    }

    # Fonction : Gérer toutes les opérations
    manage_all() {
        echo "Exécution de toutes les actions principales..."

        if [[ -z "$DOMAIN" ]]; then
            echo "Le domaine n'est pas défini. Utilisez l'option -d pour spécifier un domaine."
            exit 1
        fi

        if [[ -z "$ROOT" ]]; then
            echo "Le répertoire racine n'est pas défini. Utilisation par défaut : /var/www/$DOMAIN."
            ROOT="/var/www/$DOMAIN"
        fi

        install_nginx
        create_site "$DOMAIN"
        setup_ssl "$DOMAIN"
        restart_nginx

        echo "Toutes les opérations ont été effectuées pour $DOMAIN avec le répertoire racine $ROOT."
    }

    # Utilisation de getopts pour gérer les options
    while getopts "i:c:d:R:s:a:h" opt; do
        case $opt in
            i) install_nginx ;;
            c) create_site $OPTARG ;;
            d) DOMAIN=$OPTARG ;;  # Domaine
            R) ROOT=$OPTARG ;;    # Racine
            s) setup_ssl $OPTARG ;;
            a) manage_all ;;
            h) echo "\nOptions disponibles :"
               echo "  -i                  Installer Nginx"
               echo "  -c <domaine>        Créer un nouveau site"
               echo "  -d <domaine>        Spécifier le domaine"
               echo "  -R <racine>         Spécifier le répertoire racine"
               echo "  -s <domaine>        Configurer SSL avec Certbot"
               echo "  -a                  Exécuter toutes les actions principales"
               echo "  -h                  Afficher l'aide"
               exit 0 ;;
            *) echo "Option invalide. Utilisez -h pour afficher l'aide."; exit 1 ;;
        esac
    done
}

# Appeler la fonction cunginx avec les arguments fournis
cunginx "$@"
