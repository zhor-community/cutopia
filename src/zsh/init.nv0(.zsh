#!/bin/zsh
#chsh -s $(which zsh)
set -e  # Exit script on any error
set -u  # Prevent using undefined variables
set -o pipefail  # Exit if any command in a pipeline fails

# 🔹 Configuration
APP_PATH="/opt/cutopia"
LOG_FILE="/var/log/cutopia_init.log"
CALPINEPATH="$APP_PATH"

# 🔹 Utility functions
log() {
    echo -e "\e[1;34m[ℹ] $1\e[0m"
    echo "$(date '+%Y-%m-%d %H:%M:%S') [INFO] $1" >> "$LOG_FILE"
}

error_exit() {
    echo -e "\e[1;31m[✘] ERROR: $1\e[0m" >&2
    echo "$(date '+%Y-%m-%d %H:%M:%S') [ERROR] $1" >> "$LOG_FILE"
    exit 1
}

log "🛠 Starting system initialization..."

# 🔹 Verify application directory existence
if [ ! -d "$APP_PATH" ]; then
    error_exit "Directory $APP_PATH does not exist."
fi

# 🔹 Check and fix permissions
log "🔑 Checking and setting permissions..."
chown -R root:root "$APP_PATH"
chmod -R 755 "$APP_PATH"

# 🔹 Load configurations and scripts
log "📂 Loading configurations..."
#source $CALPINEPATH/sec/env/calpine
#source $CALPINEPATH/sec/env/cucli.env

# 🔹 Install required packages
log "📦 Installing required packages..."
source $CALPINEPATH/src/zsh/cuapk.zsh
cuapk add zsh curl bash jq git sudo

# 🔹 Load Cutopia Modules
log "📂 Installing Cutopia Modules..."
source $CALPINEPATH/src/zsh/cucli.zsh

log "ℹ️  Displaying help:"
cucli help | sed 's/^/   /'

log "📋 Listing available modules:"
cucli list | sed 's/^/   /'

log "⬇️ Installing module: cuvps"
cucli install cuvps && echo -e "${GREEN}✔ Successfully installed cuvps${RESET}" || echo -e "${RED}✖ Failed to install cuvps${RESET}"

log "✅ Enabling module: cuipfs"
cucli enable cuipfs && echo -e "${GREEN}✔ Successfully enabled cuipfs${RESET}" || echo -e "${RED}✖ Failed to enable cuipfs${RESET}"

log "⛔ Disabling module: cudocker"
cucli disable cudocker && echo -e "${YELLOW}⚠ cudocker disabled${RESET}" || echo -e "${RED}✖ Failed to disable cudocker${RESET}"

log "🔍 Checking cucli version:"
cucli version | sed 's/^/   /'
# 🔹 Install required packages
