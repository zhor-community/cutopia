#!/bin/zsh

# 1️⃣ Chargement des configurations
JSON_FILE="proxmox.json"
# Vérifier si jq et le fichier JSON existent
# Lire les paramètres globaux (host, node, storage...)

# 2️⃣ Définition des fonctions principales

## ➤ Fonction pour créer une VM
create_vm() {
    # Paramètres : Nom, ID, OS, ISO, CPU, RAM, Disque, Réseau
    # Exécuter `qm create`
}

## ➤ Fonction pour créer un conteneur (CT)
create_ct() {
    # Paramètres : Nom, ID, Template, CPU, RAM, Disque, Réseau
    # Exécuter `pct create`
}

## ➤ Fonction pour restaurer une VM ou un CT
restore_instance() {
    # Paramètre : ID de la VM/CT
    # Exécuter `qmrestore` ou `pct restore`
}

## ➤ Fonction pour sauvegarder toutes les instances
backup_instances() {
    # Vérifier si la sauvegarde est activée
    # Exécuter la commande définie dans `proxmox.json`
}

## ➤ Fonction pour surveiller les ressources de Proxmox
monitor_proxmox() {
    # Récupérer et afficher CPU, RAM et Disque via `pvesh`
}

## ➤ Fonction pour créer toutes les VMs et CTs
create_all() {
    # Lire `proxmox.json`
    # Boucle pour créer chaque VM
    # Boucle pour créer chaque CT
}

## ➤ Fonction pour afficher l’aide
show_help() {
    # Afficher les options disponibles (-c, -r, -b, -m, -h)
}

# 3️⃣ Gestion des options avec `getopts`
while getopts "cr:bmh" opt; do
    case "$opt" in
        c) create_all ;;  # Création de toutes les VMs et CTs
        r) restore_instance "$OPTARG" ;;  # Restauration d'une VM/CT
        b) backup_instances ;;  # Sauvegarde
        m) monitor_proxmox ;;  # Monitoring
        h) show_help ;;  # Afficher l'aide
        *) show_help ;;  # Par défaut, afficher l'aide
    esac
done

# 4️⃣ Si aucune option n’est fournie, afficher l’aide
if [[ $OPTIND -eq 1 ]]; then
    show_help
fi

