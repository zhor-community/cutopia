
#!/bin/zsh

# ==========================================
# Iconlors.zsh - Icônes et Couleurs pour Zsh
# ==========================================
# Description :
#   Ce script fournit une bibliothèque d'icônes Unicode et de couleurs ANSI
#   pour améliorer la lisibilité et l'esthétique dans le terminal.
#
# Auteur : AbdElHakim ZOUAÏ
# Email  : abdelhakimzouai@gmail.com
# ==========================================

# ==========================================
# Couleurs ANSI (Basique et Vive)
# ==========================================
RESET_COLOR="%{$reset_color%}"  # Réinitialise la couleur
BOLD="%{$fg_bold%}"


# Standard Colors
COLORS=(
  "BLACK:%{$fg[black]%}" 
  "RED:%{$fg[red]%}" 
  "GREEN:%{$fg[green]%}" 
  "YELLOW:%{$fg[yellow]%}" 
  "BLUE:%{$fg[blue]%}" 
  "MAGENTA:%{$fg[magenta]%}" 
  "CYAN:%{$fg[cyan]%}" 
  "WHITE:%{$fg[white]%}"
)

# Bright Colors
BRIGHT_COLORS=(
  "BRIGHT_RED:%{$fg_bold[red]%}" 
  "BRIGHT_GREEN:%{$fg_bold[green]%}" 
  "BRIGHT_YELLOW:%{$fg_bold[yellow]%}" 
  "BRIGHT_BLUE:%{$fg_bold[blue]%}" 
  "BRIGHT_MAGENTA:%{$fg_bold[magenta]%}" 
  "BRIGHT_CYAN:%{$fg_bold[cyan]%}" 
  "BRIGHT_WHITE:%{$fg_bold[white]%}"
)
# Initialisation des couleurs
for color in "${COLORS[@]}" "${BRIGHT_COLORS[@]}"; do
  eval "${color//:/=}"
done

# ==========================================
# Icônes Unicode (Étendu)
# ==========================================
# Générales
ICON_INFO="ℹ️ "       # Information
ICON_SUCCESS="✅ "   # Succès
ICON_WARNING="⚠️ "    # Avertissement
ICON_ERROR="❌ "     # Erreur
ICON_OK="👍 "        # OK
ICON_FAIL="👎 "      # Échec
ICON_CLOCK="⏰ "     # Temps
ICON_CHECK="✔️ "     # Validation

# Fichiers et Répertoires
ICON_FOLDER="📁 "    # Répertoire
ICON_FILE="📄 "      # Fichier
ICON_BINARY="📦 "    # Fichier binaire
ICON_SCRIPT="📜 "    # Script
ICON_CONFIG="⚙️ "    # Fichier de configuration
ICON_DOWNLOAD="📥 "  # Téléchargement
ICON_UPLOAD="📤 "    # Téléversement

# Réseau et Services
ICON_SSH="🔒 "       # SSH
ICON_NETWORK="🌐 "   # Réseau
ICON_DATABASE="🗄️ "  # Base de données
ICON_SERVER="🖥️ "    # Serveur
ICON_FIREWALL="🔥 "  # Pare-feu

# Développement
ICON_CODE="💻 "      # Code source
ICON_BUILD="🏗️ "    # Compilation
ICON_TEST="🧪 "      # Test
ICON_DEPLOY="🚀 "    # Déploiement
ICON_BUG="🐛 "       # Bug
ICON_PATCH="🔧 "     # Correctif

# Statuts
ICON_START="▶️ "     # Démarrage
ICON_STOP="⏹️ "     # Arrêt
ICON_PAUSE="⏸️ "    # Pause
ICON_RESTART="🔄 "   # Redémarrage
ICON_LOADING="⏳ "   # Chargement

# Sécurité
ICON_LOCK="🔒 "      # Verrouillage
ICON_UNLOCK="🔓 "    # Déverrouillage
ICON_KEY="🔑 "       # Clé
ICON_WARNING_SHIELD="🛡️ " # Avertissement de sécurité

# Indicateurs Visuels
ICON_STAR="⭐ "       # Favori
ICON_HEART="❤️ "    # Cœur
ICON_ARROW_RIGHT="➡️ " # Flèche droite
ICON_ARROW_LEFT="⬅️ "  # Flèche gauche
ICON_ARROW_UP="⬆️ "   # Flèche haut
ICON_ARROW_DOWN="⬇️ " # Flèche bas

# Tâches et Processus
ICON_TASK="📝 "      # Tâche
ICON_PROCESS="🔄 "   # Processus
ICON_DONE="✔️ "      # Terminé
ICON_PENDING="⌛ "   # En attente
ICON_CANCEL="❌ "    # Annulé

# ==========================================
# Alias pour Messages Stylisés
# ==========================================
alias log_info="echo \"${BOLD}${ICON_INFO}${CYAN}\""
alias log_success="echo \"${BOLD}${ICON_SUCCESS}${GREEN}\""
alias log_warning="echo \"${BOLD}${ICON_WARNING}${YELLOW}\""
alias log_error="echo \"${BOLD}${ICON_ERROR}${RED}\""

# Exemple d'utilisation :
#   log_info "Démarrage de l'installation..."
#   log_success "Installation réussie !"
#   log_warning "Configuration manquante détectée."
#   log_error "Échec critique : Veuillez vérifier les logs."

# ==========================================
# Prompt Personnalisé
# ==========================================
PROMPT='%{$fg_bold[green]%}%n@%m%{$reset_color%}:%{$fg_bold[blue]%}%~%{$reset_color%} $(git_prompt_info)${RESET_COLOR} ➜ '

# ==========================================
# Fonctions Utiles
# ==========================================
# Affiche toutes les icônes disponibles avec leurs descriptions.
function show_icons() {
  echo "${BOLD}${CYAN}Liste des Icônes disponibles :${RESET_COLOR}"
  echo "${ICON_INFO} Information"
  echo "${ICON_SUCCESS} Succès"
  echo "${ICON_WARNING} Avertissement"
  echo "${ICON_ERROR} Erreur"
  echo "${ICON_FOLDER} Répertoire"
  echo "${ICON_FILE} Fichier"
  echo "${ICON_SCRIPT} Script"
  echo "${ICON_SSH} SSH"
  echo "${ICON_NETWORK} Réseau"
  echo "${ICON_CODE} Code source"
  echo "${ICON_DEPLOY} Déploiement"
  echo "${ICON_LOCK} Verrouillage"
  echo "${ICON_UNLOCK} Déverrouillage"
  echo "${ICON_BUG} Bug"
  echo "${ICON_PATCH} Correctif"
  echo "${ICON_LOADING} Chargement"
  echo "${ICON_DONE} Terminé"
  echo "${ICON_PENDING} En attente"
}

# Ajoute une couleur personnalisée à une chaîne donnée.
# Usage : colorize "Message" "${GREEN}"
function colorize() {
  local message="$1"
  local color="$2"
  echo "${color}${message}${RESET_COLOR}"
}

# ==========================================
# Message de Chargement
# ==========================================
log_info "Iconlors.zsh a été chargé avec succès !"

