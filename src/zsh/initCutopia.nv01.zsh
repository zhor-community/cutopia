#!/bin/zsh

# ==========================================
# initCutopia.zsh - Cutopia Initialization
# ==========================================
# Description:
#   This script initializes Cutopia by installing all modules marked
#   as included in the `modules.json` file.
#
# Author : AbdElHakim ZOUAÏ
# Email  : abdelhakimzouai@gmail.com
# ==========================================

# Load environment variables
ENV_FILE="/resources/env/cucli.env"
if [[ -f "$ENV_FILE" ]]; then
    source "$ENV_FILE"
    echo "⏳ Load environment from : $ENV_FILE..."
else
    echo "❌ [ERROR] Environment file not found: $ENV_FILE"
    exit 1
fi

# Check if cucli.zsh exists and is executable
CUCLI_SCRIPT="/resources/zsh/cucli.zsh"
if [[ ! -x "$CUCLI_SCRIPT" ]]; then
    echo "❌ [ERROR] cucli.zsh script not found or not executable: $CUCLI_SCRIPT"
    exit 1
fi

# Load modules from modules.json
MODULES_FILE="$MODULES_JSON"
if [[ ! -f "$MODULES_FILE" ]]; then
    echo "❌ [ERROR] modules.json file not found: $MODULES_FILE"
    exit 1
fi

# List modules to be installed
MODULES=$(jq -r '.modules[] | select(.included == true) | .name' "$MODULES_FILE")
if [[ -z "$MODULES" ]]; then
    echo "⚠️  No included modules found in $MODULES_FILE."
    exit 0
fi


# Initialize Cutopia by installing all included modules
echo "🚀 Initializing Cutopia..."
for module in $MODULES; do
    cucli install "$module"
done

echo "🎉 Cutopia initialization completed successfully!"
