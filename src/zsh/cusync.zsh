
cusync() {
    # Définition des valeurs par défaut
    local src_path="~/.cutopia/src/"         # Chemin source
    local dest_path="/opt/cutopia/src/"        # Chemin destination
    local ssh_user="root"     # Utilisateur SSH par défaut
    local ssh_host="cuipfs"  # Adresse du VPS par défaut
    local ssh_port=22         # Port SSH par défaut
    local ssh_key="$HOME/.ssh/id_rsa"  # Clé SSH par défaut

    # Lecture des options avec getopts
    while getopts "s:d:u:h:p:k:" opt; do
        case $opt in
            s) src_path="$OPTARG" ;;       # Chemin source
            d) dest_path="$OPTARG" ;;      # Chemin destination
            u) ssh_user="$OPTARG" ;;       # Utilisateur SSH
            h) ssh_host="$OPTARG" ;;       # Adresse du VPS
            p) ssh_port="$OPTARG" ;;       # Port SSH
            k) ssh_key="$OPTARG" ;;        # Fichier de clé SSH
            *) 
                echo "Usage: sync_files -s source -d destination [-u user] [-h host] [-p port] [-k key_file]"
                return 1
                ;;
        esac
    done

    # Vérification des paramètres obligatoires
    if [[ -z "$src_path" || -z "$dest_path" ]]; then
        echo "Erreur : source et destination sont obligatoires."
        echo "Usage: sync_files -s source -d destination [-u user] [-h host] [-p port] [-k key_file]"
        return 1
    fi

    # Construction de la commande SSH
    local ssh_cmd="ssh -p $ssh_port"
    [[ -n "$ssh_key" ]] && ssh_cmd+=" -i $ssh_key"

    # Exécution de rsync
    rsync -avz -e "$ssh_cmd" "$src_path" "$ssh_user@$ssh_host:$dest_path"
}

# Exemple d'utilisation :
# sync_files -s /chemin/local/ -d /chemin/destination/
# sync_files -s /chemin/local/ -d /chemin/destination/ -u monuser -h 192.168.1.100 -p 2222 -k ~/.ssh/autre_cle
cusync -s ~/.cutopia/src -d /opt/cutopia/src -u root -h cuipfs -p 22 -k ~/.ssh/id_rsa
