# Detect the Linux distribution
cuapk() {
detect_distro() {
  if [ -f /etc/os-release ]; then
    . /etc/os-release
    echo $ID
  else
    echo "unknown"
  fi
}

# Install a package
install_package() {
  local package=$1
  case $(detect_distro) in
    alpine)
      sudo apk add $package
      ;;
    debian|ubuntu)
      sudo apt-get install -y $package
      ;;
    fedora)
      sudo dnf install -y $package
      ;;
    arch)
      sudo pacman -S --noconfirm $package
      ;;
    *)
      echo "Unsupported distribution: $(detect_distro)"
      ;;
  esac
}

# List installed packages
list_packages() {
  case $(detect_distro) in
    alpine)
      apk info
      ;;
    debian|ubuntu)
      dpkg -l
      ;;
    fedora)
      rpm -qa
      ;;
    arch)
      pacman -Q
      ;;
    *)
      echo "Unsupported distribution: $(detect_distro)"
      ;;
  esac
}

# Update all packages
update_packages() {
  case $(detect_distro) in
    alpine)
      sudo apk update && sudo apk upgrade
      ;;
    debian|ubuntu)
      sudo apt-get update && sudo apt-get upgrade -y
      ;;
    fedora)
      sudo dnf update -y
      ;;
    arch)
      sudo pacman -Syu --noconfirm
      ;;
    *)
      echo "Unsupported distribution: $(detect_distro)"
      ;;
  esac
}

# Remove a package
remove_package() {
  local package=$1
  case $(detect_distro) in
    alpine)
      sudo apk del $package
      ;;
    debian|ubuntu)
      sudo apt-get remove -y $package
      ;;
    fedora)
      sudo dnf remove -y $package
      ;;
    arch)
      sudo pacman -R --noconfirm $package
      ;;
    *)
      echo "Unsupported distribution: $(detect_distro)"
      ;;
  esac
}

# Check if a package is installed
check_package() {
  local package=$1
  case $(detect_distro) in
    alpine)
      apk info | grep -w $package
      ;;
    debian|ubuntu)
      dpkg -l | grep -w $package
      ;;
    fedora)
      rpm -qa | grep -w $package
      ;;
    arch)
      pacman -Q | grep -w $package
      ;;
    *)
      echo "Unsupported distribution: $(detect_distro)"
      ;;
  esac
}

# Display usage information
usage() {
  echo "Usage: cuapk {add|list|update|remove|check} [packages...]"
  echo "Commands:"
  echo "  add [packages...]  Install one or more packages"
  echo "  list                   List all installed packages"
  echo "  update                 Update all packages"
  echo "  remove [packages...]   Remove one or more packages"
  echo "  check [packages...]    Check if one or more packages are installed"
}

# Main command
  local command=$1
  shift
  case $command in
    add)
      for package in "$@"; do
        install_package "$package"
      done
      ;;
    list)
      list_packages
      ;;
    update)
      update_packages
      ;;
    remove)
      for package in "$@"; do
        remove_package "$package"
      done
      ;;
    check)
      for package in "$@"; do
        check_package "$package"
      done
      ;;
    *)
      usage
      ;;
  esac
}

# End of script cuapk.zsh

