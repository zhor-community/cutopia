#source cusource.zsh
#RUN apk add zsh git tree sudo neovim ranger wget curl go gnupg nodejs yarn npm openrc 
#RUN apk --no-cache add zsh curl git sudo openrc wget
############################################################################
#                                                                          #
#                                 Pkgs                                     #
#                                                                          #
############################################################################
function addApks() {
    echo "Updating package lists..."
    #sudo apt-get update

    echo "Upgrading existing packages..."
    #sudo apt-get upgrade -y
    cuapk update

    echo "Installing packages..."
    cuapk add \
        git \
        tree \
        sudo \
        ranger \
        wget \
        curl \
        gnupg2 \
        bash \
        ca-certificates \
        fzf \
        htop \
        ripgrep \
        jq \
        make \
        ncdu \
        shellcheck
#        zsh \
#        go \
#        neovim \
#        nodejs \
#        yarn \
#        npm \
#        tmux \
#        docker.io \
#        zsh-syntax-highlighting \
#        zsh-autosuggestions \

    echo "All packages have been installed successfully."
}

############################################################################
#                                                                          #
#                                 sudo                                     #
#                                                                          #
############################################################################

function cfgSudo() {
    # Check if a username was provided
    if [ -z "$1" ]; then
        echo "Error: No username provided."
        echo "Usage: cfgSudo <username> [-y]"
        return 1
    fi

    local username="$1"
    local force_nopasswd=false

    # Check if the '-y' option is provided to automatically configure without password
    if [[ "$2" == "-y" ]]; then
        force_nopasswd=true
    fi

    # Check if the user exists, and create the user if not
    if ! id -u "$username" > /dev/null 2>&1; then
        echo "User '$username' does not exist. Creating the user..."
        sudo useradd -m "$username"
        if [ $? -ne 0 ]; then
            echo "Error: Failed to create the user '$username'."
            return 1
        fi
        echo "User '$username' created successfully."
        # Set a password for the newly created user
        sudo passwd "$username"
    else
        echo "User '$username' already exists."
    fi

    # Add the user to the sudo or wheel group depending on the distribution
    if grep -qEi "(debian|ubuntu)" /etc/os-release; then
        sudo usermod -aG sudo "$username"
        echo "User '$username' has been added to the sudo group."
    elif grep -qEi "(redhat|centos|fedora)" /etc/os-release; then
        sudo usermod -aG wheel "$username"
        echo "User '$username' has been added to the wheel group."
    else
        echo "Error: Unsupported operating system."
        return 1
    fi

    # Automatically configure sudo without password if '-y' is used
    if $force_nopasswd; then
        echo "$username ALL=(ALL) NOPASSWD:ALL" | sudo tee "/etc/sudoers.d/$username" > /dev/null
        sudo chmod 0440 "/etc/sudoers.d/$username"
        echo "sudo configured without a password for '$username'."
    else
        # Option to configure sudo without a password with confirmation
        read -p "Configure sudo without a password for '$username'? (y/n): " response
        if [[ "$response" =~ ^[Yy]$ ]]; then
            echo "$username ALL=(ALL) NOPASSWD:ALL" | sudo tee "/etc/sudoers.d/$username" > /dev/null
            sudo chmod 0440 "/etc/sudoers.d/$username"
            echo "sudo configured without a password for '$username'."
        else
            echo "sudo configured with a password for '$username'."
        fi
    fi
}
############################################################################
#                                                                          #
#                                nodejs                                    #
#                                                                          #
############################################################################
function addNodejs() {
    local nvm_version="v0.40.1"
    local nvm_install_url="https://raw.githubusercontent.com/nvm-sh/nvm/$nvm_version/install.sh"

    echo "Installing NVM (Node Version Manager)..."

    # Check if curl or wget is available and use it to install NVM
    if command -v curl > /dev/null; then
        curl -o- "$nvm_install_url" | bash
    elif command -v wget > /dev/null; then
        wget -qO- "$nvm_install_url" | bash
    else
        echo "Error: Neither curl nor wget is installed. Please install one and try again."
        return 1
    fi

    # Load NVM into the current shell session
    export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm

    echo "Installing the latest LTS version of Node.js..."
    nvm install --lts
    nvm use --lts

    echo "Ensuring npm is up-to-date..."
    npm install --global npm


    echo "Installing the latest version of Yarn..."
    npm install --global yarn

    echo "NVM, Node.js, Yarn, and npm have been installed and configured."
}

############################################################################
#                                                                          #
#                                golang                                    #
#                                                                          #
############################################################################
function addGolang() {
    local go_version="1.21.1"  # Specify the version of Go you want to install
    local go_download_url="https://golang.org/dl/go$go_version.linux-amd64.tar.gz"

    echo "Downloading Go $go_version..."

    # Download the Go binary
    if command -v curl > /dev/null; then
        curl -LO "$go_download_url"
    elif command -v wget > /dev/null; then
        wget "$go_download_url"
    else
        echo "Error: Neither curl nor wget is installed. Please install one and try again."
        return 1
    fi

    echo "Removing any previous Go installation..."
    sudo rm -rf /usr/local/go

    echo "Installing Go $go_version..."
    sudo tar -C /usr/local -xzf "go$go_version.linux-amd64.tar.gz"

    echo "Setting up Go environment variables..."

    # Append Go environment variables to .zshrc
    {
        echo 'export PATH=$PATH:/usr/local/go/bin'
        echo 'export GOPATH=$HOME/go'
        echo 'export PATH=$PATH:$GOPATH/bin'
    } >> ~/.zshrc

    # Source .zshrc to apply the changes
    source ~/.zshrc

    echo "Go $go_version has been installed and configured."
}

############################################################################
#                                                                          #
#                                  php                                     #
#                                                                          #
############################################################################
function addPhp() {
    echo "Updating package lists..."
    sudo apt-get update

    echo "Installing PHP and common extensions..."

    # Install PHP and common extensions
    sudo apt-get install -y \
        php \
        php-cli \
        php-fpm \
        php-mysql \
        php-xml \
        php-mbstring \
        php-curl \
        php-zip \
        php-gd \
        php-intl \
        php-bcmath \
        php-soap

    # Optional: Install additional PHP packages based on your needs
    sudo apt-get install -y php-redis php-sqlite3 php-pgsql

    echo "PHP and extensions have been installed and configured."

    # Verify installation
    php -v
}
############################################################################
#                                                                          #
#                                  zsh                                     #
#                                                                          #
############################################################################

#############################################################################
#                                                                           #
#                                                                           #
#                          cutopia/calpine/zshrc                            #
#                                                                           #
#                                                                           #
#############################################################################
#cuapk zsh shadow
#sudo chsh -s $(which zsh)
#sudo chsh -s /bin/zsh
#chsh -s /bin/zsh
#mkdir -p /home/hermit/.antigen
#curl -L git.io/antigen > /home/hermit/.antigen/antigen.zsh
#chown -R hermit:hermit /home/hermit/.antigen
#sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
#mkdir -p /home/hermit/.cutopia
#chown -R hermit:hermit /home/hermit/.cutopia
#ln -s /home/hermit/.cutopia/cugo/src/zsh/zshrc /home/hermit/.zshrc
#chown -R hermit:hermit /home/hermit/.zshrc
#
#mkdir ~/.antigen/
#curl -L git.io/antigen > ~/.antigen/antigen.zsh
#ln -s ~/.calpine/zsh/zshrc ~/.zshrc
#############################################################################
#                                                                          #
#                                 zsh                                      #
#                                                                          #
############################################################################
function addZsh() {
    local username=""
    local cutopia_dir=""

    # Parse options
    while [[ "$#" -gt 0 ]]; do
        case $1 in
            --user) username="$2"; shift ;;
            --repo) cutopia_dir="$2"; shift ;;
            *) echo "Unknown parameter passed: $1"; return 1 ;;
        esac
        shift
    done

    if [ -z "$username" ] || [ -z "$cutopia_dir" ]; then
        echo "Usage: setup_zsh_env --user <username> --repo <cutopia_directory>"
        return 1
    fi

    echo "Setting up Zsh environment for user '$username' with .cutopia directory '$cutopia_dir'..."

    # Install zsh if not already installed
    cuapk update
    cuapk zsh

    # Change the default shell to zsh for the specified user
    sudo chsh -s "$(which zsh)" "$username"

    # Set up Antigen
    mkdir -p /home/"$username"/.antigen
    curl -L git.io/antigen > /home/"$username"/.antigen/antigen.zsh
    chown -R "$username":"$username" /home/"$username"/.antigen

    # Install Oh My Zsh
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

    # Set up Cutopia
    mkdir -p /home/"$username"/"$cutopia_dir"
    chown -R "$username":"$username" /home/"$username"/"$cutopia_dir"
    ln -s /home/"$username"/"$cutopia_dir"/cugo/src/zsh/zshrc /home/"$username"/.zshrc
    chown -R "$username":"$username" /home/"$username"/.zshrc

    echo "Zsh environment setup completed for user '$username'."
    #setup_zsh_env --user hermit --repo cutopia

}

#############################################################################
#                                                                          #
#                                python                                    #
#                                                                          #
############################################################################
#zsh cupy.zsh
#zsh django.zsh


############################################################################
#                                                                          #
#                               openssh                                    #
#                                                                          #
############################################################################
addSsh() {
    local os=$(uname -s)
    local distro

    case "$os" in
        Linux)
            # Detect the Linux distribution
            if [ -f /etc/os-release ]; then
                . /etc/os-release
                distro=$ID
            elif [ -f /etc/debian_version ]; then
                distro="debian"
            elif [ -f /etc/centos-release ]; then
                distro="centos"
            else
                echo "Unsupported operating system."
                return 1
            fi

            case "$distro" in
                alpine)
                    echo "Detected Alpine Linux."
                    if [ -f "sshInstaller.alpine.zsh" ]; then
                        source sshInstaller.alpine.zsh
                    else
                        echo "The sshInstaller.alpine.zsh script is missing."
                        return 1
                    fi
                    ;;
                debian)
                    echo "Detected Debian."
                    if [ -f "sshInstaller.debian.zsh" ]; then
                        source sshInstaller.debian.zsh
                    else
                        echo "The sshInstaller.debian.zsh script is missing."
                        return 1
                    fi
                    ;;
                ubuntu)
                    echo "Detected Ubuntu."
                    if [ -f "sshInstaller.ubuntu.zsh" ]; then
                        source sshInstaller.ubuntu.zsh
                    else
                        echo "The sshInstaller.ubuntu.zsh script is missing."
                        return 1
                    fi
                    ;;
                centos)
                    echo "Detected CentOS."
                    if [ -f "sshInstaller.centos.zsh" ]; then
                        source sshInstaller.centos.zsh
                    else
                        echo "The sshInstaller.centos.zsh script is missing."
                        return 1
                    fi
                    ;;
                fedora)
                    echo "Detected Fedora."
                    if [ -f "sshInstaller.fedora.zsh" ]; then
                        source sshInstaller.fedora.zsh
                    else
                        echo "The sshInstaller.fedora.zsh script is missing."
                        return 1
                    fi
                    ;;
                *)
                    echo "Unsupported Linux distribution: $distro"
                    return 1
                    ;;
            esac
            ;;
        *)
            echo "Unsupported operating system: $os"
            return 1
            ;;
    esac
}

#apk add openssh
#echo 'PasswordAuthentication yes' >> /etc/ssh/sshd_config
#mkdir /run/openrc/
#touch /run/openrc/softlevel
#rc-updaggte add sshd
#rc-status
#rc-service sshd start
#ssh-keygen -A
#exec /usr/sbin/sshd -D & #-e "$@"
#rc-status
#rc-service sshd start
##rc-service sshd restart
# zsh /app/src/cussh/init.zsh
#ssh-keygen -t ed25519 -C "abdelhakimzouai@gmail.com"
#eval "$(ssh-agent -s)"
#ssh-add ~/.ssh/id_ed25519
#cat ~/.ssh/id_ed25519.pub
# Then, go to GitLab and navigate to:
#
#User Settings > SSH Keys
#Paste the copied SSH key into the "Key" field and give it a relevant title.
#Click "Add key".
#ssh -T git@gitlab.com
#git clone git@gitlab.com:zhor-community/calpine.git ~/.calpine
#
######################################################################### ##
#                                                                          #
#                                Docker                                    #
#                                                                          #
############################################################################
#https://wiki.alpinelinux.org/wiki/Docker#Installation
#apk add docker
#apk add docker-cli-compose
#sudo addgroup $USER docker
#rc-update add docker default
#service docker start
#zsh dockerInstaller.zsh
addDocker() {
    local os=$(uname -s)
    local distro

    case "$os" in
        Linux)
            # Detect the Linux distribution
            if [ -f /etc/os-release ]; then
                . /etc/os-release
                distro=$ID
            elif [ -f /etc/debian_version ]; then
                distro="debian"
            elif [ -f /etc/centos-release ]; then
                distro="centos"
            else
                echo "Unsupported operating system."
                return 1
            fi

            case "$distro" in
                alpine)
                    echo "Detected Alpine Linux."
                    if [ -f "dockerInstaller.alpine.zsh" ]; then
                        source dockerInstaller.alpine.zsh
                    else
                        echo "The dockerInstaller.alpine.zsh script is missing."
                        return 1
                    fi
                    ;;
                debian)
                    echo "Detected Debian."
                    if [ -f "dockerInstaller.debian.zsh" ]; then
                        source dockerInstaller.debian.zsh
                    else
                        echo "The dockerInstaller.debian.zsh script is missing."
                        return 1
                    fi
                    ;;
                ubuntu)
                    echo "Detected Ubuntu."
                    if [ -f "dockerInstaller.ubuntu.zsh" ]; then
                        source dockerInstaller.ubuntu.zsh
                    else
                        echo "The dockerInstaller.ubuntu.zsh script is missing."
                        return 1
                    fi
                    ;;
                centos)
                    echo "Detected CentOS."
                    if [ -f "dockerInstaller.centos.zsh" ]; then
                        source dockerInstaller.centos.zsh
                    else
                        echo "The dockerInstaller.centos.zsh script is missing."
                        return 1
                    fi
                    ;;
                fedora)
                    echo "Detected Fedora."
                    if [ -f "dockerInstaller.fedora.zsh" ]; then
                        source dockerInstaller.fedora.zsh
                    else
                        echo "The dockerInstaller.fedora.zsh script is missing."
                        return 1
                    fi
                    ;;
                *)
                    echo "Unsupported Linux distribution: $distro"
                    return 1
                    ;;
            esac
            ;;
        *)
            echo "Unsupported operating system: $os"
            return 1
            ;;
    esac
}

############################################################################
#                                                                          #
#                               calpine                                    #
#                                                                          #
############################################################################


#############################################################################
#                                                                          #
#                               caddy                                      #
#                                                                          #
############################################################################
#https://chatgpt.com/share/6143cbdc-ec50-48dd-b569-ad3fdadd5fc7
#curl -sS https://webi.sh/caddy | sh
#source ~/.config/envman/PATH.env
#caddy start


############################################################################
#                                                                          #
#                              apache2                                     #
#                                                                          #
############################################################################
#https://wiki.alpinelinux.org/wiki/Apache
#apk add apache2
#rc-service apache2 start
#rc-update add apache2
#rc-service apache2 restart

#https://wiki.alpinelinux.org/wiki/Apache_with_php-fpm
#apk add apache2-proxy php8-fpm
#rc-service php-fpm8 start
#rc-update add php-fpm8
#rc-service apache2 start
#rc-update add apache2


#############################################################################
#                                                                          #
#                            nextcloud                                     #
#                                                                          #
############################################################################
#NEXTBRANCH=
#git clone git@github.com:nextcloud/server.git --branch $NEXTBRANCH
#cd server
#git submodule update --init

############################################################################
#                                                                          #
#                            e-learing tools                               #
#                                                                          #
############################################################################
# cuapk add screenkey onboard
############################################################################
#                                                                          #
#                                  latex                                   #
#                                                                          #
############################################################################

