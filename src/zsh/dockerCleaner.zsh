#!/bin/zsh

# Fonction de nettoyage Docker
docker_cleanup() {
    local clean_containers=0
    local clean_images=0
    local clean_volumes=0
    local clean_networks=0
    local clean_all=0

    # Fonction d'affichage de l'aide
    usage() {
        echo "Usage: $0 [-c] [-i] [-v] [-n] [-a]"
        echo "  -c    Supprimer tous les conteneurs"
        echo "  -i    Supprimer toutes les images"
        echo "  -v    Supprimer tous les volumes"
        echo "  -n    Supprimer tous les réseaux"
        echo "  -a    Tout nettoyer (conteneurs, images, volumes, et réseaux)"
        exit 1
    }

    # Analyser les options
    while getopts "civna" opt; do
        case ${opt} in
            c)
                clean_containers=1
                ;;
            i)
                clean_images=1
                ;;
            v)
                clean_volumes=1
                ;;
            n)
                clean_networks=1
                ;;
            a)
                clean_all=1
                ;;
            *)
                usage
                ;;
        esac
    done

    # Si aucune option n'est passée, afficher l'aide
    if [ $OPTIND -eq 1 ]; then
        usage
    fi

    # Nettoyage selon les options
    if [ $clean_all -eq 1 ]; then
        docker system prune -a --volumes
    else
        if [ $clean_containers -eq 1 ]; then
            docker stop $(docker ps -aq)
            docker rm $(docker ps -aq)
        fi
        if [ $clean_images -eq 1 ]; then
            docker rmi $(docker images -q)
        fi
        if [ $clean_volumes -eq 1 ]; then
            docker volume rm $(docker volume ls -q)
        fi
        if [ $clean_networks -eq 1 ]; then
            docker network prune -f
        fi
    fi
}

# Appeler la fonction de nettoyage
# docker_cleanup "$@"
# CRUD Docker clean =delette
