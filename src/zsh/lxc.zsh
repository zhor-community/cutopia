#!/bin/zsh
CUTOPIAPATH=/opt/cutopia
# Charger le script de templating
source $CUTOPIAPATH/src/zsh/lxc.templater.old.zsh

# Liste des conteneurs à créer
CONTAINERS=("cutopia" "ticenergy" "tassili" "zerozone" "zhor" "octomatics")

# Vérifier si le modèle existe avant de créer les conteneurs
if ! lxc image list | grep -q "$TEMPLATE_NAME"; then
    echo "Erreur : Le modèle $TEMPLATE_NAME n'existe pas. Exécution du script de création..."
    main  # Exécute le script pour créer le modèle si nécessaire
fi

# Boucle pour créer les conteneurs
for CONTAINER in "${CONTAINERS[@]}"; do
    echo "Création du conteneur $CONTAINER..."

    # Vérifier si le conteneur existe déjà
    if lxc list --format csv -c n | grep -q "^$CONTAINER$"; then
        echo "Le conteneur $CONTAINER existe déjà. Suppression en cours..."
        lxc stop "$CONTAINER" --force
        lxc delete "$CONTAINER" --force
    fi

    # Lancer le conteneur à partir du modèle
    lxc launch "$TEMPLATE_NAME" "$CONTAINER" || error_exit "Échec de la création du conteneur $CONTAINER."

    # Configuration du conteneur
    configure_container --cname "$CONTAINER" --mem 1GB --cpu 2
    lxc exec "$CONTAINER" -- useradd -m admin

    # Copier les fichiers et exécuter l'initialisation
    copy_to_lxc "$CONTAINER" "admin"
    lxc exec "$CONTAINER" -- sh /home/admin/.cutopia/init.zsh

    echo "Conteneur $CONTAINER créé avec succès !"
done

echo "Tous les conteneurs ont été créés avec succès."

