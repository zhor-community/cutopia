#!/bin/zsh

#nginx -g "daemon off;" &

# =============================================================================
#                               Cuboot: Bootstrap
# =============================================================================
# Description:
# This script initializes the Cutopia environment based on different modes:
# VPS, LXC, Xen, QEMU/KVM, Docker, BIOS, and an interactive Zsh shell.
# It includes specific boot functions to manage the boot process for each mode.
#
# Author: AbdElHakim ZOUAÏ <abdelhakimzouai@gmail.com>
# =============================================================================

# Enable error handling
set -euo pipefail
trap "echo 'An error occurred: exiting script.'; exit 1" ERR

# Variables
CALPINEPATH=${CALPINEPATH:-/opt/cutopia}
DEFAULT_MODE="interactive"

# Function to show usage
show_usage() {
    echo "Usage: cuboot.zsh [MODE]"
    echo ""
    echo "Available modes:"
    echo "  interactive   - Launch an interactive Zsh shell (default)."
    echo "  nginx         - Start Nginx in the foreground."
    echo "  vps           - Configure the environment for a VPS."
    echo "  lxc           - Configure the environment for an LXC container."
    echo "  xen           - Configure the environment for Xen hypervisor."
    echo "  qvm           - Configure the environment for QEMU/KVM."
    echo "  docker        - Configure the environment for Docker."
    echo "  bios          - Run BIOS initialization tasks."
    echo "  boot          - Start the complete boot process."
    echo "  help          - Display this help message."
    echo ""
}

# Function to prepare the execution environment
prepare_environment() {
    echo "Preparing the execution environment..."
    mkdir -p /run/openrc
    touch /run/openrc/softlevel
}

# Function to start Nginx
start_nginx() {
    echo "Starting Nginx in the foreground..."
    prepare_environment
    nginx -g "daemon off;"
}

# Function to launch an interactive Zsh shell
launch_shell() {
    echo "Launching an interactive Zsh shell..."
    exec zsh
}

# Function to configure the VPS environment
configure_vps() {
    echo "Configuring the environment for VPS..."
    apk add --no-cache htop net-tools
    echo "VPS environment successfully configured."
}

# Function to configure the LXC environment
configure_lxc() {
    echo "Configuring the environment for LXC..."
    # Add LXC-specific configurations or tools here
    echo "LXC environment successfully configured."
}

# Function to configure the Xen environment
configure_xen() {
    echo "Configuring the environment for Xen..."
    apk add --no-cache xen xen-tools
    echo "Xen environment successfully configured."
}

# Function to configure the QEMU/KVM environment
configure_qvm() {
    echo "Configuring the environment for QEMU/KVM..."
    apk add --no-cache qemu qemu-img libvirt
    echo "QEMU/KVM environment successfully configured."
}

# Function to configure the Docker environment
configure_docker() {
    echo "Configuring the environment for Docker..."
    apk add --no-cache docker docker-cli
    rc-update add docker default
    service docker start
    echo "Docker environment successfully configured."
}

# Function to run BIOS initialization tasks
configure_bios() {
    echo "Running BIOS initialization tasks..."
    # Place any necessary BIOS-related commands here
    echo "BIOS initialization complete."
}

# Function to start the boot process
boot_system() {
    echo "Starting the system boot..."

    # Boot for VPS
    if [[ -f "/etc/vps_config" ]]; then
        configure_vps
        echo "VPS Boot completed."
    fi

    # Boot for LXC
    if [[ -f "/etc/lxc_config" ]]; then
        configure_lxc
        echo "LXC Boot completed."
    fi

    # Boot for Xen
    if [[ -f "/etc/xen_config" ]]; then
        configure_xen
        echo "Xen Boot completed."
    fi

    # Boot for QEMU/KVM
    if [[ -f "/etc/qvm_config" ]]; then
        configure_qvm
        echo "QEMU/KVM Boot completed."
    fi

    # Boot for Docker
    if [[ -f "/etc/docker_config" ]]; then
        configure_docker
        echo "Docker Boot completed."
    fi

    # Boot for BIOS
    if [[ -f "/etc/bios_config" ]]; then
        configure_bios
        echo "BIOS Boot completed."
    fi

    # Default to starting an interactive Zsh shell if no specific mode is found
    echo "Starting default interactive Zsh shell..."
    launch_shell
}

# Main script execution
if [[ $# -eq 0 ]]; then
    MODE=$DEFAULT_MODE
else
    MODE=$1
fi

case $MODE in
    interactive)
        launch_shell
        ;;
    nginx)
        start_nginx
        ;;
    vps)
        configure_vps
        ;;
    lxc)
        configure_lxc
        ;;
    xen)
        configure_xen
        ;;
    qvm)
        configure_qvm
        ;;
    docker)
        configure_docker
        ;;
    bios)
        configure_bios
        ;;
    boot)
        boot_system
        ;;
    help)
        show_usage
        ;;
    *)
        echo "Error: Invalid mode '$MODE'."
        show_usage
        exit 1
        ;;
esac
