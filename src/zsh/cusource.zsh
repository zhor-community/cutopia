
# source envFile
# source envFile.zsh
# source nodeJS.zsh
# source golang.zsh
# source php.zsh
# source python.zsh
# source ssh.zsh
# source cudit.zsh
# source nmap.zsh
# source ranger.zsh
# source sudoers.zsh
# source apache.zsh
# source caddy.zsh
# source nginx.zsh
# source WireGuarde.zsh


# source init.zsh*
# source android.zsh
# source ansible.zsh
# source calpine.sv.zsh
# source calpine.zsh
# source debian.neomutt.sh*
# source docker.zsh
# source dockerCleaner.zsh
# source hubModem.zsh
# source hubUsb.zsh
# source m2call.zsh
# source noip.zsh
# source openvs.zsh
# source pfsense.zsh
# source ProxyPanal.zsh
# source swarm.zsh
# source system.zsh
# source terraform.zsh
# source xalpine.zsh
# source yalpine.zsh
# source yats.zsh
# source zalpine.zsh
# source cuapp.zsh
# source firebase.zsh
# source github.zsh
# source gitlab.zsh
# source sqlite.init.zsh
# source genkeys.git.gpg.zsh
# source genkeys.gpg.zsh.zsh
# source genkeys.zsh
# source genkyes.init.zsh
# source cukyes.zsh
# source cuapk.zsh
# source zshrc.zsh
# source dockershell.zsh
# source sqlite.zsh
#################################################################
#                                                               #
#                      Calpine DockerContainer                  #
#                                                               #
#################################################################

#################################################################
#                                                               #
#                      Calpine LXCContainer                     #
#                                                               #
#################################################################

source $CUTOPIAPATH/src/zsh/cuapk.zsh
source $CUTOPIAPATH/src/zsh/cusudo.zsh
source $CUTOPIAPATH/src/zsh/cugit.zsh
source $CUTOPIAPATH/src/zsh/calpine.zsh
source $CUTOPIAPATH/src/zsh/cucli.zsh
source $CUTOPIAPATH/src/zsh/yats.zsh
source $CUTOPIAPATH/src/zsh/dockerCleaner.zsh
source $CUTOPIAPATH/src/zsh/cualias.zsh
source $CUTOPIAPATH/src/zsh/cummand.zsh
source $CUTOPIAPATH/src/zsh/networks.zsh
source $CUTOPIAPATH/src/zsh/storage.zsh
source $CUTOPIAPATH/src/zsh/cusec.zsh
source $CUTOPIAPATH/src/zsh/ohmyzshPlugins.zsh
