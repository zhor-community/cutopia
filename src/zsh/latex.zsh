#!/bin/zsh
source ~/.zshrc
function addLatex(){
# Function to check if a program is installed
function check_installed {
    if ! command -v $1 &> /dev/null; then
        echo "$1 is not installed. Installing..."
        return 1
    else
        echo "$1 is already installed."
        return 0
    fi
}

# Update package list
echo "Updating package list..."
cuapk update

# Install TeX Live (complete LaTeX distribution)
if ! check_installed "pdflatex"; then
    #echo "Installing TeX Live..."
    #cuapk add texlive-full texlive-lang-french
fi

if ! check_installed "pandoc"; then
    #echo "Installing TeX Live..."
    #cuapk add texlive-full texlive-lang-french
    cuapk add pandoc
fi


if ! check_installed "pdflatex"; then
  mkcd ~/tex
  echo "Installing TeX Live..."
  # Download TeX Live installer
  echo "Downloading TeX Live installer..."
  wget https://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz

  # Extract the installer
  echo "Extracting TeX Live installer..."
  tar -xzf install-tl-unx.tar.gz

  # Find the extracted directory (since it has a dynamic name based on the date)
  INSTALL_DIR=$(find . -type d -name "install-tl-*")

  # Navigate to the extracted directory
  cd $INSTALL_DIR

  # Install TeX Live as root
  echo "Running the TeX Live installer..."
  sudo ./install-tl

  # Optionally, clean up the tarball and installation directory after installation
  cd ..
  rm -f install-tl-unx.tar.gz
  echo "TeX Live installation complete!"
fi

# Install bibliography tools (BibTeX/Biber)
if ! check_installed "biber"; then
    echo "Installing Biber for bibliography management..."
    cuapk add biber
fi

# Install latexmk to automate LaTeX compilation
if ! check_installed "latexmk"; then
    echo "Installing latexmk..."
    cuapk add latexmk
fi

# Install latexmk to automate LaTeX compilation
if ! check_installed "texlive-science"; then
    echo "Installing texlive-science..."
    cuapk add texlive-science
    mkdir -p ~/texmf
    tlmgr init-usertree
    tlmgr update --self
    tlmgr update --all
    tlmgr install siunitx
fi


# Optional LaTeX configuration (e.g., creating a directory for custom LaTeX files)
echo "Setting up LaTeX configuration..."
mkdir -p ~/texmf/tex/latex/local
}

addLatex
