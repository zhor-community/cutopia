# ==========================================
# CUPY - Python Environment Setup for Cutopia
# ==========================================
# Description:
#   A function to set up a Python virtual environment for Cutopia.
#
# Author: AbdElHakim ZOUAÏ
# Email : abdelhakimzouai@gmail.com
# ==========================================

cupy() {
    # Define colors for better readability
    GREEN="\033[0;32m"   # Success
    YELLOW="\033[0;33m"  # Information
    RED="\033[0;31m"     # Error
    RESET="\033[0m"      # Reset color

    # Check if CUTOPIAPATH is defined
    if [[ -z "$CUTOPIAPATH" ]]; then
        echo "${RED}❌ Error: CUTOPIAPATH is not defined. Please set it before running this function.${RESET}"
        return 1
    fi

    # Update the package list
    echo "${YELLOW}ℹ️ Updating package list...${RESET}"
    cuapk update || { echo "${RED}❌ Failed to update package list.${RESET}"; return 1; }

    # Install required Python packages
    echo "${YELLOW}ℹ️ Installing Python dependencies (python3, py3-pip, py3-pillow, py3-numpy)...${RESET}"
    #cuapk add python3 py3-pip py3-pillow py3-numpy || { echo "${RED}❌ Failed to install Python dependencies.${RESET}"; return 1; }
    cuapk add python3 python3-pip python3-pil python3-numpy || { echo "${RED}❌ Failed to install Python dependencies.${RESET}"; return 1; }

    # Create a Python virtual environment in $CUTOPIAPATH/cupy
    VENV_PATH="$CUTOPIAPATH/cupy"
    if [[ -d "$VENV_PATH" ]]; then
        echo "${YELLOW}⚠️ Virtual environment already exists at '$VENV_PATH'. Skipping creation.${RESET}"
    else
        echo "${YELLOW}ℹ️ Creating Python virtual environment at '$VENV_PATH'...${RESET}"
        python3 -m venv "$VENV_PATH" || { echo "${RED}❌ Failed to create Python virtual environment.${RESET}"; return 1; }
    fi

    # Activate the virtual environment
    echo "${YELLOW}ℹ️ Activating Python virtual environment...${RESET}"
    source "$VENV_PATH/bin/activate" || { echo "${RED}❌ Failed to activate Python virtual environment.${RESET}"; return 1; }

    # Confirmation message
    echo "${GREEN}✅ Python environment for Cutopia has been successfully set up and activated.${RESET}"
}
