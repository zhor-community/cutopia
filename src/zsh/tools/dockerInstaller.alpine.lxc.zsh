function install_docker() {
    local log_file="/var/log/install_docker.log"
    local service_manager=""

    # Function to log errors
    log_error() {
        echo "[$(date)] ERROR: $1" | tee -a "$log_file"
        return 1
    }

    # Function to determine the service manager
    determine_service_manager() {
        if command -v rc-update > /dev/null; then
            service_manager="rc-update"
        elif command -v service > /dev/null; then
            service_manager="service"
        else
            log_error "No supported service manager found. Please install rc-update or service."
            exit 1
        fi
    }

    determine_service_manager

    echo "[$(date)] Starting Docker installation..." | tee -a "$log_file"

    # Update package list
    echo "[$(date)] Updating package list..." | tee -a "$log_file"
    sudo apk update || log_error "Failed to update package list"

    # Install Docker
    echo "[$(date)] Installing Docker..." | tee -a "$log_file"
    sudo apk add docker || log_error "Failed to install Docker"

    # Start and enable Docker service
    echo "[$(date)] Starting and enabling Docker service..." | tee -a "$log_file"
    if [ "$service_manager" = "rc-update" ]; then
        sudo rc-update add docker boot || log_error "Failed to add Docker to boot"
        sudo service docker start || log_error "Failed to start Docker service"
    elif [ "$service_manager" = "service" ]; then
        sudo service docker start || log_error "Failed to start Docker service"
        sudo rc-update add docker boot || log_error "Failed to add Docker to boot"
    fi

    # Verify Docker installation
    echo "[$(date)] Verifying Docker installation..." | tee -a "$log_file"
    if ! sudo docker --version > /dev/null; then
        log_error "Docker version check failed"
    fi
    if ! sudo docker run hello-world > /dev/null; then
        log_error "Docker hello-world test failed"
    fi

    echo "[$(date)] Docker installation completed successfully." | tee -a "$log_file"
}
