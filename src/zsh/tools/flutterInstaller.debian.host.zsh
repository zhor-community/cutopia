cuapk update -y && sudo apt-get upgrade -y;
cuapk add -y curl git unzip xz-utils zip libglu1-mesa
cuapk add libc6:amd64 libstdc++6:amd64 lib32z1 libbz2-1.0:amd64
#echo 'export path="~/development/flutter/bin:$path"' >> ~/.zshenv

cuapk add cpu-checker
egrep -c '(vmx|svm)' /proc/cpuinfo
cuapk add libc6:i386 libncurses5:i386 libstdc++6:i386 lib32z1 libbz2-1.0:i386
sudo kvm-ok
cuapk add qemu-kvm libvirt-daemon-system libvirt-clients bridge-utils

sdkmanager --install "cmdline-tools;latest"
flutter doctor --android-licenses
export CHROME_EXECUTABLE=/path/to/chrome
sudo apt install cmake ninja-build libgtk-3-dev
echo 'export CHROME_EXECUTABLE=/usr/bin/brave-browser' >> ~/.zshrc
source ~/.zshrc


