#!/bin/zsh

# ==========================================
# cushell - Command Line Shell Manager for Cutopia
# ==========================================
# Description:
#   A modular command-line tool for managing user shells efficiently.
#
# Author: AbdElHakim ZOUAÏ
# Email : abdelhakimzouai@gmail.com
# ==========================================

cushell() {
    local OPTIND opt force=false target_shell="zsh" userName="$USER"

    while getopts ":hfs:u:" opt; do
        case $opt in
            h)
                echo "Usage: cushell [OPTIONS]"
                echo "  -h      Show this help message"
                echo "  -f      Force the shell change without confirmation"
                echo "  -s SHELL  Specify a target shell (default: zsh)"
                echo "  -u USER   Change the shell for a specific user (requires sudo)"
                return 0
                ;;
            f)
                force=true
                ;;
            s)
                target_shell=$OPTARG
                ;;
            u)
                userName=$OPTARG
                ;;
            \?)
                echo "Invalid option: -$OPTARG" >&2
                return 1
                ;;
        esac
    done

    if ! command -v "$target_shell" >/dev/null 2>&1; then
        echo "Error: The shell '$target_shell' is not installed." >&2
        return 1
    fi

    if ! grep -q "^$(command -v "$target_shell")$" /etc/shells; then
        echo "Error: The shell '$target_shell' is not a valid shell (/etc/shells)." >&2
        return 1
    fi

    if [ "$force" = false ]; then
        read -p "Are you sure you want to change the shell for '$userName' to '$target_shell'? (y/N) " confirm
        if [[ "$confirm" =~ ^[Nn] ]]; then
            echo "Operation canceled."
            return 0
        fi
    fi

    if [ "$userName" = "$USER" ]; then
        if ! chsh -s "$(command -v "$target_shell")"; then
            echo "Error: Failed to change shell for '$userName'." >&2
            return 1
        fi
    else
        if ! sudo chsh -s "$(command -v "$target_shell")" "$userName"; then
            echo "Error: Failed to change shell for user '$userName'." >&2
            return 1
        fi
    fi

    case "$target_shell" in
        zsh)
            ln -sf "$CUTOPIAPATH/src/zsh/zshrc" "/home/$userName/.zshrc"
            [ "$userName" = "$USER" ] || sudo chown -R "$userName:$userName" "/home/$userName/.zshrc"
            ;;
        bash)
            ln -sf "$CUTOPIAPATH/src/bash/bashrc" "/home/$userName/.bashrc"
            [ "$userName" = "$USER" ] || sudo chown -R "$userName:$userName" "/home/$userName/.bashrc"
            ;;
        fish)
            mkdir -p "/home/$userName/.config/fish"
            ln -sf "$CUTOPIAPATH/src/fish/config.fish" "/home/$userName/.config/fish/config.fish"
            [ "$userName" = "$USER" ] || sudo chown -R "$userName:$userName" "/home/$userName/.config/fish"
            ;;
        *)
            echo "No specific configuration found for '$target_shell'."
            ;;
    esac
}

if [[ "$0" == "$ZSH_NAME" ]]; then
    cushell "$@"
fi

