#!/bin/bash

# Vérifier si Python est installé
if ! command -v python3 &> /dev/null
then
    echo "Python n'est pas installé. Veuillez installer Python et réessayer."
    sudo apk add python 
    exit
fi

# Vérifier si pip est installé
if ! command -v pip3 &> /dev/null
then
#    echo "pip n'est pas installé. Veuillez installer pip et réessayer."
    apk add py3-pip
    exit
fi

# Demander le nom du projet
# read -p "Entrez le nom de votre projet Django: " PROJECT_NAME
PROJECT_NAME="zerozone"
mkdir django
cd django
# Créer un environnement virtuel
python3 -m venv env

# Activer l'environnement virtuel
source env/bin/activate

# Mettre à jour pip
pip install --upgrade pip

# Installer Django
pip install django

# Créer un nouveau projet Django
django-admin startproject $PROJECT_NAME

# Aller dans le répertoire du projet
cd $PROJECT_NAME

# Mettre à jour settings.py pour le mode debug et les hôtes autorisés
sed -i "s/DEBUG = False/DEBUG = True/" $PROJECT_NAME/settings.py
sed -i "s/ALLOWED_HOSTS = \[\]/ALLOWED_HOSTS = ['192.168.0.3', 'calpine', 'localhost', '127.0.0.1']/" $PROJECT_NAME/settings.py

# Appliquer les migrations initiales
python3 manage.py migrate

# Créer un super utilisateur
echo "Création d'un super utilisateur pour le projet Django"
python3 manage.py createsuperuser

# Lancer le serveur de développement
echo "Lancement du serveur de développement sur 0.0.0.0:8000..."
python3 manage.py runserver 0.0.0.0:8000

