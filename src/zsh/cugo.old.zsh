#!/bin/zsh

# ======================================================
# Golang Installation and Configuration Script (zsh)
# ======================================================
# This script automates the installation, configuration,
# and validation of Golang on a Linux system.
# Author: AbdElHakim ZOUAÏ
# Email : abdelhakimzouai@gmail.com
# ======================================================

source $CALPINEPATH/src/env/cugo.env
# Function: Check if a command exists
function command_exists() {
    command -v "$1" &> /dev/null
}

# Function: Print messages with color
function print_message() {
    local type="$1"
    local message="$2"
    case "$type" in
        info)    echo -e "\033[34mℹ️  [INFO]\033[0m $message" ;;
        success) echo -e "\033[32m✔️  [SUCCESS]\033[0m $message" ;;
        error)   echo -e "\033[31m❌ [ERROR]\033[0m $message" ;;
        *)       echo "$message" ;;
    esac
}

# Ensure script is run as root
if [[ $EUID -ne 0 ]]; then
    print_message "error" "This script must be run as root."
    exit 1
fi

# Step 1: Update the system and install dependencies
print_message "info" "Updating package repositories..."
sudo apt-get update -y && sudo apt-get upgrade -y

print_message "info" "Installing required dependencies..."
sudo apt-get install -y wget tar || {
    print_message "error" "Failed to install dependencies."
    exit 1
}

# Step 2: Download the Golang archive
print_message "info" "Downloading Go v$GO_VERSION..."
wget -q "https://golang.org/dl/go${GO_VERSION}.linux-amd64.tar.gz" -O "$TEMP_ARCHIVE" || {
    print_message "error" "Failed to download Go archive."
    exit 1
}

# Step 3: Extract and install Golang
print_message "info" "Extracting Go archive..."
sudo tar -C "$INSTALL_DIR" -xzf "$TEMP_ARCHIVE" || {
    print_message "error" "Failed to extract Go archive."
    exit 1
}

# Clean up temporary archive
rm -f "$TEMP_ARCHIVE"
print_message "success" "Golang installed in $INSTALL_DIR/go."

# Step 4: Configure environment variables
if ! grep -q "export PATH=.*go/bin" "$PROFILE_FILE"; then
    print_message "info" "Configuring environment variables in $PROFILE_FILE..."
    {
        echo ""
        echo "# Golang Configuration"
        echo "export PATH=\$PATH:/usr/local/go/bin"
        echo "export GOPATH=$WORKSPACE_DIR"
        echo "export GOBIN=\$GOPATH/bin"
        echo "export PATH=\$PATH:\$GOBIN"
    } >> "$PROFILE_FILE"
else
    print_message "info" "Golang environment variables are already configured."
fi

# Reload zsh profile
source "$PROFILE_FILE"

# Step 5: Verify installation
if command_exists go; then
    print_message "success" "Golang installed successfully! Version: $(go version)"
else
    print_message "error" "Golang installation failed."
    exit 1
fi

# Step 6: Create Go workspace directories
print_message "info" "Creating Go workspace directories at $WORKSPACE_DIR..."
mkdir -p "$WORKSPACE_DIR"/{src,bin,pkg} || {
    print_message "error" "Failed to create workspace directories."
    exit 1
}
print_message "success" "Workspace setup complete: $WORKSPACE_DIR."

# Step 7: Final message
print_message "success" "Golang is installed and ready to use. Happy coding!"
