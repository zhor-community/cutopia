# Professional and Standardized Environment Setup Script

# Load cuapk and cucli tools
source /opt/cutopia/src/zsh/cuapk.zsh
source /opt/cutopia/src/zsh/cucli.zsh

# System Dependencies managed by cuapk
declare -a SYSTEM_DEPENDENCIES=(
    "sudo" "jq" "yq" "curl" "git" "htop" "tmux" "vim" "emacs" "nano"
    "strace" "lsof" "tcpdump" "nmap" "wireshark"
)

# Supported Operating Systems
declare -a SUPPORTED_OS=(
    "ubuntu" "debian" "arch" "fedora" "alpine" "opensuse" "proxmox" 
    "openwrt" "pfsense" "android" "windows" "macos"
)

# Modules managed by cucli
declare -a LANGUAGE_MODULES=(
    "python3" "nodejs" "golang" "rust" "lua" "dart" "php" "ruby"
)
declare -a FRAMEWORK_MODULES=(
    "django" "qussar" "express" "react" "vue" "flutter" "angular" "spring"
)
declare -a DATABASE_MODULES=(
    "couchdb" "postgresql" "mariadb" "redis" "sqlite" "cassandra"
)
declare -a BLOCKCHAIN_MODULES=(
    "geth" "solana" "hyperledger" "bitcoin-core" "polkadot" "binance"
)
declare -a AI_MODULES=(
    "tensorflow" "pytorch" "scikit-learn" "opencv" "huggingface_hub"
)
declare -a IOT_MODULES=(
    "mosquitto" "nodered" "iotedge" "home-assistant" "openhab" "zephyr"
)
# Nouveaux modules serveurs web
declare -a NGINX_MODULES=(
    "nginx" "nginx-core" "nginx-extras" "nginx-dev" "rtmp-module" 
    "headers-more-module" "nginx-auth-ldap"
)

declare -a APACHE_MODULES=(
    "apache2" "httpd" "mod_ssl" "mod_php" "mod_wsgi" "mod_security" 
    "mod_evasive" "mod_pagespeed" "mod_cloudflare"
)

declare -a CADDY_MODULES=(
    "caddy" "caddy2" "caddy-http3" "caddy-authz" "caddy-cache" 
    "caddy-upload" "caddy-prometheus"
)
declare -a VIRTUALIZATION_MODULES=(
    # Hyperviseurs traditionnels
    "virtualbox" "qemu" "kvm" "xen" "vmwgfx" "hyperv" "prl_*" "vboxdrv"
    
    # Conteneurs
    "lxc" "docker" "podman" "openvz" "containerd" "rkt" "systemd-nspawn" "apptainer"
    
    # Technologies cloud
    "firecracker" "gvisor" "kata-containers" "cri-o"
    
    # Virtualisation légère
    "bhyve" "proxmox" "virtuozzo" "vyatta"
    
    # Modules noyau associés
    "vhost_net" "vhost_scsi" "vhost_vsock" "virtio" "virtio_balloon" "virtio_blk" 
    "virtio_console" "virtio_net" "virtio_pci" "virtio_ring" "virtio_scsi"
    
    # Technologies Microsoft
    "hv_balloon" "hv_netvsc" "hv_storvsc" "hv_utils" "hv_vmbus"
    
    # Autres
    "nvidia_nvlink" "zfs" "ksm" "ksmd" "vboxnetflt"
)

declare -a APP_MODULES=(
    "nextcloud" "dolibarr" "moodle" "opencart" "wordpress" "prestashop"
    "drupal" "joomla" "magento" "suitecrm" "erpnext" "redmine" "phpmyadmin" "pgAdmin4"
)
declare -a TOOLS_MODULES=(
    "ansible" "terraform" "packer" "vagrant" "helm" "kubectl" 
    "kustomize"  "strace" "netdata"
)
declare -a SECURITY_MODULES=(
    "ufw" "fail2ban" "suricata" "knockd" "clamav" "clamav-daemon"
    "rkhunter" "chkrootkit" "unattended-upgrades" "acl" "rsync"
    "logwatch" "monit" "libapache2-mod-security2"
)

# Function to install system dependencies using cuapk
install_system_dependencies() {
    echo "Installing system dependencies..."
    for package in "${SYSTEM_DEPENDENCIES[@]}"; do
        cuapk add "$package"
    done
    echo "System dependencies installation complete."
}

# Function to install modules using cucli
install_modules_with_cucli() {
    local module_category="$1"
    shift
    local modules=("$@")
    
    echo "Installing $module_category modules..."
    for module in "${modules[@]}"; do
        cucli install "$module"
    done
    echo "$module_category modules installation complete."
}

# Execute all installations separately
install_system_dependencies
install_modules_with_cucli "VIRT" "${VIRTUALIZATION_MODULES[@]}"
install_modules_with_cucli "Language" "${LANGUAGE_MODULES[@]}"
install_modules_with_cucli "Framework" "${FRAMEWORK_MODULES[@]}"
install_modules_with_cucli "Database" "${DATABASE_MODULES[@]}"
install_modules_with_cucli "Blockchain" "${BLOCKCHAIN_MODULES[@]}"
install_modules_with_cucli "AI" "${AI_MODULES[@]}"
install_modules_with_cucli "IoT" "${IOT_MODULES[@]}"
install_modules_with_cucli "NginxM" "${NGINX_MODULES[@]}"
install_modules_with_cucli "ApachM" "${APACHE_MODULES[@]}"
install_modules_with_cucli "CaddyM" "${CADDY_MODULES[@]}"
install_modules_with_cucli "Security" "${SECURITY_MODULES[@]}"

# Nouveaux modules serveurs web
# End of script

