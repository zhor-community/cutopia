#!/bin/zsh

##############################################################################
#                        Environment Configuration                           #
##############################################################################

# Define the main Cutopia path
export CUTOPIAPATH="/opt/cutopia"
export userName="xtal"

# Check if the main directory exists
if [[ ! -d "$CUTOPIAPATH" ]]; then
    echo "Error: The directory $CUTOPIAPATH does not exist. Please check the installation." >&2
    return 1
fi

# Load environment variables if they exist
for env_file in host vps docker lxd xen kvm; do
    env_path="$CUTOPIAPATH/sec/env/$env_file"
    [[ -f "$env_path" ]] && source "$env_path"
done

# Load custom aliases and functions
source "$CUTOPIAPATH/src/zsh/cusource.zsh"

# Configure paths
export PATH="$CUTOPIAPATH/bin:$PATH"
export MANPATH="$CUTOPIAPATH/man:$MANPATH"

# Enable Zsh auto-completion
autoload -Uz compinit && compinit

# Useful aliases
alias ll='ls -lah'
alias cu='cd $CUTOPIAPATH'
alias cudo='cd $CUTOPIAPATH/docker'

##############################################################################
#                        User Configuration                                  #
##############################################################################

# Verify and create the user using `cusudo`
if [[ -z "$userName" ]]; then
    echo "Error: The variable 'userName' is not defined." >&2
    return 1
fi

# Set a default password if not specified
userPassword="${userPassword:-DefaultPass123}"

# Ensure the script is run with root privileges
if [[ $EUID -ne 0 ]]; then
    echo "Error: This script must be run as root." >&2
    return 1
fi

# Create the user with passwordless sudo
if ! id "$userName" &>/dev/null; then
    echo "Creating user '$userName'..."
    cusudo -u "$userName" -p "$userPassword" -y
    echo "User '$userName' successfully created."
else
    echo "User '$userName' already exists."
fi

# Create and add user to the `cutopia` group
if ! getent group cutopia &>/dev/null; then
    groupadd cutopia
    echo "Group 'cutopia' created."
fi

usermod -aG cutopia "$userName"

# Securely set permissions for `/opt/cutopia`
chown -R "$userName":cutopia "$CUTOPIAPATH"
chmod -R 770 "$CUTOPIAPATH"

echo "User '$userName' has been added to the 'cutopia' group and granted access to '$CUTOPIAPATH'."

##############################################################################
#                        Welcome Message                                     #
##############################################################################

echo "✅ Configuration complete! Welcome to the Cutopia environment, $userName."

