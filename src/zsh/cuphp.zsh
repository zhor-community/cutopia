#!/bin/zsh

# Couleurs pour les messages
RED="\033[0;31m"
GREEN="\033[0;32m"
BLUE="\033[0;34m"
RESET="\033[0m"

echo "${BLUE}=== Initialisation complète de PHP dans le conteneur Docker ===${RESET}"

# Mise à jour des dépôts et installation des outils nécessaires
echo "${GREEN}→ Mise à jour des dépôts et installation des outils de base...${RESET}"
apk update && apk add --no-cache \
    bash \
    curl \
    git \
    libpng-dev \
    libjpeg-turbo-dev \
    libwebp-dev \
    libfreetype6-dev \
    libxml2-dev \
    libzip-dev \
    oniguruma-dev \
    zlib-dev \
    openrc \
    shadow \
    supervisor

apk add --no-cache freetype-dev libpng-dev libjpeg-turbo-dev
# Installation de PHP et des extensions principales
echo "${GREEN}→ Installation de PHP et des extensions nécessaires...${RESET}"
apk add --no-cache php php-fpm php-cli php-mysqli php-pdo php-pdo_mysql php-gd php-opcache php-curl php-mbstring php-xml php-zip php-intl php-soap php-bcmath php-tokenizer php-session

# Ajout des extensions PHP via docker-php-ext-install
echo "${GREEN}→ Ajout et compilation des extensions PHP via docker-php-ext-install...${RESET}"

# Compilation des extensions GD, PDO, et ZIP
docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp
docker-php-ext-install gd mysqli pdo pdo_mysql zip bcmath soap intl

# Nettoyage des fichiers temporaires
echo "${GREEN}→ Nettoyage des fichiers temporaires...${RESET}"
apk del libpng-dev libjpeg-turbo-dev libwebp-dev libfreetype6-dev libxml2-dev libzip-dev oniguruma-dev zlib-dev

# Création des répertoires nécessaires
echo "${GREEN}→ Création des répertoires et configuration des permissions...${RESET}"
mkdir -p /var/www/html /run/php /var/log/supervisor
chown -R www-data:www-data /var/www/html
chmod -R 755 /var/www/html

# Exemple de fichier PHP pour test
echo "${GREEN}→ Création d'un fichier de test PHP...${RESET}"
cat > /var/www/cutopia.org/info.php <<EOF
<?php
echo "Bienvenue sur votre serveur PHP !";
phpinfo();
?>
EOF

# Configuration de PHP-FPM
echo "${GREEN}→ Configuration de PHP-FPM...${RESET}"
PHP_FPM_CONF="/etc/php8/php-fpm.conf"
if [ -f $PHP_FPM_CONF ]; then
  sed -i 's/;daemonize = yes/daemonize = no/' $PHP_FPM_CONF
else
  echo "${RED}× Fichier php-fpm.conf introuvable, vérifiez l'installation.${RESET}"

cat > $PHP_FPM_CONF <<EOF
[global]
pid = /run/php/php-fpm.pid
error_log = /var/log/php-fpm.log

[www]
listen = 127.0.0.1:80
user = www-data
group = www-data
pm = dynamic
pm.max_children = 10
pm.start_servers = 2
pm.min_spare_servers = 2
pm.max_spare_servers = 4
EOF
fi

# Installation et configuration de NGINX
#echo "${GREEN}→ Installation et configuration de NGINX...${RESET}"
#apk add --no-cache nginx
#cat > /etc/nginx/conf.d/default.conf <<EOF
#server {
#    listen 80;
#    server_name localhost;
#    root /var/www/html;
#    index index.php index.html;
#
#    location ~ \.php\$ {
#        include fastcgi_params;
#        fastcgi_pass 127.0.0.1:9000;
#        fastcgi_index index.php;
#        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
#    }
#
#    location / {
#        try_files \$uri \$uri/ =404;
#    }
#}
#EOF

# Configuration de Supervisor pour gérer les processus
echo "${GREEN}→ Configuration de Supervisor pour PHP-FPM et NGINX...${RESET}"
cat > /etc/supervisor/conf.d/supervisord.conf <<EOF
[supervisord]
nodaemon=true

[program:php-fpm]
command=/usr/sbin/php-fpm8 --nodaemonize
autostart=true
autorestart=true

[program:nginx]
command=/usr/sbin/nginx -g "daemon off;"
autostart=true
autorestart=true
EOF

# Démarrage des services via Supervisor
echo "${GREEN}→ Démarrage des services avec Supervisor...${RESET}"
supervisord -c /etc/supervisor/conf.d/supervisord.conf

# Message de succès
echo "${BLUE}=== Installation et configuration terminées avec succès ! ===${RESET}"

exit 0
