# Professional and Standardized Environment Setup Script

# Load cuapk and cucli tools
source /opt/cutopia/src/zsh/cuapk.zsh
source /opt/cutopia/src/zsh/cucli.zsh

# System Dependencies managed by cuapk
declare -a SYSTEM_DEPENDENCIES=(
    #"qemu" "kvm" "xen" "lxc" "docker" "podman" "openvz"
    "lxc"
)

# Supported Operating Systems
declare -a SUPPORTED_OS=(
    "ubuntu" "debian" "arch" "fedora" "alpine" "opensuse" "proxmox" 
    "openwrt" "pfsense" "android" "windows" "macos"
)

# Modules managed by cucli
declare -a LANGUAGE_MODULES=(
    "python3" "nodejs" "golang" "rust" "lua" "dart" "php" "ruby"
)
declare -a FRAMEWORK_MODULES=(
    "django" "qussar" "express" "react" "vue" "flutter" "angular" "spring"
)
declare -a DATABASE_MODULES=(
    "couchdb" "postgresql" "mariadb" "redis" "sqlite" "cassandra"
)
declare -a BLOCKCHAIN_MODULES=(
    "geth" "solana" "hyperledger" "bitcoin-core" "polkadot" "binance"
)
declare -a AI_MODULES=(
    "tensorflow" "pytorch" "scikit-learn" "opencv" "huggingface_hub"
)
declare -a IOT_MODULES=(
    "mosquitto" "nodered" "iotedge" "home-assistant" "openhab" "zephyr"
)

# Function to install system dependencies using cuapk
install_system_dependencies() {
    echo "Installing system dependencies..."
    for package in "${SYSTEM_DEPENDENCIES[@]}"; do
        cuapk add "$package"
    done
    echo "System dependencies installation complete."
}

# Function to install modules using cucli
install_modules_with_cucli() {
    local module_category="$1"
    shift
    local modules=("$@")
    
    echo "Installing $module_category modules..."
    for module in "${modules[@]}"; do
        cucli install "$module"
    done
    echo "$module_category modules installation complete."
}

# Execute all installations separately
install_system_dependencies
install_modules_with_cucli "Language" "${LANGUAGE_MODULES[@]}"
install_modules_with_cucli "Framework" "${FRAMEWORK_MODULES[@]}"
install_modules_with_cucli "Database" "${DATABASE_MODULES[@]}"
install_modules_with_cucli "Blockchain" "${BLOCKCHAIN_MODULES[@]}"
install_modules_with_cucli "AI" "${AI_MODULES[@]}"
install_modules_with_cucli "IoT" "${IOT_MODULES[@]}"

# End of script

