#!/bin/zsh

CUTOPIAPATH=/opt/cutopia
#source /opt/cutopia/zsh/
source $CUTOPIAPATH/src/zsh/cusource.zsh
#source $CALPINEPATH/src/zsh/cusource.zsh
# Définition des couleurs pour l'affichage
GREEN="\033[1;32m"
RED="\033[1;31m"
YELLOW="\033[1;33m"
BLUE="\033[1;34m"
RESET="\033[0m"

# Charger le script contenant les commandes LXD personnalisées
CRUD_SCRIPT="$CUTOPIAPATH/src/zsh/lxd.crud.zsh"
if [[ ! -f "$CRUD_SCRIPT" ]]; then
    echo -e "${RED}❌ Erreur : Le fichier '$CRUD_SCRIPT' est introuvable.${RESET}"
    exit 1
fi
source "$CRUD_SCRIPT"

# Liste des conteneurs par défaut
DEFAULT_CONTAINERS=("cutopia" "ticenergy" "tassili" "zerozone" "zhor" "octomatics")

# Vérifier si LXD est installé
check_lxd() {
    if ! command -v lxd >/dev/null 2>&1; then
        echo -e "${RED}❌ LXD n'est pas installé. Exécutez './lxd.crud.zsh -i' pour l'installer.${RESET}"
        exit 1
    fi
}

# Vérifier si LXD est initialisé
check_lxd_initialized() {
    if ! lxc info >/dev/null 2>&1; then
        echo -e "${YELLOW}⚠️ LXD n'est pas initialisé. Initialisation en cours...${RESET}"
        initialize_lxd
    fi
}

# Fonction pour créer un conteneur proprement
create_container() {
    local CONTAINER_NAME="$1"

    echo -e "${BLUE}🚀 Création du conteneur '$CONTAINER_NAME'...${RESET}"

    # Vérifier si le conteneur existe déjà
    if lxc list --format csv -c n | grep -q "^$CONTAINER_NAME$"; then
        echo -e "${YELLOW}🔄 Le conteneur '$CONTAINER_NAME' existe déjà. Suppression...${RESET}"
        stop_container "$CONTAINER_NAME"
        delete_container "$CONTAINER_NAME"
    fi

    # Lancer un nouveau conteneur avec l'image Ubuntu 22.04
    lxc launch images:ubuntu/22.04 "$CONTAINER_NAME"

    # Ajouter un utilisateur admin avec un mot de passe sécurisé
    echo -e "${YELLOW}👤 Ajout de l'utilisateur 'admin'...${RESET}"
    lxc exec "$CONTAINER_NAME" -- useradd -m admin
    lxc exec "$CONTAINER_NAME" -- sh -c "echo 'admin:password' | chpasswd"

    echo -e "${GREEN}✅ Conteneur '$CONTAINER_NAME' créé avec succès !${RESET}"
}

# Fonction pour créer plusieurs conteneurs
create_multiple_containers() {
    local containers=("$@")
    for CONTAINER in "${containers[@]}"; do
        create_container "$CONTAINER"
    done
    echo -e "${GREEN}🎉 Tous les conteneurs ont été créés avec succès.${RESET}"
}

# Affichage de l'aide
display_help() {
    echo -e "${GREEN}Usage : lxd.create.zsh [options]${RESET}"
    echo "Options :"
    echo "  -c \"cont1 cont2 ...\"   Créer des conteneurs spécifiques"
    echo "  -d                     Utiliser la liste de conteneurs par défaut"
    echo "  -h                     Afficher cette aide"
    exit 0
}

# Vérifier et traiter les arguments passés en ligne de commande
USE_DEFAULT_CONTAINERS=false
CUSTOM_CONTAINERS=()

while getopts "c:dh" opt; do
    case "$opt" in
        c)
            CUSTOM_CONTAINERS=(${(s: :)OPTARG})  # Split argument into array
            ;;
        d)
            USE_DEFAULT_CONTAINERS=true
            ;;
        h)
            display_help
            ;;
        *)
            display_help
            ;;
    esac
done

# Vérifications avant la création des conteneurs
check_lxd
check_lxd_initialized

# Lancer la création des conteneurs en fonction des options choisies
if [[ ${#CUSTOM_CONTAINERS[@]} -gt 0 ]]; then
    create_multiple_containers "${CUSTOM_CONTAINERS[@]}"
elif [[ "$USE_DEFAULT_CONTAINERS" == true ]]; then
    create_multiple_containers "${DEFAULT_CONTAINERS[@]}"
else
    echo -e "${RED}❌ Aucun conteneur spécifié. Utilisez -c ou -d.${RESET}"
    display_help
fi

