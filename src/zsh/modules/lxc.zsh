#!/bin/zsh
source ~/.zshrc
# Mise à jour des paquets disponibles
cuapk update
# Installation de LXD depuis les sources par défaut
cuapk add lxd lxd-client
cuapk add lxd-installer
# Ajout de l'utilisateur actuel au groupe LXD
sudo usermod -aG lxd $USER
# Initialisation de LXD
sudo ip link add name lxdbr0 type bridge
sudo ip link set lxdbr0 up 

lxd init
