#!/usr/bin/env sh
# Module SSH - CIS Benchmark 5.2.1 compliant

module::ssh_setup() {
  # Configuration SSH minimaliste (sshd_config)
  config::template "sshd_config" > "/etc/ssh/sshd_config"

  # Configuration des clés
  for user in "${!SYS_USERS[@]}"; do
    mkdir -p "/home/${user}/.ssh"
    chmod 700 "/home/${user}/.ssh"
    cp "${CONFIG_DIR}/templates/authorized_keys" "/home/${user}/.ssh/"
    chown -R "${user}:${user}" "/home/${user}/.ssh"
  done

  # Application
  service sshd restart
  sysctl -w net.ipv4.tcp_keepalive_time=300
}

module::ssh_validate() {
  sshd -t || return 1
  [ "$(sshd -T | grep 'permitrootlogin')" = "permitrootlogin no" ] || return 2
}
