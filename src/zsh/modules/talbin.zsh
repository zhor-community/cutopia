#!/bin/zsh

# Default values
myPath="."
myGroup="cutopia"
myContainer="talbin"
myTemplate="alpine"  # Template for LXC container creation
myOperation="start"

# Function to manage LXC containers with CRUD operations
talbin() {
    # Define colors
    GREEN="\033[1;32m"
    RED="\033[1;31m"
    YELLOW="\033[1;33m"
    RESET="\033[0m"
    ICON_SUCCESS="✅ "
    ICON_ERROR="❌ "

    # Check if LXC is installed
    if ! command -v lxc &>/dev/null; then
        echo "LXC is not installed. Please install LXC before continuing."
        exit 1
    fi

    while getopts ":p:g:t:o:c:" opt; do
        case $opt in
            p) myPath=$OPTARG ;;
            g) myGroup=$OPTARG ;;
            t) myTemplate=$OPTARG ;;
            o) myOperation=$OPTARG ;;
            c) myContainer=$OPTARG ;;
            \?)
                echo "Invalid option: -$OPTARG" >&2
                exit 1
                ;;
            :)
                echo "Option -$OPTARG requires an argument." >&2
                exit 1
                ;;
        esac
    done

    case $myOperation in
        "start")
            echo "Starting LXC container: $myContainer"
            if lxc-info -n "$myContainer" &>/dev/null; then
                if lxc-info -n "$myContainer" | grep -q "RUNNING"; then
                    echo -e "${ICON_ERROR}${RED} Container $myContainer is already running.${RESET}"
                else
                    lxc-start -n "$myContainer" -d && echo -e "${ICON_SUCCESS}${GREEN} Container $myContainer started.${RESET}"
                fi
            else
                echo -e "${ICON_ERROR}${RED} Container $myContainer does not exist.${RESET}"
            fi
            ;;
        "create")
            echo "Creating LXC container: $myContainer from template: $myTemplate"
            if lxc-info -n "$myContainer" &>/dev/null; then
                echo -e "${ICON_ERROR}${RED} Container $myContainer already exists.${RESET}"
            else
                lxc-create -t "$myTemplate" -n "$myContainer" -- -p "$myPath" && echo -e "${ICON_SUCCESS}${GREEN} Container $myContainer created.${RESET}"
            fi
            ;;
        "read")
            echo "Reading LXC container status: $myContainer"
            if lxc-info -n "$myContainer" &>/dev/null; then
                echo -e "${ICON_SUCCESS}${GREEN} Container $myContainer exists.${RESET}"
                lxc-info -n "$myContainer"
            else
                echo -e "${ICON_ERROR}${RED} Container $myContainer does not exist.${RESET}"
            fi
            ;;
        "update")
            echo "Updating LXC container: $myContainer"
            if lxc-info -n "$myContainer" &>/dev/null; then
                lxc-stop -n "$myContainer" && lxc-destroy -n "$myContainer" && lxc-create -t "$myTemplate" -n "$myContainer" -- -p "$myPath" && echo -e "${ICON_SUCCESS}${GREEN} Container $myContainer updated.${RESET}"
            else
                echo -e "${ICON_ERROR}${RED} Container $myContainer does not exist.${RESET}"
            fi
            ;;
        "upgrade")
            echo "Upgrading LXC container: $myContainer"
            if lxc-info -n "$myContainer" &>/dev/null; then
                lxc-stop -n "$myContainer" && lxc-destroy -n "$myContainer" && lxc-create -t "$myTemplate" -n "$myContainer" -- -p "$myPath" && echo -e "${ICON_SUCCESS}${GREEN} Container $myContainer upgraded.${RESET}"
            else
                echo -e "${ICON_ERROR}${RED} Container $myContainer does not exist.${RESET}"
            fi
            ;;
        "delete")
            echo "Deleting LXC container: $myContainer"
            if lxc-info -n "$myContainer" &>/dev/null; then
                lxc-stop -n "$myContainer" && lxc-destroy -n "$myContainer" && echo -e "${ICON_SUCCESS}${GREEN} Container $myContainer deleted.${RESET}"
            else
                echo -e "${ICON_ERROR}${RED} Container $myContainer does not exist.${RESET}"
            fi
            ;;
        *)
            echo "Invalid operation: $myOperation. Supported operations: create, read, update, upgrade, delete." >&2
            exit 1
            ;;
    esac
}
