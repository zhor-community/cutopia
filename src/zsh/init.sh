#!/bin/sh

# Environment Setup
source $CALPINEPATH/src/env/calpine
source $CALPINEPATH/src/zsh/cuapk.zsh
source $CALPINEPATH/src/zsh/cuser.zsh
source $CALPINEPATH/src/zsh/cusudo.zsh
source $CALPINEPATH/src/zsh/cucli.zsh

# User Setup
cusudo -u "$userName" -p "userPassword"
mkdir -p /run/openrc
touch /run/openrc/softlevel
mkdir -p /etc/init.d
#su --login $userName
su - $userName

# Install Basic Utilities
#cuapk add zsh curl bash jq sudo git openrc
# Install CLI Tools
#cucli install --all


