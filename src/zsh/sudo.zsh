############################################################################
#                                                                          #
#                                 sudo                                     #
#                                                                          #
############################################################################

function cfgSudo() {
    # Check if a username was provided
    if [ -z "$1" ]; then
        echo "Error: No username provided."
        echo "Usage: cfgSudo <username> [-y]"
        return 1
    fi

    local username="$1"
    local force_nopasswd=false

    # Check if the '-y' option is provided to automatically configure without password
    if [[ "$2" == "-y" ]]; then
        force_nopasswd=true
    fi

    # Check if the user exists, and create the user if not
    if ! id -u "$username" > /dev/null 2>&1; then
        echo "User '$username' does not exist. Creating the user..."
        sudo useradd -m "$username"
        if [ $? -ne 0 ]; then
            echo "Error: Failed to create the user '$username'."
            return 1
        fi
        echo "User '$username' created successfully."
        # Set a password for the newly created user
        sudo passwd "$username"
    else
        echo "User '$username' already exists."
    fi

    # Add the user to the sudo or wheel group depending on the distribution
    if grep -qEi "(debian|ubuntu)" /etc/os-release; then
        sudo usermod -aG sudo "$username"
        echo "User '$username' has been added to the sudo group."
    elif grep -qEi "(redhat|centos|fedora)" /etc/os-release; then
        sudo usermod -aG wheel "$username"
        echo "User '$username' has been added to the wheel group."
    else
        echo "Error: Unsupported operating system."
        return 1
    fi

    # Automatically configure sudo without password if '-y' is used
    if $force_nopasswd; then
        echo "$username ALL=(ALL) NOPASSWD:ALL" | sudo tee "/etc/sudoers.d/$username" > /dev/null
        sudo chmod 0440 "/etc/sudoers.d/$username"
        echo "sudo configured without a password for '$username'."
    else
        # Option to configure sudo without a password with confirmation
        read -p "Configure sudo without a password for '$username'? (y/n): " response
        if [[ "$response" =~ ^[Yy]$ ]]; then
            echo "$username ALL=(ALL) NOPASSWD:ALL" | sudo tee "/etc/sudoers.d/$username" > /dev/null
            sudo chmod 0440 "/etc/sudoers.d/$username"
            echo "sudo configured without a password for '$username'."
        else
            echo "sudo configured with a password for '$username'."
        fi
    fi
}
