# ==========================================
# CUDIT - Neovim and Development Environment Setup for Cutopia
# ==========================================
# Description:
#   A function to set up Neovim and a complete development environment for Cutopia.
#
# Author: AbdElHakim ZOUAÏ
# Email : abdelhakimzouai@gmail.com
# ==========================================

cudit() {
    CUTOPIAPATH=/opt/cutopia
    source $CUTOPIAPATH/sec/env/cutopia.env
    # Define colors for better readability
    GREEN="\033[0;32m"   # Success
    YELLOW="\033[0;33m"  # Information
    RED="\033[0;31m"     # Error
    RESET="\033[0m"      # Reset color

    # Check if CUTOPIAPATH is defined
    if [[ -z "$CUTOPIAPATH" ]]; then
        echo "${RED}❌ Error: CUTOPIAPATH is not defined. Please set it before running this function.${RESET}"
        return 1
    fi

    # Source the Cutopia APK management script
    source $CUTOPIAPATH/src/zsh/cuapk.zsh

    # Step 1: Remove existing Neovim installation
    echo "${YELLOW}ℹ️ Removing existing Neovim installation...${RESET}"
    cuapk remove neovim || { echo "${RED}❌ Failed to remove Neovim.${RESET}"; return 1; }

    # Step 2: Install Node.js, npm, and yarn
    echo "${YELLOW}ℹ️ Installing Node.js, npm, and yarn...${RESET}"
    cuapk  add nodejs npm yarn build-base unzip || { echo "${RED}❌ Failed to install Node.js, npm, yarn or build-base .${RESET}"; return 1; }
    # Download and install nvm:
	curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.40.1/install.sh | bash

	# Download and install Node.js:
	nvm install 22

	# Verify the Node.js version:
	node -v # Should print "v22.14.0".
	nvm current # Should print "v22.14.0".

	# Verify npm version:
	npm -v # Should print "10.9.2".


    # Step 3: Download and extract the latest Neovim binary
    rm -rf /opt/nvim-linux64 || { echo "${RED}❌ Failed to remove old Neovim directory.${RESET}"; return 1; }
         # Download Neovim and verify success before extracting
    #if curl -fsLO https://github.com/neovim/neovim/releases/latest/download/nvim-linux64.tar.gz; then
    if curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim-linux-x86_64.tar.gz; then
        # The download succeeded, proceed with extraction
        if sudo tar -C /opt -xzf nvim-linux-x86_64.tar.gz; then
            echo "${GREEN}✅ Neovim binary successfully extracted.${RESET}"
        else
            # Extraction failed
            echo "${RED}❌ Failed to extract Neovim binary.${RESET}" >&2
            return 1
        fi
    else
        # Download failed
        echo "${RED}❌ Failed to download Neovim binary.${RESET}" >&2
        return 1
    fi
    echo "${YELLOW}ℹ️ Downloading and extracting the latest Neovim binary...${RESET}"
    mv nvim-linux-x86_64 /opt/
    rm -f nvim-linux-x86_64.tar.gz

    # Step 4: Clean existing Neovim configurations
    echo "${YELLOW}ℹ️ Cleaning existing Neovim configurations...${RESET}"
    rm -rf $HOME/.config/nvim || true
    rm -rf ~/.local/share/nvim || true

    # Step 5: Install Nerd Font (Droid Sans Mono)
    echo "${YELLOW}ℹ️ Installing Droid Sans Mono Nerd Font...${RESET}"

    # Create the fonts directory if it doesn't exist
    mkdir -p ~/.local/share/fonts

    # Download the font archive
    if curl -fsLo "DroidSansMono.zip" https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/DroidSansMono.zip; then
        # Extraction of the font files
        if unzip DroidSansMono.zip -d ~/.local/share/fonts; then
            echo "${GREEN}✅ Droid Sans Mono font successfully installed.${RESET}"
            rm -rf DroidSansMono.zip  # Clean up the downloaded zip file
        else
            echo "${RED}❌ Failed to extract Droid Sans Mono font.${RESET}" >&2
            return 1
        fi
    else
        echo "${RED}❌ Failed to download Droid Sans Mono font.${RESET}" >&2
        return 1
    fi

    # Update font configuration based on the Linux distribution
    if [ -f /etc/os-release ]; then
        . /etc/os-release  # Load OS release information
        case $ID in
            alpine)
                # For Alpine Linux
                cuapk add fontconfig ttf-dejavu || { echo "${RED}❌ Failed to install font dependencies.${RESET}"; return 1; }
                fc-cache -fv || { echo "${RED}❌ Failed to update font cache.${RESET}"; return 1; }
                ;;
            debian|ubuntu)
                # For Debian/Ubuntu-based systems
                cuapk add fontconfig || { echo "${RED}❌ Failed to install fontconfig.${RESET}"; return 1; }
                sudo dpkg-reconfigure fontconfig-config || { echo "${RED}❌ Failed to reconfigure font settings.${RESET}"; return 1; }
                fc-cache -fv || { echo "${RED}❌ Failed to update font cache.${RESET}"; return 1; }
                ;;
            *)
                # For other Linux distributions
                cuapk add fontconfig || { echo "${RED}❌ Failed to install fontconfig.${RESET}"; return 1; }
                sudo fc-cache -fv || { echo "${RED}❌ Failed to update font cache.${RESET}"; return 1; }
                ;;
        esac
    else
        echo "${YELLOW}⚠️ Unable to detect Linux distribution. Skipping font reconfiguration.${RESET}" >&2
    fi

    echo "${GREEN}✅ Font installation and configuration completed.${RESET}"



    # Step 6: Set up Python virtual environment and install pynvim
    echo "${YELLOW}ℹ️ Setting up Python virtual environment and installing pynvim...${RESET}"
    #python3 -m venv "$CUTOPIAPATH/cupy" || { echo "${RED}❌ Failed to create Python virtual environment.${RESET}"; return 1; }
    #source "$CUTOPIAPATH/cupy/bin/activate" || { echo "${RED}❌ Failed to activate Python virtual environment.${RESET}"; return 1; }
    #pip3 install pynvim || { echo "${RED}❌ Failed to install pynvim.${RESET}"; return 1; }

    # Step 7: Install additional tools
    echo "${YELLOW}ℹ️ Installing additional tools (plantuml, zathura)...${RESET}"
    #cuapk add plantuml zathura || { echo "${RED}❌ Failed to install plantuml or zathura.${RESET}"; return 1; }

    # Step 8: Fix Node.js permissions
    #userName="developer"

    # Change ownership for /usr/local/lib/node_modules
    #chown -R "$userName" /usr/local/lib/node_modules
    #|| { 
    #    echo "${RED}❌ Failed to fix Node.js permissions for /usr/local/lib/node_modules.${RESET}" >&2
    #    return 1
    #}

    # Change ownership for /usr/local/bin
    #chown -R "$userName" /usr/local/bin || { 
    #    echo "${RED}❌ Failed to fix Node.js permissions for /usr/local/bin.${RESET}" >&2
    #    return 1
    #}

    # Step 9: Install global Node.js packages
    echo "${YELLOW}ℹ️ Installing global Node.js packages (prettier, prettierd)...${RESET}"
    npm install -g prettier || { echo "${RED}❌ Failed to install prettier.${RESET}"; return 1; }
    npm install -g @fsouza/prettierd || { echo "${RED}❌ Failed to install prettierd.${RESET}"; return 1; }

    # Step 10: Install Rust and Stylua
    echo "${YELLOW}ℹ️ Installing Rust and Stylua...${RESET}"
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y || { echo "${RED}❌ Failed to install Rust.${RESET}"; return 1; }
    source "$HOME/.cargo/env" || { echo "${RED}❌ Failed to load Rust environment.${RESET}"; return 1; }
    cargo install stylua || { echo "${RED}❌ Failed to install Stylua.${RESET}"; return 1; }

    # Step 11: Clone Cudit configuration
    #echo "${YELLOW}ℹ️ Cloning Cudit configuration...${RESET}"
    #git clone --depth 1 https://gitlab.com/zhor-community/cudit.git ~/.cudit || { echo "${RED}❌ Failed to clone Cudit configuration.${RESET}"; return 1; }

    # Step 12: Set up Packer for Neovim
    echo "${YELLOW}ℹ️ Setting up Packer for Neovim...${RESET}"
    sudo rm -rf /home/$userName/.local/share/nvim/site/pack/packer/start/packer.nvim || true
    #sudo mkdir -p /home/$userName/.local/share/nvim/site/pack/packer/start/ || true
    sudo git clone --depth 1 https://github.com/wbthomason/packer.nvim /home/$userName/.local/share/nvim/site/pack/packer/start/packer.nvim || { echo "${RED}❌ Failed to clone Packer for Neovim.${RESET}"; return 1; }

    # Step 13: Link Cudit configuration to Neovim
    echo "${YELLOW}ℹ️ Linking Cudit configuration to Neovim...${RESET}"
    ln -sf $CUTOPIAPATH/src/cudit /home/$userName/.config/nvim || { echo "${RED}❌ Failed to link Cudit configuration.${RESET}"; return 1; }
    sudo chown -R $userName:$userName ~/.local/share/nvim

    # Step 14: Install Neovim plugins
    echo "${YELLOW}ℹ️ Installing Neovim plugins...${RESET}"
    nvim -c 'PackerInstall' -c 'PackerSync' -c 'qa' || { echo "${RED}❌ Failed to install Neovim plugins.${RESET}"; return 1; }

    # Step 15: Set up Markdown Preview
    echo "${YELLOW}ℹ️ Setting up Markdown Preview...${RESET}"
    cd /home/$userName/.local/share/nvim/site/pack/packer/opt/markdown-preview.nvim/app || { echo "${RED}❌ Failed to navigate to Markdown Preview directory.${RESET}"; return 1; }
    npm install || { echo "${RED}❌ Failed to install Markdown Preview dependencies.${RESET}"; return 1; }

    # Step 16: Configure root user's Neovim setup
    echo "${YELLOW}ℹ️ Configuring root user's Neovim setup...${RESET}"
    mkdir -p /root/.config || true
    ln -sf $CUTOPIAPATH/src/lua/cudit /root/.config/nvim || { echo "${RED}❌ Failed to link Cudit configuration for root user.${RESET}"; return 1; }
    git clone --depth 1 https://github.com/wbthomason/packer.nvim /root/.local/share/nvim/site/pack/packer/start/packer.nvim || { echo "${RED}❌ Failed to clone Packer for root user.${RESET}"; return 1; }

    # Final success message
    echo "${GREEN}✅ Cudit setup completed successfully.${RESET}"
}
cudit
