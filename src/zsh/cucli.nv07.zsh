#!/bin/zsh

# ========================
# Cutopia CLI (cucli)
# Gestion des modules avec JSON
# ========================

# Définition des variables globales
MODULES_JSON="/opt/cutopia/src/json/modules.json"

# Vérifier si jq est installé
if ! command -v jq &> /dev/null; then
    echo "[ERREUR] jq est requis mais n'est pas installé. Veuillez l'installer."
    exit 1
fi

# Vérifier si le fichier JSON est valide
if [[ ! -f $MODULES_JSON ]] || ! jq empty "$MODULES_JSON" 2>/dev/null; then
    echo "[ERREUR] Fichier JSON introuvable ou mal formaté : $MODULES_JSON"
    exit 1
fi

# Charger les modules depuis le JSON
function load_modules() {
    MODULES=$(jq -c '.modules[]' "$MODULES_JSON")
}

# Afficher l'aide
function show_help() {
    cat <<EOF
Usage : cucli [commande] [options]

Commandes disponibles :
  list               Liste tous les modules disponibles
  install <module>   Installe un module spécifique
  enable <module>    Active un module
  disable <module>   Désactive un module
  run <module>       Exécute l'installation d'un module
  update             Met à jour la liste des modules
  help               Affiche cette aide
EOF
}

# Lister les modules
function list_modules() {
    echo "\n📦 Modules disponibles :"
    echo "$MODULES" | jq -r '. | "- \(.name) : \(.description) [Inclus: \(.included)]"'
}

# Modifier le statut d'un module (activation/désactivation)
function modify_inclusion() {
    local module_name="$1"
    local new_status="$2"
    jq --arg name "$module_name" --argjson status $new_status '
        .modules |= map(if .name == $name then .included = $status else . end)
    ' "$MODULES_JSON" > "$MODULES_JSON.tmp" && mv "$MODULES_JSON.tmp" "$MODULES_JSON"
    echo "✅ Module '$module_name' mis à jour : included=$new_status"
}

# Activer un module
function enable_module() {
    modify_inclusion "$1" true
}

# Désactiver un module
function disable_module() {
    modify_inclusion "$1" false
}

# Installer un module
function install_module() {
    local module_name="$1"
    local module_command=$(echo "$MODULES" | jq -r --arg name "$module_name" '. | select(.name == $name) | .install_command')
    if [[ -z "$module_command" || "$module_command" == "null" ]]; then
        echo "[ERREUR] Module '$module_name' introuvable."
        exit 1
    fi
    echo "🔧 Installation du module '$module_name'..."
    eval "$module_command"
    echo "✅ Module '$module_name' installé avec succès."
}

# Mettre à jour la liste des modules
function update_modules() {
    echo "🔄 Mise à jour des modules en cours..."
    curl -s -o "$MODULES_JSON" "https://exemple.com/modules.json"
    if [[ $? -eq 0 ]]; then
        echo "✅ Modules mis à jour avec succès."
    else
        echo "[ERREUR] Impossible de mettre à jour les modules."
        exit 1
    fi
}

# Fonction principale
function cucli() {
    local command="$1"
    shift

    # Charger les modules
    load_modules
    
    case "$command" in
        list)
            list_modules
            ;;
        install)
            [[ -z "$1" ]] && { echo "[ERREUR] Nom du module requis pour 'install'."; exit 1; }
            install_module "$1"
            ;;
        enable)
            [[ -z "$1" ]] && { echo "[ERREUR] Nom du module requis pour 'enable'."; exit 1; }
            enable_module "$1"
            ;;
        disable)
            [[ -z "$1" ]] && { echo "[ERREUR] Nom du module requis pour 'disable'."; exit 1; }
            disable_module "$1"
            ;;
        run)
            [[ -z "$1" ]] && { echo "[ERREUR] Nom du module requis pour 'run'."; exit 1; }
            install_module "$1"
            ;;
        update)
            update_modules
            ;;
        help|*)
            show_help
            ;;
    esac
}

# Exécuter la fonction principale
cucli "$@"

