#!/bin/zsh

# ==============================
# cunit - Installation et vérification du système cutopia
# Version : 1.0
# Auteur : [Votre nom]
# Description : Installe ou vérifie la configuration du système cutopia.
#               Si le système n'est pas installé, il est configuré automatiquement.
# Options disponibles :
#   -d : Spécifier un répertoire personnalisé (par défaut : /opt/cutopia)
#   -g : Spécifier un groupe personnalisé (par défaut : cutopia)
#   -o : Spécifier un propriétaire personnalisé (par défaut : root)
#   -h : Afficher l'aide
# ==============================

# Variables par défaut
DEFAULT_DIR="/opt/cutopia"
DEFAULT_GROUP="cutopia"
DEFAULT_OWNER="hermit"

# Fonction principale
cunit() {
    local directory="$DEFAULT_DIR"
    local group_name="$DEFAULT_GROUP"
    local owner="$DEFAULT_OWNER"
    local is_installed=1  # Par défaut, on suppose que le système n'est pas installé

    # Gestion des options avec getopts
    while getopts ":d:g:o:vh" opt; do
        case $opt in
            d)  # Répertoire personnalisé
                directory=$OPTARG
                ;;
            g)  # Groupe personnalisé
                group_name=$OPTARG
                ;;
            o)  # Propriétaire personnalisé
                owner=$OPTARG
                ;;
            v)  # Mode verbose
                VERBOSE=1
                ;;
            h)  # Afficher l'aide
                display_help
                return 0
                ;;
            \?) # Option invalide
                echo "Erreur : Option invalide -$OPTARG." >&2
                display_help
                return 1
                ;;
        esac
    done

    # Vérifier les privilèges root
    if [[ $EUID -ne 0 ]]; then
        echo "Erreur : Ce script doit être exécuté en tant que root ou avec sudo." >&2
        return 1
    fi

    # Étape 1 : Vérifier si le système est déjà installé
    if check_system "$directory" "$group_name" "$owner"; then
        if [[ -n $VERBOSE ]]; then
            echo "Le système cutopia est déjà correctement configuré."
        fi
        return 0
    else
        is_installed=0
    fi

    # Étape 2 : Installer ou réinstaller le système
    if [[ $is_installed -eq 0 ]]; then
        if [[ -n $VERBOSE ]]; then
            echo "Le système cutopia n'est pas configuré. Installation en cours..."
        fi
        cuinit -d "$directory" -g "$group_name" -o "$owner" || { echo "Échec de l'installation." >&2; return 1; }
    fi

    # Étape 3 : Vérification finale
    if check_system "$directory" "$group_name" "$owner"; then
        echo "Le système cutopia a été correctement configuré."
    else
        echo "Erreur : La configuration du système cutopia a échoué." >&2
        return 1
    fi
}

# Fonction de vérification du système
check_system() {
    local dir=$1
    local group=$2
    local own=$3

    # Vérifier si le répertoire existe
    if [[ ! -d "$dir" ]]; then
        return 1
    fi

    # Vérifier le propriétaire et le groupe
    local current_owner_group
    current_owner_group=$(stat -c '%U:%G' "$dir" 2>/dev/null)
    if [[ "$current_owner_group" != "$own:$group" ]]; then
        return 1
    fi

    # Vérifier les permissions
    local current_permissions
    current_permissions=$(stat -c '%a' "$dir" 2>/dev/null)
    if [[ "$current_permissions" != "775" ]]; then
        return 1
    fi

    # Vérifier l'existence du groupe
    if ! getent group "$group" > /dev/null; then
        return 1
    fi

    return 0
}

# Fonction d'affichage de l'aide
display_help() {
    cat <<EOF
Usage: cunit [-d directory] [-g group_name] [-o owner] [-v] [-h]

Options :
  -d <répertoire>   Spécifie un répertoire personnalisé (par défaut : $DEFAULT_DIR).
  -g <groupe>       Spécifie un groupe personnalisé (par défaut : $DEFAULT_GROUP).
  -o <propriétaire> Spécifie un propriétaire personnalisé (par défaut : $DEFAULT_OWNER).
  -v                Activer le mode verbose (affiche des détails supplémentaires).
  -h                Affiche cette aide.

Description :
  Vérifie si le système cutopia est correctement configuré. Si non, il installe ou réinstalle
  la configuration via la fonction setup_cutopia.

Exemples :
  cunit -d /var/data -g mygroup -o myuser
  cunit -v  # Mode verbose
EOF
}

# Exécution du script
if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
    cunit "$@"
fi
