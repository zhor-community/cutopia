#!/bin/zsh

# Function to handle CRUD operations and installation
cugit() {
    local action=$1
    local project_url=$2
    local branch=${3:-main}    # Default branch to 'main' if not provided
    local directory=${4:-./}   # Default directory to current directory if not provided

    # Function to display usage
    usage() {
        echo "Usage: $0 -a <action> [-p <project_url>] [-b <branch>] [-d <directory>]"
        echo "Actions: install, create, read, update, delete"
        exit 1
    }

    # Parse options
    while getopts "a:p:b:d:" opt; do
        case "$opt" in
            a) action=$OPTARG ;;
            p) project_url=$OPTARG ;;
            b) branch=$OPTARG ;;
            d) directory=$OPTARG ;;
            *) usage ;;
        esac
    done

    # Validate required options
    if [[ -z "$action" ]]; then
        echo "Error: Action is required."
        usage
    fi

    # Validate action
    case "$action" in
        install)
            if [[ -z "$project_url" ]]; then
                echo "Error: Project URL is required for installation."
                usage
            fi
            if ! command -v git &> /dev/null; then
                echo "Error: git is not installed."
                exit 1
            fi
            echo "Installing project from $project_url (branch: $branch) to $directory..."
            if git clone --branch "$branch" "$project_url" "$directory"; then
	       cd "$directory"
	       zsh init.zsh
                echo "Project successfully installed in $directory"
            else
                echo "Error: Failed to install project."
            fi
            ;;
        create)
            echo "Creating a new entity..."
            # Your creation logic here
            ;;
        read)
            echo "Reading an entity..."
            # Your reading logic here
            ;;
        update)
            echo "Updating an entity..."
            # Your update logic here
            ;;
        delete)
            echo "Deleting an entity..."
            # Your deletion logic here
            ;;
        *)
            echo "Error: Invalid action: $action"
            usage
            ;;
    esac
}

# Execute the main function
###cugit "$@"
function gACP() {
    local GitMessage="$1"
    if [ -z "$GitMessage" ]; then
        echo "Error: Commit message is required."
        return 1
    fi
    git add .
    git commit -m "$GitMessage"
    git push
}
