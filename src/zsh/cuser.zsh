cuser() {
  # Initialiser les variables par défaut
  local userName="hermit"
  local userPassword="P@ssw0rd"
  local sudoUser="hermit"
  local force_nopasswd=false
  local userExists=false

  # Fonction d'affichage de l'usage
  usage() {
    echo "Usage : $0 [-u nom_utilisateur] [-p mot_de_passe] [-s sudo_user] [-n (sans mot de passe)]"
    echo "  -u nom_utilisateur   : Nom de l'utilisateur à ajouter (par défaut : hermit)"
    echo "  -p mot_de_passe      : Mot de passe de l'utilisateur (par défaut : P@ssw0rd)"
    echo "  -s sudo_user         : Utilisateur à qui donner des droits sudo (par défaut : hermit)"
    echo "  -n                   : Créer un utilisateur sans mot de passe"
    return 1
  }

  # Vérification si l'utilisateur est root
  if [[ $EUID -ne 0 ]]; then
    echo "Erreur : Cette commande doit être exécutée avec des privilèges root."
    return 1
  fi

  # Traiter les options via getopts
  while getopts "u:p:s:n" opt; do
    case "$opt" in
      u) userName="$OPTARG" ;;  # Nom d'utilisateur
      p) userPassword="$OPTARG" ;;  # Mot de passe
      s) sudoUser="$OPTARG" ;;  # Utilisateur pour sudo
      n) force_nopasswd=true ;;  # Désactiver la demande de mot de passe
      \?) usage ;;
    esac
  done

  # Vérification de la présence d'un utilisateur existant
  if id "$userName" &>/dev/null; then
    echo "Erreur : L'utilisateur '$userName' existe déjà."
    return 1
  fi

  # Demander un mot de passe si l'option '-n' n'est pas utilisée
  if [[ "$force_nopasswd" = false ]]; then
    read -s -p "Entrez le mot de passe pour $userName : " password_input
    echo
    userPassword=$password_input
  fi

  # Vérifier la distribution pour adapter la commande d'ajout
  if [[ -f /etc/os-release ]]; then
    source /etc/os-release
    dist_name=$ID
  else
    echo "Erreur : Impossible de détecter la distribution Linux."
    return 1
  fi

  # Ajouter l'utilisateur selon la distribution
  case "$dist_name" in
    debian|ubuntu)
      echo "Ajout de l'utilisateur $userName sur Debian/Ubuntu"
      adduser --disabled-password --gecos "" "$userName"
      echo "$userName:$userPassword" | chpasswd
      ;;
    rhel|centos|fedora)
      echo "Ajout de l'utilisateur $userName sur Red Hat/CentOS/Fedora"
      useradd -m "$userName"
      echo "$userName:$userPassword" | chpasswd
      ;;
    arch)
      echo "Ajout de l'utilisateur $userName sur Arch Linux"
      useradd -m "$userName"
      echo "$userName:$userPassword" | chpasswd
      ;;
    alpine)
      echo "Ajout de l'utilisateur $userName sur Alpine Linux"
      adduser -D "$userName"
      echo "$userName:$userPassword" | chpasswd
      ;;
    suse)
      echo "Ajout de l'utilisateur $userName sur SUSE Linux"
      useradd -m "$userName"
      echo "$userName:$userPassword" | chpasswd
      ;;
    *)
      echo "Erreur : Distribution non prise en charge."
      return 1
      ;;
  esac

  # Ajouter l'utilisateur aux droits sudo si un utilisateur sudo est spécifié
  if [[ -n "$sudoUser" && "$sudoUser" != "$userName" ]]; then
    echo "Ajout de $userName aux droits sudo via $sudoUser"
    usermod -aG sudo "$userName"
  fi

  echo "L'utilisateur '$userName' a été ajouté avec succès."
}
