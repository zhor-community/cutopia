#!/bin/zsh

CURON_FILE="$HOME/curon.json"

# Vérifie si 'jq' est installé (nécessaire pour manipuler JSON)
if ! command -v jq &>/dev/null; then
    echo "❌ Erreur : 'jq' est requis. Installez-le avec 'sudo apt install jq' (Debian/Ubuntu) ou 'brew install jq' (MacOS)."
    return 1
fi

# Vérifie si le fichier JSON existe, sinon le crée
if [[ ! -f "$CURON_FILE" ]]; then
    echo '{ "tasks": [] }' > "$CURON_FILE"
fi

# Met à jour crontab à partir du JSON
function update_crontab_from_json() {
    echo "🔄 Mise à jour de crontab depuis $CURON_FILE..."

    # Génère la liste des tâches
    local cron_entries=$(jq -r '.tasks[] | "\(.schedule) \(.command)"' "$CURON_FILE")

    # Applique les tâches à crontab
    (echo "$cron_entries") | crontab -

    echo "✅ Crontab mis à jour avec succès !"
}

# Fonction principale curon
function curon() {
    local action="$1"
    shift

    case "$action" in
        list)
            echo "📜 Liste des tâches cron enregistrées :"
            jq -r '.tasks[] | "\(.schedule) → \(.command)"' "$CURON_FILE"
            ;;

        add)
            if [[ -z "$1" || -z "$2" ]]; then
                echo "❌ Utilisation : curon add 'cron_time' 'commande'"
                echo "   Exemple : curon add '*/10 * * * *' '/home/user/backup.sh'"
                return 1
            fi

            local cron_time="$1"
            local command="$2"

            # Ajout dans JSON
            jq --arg schedule "$cron_time" --arg command "$command" \
                '.tasks += [{"schedule": $schedule, "command": $command}]' "$CURON_FILE" > tmp.json && mv tmp.json "$CURON_FILE"

            update_crontab_from_json
            echo "✅ Tâche ajoutée : $cron_time → $command"
            ;;

        remove)
            if [[ -z "$1" ]]; then
                echo "❌ Utilisation : curon remove 'commande_partielle'"
                echo "   Exemple : curon remove 'backup.sh'"
                return 1
            fi

            local pattern="$1"

            # Supprime l'entrée contenant le motif
            jq --arg pattern "$pattern" 'del(.tasks[] | select(.command | contains($pattern)))' "$CURON_FILE" > tmp.json && mv tmp.json "$CURON_FILE"

            update_crontab_from_json
            echo "🗑️ Tâche supprimée contenant : '$pattern'"
            ;;

        edit)
            echo "✏️ Édition des tâches JSON dans $CURON_FILE..."
            ${EDITOR:-nano} "$CURON_FILE"
            update_crontab_from_json
            ;;

        clear)
            echo "⚠️ Suppression de toutes les tâches cron enregistrées !"
            read -q "REPLY?Es-tu sûr ? (o/N) " && echo
            if [[ "$REPLY" =~ ^[OoYy]$ ]]; then
                echo '{ "tasks": [] }' > "$CURON_FILE"
                update_crontab_from_json
                echo "✅ Toutes les tâches cron ont été supprimées."
            else
                echo "❌ Annulation de l’opération."
            fi
            ;;

        help|--help|-h)
            echo "📌 Utilisation de curon :"
            echo "  curon list                → Afficher les tâches cron enregistrées"
            echo "  curon add 'cron' 'cmd'    → Ajouter une tâche cron dans JSON"
            echo "  curon remove 'cmd'        → Supprimer une tâche cron du JSON"
            echo "  curon edit                → Modifier les tâches via nano/vim"
            echo "  curon clear               → Supprimer toutes les tâches du JSON"
            ;;

        *)
            echo "❌ Commande invalide. Utilisez 'curon help' pour afficher l'aide."
            return 1
            ;;
    esac
}

