#!/bin/bash

# Vérifiez si l'utilisateur exécute le script en tant que root
if [[ $(id -u) -ne 0 ]]; then
    echo "Vous devez exécuter ce script en tant que root (ou avec sudo)"
    exit 1
fi

# Définir les noms des conteneurs et les services associés
containers=(
    "zhor"
    "runia"
    "zerozone"
    "calpine"
    "ticenergy"
    "sohib"
    "cutopia"
    "tassili"
)

# Créer les conteneurs LXC pour chaque service
create_container() {
    container_name=$1
    image="ubuntu:20.04"

    # Créer le conteneur avec l'image de base Ubuntu 20.04
    lxc launch $image $container_name

    # Configurer les ressources du conteneur
    echo "Configuration des ressources pour le conteneur $container_name..."
    lxc config device add $container_name memory disk source=/tmp/$container_name-mem
    lxc config set $container_name limits.memory 4GB  # Ajustez la mémoire selon les besoins

    # Ajouter un réseau spécifique (exemple)
    lxc network attach lxdbr0 $container_name eth0

    # Ajouter des règles de pare-feu (si nécessaire)
    # lxc config set $container_name security.nesting true
    # lxc config set $container_name security.privileged true

    echo "Conteneur $container_name créé et configuré."
}

# Installer les services nécessaires pour chaque conteneur
install_services() {
    container_name=$1
    echo "Installation des services dans $container_name..."

    # Exécuter les commandes d'installation de base dans chaque conteneur
    lxc exec $container_name -- bash -c "
        apt update && apt upgrade -y
    "

    # Cas spécifiques pour chaque conteneur

    # Conteneur Zhor (Blockchain)
    if [[ $container_name == "zhor-blockchain" ]]; then
        lxc exec $container_name -- bash -c "
            apt install -y nodejs npm
            # Installez les dépendances spécifiques à Zhor Blockchain ici
        "
    fi

    # Conteneur Runia (IA)
    if [[ $container_name == "runia-ai" ]]; then
        lxc exec $container_name -- bash -c "
            apt install -y python3 python3-pip
            pip3 install tensorflow pytorch
            # Installez d'autres dépendances pour l'IA ici
        "
    fi

    # Conteneur Cudata (IPFS, Redis, MariaDB)
    if [[ $container_name == "cudata-ipfs" ]]; then
        lxc exec $container_name -- bash -c "
            apt install -y ipfs
            # Configurez IPFS ici
        "
    fi
    if [[ $container_name == "cudata-redis" ]]; then
        lxc exec $container_name -- bash -c "
            apt install -y redis-server
            # Configurez Redis ici
        "
    fi
    if [[ $container_name == "cudata-mariadb" ]]; then
        lxc exec $container_name -- bash -c "
            apt install -y mariadb-server
            # Configurez MariaDB ici
        "
    fi

    # Conteneur TicEnergy (Nginx, Caddy, Traefik)
    if [[ $container_name == "ticenergy-nginx" ]]; then
        lxc exec $container_name -- bash -c "
            apt install -y nginx
            # Configurez Nginx ici
        "
    fi
    if [[ $container_name == "ticenergy-caddy" ]]; then
        lxc exec $container_name -- bash -c "
            apt install -y caddy
            # Configurez Caddy ici
        "
    fi
    if [[ $container_name == "ticenergy-traefik" ]]; then
        lxc exec $container_name -- bash -c "
            apt install -y traefik
            # Configurez Traefik ici
        "
    fi

    # Conteneur Tassili (IoT)
    if [[ $container_name == "tassili-iot" ]]; then
        lxc exec $container_name -- bash -c "
            apt install -y mosquitto
            # Configurez IoT (MQTT, CoAP) ici
        "
    fi

    echo "Services installés pour $container_name."
}

# Créer et configurer chaque conteneur
for container in "${containers[@]}"; do
    create_container $container
    install_services $container
done

echo "Tous les conteneurs ont été créés et configurés avec succès !"
