
addTool() {
    # Define colors for output
    RED='\033[0;31m'
    GREEN='\033[0;32m'
    YELLOW='\033[1;33m'
    CYAN='\033[0;36m'
    RESET='\033[0m'

    # Define icons for better user experience
    SUCCESS="${GREEN}✔${RESET}"
    ERROR="${RED}✖${RESET}"
    WARNING="${YELLOW}⚠${RESET}"
    INFO="${CYAN}ℹ${RESET}"

    # Variables for options and parameters
    local tool=""
    local env=""
    local os=$(uname -s)
    local distro=""
    local script_path=""

    # Define supported tools, environments, and distributions
    local supported_tools=("ssh" "docker" "lxc" "kvm" "qemu" "xen" "host" "ansible" "sudo")
    local supported_envs=("vps" "dockerContainer" "lxcContainer" "kvmImage" "qemuImage" "xenImage" "host")
    local supported_distros=("alpine" "debian" "ubuntu" "centos" "fedora" "arch" "opensuse" "redhat")

    # Parse options with getopts
    while getopts ":t:e:" opt; do
        case $opt in
            t) tool=$OPTARG ;;
            e) env=$OPTARG ;;
            \?) echo -e "${ERROR} Invalid option: -$OPTARG" && return 1 ;;
            :) echo -e "${ERROR} Option -$OPTARG requires an argument." && return 1 ;;
        esac
    done

    # Validate the tool
    if [[ ! " ${supported_tools[@]} " =~ " ${tool} " ]]; then
        echo -e "${ERROR} Invalid tool: ${tool}"
        echo -e "${INFO} Supported tools: ${supported_tools[*]}"
        return 1
    fi

    # Validate the environment
    if [[ ! " ${supported_envs[@]} " =~ " ${env} " ]]; then
        echo -e "${ERROR} Invalid environment: ${env}"
        echo -e "${INFO} Supported environments: ${supported_envs[*]}"
        return 1
    fi

    # Detect Linux distribution
    if [[ "$os" == "Linux" ]]; then
        if [[ -f /etc/os-release ]]; then
            . /etc/os-release
            distro=$ID
        elif [[ -f /etc/debian_version ]]; then
            distro="debian"
        elif [[ -f /etc/centos-release ]]; then
            distro="centos"
        else
            echo -e "${ERROR} Unsupported Linux system."
            return 1
        fi
    else
        echo -e "${ERROR} Unsupported operating system: ${os}"
        return 1
    fi

    # Validate detected distribution
    if [[ ! " ${supported_distros[@]} " =~ " ${distro} " ]]; then
        echo -e "${WARNING} Unsupported Linux distribution: ${distro}"
        echo -e "${INFO} Consider adding support for this distribution."
        return 1
    fi

    # Build the script path
    script_path="$CALPINEPATH/src/zsh/cutools/${tool}Installer.${distro}.${env}.zsh"

    # Display detected information
    echo -e "${INFO} Detected distribution: ${CYAN}${distro}${RESET}"
    echo -e "${INFO} Detected environment: ${CYAN}${env}${RESET}"
    echo -e "${INFO} Tool to install: ${CYAN}${tool}${RESET}"

    # Check and execute the installation script
    if [[ -f "$script_path" ]]; then
        echo -e "${SUCCESS} Script found: ${script_path}"
        zsh "$script_path"
        if [[ $? -eq 0 ]]; then
            echo -e "${SUCCESS} ${tool} installation successful."
        else
            echo -e "${ERROR} Failed to install ${tool}."
            return 1
        fi
    else
        echo -e "${ERROR} Installation script not found: ${script_path}"
        return 1
    fi
}
#Install Docker on a VPS with Ubuntu
#addTool -t docker -e vps
#ℹ Detected distribution: ubuntu
#ℹ Detected environment: vps
#ℹ Tool to install: docker
#✔ Script found: ./installers/dockerInstaller.ubuntu.vps.zsh
#✔ docker installation successful.

#addTool -t ssh -e dockerContainer
#ℹ Detected distribution: debian
#ℹ Detected environment: dockerContainer
#ℹ Tool to install: ssh
#✖ Installation script not found: ./installers/sshInstaller.debian.dockerContainer.zsh



