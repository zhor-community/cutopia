#!/bin/zsh

# Valeurs par défaut
DEFAULT_PATH="~/.cutopia"
#CALPINEPATH="$DEFAULT_PATH"
DEFAULT_GROUP="cutopia"
DEFAULT_IMAGE="calpine"
DEFAULT_OPERATION="start"
DEFAULT_CONTAINER="calpine"
DEFAULT_ENV_FILE="calpine"  # Valeurs autorisées : "calpine", "cutopia", "yats"
DEFAULT_PROFILE="admin"     # Valeurs autorisées : "guest", "user", "admin", "hermit", "root"

    display_variables() {
      # Définition des couleurs
      local green='\033[1;32m'
      local blue='\033[1;34m'
      local yellow='\033[1;33m'
      local reset='\033[0m'

      # Définition des icônes
      local path_icon="📂"
      local group_icon="👥"
      local image_icon="🖼️"
      local operation_icon="⚙️"
      local container_icon="📦"
      local envfile_icon="📄"
      local profile_icon="🧩"

      # Affichage des variables avec icônes et couleurs
      echo -e "${path_icon} ${green}myPath${reset}: ${blue}$DEFAULT_PATH${reset}"
      echo -e "${group_icon} ${green}myGroup${reset}: ${blue}$DEFAULT_GROUP${reset}"
      echo -e "${image_icon} ${green}myImage${reset}: ${blue}$DEFAULT_IMAGE${reset}"
      echo -e "${operation_icon} ${green}myOperation${reset}: ${blue}$DEFAULT_OPERATION${reset}"
      echo -e "${container_icon} ${green}myContainer${reset}: ${blue}$DEFAULT_CONTAINER${reset}"
      echo -e "${envfile_icon} ${green}myEnvFile${reset}: ${blue}$DEFAULT_ENV_FILE${reset}"
      echo -e "${profile_icon} ${green}myProfile${reset}: ${blue}$DEFAULT_PROFILE${reset}"
    }
    display_variables
# Fonction principale : Gérer les conteneurs Docker et les images
calpine() {
    # Initialiser les variables avec les valeurs par défaut
    local myPath="$DEFAULT_PATH"
    local myGroup="$DEFAULT_GROUP"
    local myImage="$DEFAULT_IMAGE"
    local myOperation="$DEFAULT_OPERATION"
    local myContainer="$DEFAULT_CONTAINER"
    local myEnvFile="$DEFAULT_ENV_FILE"
    local myProfile="$DEFAULT_PROFILE"

    # Vérifier si Docker est installé
    if ! command -v docker &>/dev/null; then
        echo "Erreur : Docker n'est pas installé. Veuillez installer Docker et réessayer."
        exit 1
    fi

    # Analyser les options de la ligne de commande
    while getopts ":p:g:i:o:c:e:r:" opt; do
        case $opt in
            p) myPath="$OPTARG" ;;
            g) myGroup="$OPTARG" ;;
            i) myImage="$OPTARG" ;;
            o) myOperation="$OPTARG" ;;
            c) myContainer="$OPTARG" ;;
            e) myEnvFile="$OPTARG" ;;  # Spécifier le fichier d'environnement
            r) myProfile="$OPTARG" ;;  # Spécifier le profil utilisateur
            \?)
                echo "Erreur : Option invalide -$OPTARG" >&2
                exit 1
                ;;
            :)
                echo "Erreur : L'option -$OPTARG nécessite un argument." >&2
                exit 1
                ;;
        esac
    done

    # Validation du fichier d'environnement
    if [[ ! "$myEnvFile" =~ ^(calpine|cutopia|yats)$ ]]; then
        echo "Erreur : Fichier d'environnement invalide '$myEnvFile'. Valeurs autorisées : calpine, cutopia, yats."
        exit 1
    fi

    # Validation du profil utilisateur
    if [[ ! "$myProfile" =~ ^(guest|user|admin|hermit|root)$ ]]; then
        echo "Erreur : Profil invalide '$myProfile'. Valeurs autorisées : guest, user, admin, hermit, root."
        exit 1
    fi

    # Afficher le résumé de l'opération
    echo "Opération : $myOperation"
    echo "Chemin : $myPath | Groupe : $myGroup | Image : $myImage | Conteneur : $myContainer"
    echo "Fichier d'environnement : $myEnvFile | Profil : $myProfile"

    # Afficher l'état du répertoire de travail
    echo "Répertoire de travail actuel : $(pwd)"
    echo "Liste des fichiers dans /tmp/ :"
    ls /tmp/

    echo "Liste des fichiers dans /home/$USER/.cutopia :"
    ls /home/$USER/.cutopia

    echo "Valeur de CALPINEPATH : $CALPINEPATH"

    # Exécution de l'opération demandée
    case $myOperation in
        "start")
            echo "Démarrage du conteneur Docker : $myContainer"

            # Vérifier si l'image Docker existe
            if ! docker images -q "$myGroup/$myImage" &>/dev/null; then
                echo "Image '$myGroup/$myImage' introuvable. Construction de l'image..."
                # Construire l'image si elle n'existe pas
                docker build -t "$myGroup/$myImage:latest" "$myPath"
            fi

            # Vérifier si le fichier d'environnement existe
            if [[ ! -f "$CALPINEPATH/src/env/$myEnvFile.env" ]]; then
                echo "Fichier d'environnement '$myEnvFile.env' introuvable. Création d'un fichier par défaut..."
                # Créer un fichier d'environnement par défaut
                echo "# Fichier d'environnement par défaut" > "$CALPINEPATH/src/env/$myEnvFile.env"
                echo "userName='default'" >> "$CALPINEPATH/src/env/$myEnvFile.env"
                echo "userPassword='defaultPassword'" >> "$CALPINEPATH/src/env/$myEnvFile.env"
                echo "sudoUser='admin'" >> "$CALPINEPATH/src/env/$myEnvFile.env"
                echo "CALPINEPATH='/tmp'" >> "$CALPINEPATH/src/env/$myEnvFile.env"
                echo "Fichier d'environnement par défaut '$myEnvFile.env' créé."
            fi

            # Vérifier si le conteneur existe
            if docker ps -a --format '{{.Names}}' | grep -w "$myContainer" &>/dev/null; then
                # Vérifier si le conteneur est en cours d'exécution
                if docker ps --format '{{.Names}}' | grep -w "$myContainer" &>/dev/null; then
                    echo "Le conteneur '$myContainer' est déjà en cours d'exécution. Connexion..."
                    docker exec -it "$myContainer" sh
                else
                    echo "Le conteneur '$myContainer' existe mais n'est pas en cours d'exécution. Démarrage..."
                    if docker start "$myContainer" &>/dev/null; then
                        echo "Le conteneur '$myContainer' a été démarré avec succès. Connexion..."
                        docker exec -it "$myContainer" sh
                    else
                        echo "Erreur : Échec du démarrage du conteneur '$myContainer'."
                        exit 1
                    fi
                fi
            else
                echo "Erreur : Aucune tel conteneur : '$myContainer'. Création du conteneur..."
                # Créer et démarrer le conteneur si celui-ci n'existe pas
                docker run -it -d --name "$myContainer" --env-file "$CALPINEPATH/src/env/$myEnvFile.env" "$myGroup/$myImage"
                echo "Conteneur '$myContainer' créé et démarré avec succès."
                docker exec -it "$myContainer" sh
            fi
            ;;
        "create")
            echo "Création du conteneur Docker : $myContainer"

            # Vérifier si le chemin spécifié existe
            if [[ ! -d "$myPath" ]]; then
                echo "Erreur : Le chemin spécifié '$myPath' n'existe pas."
                exit 1
            fi

            # Vérifier si l'image Docker existe
            if ! docker images -q "$myGroup/$myImage" &>/dev/null; then
                echo "Image '$myGroup/$myImage' introuvable. Construction de l'image..."
                docker build -t "$myGroup/$myImage:latest" "$myPath"
            fi

            # Vérifier si le fichier d'environnement existe
            if [[ ! -f "$CALPINEPATH/src/env/$myEnvFile.env" ]]; then
                echo "Fichier d'environnement '$myEnvFile.env' introuvable. Création d'un fichier par défaut..."
                echo "# Fichier d'environnement par défaut" > "$CALPINEPATH/src/env/$myEnvFile.env"
                echo "userName='default'" >> "$CALPINEPATH/src/env/$myEnvFile.env"
                echo "userPassword='defaultPassword'" >> "$CALPINEPATH/src/env/$myEnvFile.env"
                echo "sudoUser='admin'" >> "$CALPINEPATH/src/env/$myEnvFile.env"
                echo "CALPINEPATH='/tmp'" >> "$CALPINEPATH/src/env/$myEnvFile.env"
                echo "Fichier d'environnement par défaut '$myEnvFile.env' créé."
            fi

            # Créer le conteneur Docker
            docker run -it -d --name "$myContainer" --env-file "$CALPINEPATH/src/env/$myEnvFile.env" "$myGroup/$myImage"
            echo "Conteneur '$myContainer' créé avec succès."
            ;;
        "update")
            echo "Mise à jour du conteneur Docker : $myContainer"
            docker stop "$myContainer" && docker rm "$myContainer"
            docker run -it -d --name "$myContainer" --env-file "$CALPINEPATH/src/env/$myEnvFile.env" "$myGroup/$myImage"
            echo "Conteneur '$myContainer' mis à jour avec succès."
            ;;
        "upgrade")
            echo "Mise à jour de l'image Docker et du conteneur : $myContainer"
            docker stop "$myContainer" && docker rm "$myContainer" && docker rmi "$myGroup/$myImage"
            docker build -t "$myGroup/$myImage:latest" "$myPath"
            docker run -it -d --name "$myContainer" --env-file "$CALPINEPATH/src/env/$myEnvFile.env" "$myGroup/$myImage"
            echo "Conteneur '$myContainer' mis à niveau avec succès."
            ;;
        "delete")
            echo "Suppression du conteneur et de l'image Docker : $myContainer"
            docker stop "$myContainer" && docker rm "$myContainer" && docker rmi "$myGroup/$myImage"
            echo "Conteneur '$myContainer' et image '$myGroup/$myImage' supprimés avec succès."
            ;;
        *)
            echo "Erreur : Opération invalide '$myOperation'."
            echo "Opérations autorisées : create, read, update, upgrade, delete, start."
            exit 1
            ;;
    esac
}

# Fin du script
