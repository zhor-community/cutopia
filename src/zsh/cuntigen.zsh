#!/bin/zsh

# Define the Antigen installation directory
ANTIGEN_HOME="${XDG_DATA_HOME:-${HOME}/.antigen}"
ANTIGEN_URL="https://git.io/antigen"

# Create the parent directory if it doesn't exist
if [ ! -d "$ANTIGEN_HOME" ]; then
  echo "Creating Antigen directory: $ANTIGEN_HOME"
  mkdir -p $ANTIGEN_HOME
fi

# Download Antigen if not already present
if [ ! -f "$ANTIGEN_HOME/antigen.zsh" ]; then
  echo "Downloading Antigen from $ANTIGEN_URL"
  curl -L "$ANTIGEN_URL" -o "$ANTIGEN_HOME/antigen.zsh"
  if [ $? -ne 0 ]; then
    echo "Error: Failed to download Antigen."
    exit 1
  fi
else
  echo "Antigen is already downloaded."
fi

# Load Antigen
if [ -f "$ANTIGEN_HOME/antigen.zsh" ]; then
  echo "Loading Antigen..."
  source "$ANTIGEN_HOME/antigen.zsh"
else
  echo "Error: Could not find the antigen.zsh file."
  exit 1
fi

echo "Antigen successfully set up."
