#!/bin/zsh

# Emplacement du fichier JSON
JSON_FILE="/opt/cutopia/src/yml/composer/services.json"
BASE_DIR="/opt/cutopia/src/yml/composer"

cumpose() {
    if [[ ! -f "$JSON_FILE" ]]; then
        echo "❌ Erreur : fichier $JSON_FILE introuvable."
        return 1
    fi

    if [[ "$1" != "--edition" || -z "$2" ]]; then
        echo "Usage: cumpose --edition <ecom|educ|sec>"
        return 1
    fi

    local edition="$2"
    local services=$(jq -r --arg edition "$edition" '.services | to_entries | map(select(.value.edition | index($edition))) | map(.key) | @sh' "$JSON_FILE")

    if [[ -z "$services" || "$services" == "null" ]]; then
        echo "❌ Erreur : édition '$edition' introuvable ou sans services définis."
        return 1
    fi

    # Convertir la liste en tableau Zsh
    eval "local services_array=($services)"

    echo "🚀 Lancement de l'édition '$edition' avec les services : ${services_array[*]}"

    for service in "${services_array[@]}"; do
        local compose_file="$BASE_DIR/${service}.yml"

        if [[ -f "$compose_file" ]]; then
            echo "➡️  Démarrage de $service..."
            docker compose -f "$compose_file" up -d
        else
            echo "⚠️  Fichier $compose_file introuvable."
        fi
    done
}

# Vérifier si le script est exécuté directement
if [[ "${BASH_SOURCE[0]}" == "$0" ]]; then
    cumpose "$@"
fi

