#!/bin/zsh

# Variables
CONTAINER_NAME="cutopia"
IMAGE="alpine/edge/cloud"  # Change this to any LXD image you prefer
SHARED_FOLDER="/opt/cutopia"
INIT_SCRIPT="$SHARED_FOLDER/init.zsh"

lxc stop cutopia
lxc delete cutopia

# 1. Check if LXD is installed
if ! command -v lxc &> /dev/null; then
    echo "❌ LXD is not installed! Install it with: sudo snap install lxd"
    exit 1
fi

# 2. Check if the container already exists
if lxc list | grep -q "^| $CONTAINER_NAME "; then
    echo "⚠️  Container $CONTAINER_NAME already exists. Deleting and recreating..."
    lxc delete -f $CONTAINER_NAME
fi

# 3. Create the LXD container with the specified image
echo "🚀 Creating container $CONTAINER_NAME with image $IMAGE..."
lxc launch images:$IMAGE $CONTAINER_NAME

# 4. Wait for the container to start
sleep 5

# 5. Share the folder between the host and the container
echo "🔗 Configuring shared folder..."
lxc config device add $CONTAINER_NAME shared-folder disk source=$SHARED_FOLDER path=/opt/cutopia

# 6. Add the initialization script
if [[ -f "$INIT_SCRIPT" ]]; then
    chmod +x "$INIT_SCRIPT"
    echo "⚙️  Adding initialization script..."
    #lxc exec $CONTAINER_NAME -- /bin/bash -c "echo '#!/bin/bash' > /etc/profile.d/init.sh && echo '$INIT_SCRIPT' >> /etc/profile.d/init.sh && chmod +x /etc/profile.d/init.sh"
    lxc exec $CONTAINER_NAME -- /bin/ash -c "apk add --no-cache bash zsh sudo git curl"

    lxc exec $CONTAINER_NAME -- /bin/ash -c "echo '#!/bin/ash' > /etc/profile.d/init.sh && echo 'source $INIT_SCRIPT' >> /etc/profile.d/init.sh && chmod +x /etc/profile.d/init.sh"
else
    echo "⚠️  Initialization script $INIT_SCRIPT does not exist! Please create it."
fi

# 7. Restart the container to apply changes
echo "🔄 Restarting container..."
lxc restart $CONTAINER_NAME

# 8. Check container status
sleep 2
lxc list $CONTAINER_NAME

echo "✅ Container $CONTAINER_NAME has been successfully set up and started!"

