# Valeurs par défaut
DEFAULT_PATH="~/.cutopia"
DEFAULT_CPATH="~/.cutopia"
DEFAULT_STARTUP_GROUP="cutopia"
DEFAULT_IMAGE="calpine"
DEFAULT_OPERATION="start"
DEFAULT_CONTAINER="calpinc"
DEFAULT_ENV_FILE="calpine"  # Valeurs autorisées : "calpine", "cutopia", "yats"
DEFAULT_PROFILE="admin"     # Valeurs autorisées : "guest", "user", "admin", "hermit", "root"

# Vérification des variables obligatoires
required_variables=(
  DEFAULT_PATH
  DEFAULT_CPATH
  DEFAULT_STARTUP_GROUP
  DEFAULT_IMAGE
  DEFAULT_OPERATION
  DEFAULT_CONTAINER
  DEFAULT_ENV_FILE
  DEFAULT_PROFILE
)

for var in "${required_variables[@]}"; do
  if [ -z "${!var}" ]; then
    echo "Erreur : la variable ${var} n'est pas définie ou est vide."
    exit 1
  fi
done

# Fonction pour afficher les variables
function display_variables() {
  # Définition des couleurs
  local green='\033[1;32m'
  local blue='\033[1;34m'
  local yellow='\033[1;33m'
  local reset='\033[0m'

  # Définition des icônes
  local path_icon="📂"
  local group_icon="👥"
  local image_icon="🖼️"
  local operation_icon="⚙️"
  local container_icon="📦"
  local envfile_icon="📄"
  local profile_icon="🧩"

  # Affichage des variables avec icônes et couleurs
  echo -e "${path_icon} ${green}myDockerfilePath${reset}: ${blue}$DEFAULT_PATH${reset}"
  echo -e "${group_icon} ${green}myStartupGroup${reset}: ${blue}$DEFAULT_STARTUP_GROUP${reset}"
  echo -e "${image_icon} ${green}myImage${reset}: ${blue}$DEFAULT_IMAGE${reset}"
  echo -e "${operation_icon} ${green}myOperation${reset}: ${blue}$DEFAULT_OPERATION${reset}"
  echo -e "${container_icon} ${green}myContainer${reset}: ${blue}$DEFAULT_CONTAINER${reset}"
  echo -e "${envfile_icon} ${green}myEnvFile${reset}: ${blue}$DEFAULT_ENV_FILE${reset}"
  echo -e "${profile_icon} ${green}myProfile${reset}: ${blue}$DEFAULT_PROFILE${reset}"
}

# Appel de la fonction
display_variables
