
#!/bin/zsh

# Deploy QuasarJS application to IPFS using cuipfs.zsh
# Author: [Your Name]

# Variables
APP_DIR="./dist/spa"         # Path to the built QuasarJS app
OUTPUT_DIR="./dist"          # Build output directory
CUIPFS_COMMAND="cuipfs.zsh"  # cuipfs.zsh command
GATEWAY="https://ipfs.io/ipfs" # Public gateway

# Step 1: Build the QuasarJS application
echo "Building QuasarJS application..."
quasar build || { echo "Build failed. Exiting."; exit 1; }

# Step 2: Navigate to the build directory
if [ ! -d "$APP_DIR" ]; then
    echo "Build directory not found at $APP_DIR. Exiting."
    exit 1
fi
cd "$APP_DIR"

# Step 3: Add the application to IPFS using cuipfs.zsh
echo "Adding application to IPFS..."
CID=$($CUIPFS_COMMAND add . --pin | tail -n 1 | awk '{print $NF}')

if [ -z "$CID" ]; then
    echo "Failed to add application to IPFS. Exiting."
    exit 1
fi
echo "Application added to IPFS with CID: $CID"

# Step 4: Print the public gateway link
APP_LINK="$GATEWAY/$CID"
echo "Your application is now deployed to IPFS!"
echo "Access it at: $APP_LINK"

# Optional: Store the CID for future reference
echo "$CID" > "$OUTPUT_DIR/ipfs_cid.txt"
echo "CID saved to $OUTPUT_DIR/ipfs_cid.txt"

# Step 5: Exit
exit 0
