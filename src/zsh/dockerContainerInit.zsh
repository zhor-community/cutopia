#!/bin/zsh

# Color definitions for output
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
CYAN='\033[0;36m'
RESET='\033[0m'

# Output symbols
SUCCESS="${GREEN}✔${RESET}"
ERROR="${RED}✖${RESET}"
INFO="${CYAN}ℹ${RESET}"

# Validate CALPINEPATH environment variable
if [[ -z "$CALPINEPATH" ]]; then
    echo -e "${ERROR} CALPINEPATH is not set. Please define it before running this script."
    exit 1
fi

# Tools to be installed
tools=("ssh" "ansible" "sudo")

# Docker container configuration
container_name="calpineAddTools"
image_name="debian:latest"

# Step 1: Create the Docker container
echo -e "${INFO} Creating Docker container: ${CYAN}${container_name}${RESET} using image ${CYAN}${image_name}${RESET}."
docker run -dit --name "$container_name" "$image_name" bash
if [[ $? -ne 0 ]]; then
    echo -e "${ERROR} Failed to create the Docker container."
    exit 1
fi
echo -e "${SUCCESS} Docker container ${CYAN}${container_name}${RESET} created successfully."

# Step 2: Install tools in the container
for tool in "${tools[@]}"; do
    echo -e "${INFO} Installing tool: ${CYAN}${tool}${RESET} in container ${CYAN}${container_name}${RESET}."
    docker exec "$container_name" zsh -c "CALPINEPATH=$CALPINEPATH; addTool -t $tool -e dockerContainer"
    if [[ $? -ne 0 ]]; then
        echo -e "${ERROR} Failed to install ${CYAN}${tool}${RESET}."
    else
        echo -e "${SUCCESS} Tool ${CYAN}${tool}${RESET} installed successfully."
    fi
done

# Step 3: Completion message
echo -e "${SUCCESS} Docker container initialization completed successfully."
