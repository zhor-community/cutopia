#!/usr/bin/env zsh
########################################################################
#                                                                      #
#                                                                      #
#                          container config                            #
#                           entrypoint.zsh                             #
#                                                                      #
########################################################################

# Fonction pour configurer un conteneur LXC
configure_container() {
    # Initialisation des variables avec des valeurs par défaut
    local cname="${1:-}" ifile="${2:-}" ofile="${3:-}" mem="${4:-512MB}" cpu="${5:-2}" apk="${6:-}" user="${7:-}" pass="${8:-}"

    if [[ -z "$cname" || -z "$ifile" || -z "$ofile" ]]; then
        echo "Erreur : Paramètres manquants pour configure_container."
        return 1
    fi

    echo "Configuration du conteneur $cname..."
    echo "Mémoire : $mem, CPU : $cpu"
    echo "Copie de $ifile vers $ofile"

    # Ajoutez ici la logique pour configurer le conteneur
}

# Fonction pour copier un dossier de l'hôte vers un conteneur LXC
copy_to_lxc() {
    local CONTAINER="$1" USER="$2" SRC_DIR="${3:-$HOME/.cutopia}" DEST_DIR="${4:-/home/$USER/.cutopia}"

    # Vérification si le dossier source existe
    if [[ ! -d "$SRC_DIR" ]]; then
        echo "Erreur : Le dossier source $SRC_DIR n'existe pas."
        return 1
    fi

    # Vérification si le conteneur LXC existe
    if ! lxc info "$CONTAINER" > /dev/null 2>&1; then
        echo "Erreur : Le conteneur LXC nommé $CONTAINER n'existe pas."
        return 1
    fi

    # Vérification si le conteneur est en cours d'exécution
    if ! lxc list "$CONTAINER" | grep -q "RUNNING"; then
        echo "Le conteneur $CONTAINER n'est pas en cours d'exécution. Démarrage..."
        if ! lxc start "$CONTAINER"; then
            echo "Erreur : Impossible de démarrer le conteneur."
            return 1
        fi
    fi

    # Vérification si le répertoire de destination existe dans le conteneur
    if ! lxc exec "$CONTAINER" -- test -d "$DEST_DIR"; then
        echo "Création du répertoire de destination $DEST_DIR dans le conteneur."
        if ! lxc exec "$CONTAINER" -- mkdir -p "$DEST_DIR"; then
            echo "Erreur : Impossible de créer le répertoire de destination dans le conteneur."
            return 1
        fi
    fi

    # Copier le dossier source vers le conteneur
    echo "Copie des fichiers du dossier $SRC_DIR vers le conteneur $CONTAINER dans $DEST_DIR..."
    if ! lxc file push "$SRC_DIR"/* "$CONTAINER$DEST_DIR" --recursive; then
        echo "Erreur : Échec de la copie des fichiers."
        return 1
    fi

    # Vérifier les permissions dans le conteneur et les ajuster si nécessaire
    echo "Vérification et ajustement des permissions dans le conteneur..."
    if ! lxc exec "$CONTAINER" -- chown -R "$USER:$USER" "$DEST_DIR"; then
        echo "Erreur : Impossible de modifier les permissions du répertoire dans le conteneur."
        return 1
    fi

    echo "Copie terminée avec succès vers $CONTAINER pour l'utilisateur $USER !"
    return 0
}

# Configuration des variables globales
userName="admin"
CUTOPIAPATH="/opt/cutopia"
source $CUTOPIAPATH/sec/env/lxc.container.env

# Chemin du dossier que vous voulez tester
#DIR="/home/$userName/$CUTOPIAPATH"
DIR="$CUTOPIAPATH"

# Vérification de l'existence du dossier
if [[ -d "$DIR" ]]; then
    echo "Le dossier $DIR existe."
    if [[ -f "$DIR/src/zsh/zshrc" && -f "$DIR/src/zsh/init.zsh" ]]; then
        source "$DIR/src/zsh/zshrc"
        source "$DIR/src/zsh/init.zsh"
    else
        echo "Les fichiers zshrc ou init.zsh sont manquants dans $DIR."
    fi
else
    echo "Le dossier $DIR n'existe pas."
fi

# Exemple d'utilisation de copy_to_lxc
# copy_to_lxc calpine admin /chemin/source /home/admin/mon_dossier
