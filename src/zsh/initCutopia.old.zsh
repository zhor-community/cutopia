
#!/bin/bash

# Inclusion des fonctions (si stockées dans un fichier séparé)
source /resources/zsh/initCutopiaFunctions.zsh

# Script principal
log_info "Démarrage de l'initialisation de Cutopia..."

check_root
check_internet
check_os_compatibility
update_system
install_packages "zsh git curl vim"

set_default_shell "/bin/zsh"
install_oh_my_zsh
apply_custom_zsh_config

setup_project_structure
install_project_dependencies
configure_environment_variables

verify_installation "zsh"
run_health_check

cleanup_temp_files
remove_unused_dependencies
reduce_image_size

log_success "Initialisation de Cutopia terminée avec succès."
exit 0
