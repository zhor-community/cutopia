
#!/bin/bash

# Default Configuration
DEFAULT_PATH="$HOME/.cutopia"
DEFAULT_IMAGE="calpine"
DEFAULT_CONTAINER="calpinc"
DEFAULT_ENV_FILE="$HOME/.cutopia/.env"
DEFAULT_PROFILE="admin"

# Variables
PATH_TO_FILE="$DEFAULT_PATH"
IMAGE="$DEFAULT_IMAGE"
CONTAINER="$DEFAULT_CONTAINER"
ENV_FILE="$DEFAULT_ENV_FILE"
PROFILE="$DEFAULT_PROFILE"
calpine(){
# Helper Function for Usage
usage() {
    echo "Usage: $0 -o [operation] [-f file] [-t tag] [-i image_id] [-s source_tag] [-d destination_tag] [-c container] [-e env_file]"
    echo "Operations:"
    echo "  create   - Build a Docker image."
    echo "  read     - List or inspect Docker images."
    echo "  update   - Tag a Docker image."
    echo "  delete   - Remove a Docker image or container."
    echo "  start    - Start a Docker container."
    echo "  upgrade  - Pull the latest version of an image."
    echo "Additional Options:"
    echo "  -f FILE      Path to Dockerfile (default: $DEFAULT_PATH)."
    echo "  -t TAG       Tag for Docker image (default: $DEFAULT_IMAGE)."
    echo "  -i ID        Image ID."
    echo "  -s TAG       Source tag for retagging."
    echo "  -d TAG       Destination tag for retagging."
    echo "  -c CONTAINER Name of the Docker container (default: $DEFAULT_CONTAINER)."
    echo "  -e ENV_FILE  Path to the environment file (default: $DEFAULT_ENV_FILE)."
    exit 1
}

# Validation Function
validate_variables() {
    if [ "$OPERATION" == "create" ] && [ ! -f "$PATH_TO_FILE" ]; then
        echo "Error: Dockerfile not found at $PATH_TO_FILE."
        exit 1
    fi

    if [ "$OPERATION" == "start" ] && [ ! -f "$ENV_FILE" ]; then
        echo "Error: Environment file not found at $ENV_FILE."
        exit 1
    fi

    if [[ "$OPERATION" == "create" || "$OPERATION" == "update" ]] && [ -z "$IMAGE" ]; then
        echo "Error: Image tag (-t) is required for 'create' and 'update' operations."
        exit 1
    fi

    if [[ "$OPERATION" == "start" || "$OPERATION" == "delete" ]] && [ -z "$CONTAINER" ]; then
        echo "Error: Container name (-c) is required for 'start' and 'delete' operations."
        exit 1
    fi

    echo "Validation successful. Proceeding with operation."
}

# Parse Arguments
while getopts "o:f:t:i:s:d:c:e:" opt; do
    case $opt in
        o) OPERATION=$OPTARG ;;
        f) PATH_TO_FILE=$OPTARG ;;
        t) IMAGE=$OPTARG ;;
        i) IMAGE_ID=$OPTARG ;;
        s) SOURCE_TAG=$OPTARG ;;
        d) DEST_TAG=$OPTARG ;;
        c) CONTAINER=$OPTARG ;;
        e) ENV_FILE=$OPTARG ;;
        *) usage ;;
    esac
done

# Validate Operation
if [ -z "$OPERATION" ]; then
    usage
fi

# Validate Variables
validate_variables

# CRUD Operations
case $OPERATION in
    create)
        echo "Building Docker image from $PATH_TO_FILE with tag $IMAGE..."
        docker build -f "$PATH_TO_FILE" -t "$IMAGE" .
        ;;
    read)
        if [ -z "$IMAGE_ID" ]; then
            echo "Listing Docker images..."
            docker images
        else
            echo "Inspecting Docker image $IMAGE_ID..."
            docker inspect "$IMAGE_ID"
        fi
        ;;
    update)
        echo "Tagging image $SOURCE_TAG as $DEST_TAG..."
        docker tag "$SOURCE_TAG" "$DEST_TAG"
        ;;
    delete)
        if docker ps -a --format "{{.Names}}" | grep -q "^$CONTAINER$"; then
            echo "Removing Docker container $CONTAINER..."
            docker rm -f "$CONTAINER"
        else
            echo "Removing Docker image $IMAGE_ID..."
            docker rmi "$IMAGE_ID"
        fi
        ;;
    start)
        echo "Starting Docker container $CONTAINER with environment file $ENV_FILE..."
        docker run --env-file "$ENV_FILE" --name "$CONTAINER" -d "$IMAGE"
        ;;
    upgrade)
        echo "Upgrading Docker image $IMAGE to the latest version..."
        docker pull "$IMAGE"
        ;;
    *)
        echo "Invalid operation: $OPERATION"
        usage
        ;;
esac
}
