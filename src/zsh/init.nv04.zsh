#!/bin/zsh
#chsh -s $(which zsh)
set -e  # Exit script on any error
set -u  # Prevent using undefined variables
set -o pipefail  # Exit if any command in a pipeline fails

# 🔹 Configuration
APP_PATH="/opt/cutopia"
LOG_FILE="/var/log/cutopia_init.log"
CALPINEPATH="$APP_PATH"

# 🔹 Utility functions
log() {
    echo -e "\e[1;34m[ℹ] $1\e[0m"
    echo "$(date '+%Y-%m-%d %H:%M:%S') [INFO] $1" >> "$LOG_FILE"
}

error_exit() {
    echo -e "\e[1;31m[✘] ERROR: $1\e[0m" >&2
    echo "$(date '+%Y-%m-%d %H:%M:%S') [ERROR] $1" >> "$LOG_FILE"
    exit 1
}

log "🛠 Starting system initialization..."

# 🔹 Verify application directory existence
if [ ! -d "$APP_PATH" ]; then
    error_exit "Directory $APP_PATH does not exist."
fi

# 🔹 Check and fix permissions
log "🔑 Checking and setting permissions..."
chown -R root:root "$APP_PATH"
chmod -R 755 "$APP_PATH"

# 🔹 Load configurations and scripts
log "📂 Loading configurations..."
source $CALPINEPATH/sec/env/calpine
source $CALPINEPATH/sec/env/cucli.env
source $CALPINEPATH/src/zsh/cuapk.zsh
source $CALPINEPATH/src/zsh/cuser.zsh
source $CALPINEPATH/src/zsh/cusudo.zsh
source $CALPINEPATH/src/zsh/cucli.zsh

# 🔹 Install required packages
log "📦 Installing required packages..."
cuapk add zsh curl bash jq git sudo

#apt update && sudo apt install -y locales
#locale-gen fr_FR.UTF-8
#update-locale LANG=fr_FR.UTF-8
#export LANG=fr_FR.UTF-8
#export LC_ALL=fr_FR.UTF-8

# 🔹 Install Cutopia components
log "⚙ Installing Cutopia components..."
CUCLI_COMPONENTS=("cunode" "curust" "cudit" "cuzsh" "cuipfs" "cuquasar")

for component in "${CUCLI_COMPONENTS[@]}"; do
    log "🔄 Installing $component..."
    cucli install "$component" || log "⚠ Failed to install $component."
done

# 🔹 Verify and start required services
log "🚀 Checking and starting necessary services..."
SERVICES=("docker" "redis" "couchdb" "ipfs")

for service in "${SERVICES[@]}"; do
    if systemctl is-active --quiet "$service"; then
        log "✅ Service $service is already running."
    else
        log "⏳ Starting service $service..."
        systemctl start "$service" && systemctl enable "$service" || log "⚠ Failed to start $service."
    fi
done

log "✅ Initialization completed successfully! 🎉"
