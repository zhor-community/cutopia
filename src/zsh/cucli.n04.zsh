❯ cat src/zsh/cucli.zsh
#!/bin/zsh

# ==========================================
# CUCLI - Command Line Interface for Cutopia
# ==========================================
# Description:
#   A modular command-line interface for managing Cutopia modules efficiently.
# 
# Author: AbdElHakim ZOUAÏ
# Email : abdelhakimzouai@gmail.com
# ==========================================


# Main function to parse and execute commands
function cucli() {
# Configuration files
source $CALPINEPATH/sec/env/calpine
ENV_FILE="$CALPINEPATH/sec/env/cucli.env"

# Load environment variables
if [[ -f "$ENV_FILE" ]]; then
    source "$ENV_FILE"
    echo -e "\033[32m✔️ [SUCCESS] Environment file loaded: $ENV_FILE\033[0m"
else
    echo -e "\033[31m❌ [ERROR] Environment file not found: $ENV_FILE\033[0m"
    exit 1
fi

# Check required dependencies
function check_dependencies() {
    # Required dependencies
    local dependencies=("jq" "zsh" "curl")

    # Check dependencies
    for dep in "${dependencies[@]}"; do
        if ! command -v "$dep" &> /dev/null; then
            echo -e "\033[31m❌ [ERROR] Missing dependency: $dep. Please install it before continuing.\033[0m"
            exit 1
        else
            echo -e "\033[32m✔️ [SUCCESS] Dependency found: $dep\033[0m"
        fi
    done

    # Confirmation message
    echo -e "\033[34m✅ [INFO] All required dependencies are installed.\033[0m"
}

# Load modules from the JSON file
function load_modules() {
    if [[ ! -f "$MODULES_JSON" ]]; then
        echo -e "\033[31m❌ [ERROR] JSON file for modules not found: $MODULES_JSON\033[0m"
        exit 1
    fi

    MODULES=$(jq -c '.modules[]' "$MODULES_JSON")
    if [[ -z "$MODULES" ]]; then
        echo -e "\033[31m❌ [ERROR] No modules found in JSON file: $MODULES_JSON\033[0m"
        exit 1
    fi

    echo -e "\033[32m✔️ [SUCCESS] Modules loaded from file: $MODULES_JSON\033[0m"
    echo -e "\033[34mℹ️ [INFO] Available modules:\033[0m"

    echo "$MODULES" | while IFS= read -r module; do
        name=$(echo "$module" | jq -r '.name // "Unknown"')
        description=$(echo "$module" | jq -r '.description // "No description available"')
        install_command=$(echo "$module" | jq -r '.install_command // "Not specified"')
        version=$(echo "$module" | jq -r '.version // "N/A"')
        author=$(echo "$module" | jq -r '.author // "Anonymous"')
        dependencies=$(echo "$module" | jq -r '.dependencies // [] | join(", ")')
        features=$(echo "$module" | jq -r '.features // [] | join(", ")')
        documentation=$(echo "$module" | jq -r '.documentation // "Not specified"')

        echo -e "\033[36m🛠️ Module: \033[0m$name"
        echo -e "\033[36m📝 Description: \033[0m$description"
        echo -e "\033[36m💻 Install Command: \033[0m$install_command"
        
        [[ "$version" != "N/A" ]] && echo -e "\033[36m🔖 Version: \033[0m$version"
        [[ "$author" != "Anonymous" ]] && echo -e "\033[36m✍️ Author: \033[0m$author"
        [[ -n "$dependencies" ]] && echo -e "\033[36m🔗 Dependencies: \033[0m$dependencies"
        [[ -n "$features" ]] && echo -e "\033[36m✨ Features: \033[0m$features"
        [[ "$documentation" != "Not specified" ]] && echo -e "\033[36m📚 Documentation: \033[0m$documentation"

        echo -e "\033[34m-----------------------------------------\033[0m"
    done
}

# Function: Display help menu
function show_help() {
    echo "Usage: cucli [command] [options]"
    echo ""
    
    echo "Available commands:"
    echo -e "  🔍 \033[1;33mlist\033[0m                \033[0;37mList all available modules\033[0m"
    echo -e "  ⬇️  \033[1;33minstall <module>\033[0m    \033[0;37mInstall a specific module\033[0m"
    echo -e "  ✅ \033[1;33menable <module>\033[0m      \033[0;37mEnable a specific module\033[0m"
    echo -e "  🚫 \033[1;33mdisable <module>\033[0m     \033[0;37mDisable a specific module\033[0m"
    echo -e "  ⚙️  \033[1;33mrun <module>\033[0m         \033[0;37mExecute the installation script of a module\033[0m"
    echo -e "  ℹ️  \033[1;33mhelp\033[0m                \033[0;37mDisplay this help menu\033[0m"
}

# Function: Modify the 'included' status of a module
function modify_inclusion() {
    local module_name="$1"
    local new_status="$2"

    if ! echo "$MODULES" | jq -e --arg name "$module_name" '.modules[] | select(.name == $name)' > /dev/null; then
        echo -e "❌ \033[1;31mError:\033[0m Module '\033[1;33m$module_name\033[0m' not found."
        exit 1
    fi

    jq --arg name "$module_name" --argjson status "$new_status" '
        .modules |= map(if .name == $name then .included = $status else . end)
    ' "$MODULES_JSON" > "$MODULES_JSON.tmp" && mv "$MODULES_JSON.tmp" "$MODULES_JSON"

    if [ "$new_status" = true ]; then
        echo -e "✅ \033[1;32mSuccess:\033[0m Module '\033[1;33m$module_name\033[0m' has been enabled."
    else
        echo -e "🚫 \033[1;32mSuccess:\033[0m Module '\033[1;33m$module_name\033[0m' has been disabled."
    fi
}

function enable_module() {
    modify_inclusion "$1" true
}

function disable_module() {
    modify_inclusion "$1" false
}

install_module() {
  local module_name="$1"
  local json_file=$MODULES_JSON
  
  local install_command=$(jq -r ".modules[] | select(.name == \"$module_name\") | .install_command" "$json_file")

  if [[ -z "$install_command" ]]; then
    echo "Module $module_name not found."
    return 1
  fi

  echo "Installing $module_name..."
  eval "$install_command"
}

# Main command parsing
local command="$1"
shift

if [[ ! -f "$ENV_FILE" ]]; then
    echo -e "❌ \033[1;31mError:\033[0m Environment file not found: \033[1;33m$ENV_FILE\033[0m"
    exit 1
fi
source "$ENV_FILE"

check_dependencies
load_modules

sudo mkdir -p "$LOG_DIR"

case "$command" in
    list)
        echo -e "📂 \033[1;34mAvailable Modules:\033[0m"
        echo "$MODULES" | jq -r '.modules[] | "🛠️  \(.name) - \(.description) [Included: \(.included)]"'
        ;;
    install)
        if [[ "$1" == "--all" ]]; then
            echo -e "🔄 Installing all included modules..."
            for module in $(jq -r '.modules[] | select(.included == true) | .name' "$MODULES_JSON"); do
                install_module "$module"
            done
        elif [[ -z "$1" ]]; then
            echo -e "❌ \033[1;31mError:\033[0m Module name required for \033[1;33minstall\033[0m."
            exit 1
        else
            install_module "$1"
        fi
        ;;
    enable)
        if [[ -z "$1" ]]; then
            echo -e "❌ \033[1;31mError:\033[0m Module name required for \033[1;33menable\033[0m."
            exit 1
        fi
        enable_module "$1"
        ;;
    disable)
        if [[ -z "$1" ]]; then
            echo -e "❌ \033[1;31mError:\033[0m Module name required for \033[1;33mdisable\033[0m."
            exit 1
        fi
        disable_module "$1"
        ;;
    run)
        if [[ -z "$1" ]]; then
            echo -e "❌ \033[1;31mError:\033[0m Module name required for \033[1;33mrun\033[0m."
            exit 1
        fi
        install_module "$1"
        ;;
    help|*)
        show_help
        ;;
esac
}
