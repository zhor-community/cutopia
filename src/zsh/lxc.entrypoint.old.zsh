❯ cat src/zsh/lxc.entrypoint.zsh
#!/bin/bash
########################################################################
#                                                                      #
#                                                                      #
#                          container config                            #
#                           entrypoint.zsh                             #
#                                                                      #
########################################################################

# Utilisation de la fonction
# configure_container "nom_du_conteneur" "/chemin/vers/fichier.txt" "/chemin/destination/fichier.txt" "512MB" "2" "vim curl" "myuser" "password"
#configure_container --cname calpine --ifile .cutopia/ --ofile /root/monfichier --mem 1GB --cpu 4 --apk "nginx curl" --user admin --pass admin123


configure_container() {
    # Initialisation des variables avec des valeurs par défaut
}
ls
username=admin
CALPINE=.cutopia
# Chemin du dossier que tu veux tester
source lxc.container.env
DIR="/home/$username/$CALPINE"

# Vérification de l'existence du dossier
if [ -d "$DIR" ]; then
  echo "Le dossier $DIR existe."
  source $DIR/cugo/src/zsh/zshrc
  source $DIR/cugo/src/zsh/init.zsh
else
  echo "Le dossier $DIR n'existe pas."
fi

#!/bin/bash

# Fonction pour copier un dossier de l'hôte vers un conteneur LXC
copy_to_lxc() {
  # Paramètres : $1 = nom du conteneur, $2 = utilisateur cible dans le conteneur, $3 = dossier source (facultatif), $4 = dossier destination (facultatif)
  CONTAINER="$1"
  USER="$2"
  SRC_DIR="${3:-$HOME/.cutopia}"  # Utilisation du dossier par défaut si non spécifié
  DEST_DIR="${4:-/home/$USER/.cutopia}"  # Utilisation du dossier par défaut si non spécifié

  # Vérification si le dossier source existe
  if [ ! -d "$SRC_DIR" ]; then
    echo "Erreur : Le dossier source $SRC_DIR n'existe pas."
    return 1
  fi

  # Vérification si le conteneur LXC existe
  if ! lxc info "$CONTAINER" > /dev/null 2>&1; then
    echo "Erreur : Le conteneur LXC nommé $CONTAINER n'existe pas."
    return 1
  fi

  # Vérification si le conteneur est en cours d'exécution
  if ! lxc list "$CONTAINER" | grep -q "RUNNING"; then
    echo "Erreur : Le conteneur $CONTAINER n'est pas en cours d'exécution."
    echo "Démarrage du conteneur..."
    lxc start "$CONTAINER"
    if [ $? -ne 0 ]; then
      echo "Erreur : Impossible de démarrer le conteneur."
      return 1
    fi
  fi

  # Vérification si le répertoire de destination existe dans le conteneur
  if ! lxc exec "$CONTAINER" -- test -d "$DEST_DIR"; then
    echo "Création du répertoire de destination $DEST_DIR dans le conteneur."
    lxc exec "$CONTAINER" -- mkdir -p "$DEST_DIR"
    if [ $? -ne 0 ]; then
      echo "Erreur : Impossible de créer le répertoire de destination dans le conteneur."
      return 1
    fi
  fi

  # Copier le dossier source vers le conteneur
  echo "Copie des fichiers du dossier $SRC_DIR vers le conteneur $CONTAINER dans $DEST_DIR..."
  lxc file push "$SRC_DIR"/* "$CONTAINER$DEST_DIR" --recursive
  if [ $? -ne 0 ]; then
    echo "Erreur : Échec de la copie des fichiers."
    return 1
  fi

  # Vérifier les permissions dans le conteneur et les ajuster si nécessaire
  echo "Vérification et ajustement des permissions dans le conteneur..."
  lxc exec "$CONTAINER" -- chown -R "$USER:$USER" "$DEST_DIR"
  lxc exec "$CONTAINER" -- chmod -R 755 "$DEST_DIR"
  if [ $? -ne 0 ]; then
    echo "Erreur : Impossible de modifier les permissions du répertoire dans le conteneur."
    return 1
  fi

  echo "Copie terminée avec succès vers $CONTAINER pour l'utilisateur $USER !"
  return 0
}

# Utilisation de la fonction avec des paramètres
# Exemple d'utilisation : copy_to_lxc calpine admin
#copy_to_lxc "$@"
#./lxc_copy_cutopia.sh calpine admin /chemin/source /home/admin/mon_dossier

