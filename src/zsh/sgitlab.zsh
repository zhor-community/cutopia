function setup_gitlab_ssh() {
    # Variables par défaut
    local ssh_key_path="$HOME/.ssh/id_ed25519"
    local ssh_pub_key_path="$ssh_key_path.pub"
    local email=""
    local key_type="ed25519" # Par défaut, utiliser Ed25519
    local test_only=false

    # Analyse des options avec getopts
    while getopts ":e:k:t:a:" opt; do
        case ${opt} in
            e ) # Option pour spécifier l'email
                email=$OPTARG
                ;;
            k ) # Option pour spécifier le chemin de la clé SSH
                ssh_key_path=$OPTARG
                ssh_pub_key_path="${ssh_key_path}.pub"
                ;;
            t ) # Option pour tester uniquement la connexion SSH
                test_only=true
                ;;
            a ) # Option pour spécifier le type de clé (rsa, ecdsa, ed25519)
                key_type=$OPTARG
                ;;
            \? ) # Gestion des options incorrectes
                echo "Option invalide : -$OPTARG" >&2
                return 1
                ;;
        esac
    done

    # Si le mode test-only est activé, sauter directement au test
    if [[ "$test_only" == true ]]; then
        echo "Test de la connexion avec GitLab..."
        ssh -T git@gitlab.com
        return
    fi

    # Étape 1 : Vérifier si une clé SSH existe déjà
    if [[ -f $ssh_pub_key_path ]]; then
        echo "Une clé SSH existe déjà à $ssh_pub_key_path."
        echo "Voici votre clé publique :"
        cat $ssh_pub_key_path
        echo ""
        echo "Veuillez copier cette clé dans vos paramètres GitLab (Settings > SSH Keys)."
    else
        # Étape 2 : Générer une nouvelle clé SSH
        if [[ -z "$email" ]]; then
            read -p "Entrez votre adresse e-mail pour la clé SSH (par exemple, user@example.com) : " email
        fi
        if [[ -z "$email" ]]; then
            echo "Erreur : L'email ne peut pas être vide." >&2
            return 1
        fi

        echo "Aucune clé SSH trouvée. Génération d'une nouvelle clé ($key_type)..."
        if [[ "$key_type" == "rsa" ]]; then
            ssh-keygen -t rsa -b 4096 -C "$email" -f $ssh_key_path
        elif [[ "$key_type" == "ecdsa" ]]; then
            ssh-keygen -t ecdsa -b 256 -C "$email" -f $ssh_key_path
        else
            ssh-keygen -t ed25519 -C "$email" -f $ssh_key_path
        fi

        # Étape 3 : Ajouter la clé SSH à l'agent SSH
        echo "Ajout de la clé SSH à l'agent..."
        eval "$(ssh-agent -s)"
        ssh-add $ssh_key_path

        # Étape 4 : Afficher la clé publique
        echo "Votre nouvelle clé publique est prête :"
        cat $ssh_pub_key_path
        echo ""
        echo "Veuillez copier cette clé dans vos paramètres GitLab (Settings > SSH Keys)."
    fi

    # Étape 5 : Tester la connexion avec GitLab
    echo "Test de la connexion avec GitLab..."
    ssh -T git@gitlab.com
}
mkdir -p sec/{docker,vps,cuipfs,runia,lxc}/ssh
# Generate an RSA key for Docker
setup_gitlab_ssh -a rsa -e "abdelhakimzouai@gmail.com" -k "sec/docker/ssh/id_rsa"

# Generate an ECDSA key for VPS
setup_gitlab_ssh -a ecdsa -e "abdelhakimzouai@gmail.com" -k "sec/vps/ssh/id_ecdsa"

# Generate an ED25519 key for cuIPFS
setup_gitlab_ssh -a ed25519 -e "abdelhakimzouai@gmail.com" -k "sec/cuipfs/ssh/id_ed25519"

# Generate an ED25519 key for Runia
setup_gitlab_ssh -a ed25519 -e "abdelhakimzouai@gmail.com" -k "sec/runia/ssh/id_ed25519"

# Generate an ED25519 key for LXC
setup_gitlab_ssh -a ed25519 -e "abdelhakimzouai@gmail.com" -k "sec/lxc/ssh/id_ed25519"
