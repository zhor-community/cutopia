vps-init/
├── init.sh # Script principal
├── config.sh # Configuration globale
├── modules/ # Répertoire des modules
│ ├── 01_system_update.sh
│ ├── 02_create_user.sh
│ ├── 03_firewall.sh
│ ├── 04_ssh_hardening.sh
│ └── 05_docker_install.sh
└── README.md # Documentation
