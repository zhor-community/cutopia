#!/usr/bin/env sh
# Configuration centralisée - RFC 822 compliant

# Paramètres système
export LANG="en_US.UTF-8"
export TZ="Europe/Paris"

# Chemins critiques
export LOCKFILE="/var/lock/sysinit.lock"
export BACKUP_DIR="/var/backups/sysinit"
export LOG_DIR="/var/log/sysinit"

# Politique de sécurité
export FAILURE_MODE="strict" # strict|skip
export FIREWALL_PORTS="22 80 443"
export SSH_PORT="6022"

# Utilisateurs
declare -A SYS_USERS=(
  ["admin"]="sudo:ssh:docker"
  ["audit"]="log:monitor"
)

# Dépendances
export REQUIRED_PACKAGES="
  apk:openssh nftables fail2ban
  apt:openssh-server nftables fail2ban
"
