#!/usr/bin/env bash
set -eo pipefail
shopt -s nullglob

#█████████████████████████████████████████████████████████████████████████████████
# INIT.SH - System Initialization Framework v2.3.1
# Conforme aux standards CIS, NIST et RFC 2119
#█████████████████████████████████████████████████████████████████████████████████

# Configuration (à personnaliser avant déploiement)
readonly DEPLOY_ENV="production"             # [production|staging|development]
readonly DEFAULT_USER="sysadmin"             # Utilisateur privilégié
readonly SSH_PORT="6022"                     # Port SSH personnalisé
readonly SYS_FIREWALL_PORTS=(22 6022 80 443 51820) # Ports ouverts (SSH, HTTP, HTTPS, WireGuard)

# Constantes système (ne pas modifier)
readonly SCRIPT_NAME="System Initializer"
readonly VERSION="2.3.1"
readonly LOCK_FILE="/var/lock/${SCRIPT_NAME}.lock"
readonly LOG_FILE="/var/log/${SCRIPT_NAME}.log"
readonly CONFIG_DIR="/etc/sysinit"
readonly BACKUP_DIR="${CONFIG_DIR}/backups"
readonly REQUIRED_PACKAGES=(
    "curl" "git" "jq" "gnupg" "unzip"
    "nftables" "fail2ban" "openssh-server"
)

#█████████████████████████████████████████████████████████████████████████████████
# CORE FUNCTIONS
#█████████████████████████████████████████████████████████████████████████████████

init::logger() {
    local level="$1" message="$2"
    local timestamp
    timestamp=$(date --iso-8601=seconds)
    
    case "$level" in
        "SUCCESS") color="\033[1;32m" ;;
        "ERROR")   color="\033[1;31m" ;;
        "WARN")    color="\033[1;33m" ;;
        "INFO")    color="\033[1;34m" ;;
        *)         color="\033[0m"    ;;
    esac

    echo -e "${color}[${timestamp}] ${level}: ${message}\033[0m" | tee -a "$LOG_FILE"
}

init::validate_environment() {
    [[ "$EUID" -eq 0 ]] || { init::logger "ERROR" "Exécution requise en tant que root"; exit 1; }
    [[ -f "$LOCK_FILE" ]] && { init::logger "ERROR" "Déjà exécuté - Supprimer $LOCK_FILE pour forcer"; exit 1; }
    
    local supported_distros=("debian" "ubuntu" "alpine")
    [[ " ${supported_distros[*]} " =~ $(. /etc/os-release; echo "$ID") ]] || {
        init::logger "ERROR" "Distribution non supportée"; exit 1;
    }
}

init::setup_directories() {
    mkdir -vp "$CONFIG_DIR"/{secrets,backups,state} | while read -r line; do
        init::logger "INFO" "Création: $line"
    done
    chmod 0700 "$CONFIG_DIR"/secrets
}

#█████████████████████████████████████████████████████████████████████████████████
# SECURITY MODULES
#█████████████████████████████████████████████████████████████████████████████████

init::configure_firewall() {
    local nft_conf="/etc/nftables.conf"
    
    cp -f "$nft_conf" "${BACKUP_DIR}/nftables.conf.bak-$(date +%s)"
    
    cat > "$nft_conf" <<EOF
#!/usr/sbin/nft -f
flush ruleset

table inet filter {
    chain input {
        type filter hook input priority 0; policy drop;
        
        # Connexions établies
        ct state established,related accept
        
        # Loopback
        iif lo accept
        
        # ICMP
        ip protocol icmp accept
        ip6 nexthdr ipv6-icmp accept
        
        # SSH
        tcp dport ${SSH_PORT} accept
        
        # HTTP/HTTPS
        tcp dport {80, 443} accept
        
        # WireGuard
        udp dport 51820 accept
        
        counter drop
    }
    
    chain forward {
        type filter hook forward priority 0; policy drop;
    }
    
    chain output {
        type filter hook output priority 0; policy accept;
    }
}
EOF

    nft -f "$nft_conf"
    systemctl enable --now nftables
    init::logger "SUCCESS" "Firewall nftables configuré"
}

init::create_privileged_user() {
    local ssh_key="ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAI..."
    
    if ! id "$DEFAULT_USER" &>/dev/null; then
        useradd -m -s /bin/bash -G sudo "$DEFAULT_USER"
        usermod -aG adm,dialout,cdrom,floppy,audio,dip,video,plugdev "$DEFAULT_USER"
        init::logger "INFO" "Utilisateur $DEFAULT_USER créé"
    fi

    mkdir -p "/home/${DEFAULT_USER}/.ssh"
    echo "$ssh_key" > "/home/${DEFAULT_USER}/.ssh/authorized_keys"
    chmod 600 "/home/${DEFAULT_USER}/.ssh/authorized_keys"
    chown -R "${DEFAULT_USER}:${DEFAULT_USER}" "/home/${DEFAULT_USER}/.ssh"
}

#█████████████████████████████████████████████████████████████████████████████████
# SERVICE CONFIGURATION
#█████████████████████████████████████████████████████████████████████████████████

init::configure_ssh() {
    local sshd_conf="/etc/ssh/sshd_config"
    cp -f "$sshd_conf" "${BACKUP_DIR}/sshd_config.bak-$(date +%s)"
    
    declare -A ssh_settings=(
        ["Port"]="$SSH_PORT"
        ["PermitRootLogin"]="no"
        ["PasswordAuthentication"]="no"
        ["KbdInteractiveAuthentication"]="no"
        ["UsePAM"]="yes"
        ["X11Forwarding"]="no"
        ["AllowAgentForwarding"]="no"
        ["PermitEmptyPasswords"]="no"
        ["ClientAliveInterval"]="300"
        ["MaxAuthTries"]="2"
    )

    for key in "${!ssh_settings[@]}"; do
        if grep -q "^#*${key}" "$sshd_conf"; then
            sed -i "s/^#*${key}.*/${key} ${ssh_settings[$key]}/" "$sshd_conf"
        else
            echo "${key} ${ssh_settings[$key]}" >> "$sshd_conf"
        fi
    done

    systemctl restart sshd
    init::logger "SUCCESS" "SSH durci (port ${SSH_PORT})"
}

init::setup_ipfs() {
    local ipfs_dir="/var/lib/ipfs"
    
    if [[ ! -d "$ipfs_dir" ]]; then
        curl -sL https://dist.ipfs.tech/kubo/v0.23.0/install.sh | bash
        sysctl -w net.core.rmem_max=2097152
        sysctl -w net.core.wmem_max=2097152
    fi

    sudo -u _ipfs ipfs init --profile=server
    systemctl enable --now ipfs
    init::logger "SUCCESS" "Noeud IPFS initialisé"
}

#█████████████████████████████████████████████████████████████████████████████████
# MAIN EXECUTION FLOW
#█████████████████████████████████████████████████████████████████████████████████

main() {
    exec {lock_fd}>"$LOCK_FILE"
    flock -n "$lock_fd" || { init::logger "ERROR" "Processus déjà en cours"; exit 1; }
    
    trap '{
        flock -u "$lock_fd"
        rm -f "$LOCK_FILE"
        init::logger "INFO" "Nettoyage des ressources système"
    }' EXIT

    init::validate_environment
    init::setup_directories
    
    # Workflow principal
    declare -a execution_flow=(
        init::configure_firewall
        init::create_privileged_user
        init::configure_ssh
        init::setup_ipfs
    )

    for task in "${execution_flow[@]}"; do
        if ! "$task"; then
            init::logger "ERROR" "Échec de la tâche: ${task}"
            exit 1
        fi
    done

    touch "$LOCK_FILE"
    init::logger "SUCCESS" "Initialisation système terminée"
    exit 0
}

main "$@"
