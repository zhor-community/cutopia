#!/bin/bash

# Vérification des privilèges root
if [[ $EUID -ne 0 ]]; then
    echo "Ce script doit être exécuté en tant que root. Essayez : sudo $0"
    exit 1
fi

# Variables
USER_NAME=${SUDO_USER:-$(whoami)}
LXC_CONFIG_DIR="/home/$USER_NAME/.config/lxc"
DEFAULT_CONF="$LXC_CONFIG_DIR/default.conf"

echo "🔧 Mise à jour du système..."
apt update

echo "🛠️ Installation de LXC..."
apt install -y lxc lxd

echo "🛠️ Configuration des namespaces utilisateurs..."
echo "$USER_NAME:100000:65536" | tee -a /etc/subuid /etc/subgid

echo "📁 Création du dossier de configuration LXC..."
mkdir -p "$LXC_CONFIG_DIR"

echo "✍️ Configuration des UID/GID mappings..."
cat <<EOF > "$DEFAULT_CONF"
lxc.include = /etc/lxc/default.conf
lxc.idmap = u 0 100000 65536
lxc.idmap = g 0 100000 65536
EOF
chown -R "$USER_NAME:$USER_NAME" "$LXC_CONFIG_DIR"

echo "💾 Vérification du stockage..."
if ! lxc storage list | grep -q default; then
    echo "📦 Création du pool de stockage LXC..."
    lxc storage create default dir
fi

echo "🚀 Téléchargement et création d'un conteneur Ubuntu 20.04..."
lxc launch images:ubuntu/20.04 container_ubuntu

echo "✅ Installation et configuration terminées !"
echo "📌 Pour voir le statut : lxc list"
echo "📌 Pour entrer dans le conteneur : lxc exec container_ubuntu -- /bin/bash"

