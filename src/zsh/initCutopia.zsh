#!/bin/bash

# Script pour installer tous les modules de Cutopia

# Chemin du fichier cucli.zsh
CUCLI_ZSH_PATH="/resources/zsh/cucli.zsh"

# Vérifier si cucli.zsh existe
if [[ ! -f "$CUCLI_ZSH_PATH" ]]; then
  echo "❌ [ERROR] Le fichier cucli.zsh est introuvable : $CUCLI_ZSH_PATH"
  exit 1
fi

# Lister les modules depuis le fichier JSON
MODULES_JSON="/resources/json/modules.json"

if [[ ! -f "$MODULES_JSON" ]]; then
  echo "❌ [ERROR] Le fichier modules.json est introuvable : $MODULES_JSON"
  exit 1
fi

# Extraire les modules à partir du fichier JSON
modules=$(jq -r '.modules[] | select(.included == true) | .name' "$MODULES_JSON")

# Installer chaque module
for module in $modules; do
  echo "📦 Installation du module : $module"
  cucli install "$module"
done

echo "✅ Tous les modules ont été installés avec succès !"
