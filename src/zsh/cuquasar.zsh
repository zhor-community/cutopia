#!/bin/zsh
# cuquasar.zsh - Script for managing Quasar.js applications

echo "🔄 Setting up Quasar.js environment..."

# Install Quasar CLI globally if not already installed
if ! command -v quasar &>/dev/null; then
    echo "⚙️ Installing Quasar CLI..."
    npm install -g @quasar/cli || {
        echo "❌ Failed to install Quasar CLI. Ensure npm is installed and try again."
        exit 1
    }
else
    echo "✅ Quasar CLI is already installed."
fi

# Check for a Quasar project in the current directory
if [[ -f "quasar.conf.js" ]]; then
    echo "📂 Quasar project detected. Building the project..."
    quasar build || {
        echo "❌ Failed to build the project. Check your Quasar configuration."
        exit 1
    }

    echo "🚀 Deploying Quasar application..."
    # Example: Deploy the built files to /var/www/html
    sudo cp -r dist/spa/* /var/www/html/ || {
        echo "❌ Failed to deploy the application."
        exit 1
    }
    echo "✅ Application deployed successfully to /var/www/html/."
else
    echo "❌ No Quasar project detected in the current directory."
    exit 1
fi


