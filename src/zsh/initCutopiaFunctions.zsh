#!/bin/zsh

# Chargement des icônes et des couleurs
source /resources/zsh/iconlors.zsh

# ==========================================
# Vérifie si le script est exécuté en tant que root
# ==========================================
function check_root() {
  if [[ $EUID -ne 0 ]]; then
    log_error "${ICON_ERROR} Ce script doit être exécuté avec des privilèges root. Veuillez réessayer avec sudo ou en tant qu'administrateur."
    exit 1
  else
    log_success "${ICON_SUCCESS} Droits administratifs confirmés."
  fi
}

# ==========================================
# Vérifie la connectivité Internet
# ==========================================
function check_internet() {
  local test_url="https://www.google.com"
  if curl -s --head --request GET $test_url | grep "200 OK" > /dev/null; then
    log_success "${ICON_NETWORK} Connectivité Internet vérifiée avec succès."
  else
    log_error "${ICON_ERROR} Impossible de se connecter à Internet. Veuillez vérifier votre connexion réseau."
    exit 1
  fi
}

# ==========================================
# Vérifie la disponibilité des outils essentiels
# ==========================================
function check_dependencies() {
  local dependencies=("zsh" "curl" "git")
  local missing=()

  for dep in "${dependencies[@]}"; do
    if ! command -v $dep &> /dev/null; then
      missing+=($dep)
    fi
  done

  if [[ ${#missing[@]} -gt 0 ]]; then
    log_warning "${ICON_WARNING} Les dépendances suivantes sont manquantes : ${missing[*]}"
    log_info "${ICON_INFO} Veuillez les installer avant de continuer."
    exit 1
  else
    log_success "${ICON_CHECK} Toutes les dépendances nécessaires sont installées."
  fi
}

# ==========================================
# Vérifie la compatibilité avec le système d'exploitation
# ==========================================
function check_os_compatibility() {
  local supported_os="Alpine"
  local os_name=$(cat /etc/os-release | grep -oP '^NAME="\K[^"]+')

  if [[ "$os_name" == *"$supported_os"* ]]; then
    log_success "${ICON_OK} Système d'exploitation compatible : $os_name"
  else
    log_error "${ICON_ERROR} Système d'exploitation incompatible : $os_name"
    log_info "${ICON_INFO} Ce script est conçu pour fonctionner sur $supported_os."
    exit 1
  fi
}

# ==========================================
# Appel des fonctions de vérification
# ==========================================
function run_prechecks() {
  log_info "${ICON_CLOCK} Début des vérifications préliminaires..."
  check_root
  check_internet
  check_dependencies
  check_os_compatibility
  log_success "${ICON_SUCCESS} Toutes les vérifications préliminaires ont été effectuées avec succès !"
}

# Lancer les vérifications
run_prechecks
# ==========================================
# Met à jour les dépôts de paquets
# ==========================================
function update_system() {
  log_info "${ICON_CLOCK} Mise à jour des dépôts de paquets en cours..."
  if apk update &> /dev/null; then
    log_success "${ICON_SUCCESS} Les dépôts de paquets ont été mis à jour avec succès."
  else
    log_error "${ICON_ERROR} Échec de la mise à jour des dépôts. Veuillez vérifier les logs."
    exit 1
  fi
}

# ==========================================
# Installe une liste de paquets
# ==========================================
function install_packages() {
  local packages=("$@")
  
  if [[ ${#packages[@]} -eq 0 ]]; then
    log_warning "${ICON_WARNING} Aucun paquet spécifié pour l'installation."
    return 1
  fi

  log_info "${ICON_CLOCK} Installation des paquets : ${packages[*]}"
  if apk add --no-cache "${packages[@]}" &> /dev/null; then
    log_success "${ICON_SUCCESS} Les paquets suivants ont été installés avec succès : ${packages[*]}"
  else
    log_error "${ICON_ERROR} Échec de l'installation des paquets : ${packages[*]}. Veuillez vérifier les logs."
    exit 1
  fi
}

# ==========================================
# Supprime les paquets inutiles
# ==========================================
function remove_unnecessary_packages() {
  local packages=("$@")
  
  if [[ ${#packages[@]} -eq 0 ]]; then
    log_warning "${ICON_WARNING} Aucun paquet spécifié pour la suppression."
    return 1
  fi

  log_info "${ICON_CLOCK} Suppression des paquets inutiles : ${packages[*]}"
  if apk del --no-cache "${packages[@]}" &> /dev/null; then
    log_success "${ICON_SUCCESS} Les paquets suivants ont été supprimés avec succès : ${packages[*]}"
  else
    log_error "${ICON_ERROR} Échec de la suppression des paquets : ${packages[*]}. Veuillez vérifier les logs."
    exit 1
  fi
}

# ==========================================
# Exemple d'exécution
# ==========================================
function manage_packages() {
  log_info "${ICON_CLOCK} Début de la gestion des paquets..."

  # Étape 1 : Mise à jour des dépôts
  update_system

  # Étape 2 : Installation des paquets nécessaires
  local required_packages=("zsh" "curl" "git")
  install_packages "${required_packages[@]}"

  # Étape 3 : Suppression des paquets inutiles
  local unnecessary_packages=("example-package")
  remove_unnecessary_packages "${unnecessary_packages[@]}"

  log_success "${ICON_SUCCESS} Gestion des paquets terminée avec succès !"
}

# ==========================================
# Définit un shell spécifique comme shell par défaut
# ==========================================
function set_default_shell() {
  local shell_path=$1

  if [[ -z $shell_path ]]; then
    log_error "${ICON_ERROR} Aucun chemin de shell spécifié."
    return 1
  fi

  if ! grep -q "$shell_path" /etc/shells; then
    log_error "${ICON_ERROR} $shell_path n'est pas un shell valide ou n'est pas enregistré dans /etc/shells."
    return 1
  fi

  log_info "${ICON_CLOCK} Définition de $shell_path comme shell par défaut..."
  if chsh -s "$shell_path" &> /dev/null; then
    log_success "${ICON_SUCCESS} $shell_path est maintenant le shell par défaut."
  else
    log_error "${ICON_ERROR} Échec de la définition de $shell_path comme shell par défaut."
    return 1
  fi
}

# ==========================================
# Installe Oh My Zsh et applique les configurations nécessaires
# ==========================================
function install_oh_my_zsh() {
  log_info "${ICON_CLOCK} Installation de Oh My Zsh..."
  if [[ -d "$HOME/.oh-my-zsh" ]]; then
    log_warning "${ICON_WARNING} Oh My Zsh est déjà installé. Ignoré."
    return 0
  fi

  if curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh | zsh &> /dev/null; then
    log_success "${ICON_SUCCESS} Oh My Zsh a été installé avec succès."
  else
    log_error "${ICON_ERROR} Échec de l'installation de Oh My Zsh."
    return 1
  fi
}

# ==========================================
# Applique un fichier .zshrc personnalisé ou une configuration spécifique
# ==========================================
function apply_custom_zsh_config() {
  local custom_zshrc_path=$1

  if [[ -z $custom_zshrc_path || ! -f $custom_zshrc_path ]]; then
    log_error "${ICON_ERROR} Fichier de configuration .zshrc non trouvé ou non spécifié."
    return 1
  fi

  log_info "${ICON_CLOCK} Application de la configuration personnalisée depuis $custom_zshrc_path..."
  if cp "$custom_zshrc_path" "$HOME/.zshrc" &> /dev/null; then
    log_success "${ICON_SUCCESS} Configuration personnalisée appliquée avec succès."
  else
    log_error "${ICON_ERROR} Échec de l'application de la configuration personnalisée."
    return 1
  fi
}

# ==========================================
# Exemple d'exécution des fonctions
# ==========================================
function configure_shell() {
  log_info "${ICON_CLOCK} Début de la configuration du shell..."

  # Étape 1 : Définir Zsh comme shell par défaut
  local zsh_path="/bin/zsh"
  set_default_shell "$zsh_path"

  # Étape 2 : Installer Oh My Zsh
  install_oh_my_zsh

  # Étape 3 : Appliquer une configuration personnalisée pour Zsh
  local custom_zshrc="/chemin/vers/votre/zshrc_personnalise"
  apply_custom_zsh_config "$custom_zshrc"

  log_success "${ICON_SUCCESS} Configuration du shell terminée avec succès !"
}

# Lancer la configuration du shell
configure_shell

# ==========================================
# Crée les répertoires nécessaires pour le projet
# ==========================================
function setup_project_structure() {
  log_info "${ICON_CLOCK} Création de la structure des répertoires pour le projet Cutopia..."

  local project_dirs=("/var/cutopia" "/resources" "/resources/logs" "/resources/configs")

  for dir in "${project_dirs[@]}"; do
    if [[ ! -d $dir ]]; then
      mkdir -p "$dir"
      log_success "${ICON_FOLDER} Répertoire créé : $dir"
    else
      log_warning "${ICON_WARNING} Répertoire déjà existant : $dir"
    fi
  done

  log_success "${ICON_SUCCESS} Structure des répertoires pour Cutopia créée avec succès."
}

# ==========================================
# Installe les bibliothèques ou outils spécifiques au projet
# ==========================================
function install_project_dependencies() {
  log_info "${ICON_CLOCK} Installation des dépendances du projet Cutopia..."

  # Paquets requis pour Cutopia (Python, Node.js, etc.)
  local dependencies=("python3" "py3-pip" "nodejs" "npm")

  if apk add --no-cache "${dependencies[@]}" &> /dev/null; then
    log_success "${ICON_SUCCESS} Les dépendances du projet ont été installées avec succès : ${dependencies[*]}"
  else
    log_error "${ICON_ERROR} Échec de l'installation des dépendances du projet."
    exit 1
  fi

  # Installation des modules Python spécifiques
  log_info "${ICON_CLOCK} Installation des modules Python requis..."
  if pip install flask requests &> /dev/null; then
    log_success "${ICON_SUCCESS} Modules Python installés : flask, requests"
  else
    log_error "${ICON_ERROR} Échec de l'installation des modules Python."
    exit 1
  fi

  # Installation des modules Node.js spécifiques
  log_info "${ICON_CLOCK} Installation des modules Node.js requis..."
  if npm install -g express &> /dev/null; then
    log_success "${ICON_SUCCESS} Modules Node.js installés : express"
  else
    log_error "${ICON_ERROR} Échec de l'installation des modules Node.js."
    exit 1
  fi
}

# ==========================================
# Configure les variables d'environnement nécessaires au projet
# ==========================================
function configure_environment_variables() {
  log_info "${ICON_CLOCK} Configuration des variables d'environnement pour Cutopia..."

  local env_vars=(
    "CUTOPIA_ENV=production"
    "CUTOPIA_PORT=8080"
    "CUTOPIA_LOG_PATH=/resources/logs/cutopia.log"
  )

  for var in "${env_vars[@]}"; do
    if ! grep -q "^$var" /etc/environment; then
      echo "$var" | tee -a /etc/environment > /dev/null
      log_success "${ICON_VAR} Variable configurée : $var"
    else
      log_warning "${ICON_WARNING} Variable déjà configurée : $var"
    fi
  done

  log_success "${ICON_SUCCESS} Variables d'environnement configurées avec succès."
}

# ==========================================
# Exemple d'exécution des fonctions
# ==========================================
function configure_cutopia() {
  log_info "${ICON_CLOCK} Début de la configuration spécifique à Cutopia..."

  # Étape 1 : Configurer la structure du projet
  setup_project_structure

  # Étape 2 : Installer les dépendances spécifiques au projet
  install_project_dependencies

  # Étape 3 : Configurer les variables d'environnement
  configure_environment_variables

  log_success "${ICON_SUCCESS} Configuration spécifique à Cutopia terminée avec succès !"
}

# Lancer la configuration spécifique à Cutopia
configure_cutopia

# ==========================================
# Configure le serveur SSH
# ==========================================
function setup_ssh() {
  log_info "${ICON_CLOCK} Configuration du serveur SSH..."

  local ssh_config_file="/etc/ssh/sshd_config"

  if [[ -f "$ssh_config_file" ]]; then
    # Désactiver l'accès root par SSH
    sed -i 's/^#PermitRootLogin.*/PermitRootLogin no/' "$ssh_config_file"
    log_success "${ICON_KEY} Désactivation de l'accès root par SSH."

    # Autoriser uniquement les clés SSH pour l'authentification
    sed -i 's/^#PasswordAuthentication.*/PasswordAuthentication no/' "$ssh_config_file"
    log_success "${ICON_KEY} Authentification par mot de passe désactivée."

    # Redémarrer le service SSH pour appliquer les modifications
    if service sshd restart &> /dev/null; then
      log_success "${ICON_SUCCESS} Service SSH redémarré avec succès."
    else
      log_error "${ICON_ERROR} Échec du redémarrage du service SSH."
    fi
  else
    log_error "${ICON_ERROR} Fichier de configuration SSH introuvable : $ssh_config_file"
    exit 1
  fi
}

# ==========================================
# Applique des configurations de sécurité générales
# ==========================================
function harden_system_security() {
  log_info "${ICON_CLOCK} Renforcement de la sécurité générale du système..."

  # Configurer les permissions pour les fichiers critiques
  chmod 600 /etc/passwd /etc/shadow /etc/group &> /dev/null
  log_success "${ICON_SHIELD} Permissions des fichiers critiques configurées."

  # Activer le pare-feu (ufw ou iptables)
  if command -v ufw &> /dev/null; then
    ufw default deny incoming &> /dev/null
    ufw default allow outgoing &> /dev/null
    ufw allow 22 &> /dev/null  # SSH
    ufw allow 80 &> /dev/null  # HTTP
    ufw enable &> /dev/null
    log_success "${ICON_FIREWALL} Pare-feu configuré avec succès."
  else
    log_warning "${ICON_WARNING} UFW n'est pas installé. Vérifiez la configuration d'iptables manuellement."
  fi
}

# ==========================================
# Vérifie que seuls les ports nécessaires sont ouverts
# ==========================================
function check_open_ports() {
  log_info "${ICON_CLOCK} Vérification des ports réseau ouverts..."

  local allowed_ports=("22" "80")
  local open_ports
  open_ports=$(netstat -tuln | grep LISTEN | awk '{print $4}' | awk -F: '{print $NF}' | sort -u)

  for port in $open_ports; do
    if [[ ! " ${allowed_ports[@]} " =~ " $port " ]]; then
      log_warning "${ICON_WARNING} Port ouvert non nécessaire détecté : $port"
    else
      log_success "${ICON_CHECK} Port ouvert nécessaire détecté : $port"
    fi
  done
}

# ==========================================
# Exemple d'exécution des fonctions
# ==========================================
function secure_cutopia() {
  log_info "${ICON_CLOCK} Début de la sécurisation du système pour Cutopia..."

  # Étape 1 : Configurer SSH
  setup_ssh

  # Étape 2 : Renforcer la sécurité générale
  harden_system_security

  # Étape 3 : Vérifier les ports ouverts
  check_open_ports

  log_success "${ICON_SUCCESS} Sécurisation du système pour Cutopia terminée avec succès !"
}

# Lancer la sécurisation de Cutopia
secure_cutopia

# ==========================================
# Vérifie si un outil est installé
# ==========================================
function verify_installation() {
  local tool=$1
  log_info "${ICON_CLOCK} Vérification de l'installation de ${tool}..."

  if command -v $tool &> /dev/null; then
    log_success "${ICON_CHECK} ${tool} est correctement installé."
  else
    log_error "${ICON_ERROR} ${tool} n'est pas installé ou introuvable dans le PATH."
    exit 1
  fi
}

# ==========================================
# Vérifie si un service est actif
# ==========================================
function check_service_status() {
  local service_name=$1
  log_info "${ICON_CLOCK} Vérification du statut du service ${service_name}..."

  if systemctl is-active --quiet $service_name; then
    log_success "${ICON_CHECK} Le service ${service_name} est actif."
  else
    log_warning "${ICON_WARNING} Le service ${service_name} n'est pas actif. Vérifiez la configuration."
  fi
}

# ==========================================
# Effectue une vérification générale
# ==========================================
function run_health_check() {
  log_info "${ICON_CLOCK} Exécution de la vérification générale de l'environnement..."

  # Vérifier la connectivité à localhost
  if curl -s http://localhost &> /dev/null; then
    log_success "${ICON_CHECK} Connectivité à localhost : OK."
  else
    log_error "${ICON_ERROR} Échec de la connexion à localhost. Vérifiez les services locaux."
  fi

  # Vérifier les ports ouverts
  local ports=("22" "80")
  for port in "${ports[@]}"; do
    if netstat -tuln | grep ":$port" &> /dev/null; then
      log_success "${ICON_CHECK} Port ${port} est ouvert et accessible."
    else
      log_warning "${ICON_WARNING} Port ${port} n'est pas accessible. Vérifiez les configurations."
    fi
  done
}

# ==========================================
# Exemple d'exécution des tests
# ==========================================
function run_all_tests() {
  log_info "${ICON_CLOCK} Début des tests et vérifications pour Cutopia..."

  # Étape 1 : Vérifier les installations
  verify_installation "zsh"
  verify_installation "git"

  # Étape 2 : Vérifier les services
  check_service_status "ssh"
  check_service_status "nginx"

  # Étape 3 : Vérification générale
  run_health_check

  log_success "${ICON_SUCCESS} Tous les tests et vérifications ont été complétés avec succès !"
}

# Lancer tous les tests
run_all_tests


# ==========================================
# Supprime les fichiers temporaires générés pendant le processus d'installation
# ==========================================
function cleanup_temp_files() {
  log_info "${ICON_CLOCK} Suppression des fichiers temporaires..."

  # Liste des répertoires ou fichiers temporaires communs à nettoyer
  local temp_dirs=(
    "/tmp/*"
    "/var/tmp/*"
    "/var/cache/*"
  )

  for dir in "${temp_dirs[@]}"; do
    if rm -rf $dir &> /dev/null; then
      log_success "${ICON_TRASH} Fichiers temporaires supprimés dans $dir."
    else
      log_warning "${ICON_WARNING} Impossible de supprimer les fichiers dans $dir."
    fi
  done
}

# ==========================================
# Nettoie les caches et autres données inutiles pour réduire la taille de l'image Docker
# ==========================================
function reduce_image_size() {
  log_info "${ICON_CLOCK} Réduction de la taille de l'image Docker..."

  # Nettoyer les caches de Docker et les images inutilisées
  if docker system prune -af &> /dev/null; then
    log_success "${ICON_TRASH} Nettoyage des images et caches Docker inutiles réussi."
  else
    log_warning "${ICON_WARNING} Échec du nettoyage des images Docker."
  fi
}

# ==========================================
# Désinstalle les paquets temporaires après leur utilisation
# ==========================================
function remove_unused_dependencies() {
  log_info "${ICON_CLOCK} Désinstallation des paquets temporaires..."

  # Liste des paquets temporaires à supprimer (exemples)
  local temp_packages=(
    "build-base"
    "curl-dev"
    "git-dev"
  )

  for package in "${temp_packages[@]}"; do
    if apk del $package &> /dev/null; then
      log_success "${ICON_CHECK} Paquet temporaire ${package} désinstallé."
    else
      log_warning "${ICON_WARNING} Paquet ${package} introuvable ou déjà désinstallé."
    fi
  done
}

# ==========================================
# Exemple d'exécution des fonctions de nettoyage
# ==========================================
function perform_cleanup() {
  log_info "${ICON_CLOCK} Début du processus de nettoyage pour Cutopia..."

  # Étape 1 : Nettoyage des fichiers temporaires
  cleanup_temp_files

  # Étape 2 : Réduction de la taille de l'image Docker
  reduce_image_size

  # Étape 3 : Désinstallation des dépendances inutiles
  remove_unused_dependencies

  log_success "${ICON_SUCCESS} Nettoyage terminé avec succès !"
}

# Lancer le nettoyage
perform_cleanup


# ==========================================
# Écrit les messages de log dans un fichier pour un suivi ultérieur
# ==========================================
function log_to_file() {
  local log_message=$1
  local log_file=$2

  # Vérifier si le fichier de log existe, sinon le créer
  if [[ ! -f $log_file ]]; then
    touch $log_file
  fi

  # Ajouter le message de log au fichier avec une timestamp
  echo "$(date +'%Y-%m-%d %H:%M:%S') - $log_message" >> $log_file
  log_info "${ICON_LOG} Message enregistré dans le fichier de log: $log_file"
}

# ==========================================
# Affiche un résumé des actions effectuées à la fin du script
# ==========================================
function display_log_summary() {
  log_info "${ICON_CLOCK} Affichage du résumé des actions..."

  # Exemple de résumé (cela pourrait être généré dynamiquement en fonction des actions)
  local actions_summary="Résumé des actions effectuées:\n"
  actions_summary+="${ICON_CHECK} Vérification des installations réussie.\n"
  actions_summary+="${ICON_WARNING} Service nginx non actif.\n"
  actions_summary+="${ICON_CHECK} Connectivité à localhost : OK.\n"
  actions_summary+="${ICON_TRASH} Nettoyage des fichiers temporaires effectué.\n"
  actions_summary+="${ICON_SUCCESS} Script exécuté avec succès."

  # Affichage du résumé
  echo -e "$actions_summary"
  log_to_file "$actions_summary" "/var/log/cutopia_summary.log"
}

# ==========================================
# Exemple d'exécution des fonctions de gestion des logs
# ==========================================
function perform_logging() {
  log_info "${ICON_CLOCK} Début de l'enregistrement des logs..."

  # Enregistrer des messages de log dans un fichier
  log_to_file "${ICON_CLOCK} Début des tests et vérifications pour Cutopia..." "/var/log/cutopia.log"
  log_to_file "${ICON_CHECK} Vérification de l'installation de zsh terminée." "/var/log/cutopia.log"

  # Afficher et enregistrer un résumé des actions effectuées
  display_log_summary

  log_success "${ICON_SUCCESS} Tous les logs ont été enregistrés avec succès."
}

# Lancer la gestion des logs
perform_logging


# ==========================================
# Annule les modifications effectuées par le script en cas d'erreur critique
# ==========================================
function rollback_changes() {
  log_error "${ICON_X} Erreur critique détectée. Rétablissement de l'état précédent..."

  # Exemple de rollback des étapes
  # Si un fichier a été modifié, restaurer une sauvegarde
  if [[ -f "/var/cutopia/config_backup.conf" ]]; then
    cp /var/cutopia/config_backup.conf /var/cutopia/config.conf
    log_success "${ICON_RESTORE} Configuration restaurée à partir de la sauvegarde."
  else
    log_warning "${ICON_WARNING} Aucune sauvegarde trouvée pour la configuration."
  fi

  # Exemple d'annulation de l'installation de paquets
  if apk info -e "zsh"; then
    apk del zsh
    log_success "${ICON_RESTORE} Paquet zsh désinstallé."
  else
    log_warning "${ICON_WARNING} Le paquet zsh n'a pas été installé ou a déjà été supprimé."
  fi

  log_success "${ICON_SUCCESS} Rétablissement des modifications terminé."
}

# ==========================================
# Intercepte les erreurs et exécute des actions correctives
# ==========================================
function trap_errors() {
  log_info "${ICON_CLOCK} Activation du gestionnaire d'erreurs..."

  # Définit un gestionnaire d'erreurs qui appelle rollback_changes en cas d'erreur
  trap 'rollback_changes' ERR

  # Exemple d'action risquée
  log_info "${ICON_CLOCK} Tentative d'installation d'un paquet..."
  apk add non_existent_package &> /dev/null

  # Si une erreur se produit dans cette commande, `rollback_changes` sera exécuté automatiquement
}

# ==========================================
# Exemple d'exécution de gestion des erreurs
# ==========================================
function perform_error_handling() {
  log_info "${ICON_CLOCK} Début de la gestion des erreurs..."

  # Exécution du script avec gestion des erreurs
  trap_errors

  log_success "${ICON_SUCCESS} Le script s'est exécuté correctement (en l'absence d'erreurs critiques)."
}

# Lancer la gestion des erreurs
perform_error_handling
