#!/bin/zsh

# Couleurs pour l'affichage
GREEN="\033[1;32m"
RED="\033[1;31m"
YELLOW="\033[1;33m"
RESET="\033[0m"

# Vérifier si la commande est disponible
command_exists() {
  command -v "$1" >/dev/null 2>&1
}

# Installation et configuration de LXD
install_lxd() {
  echo -e "${YELLOW}🔄 Mise à jour du système...${RESET}"
  cuapk update

  if ! command_exists lxd; then
    echo -e "${GREEN}📦 Installation de LXD...${RESET}"
    cuapk add lxd lxd-client
  else
    echo -e "${GREEN}✅ LXD est déjà installé.${RESET}"
  fi

  if ! groups $USER | grep -q "\blxd\b"; then
    echo -e "${YELLOW}👤 Ajout de l'utilisateur '$USER' au groupe lxd...${RESET}"
    sudo usermod -aG lxd $USER
    echo -e "${YELLOW}🔄 Déconnectez-vous ou exécutez 'newgrp lxd' pour appliquer les changements.${RESET}"
  else
    echo -e "${GREEN}✅ L'utilisateur '$USER' est déjà dans le groupe lxd.${RESET}"
  fi

  if ! lxc info >/dev/null 2>&1; then
    echo -e "${YELLOW}⚙️ Initialisation de LXD avec une configuration par défaut...${RESET}"
    cat <<EOF | lxd init --preseed
config: {}
networks:
- config:
    ipv4.address: auto
    ipv4.nat: "true"
    ipv6.address: none
  description: "Bridge réseau par défaut"
  managed: true
  name: lxdbr0
  type: bridge
storage_pools:
- config:
    source: /var/snap/lxd/common/lxd/storage-pools/default
  description: "Stockage par défaut"
  name: default
  driver: dir
profiles:
- config: {}
  description: "Profil par défaut"
  devices:
    root:
      path: /
      pool: default
      type: disk
  name: default
cluster: null
EOF
    echo -e "${GREEN}✅ LXD initialisé avec succès.${RESET}"
  else
    echo -e "${GREEN}✅ LXD est déjà initialisé.${RESET}"
  fi

  if ! command_exists lxc; then
    echo -e "${RED}⚠️ La commande 'lxc' n'est pas disponible. Essayez 'newgrp lxd' ou redémarrez votre session.${RESET}"
  else
    echo -e "${GREEN}✅ LXC est prêt à être utilisé.${RESET}"
  fi
}

# Lister les conteneurs
list_containers() {
  echo -e "${YELLOW}📋 Liste des conteneurs disponibles :${RESET}"
  lxc list
}

# Créer un conteneur
create_container() {
  local name="$1"
  if [ -z "$name" ]; then
    echo -e "${RED}❌ Nom du conteneur requis.${RESET}"
    exit 1
  fi

  echo -e "${YELLOW}🚀 Création du conteneur '$name'...${RESET}"
  lxc launch images:ubuntu/22.04 "$name"

  echo -e "${YELLOW}👤 Ajout de l'utilisateur 'admin'...${RESET}"
  lxc exec "$name" -- useradd -m admin
  lxc exec "$name" -- sh -c "echo 'admin:password' | chpasswd"

  echo -e "${GREEN}✅ Conteneur '$name' créé avec succès.${RESET}"
}

# Supprimer un conteneur
delete_container() {
  local name="$1"
  if [ -z "$name" ]; then
    echo -e "${RED}❌ Nom du conteneur requis.${RESET}"
    exit 1
  fi

  echo -e "${YELLOW}🛑 Arrêt du conteneur '$name'...${RESET}"
  lxc stop "$name"

  echo -e "${YELLOW}🗑 Suppression du conteneur '$name'...${RESET}"
  lxc delete "$name"

  echo -e "${GREEN}✅ Conteneur '$name' supprimé.${RESET}"
}

# Démarrer un conteneur
start_container() {
  local name="$1"
  if [ -z "$name" ]; then
    echo -e "${RED}❌ Nom du conteneur requis.${RESET}"
    exit 1
  fi

  echo -e "${YELLOW}🚀 Démarrage du conteneur '$name'...${RESET}"
  lxc start "$name"

  echo -e "${GREEN}✅ Conteneur '$name' démarré.${RESET}"
}

# Arrêter un conteneur
stop_container() {
  local name="$1"
  if [ -z "$name" ]; then
    echo -e "${RED}❌ Nom du conteneur requis.${RESET}"
    exit 1
  fi

  echo -e "${YELLOW}🛑 Arrêt du conteneur '$name'...${RESET}"
  lxc stop "$name"

  echo -e "${GREEN}✅ Conteneur '$name' arrêté.${RESET}"
}

# Afficher l'aide
show_help() {
  echo -e "${GREEN}Usage :${RESET}"
  echo "  $(basename "$0") -i            → Installer et configurer LXD"
  echo "  $(basename "$0") -l            → Lister les conteneurs"
  echo "  $(basename "$0") -c <nom>      → Créer un conteneur"
  echo "  $(basename "$0") -d <nom>      → Supprimer un conteneur"
  echo "  $(basename "$0") -s <nom>      → Démarrer un conteneur"
  echo "  $(basename "$0") -t <nom>      → Arrêter un conteneur"
  echo "  $(basename "$0") -h            → Afficher cette aide"
}

# Gestion des options avec getopts
while getopts ":ilc:d:s:t:h" opt; do
  case ${opt} in
    i)
      install_lxd
      ;;
    l)
      list_containers
      ;;
    c)
      create_container "$OPTARG"
      ;;
    d)
      delete_container "$OPTARG"
      ;;
    s)
      start_container "$OPTARG"
      ;;
    t)
      stop_container "$OPTARG"
      ;;
    h | *)
      show_help
      exit 0
      ;;
  esac
done

# Si aucun argument, afficher l'aide
if [[ $# -eq 0 ]]; then
  show_help
  exit 0
fi

