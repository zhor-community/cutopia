sudo apt update && sudo apt upgrade 
sudo apt install curl apt-transport-https -y

# Debian 11  Bullseye
#curl -fsSL https://download.opensuse.org/repositories/home:hawkeye116477:waterfox/Debian_11/Release.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/home_hawkeye116477_waterfox.gpg > /dev/null
#echo 'deb http://download.opensuse.org/repositories/home:/hawkeye116477:/waterfox/Debian_11/ /' | sudo tee /etc/apt/sources.list.d/home:hawkeye116477:waterfox.list
# Debian 12  Bookworm
curl -fsSL https://download.opensuse.org/repositories/home:hawkeye116477:waterfox/Debian_12/Release.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/home_hawkeye116477_waterfox.gpg > /dev/null
echo 'deb https://download.opensuse.org/repositories/home:/hawkeye116477:/waterfox/Debian_12/ /' | sudo tee /etc/apt/sources.list.d/home:hawkeye116477:waterfox.list
# Debian 13 Trixie
# curl -fsSL https://download.opensuse.org/repositories/home:hawkeye116477:waterfox/Debian_Testing/Release.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/home_hawkeye116477_waterfox.gpg > /dev/null
#echo 'deb https://download.opensuse.org/repositories/home:/hawkeye116477:/waterfox/Debian_Testing/ /' | sudo tee /etc/apt/sources.list.d/home:hawkeye116477:waterfox.list
sudo apt update
sudo apt install waterfox-g
# sudo apt install waterfox-classic
