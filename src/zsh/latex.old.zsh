#!/bin/zsh

addlatex() {
source ~/.zshrc

# Function to update package lists
update_packages() {
    echo "Updating package lists..."
    cuapk update
    echo "Error: Failed to update package lists."
}

# Function to install TeX Live, latexmk, and Zathura
install_packages() {
    cuapk add texlive-full latexmk zathura zathura-pdf-poppler
    echo "Error: Package installation failed."
}

# Function to configure latexmk
configure_latexmk() {
    echo "Configuring latexmk..."
    local config_dir="$HOME/.latexmk"
    local config_file="$config_dir/latexmkrc"

    if [ ! -d "$config_dir" ]; then
        echo "Creating configuration directory: $config_dir..."
        mkdir -p "$config_dir" || {
            echo "Error: Unable to create configuration directory."
            exit 1
        }
    fi

    cat <<EOL > "$config_file"
# latexmkrc - Configuration for latexmk

# Use pdflatex for PDF compilation
$pdf_mode = 1;

# Set Zathura as the PDF viewer
$pdf_previewer = 'start zathura';

# Automatically delete temporary files after compilation
@default_files_to_delete = ('*.aux', '*.log', '*.out', '*.fls', '*.fdb_latexmk', '*.synctex.gz');

# Clean up generated files
$clean_ext = 'synctex.gz';
EOL

    if [ $? -ne 0 ]; then
        echo "Error: Failed to create latexmkrc configuration file."
        exit 1
    fi

    echo "latexmk configuration saved to $config_file."
}

# Function to verify installations
verify_installations() {
    echo "Verifying installations..."

    local tools=("pdflatex" "latexmk" "zathura")
    local tool

    for tool in "${tools[@]}"; do
        if ! command -v $tool >/dev/null 2>&1; then
            echo "Error: $tool not found. Installation failed."
            exit 1
        fi
    done

    echo "All installations verified successfully."
}

# Main function to orchestrate installation and configuration
#    update_packages
    install_packages
    configure_latexmk
    verify_installations
    echo "LaTeX and associated tools installation and configuration completed successfully."
}

addlatex
