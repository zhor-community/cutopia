INSERT INTO modules (name, description, install_command, included, version, author, documentation) VALUES
('cusudo', 'A universal setup tool for configuring sudo, adding users, and granting administrative privileges, compatible with multiple Linux distributions.', 'zsh $CUTOPIAPATH/src/zsh/cusudo.zsh', false, NULL, NULL, 'N/A'),
('cuapk', 'Custom package manager for Alpine-based systems, unifying package handling across operating systems.', 'zsh $CUTOPIAPATH/src/zsh/cuapk.zsh', false, NULL, NULL, 'N/A'),
('cupython', 'Optimized runtime environment for Python applications, tailored for the Cutopia ecosystem.', 'zsh $CUTOPIAPATH/src/zsh/cupython.zsh', true, NULL, NULL, 'N/A'),
('cuzsh', 'Custom shell based on Zsh with specific extensions for advanced workflows.', 'zsh $CUTOPIAPATH/src/zsh/cuzsh.zsh', true, NULL, NULL, 'N/A'),
('cuipfs', 'Automation script for IPFS tasks, including node management, file handling, and integration with Nextcloud.', 'zsh $CUTOPIAPATH/bin/cuipfs', true, NULL, NULL, 'This module automates IPFS-related tasks, making it easier to manage decentralized file storage and connectivity.'),
('cudit', 'Enhanced Neovim configuration tailored for the Cutopia environment, focusing on productivity and Lua scripting support.', 'zsh $CUTOPIAPATH/src/zsh/cudit.zsh', false, '1.2.0', 'AbdElHakim ZOUAÏ', 'https://cutopia.example.com/docs/cudit');

INSERT INTO module_dependencies (module_id, dependency) VALUES
((SELECT id FROM modules WHERE name='cusudo'), 'sudo'),
((SELECT id FROM modules WHERE name='cusudo'), 'zsh'),
((SELECT id FROM modules WHERE name='cuipfs'), 'curl'),
((SELECT id FROM modules WHERE name='cuipfs'), 'wget'),
((SELECT id FROM modules WHERE name='cuipfs'), 'jq'),
((SELECT id FROM modules WHERE name='cuipfs'), 'ipfs'),
((SELECT id FROM modules WHERE name='cuipfs'), 'ipfs-cluster-service'),
((SELECT id FROM modules WHERE name='cuipfs'), 'inotify-tools'),
((SELECT id FROM modules WHERE name='cudit'), 'neovim>=0.8.0'),
((SELECT id FROM modules WHERE name='cudit'), 'lua>=5.4'),
((SELECT id FROM modules WHERE name='cudit'), 'git');

INSERT INTO module_features (module_id, feature) VALUES
((SELECT id FROM modules WHERE name='cusudo'), 'Automatic detection of Linux distribution'),
((SELECT id FROM modules WHERE name='cusudo'), 'Installs sudo if not already present'),
((SELECT id FROM modules WHERE name='cusudo'), 'Creates new users and assigns them administrative privileges'),
((SELECT id FROM modules WHERE name='cuipfs'), 'Node management (install, init, start, stop)'),
((SELECT id FROM modules WHERE name='cuipfs'), 'File handling (add, retrieve, pin)'),
((SELECT id FROM modules WHERE name='cudit'), 'Custom Lua scripts for Cutopia workflows'),
((SELECT id FROM modules WHERE name='cudit'), 'Integrated plugins for advanced editing capabilities');

