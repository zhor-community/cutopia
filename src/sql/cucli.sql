-- Table Startup
CREATE TABLE startup (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) UNIQUE NOT NULL,
    founder VARCHAR(255),
    funding DECIMAL(15,2),
    website VARCHAR(255)
);

-- Table Project (Chaque projet appartient à une startup)
CREATE TABLE project (
    id SERIAL PRIMARY KEY,
    startup_id INT NOT NULL,
    name VARCHAR(255) UNIQUE NOT NULL,
    description TEXT,
    maintainer VARCHAR(255),
    email VARCHAR(255),
    FOREIGN KEY (startup_id) REFERENCES startup(id) ON DELETE CASCADE
);

-- Table Modules (Chaque module appartient à un projet)
CREATE TABLE modules (
    id SERIAL PRIMARY KEY,
    project_id INT NOT NULL,
    name VARCHAR(255) UNIQUE NOT NULL,
    description TEXT,
    install_command TEXT,
    included BOOLEAN DEFAULT FALSE,
    documentation TEXT,
    FOREIGN KEY (project_id) REFERENCES project(id) ON DELETE CASCADE
);

-- Table Dependencies (One-to-Many avec Modules)
CREATE TABLE dependencies (
    id SERIAL PRIMARY KEY,
    module_id INT NOT NULL,
    dependency VARCHAR(255) NOT NULL,
    FOREIGN KEY (module_id) REFERENCES modules(id) ON DELETE CASCADE
);

-- Table Features (One-to-Many avec Modules)
CREATE TABLE features (
    id SERIAL PRIMARY KEY,
    module_id INT NOT NULL,
    feature TEXT NOT NULL,
    FOREIGN KEY (module_id) REFERENCES modules(id) ON DELETE CASCADE
);
-- Table Network (Chaque projet peut avoir un réseau)
CREATE TABLE network (
    id SERIAL PRIMARY KEY,
    project_id INT NOT NULL,
    subnet VARCHAR(255),
    gateway VARCHAR(255),
    dns VARCHAR(255),
    FOREIGN KEY (project_id) REFERENCES project(id) ON DELETE CASCADE
);

-- Table Virtual Machines (Un projet peut avoir plusieurs VMs)
CREATE TABLE vm (
    id SERIAL PRIMARY KEY,
    project_id INT NOT NULL,
    name VARCHAR(255) NOT NULL,
    cpu INT DEFAULT 1,
    ram INT DEFAULT 1024,
    storage INT DEFAULT 20,  -- Stockage en Go
    os VARCHAR(255),
    status VARCHAR(50) CHECK (status IN ('running', 'stopped', 'paused')),
    FOREIGN KEY (project_id) REFERENCES project(id) ON DELETE CASCADE
);

-- Table Docker Containers (Un module peut être exécuté dans Docker)
CREATE TABLE docker_container (
    id SERIAL PRIMARY KEY,
    module_id INT NOT NULL,
    image VARCHAR(255) NOT NULL,
    container_name VARCHAR(255) UNIQUE NOT NULL,
    ports VARCHAR(255),
    volumes VARCHAR(255),
    env_variables TEXT,
    status VARCHAR(50) CHECK (status IN ('running', 'stopped', 'paused')),
    FOREIGN KEY (module_id) REFERENCES modules(id) ON DELETE CASCADE
);

-- Table LXC Containers (Similaire à Docker, mais pour LXC)
CREATE TABLE lxc_container (
    id SERIAL PRIMARY KEY,
    module_id INT NOT NULL,
    container_name VARCHAR(255) UNIQUE NOT NULL,
    os VARCHAR(255),
    cpu INT DEFAULT 1,
    ram INT DEFAULT 1024,
    storage INT DEFAULT 10,
    network VARCHAR(255),
    status VARCHAR(50) CHECK (status IN ('running', 'stopped', 'paused')),
    FOREIGN KEY (module_id) REFERENCES modules(id) ON DELETE CASCADE
);

-- Table Docker Compose (Regroupe plusieurs conteneurs Docker liés à un projet ou un module)
CREATE TABLE docker_compose (
    id SERIAL PRIMARY KEY,
    project_id INT,   -- Peut être lié à un projet
    module_id INT,    -- Ou à un module spécifique
    compose_file TEXT NOT NULL,  -- Contenu du fichier docker-compose.yml
    status VARCHAR(50) CHECK (status IN ('running', 'stopped', 'paused')),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (project_id) REFERENCES project(id) ON DELETE CASCADE,
    FOREIGN KEY (module_id) REFERENCES modules(id) ON DELETE CASCADE
);

-- Mise à jour de docker_container pour indiquer s'il fait partie d'un docker-compose
ALTER TABLE docker_container ADD COLUMN compose_id INT;
ALTER TABLE docker_container ADD CONSTRAINT fk_compose FOREIGN KEY (compose_id) REFERENCES docker_compose(id) ON DELETE CASCADE;

-- Table Swarm Cluster (Représente un cluster Docker Swarm)
CREATE TABLE swarm_cluster (
    id SERIAL PRIMARY KEY,
    name VARCHAR(100) UNIQUE NOT NULL,  -- Nom du cluster
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

-- Table Swarm Node (Chaque machine qui fait partie d'un cluster Swarm)
CREATE TABLE swarm_node (
    id SERIAL PRIMARY KEY,
    cluster_id INT NOT NULL,
    hostname VARCHAR(100) UNIQUE NOT NULL,  -- Nom de la machine
    ip_address VARCHAR(50) UNIQUE NOT NULL, -- Adresse IP du nœud
    role VARCHAR(20) CHECK (role IN ('manager', 'worker')) NOT NULL,
    status VARCHAR(20) CHECK (status IN ('active', 'down', 'draining')) DEFAULT 'active',
    FOREIGN KEY (cluster_id) REFERENCES swarm_cluster(id) ON DELETE CASCADE
);

-- Table Swarm Service (Équivalent d'un conteneur dans Swarm, mais distribué)
CREATE TABLE swarm_service (
    id SERIAL PRIMARY KEY,
    cluster_id INT NOT NULL,
    name VARCHAR(100) NOT NULL,  -- Nom du service
    image VARCHAR(200) NOT NULL, -- Image Docker utilisée
    replicas INT DEFAULT 1, -- Nombre d'instances répliquées
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (cluster_id) REFERENCES swarm_cluster(id) ON DELETE CASCADE
);
-- Table Xen Host (Représente un serveur physique sous Xen)
CREATE TABLE xen_host (
    id SERIAL PRIMARY KEY,
    hostname VARCHAR(100) UNIQUE NOT NULL,  -- Nom de l'hôte Xen
    ip_address VARCHAR(50) UNIQUE NOT NULL, -- Adresse IP de l'hôte
    cpu_cores INT NOT NULL, -- Nombre de cœurs CPU
    ram_size INT NOT NULL, -- RAM en Mo
    storage_size INT NOT NULL, -- Stockage total en Go
    status VARCHAR(20) CHECK (status IN ('active', 'maintenance', 'down')) DEFAULT 'active'
);

-- Table Xen VM (Machines virtuelles déployées sur Xen)
CREATE TABLE xen_vm (
    id SERIAL PRIMARY KEY,
    host_id INT NOT NULL,  -- L'hôte Xen sur lequel la VM tourne
    name VARCHAR(100) UNIQUE NOT NULL,  -- Nom de la VM
    vcpus INT NOT NULL, -- Nombre de vCPU assignés
    ram_size INT NOT NULL, -- RAM assignée en Mo
    disk_size INT NOT NULL, -- Stockage assigné en Go
    ip_address VARCHAR(50) UNIQUE, -- Adresse IP de la VM
    status VARCHAR(20) CHECK (status IN ('running', 'stopped', 'suspended')) DEFAULT 'running',
    FOREIGN KEY (host_id) REFERENCES xen_host(id) ON DELETE CASCADE
);

-- Table Xen Network (Réseau pour les VMs Xen)
CREATE TABLE xen_network (
    id SERIAL PRIMARY KEY,
    name VARCHAR(100) UNIQUE NOT NULL, -- Nom du réseau
    subnet VARCHAR(50) NOT NULL, -- Plage IP (ex: 192.168.1.0/24)
    gateway VARCHAR(50) NOT NULL -- Passerelle du réseau
);

-- Table Xen VM Network (Associe les VMs à un réseau Xen)
CREATE TABLE xen_vm_network (
    id SERIAL PRIMARY KEY,
    vm_id INT NOT NULL,
    network_id INT NOT NULL,
    assigned_ip VARCHAR(50) UNIQUE NOT NULL, -- Adresse IP assignée à la VM sur ce réseau
    FOREIGN KEY (vm_id) REFERENCES xen_vm(id) ON DELETE CASCADE,
    FOREIGN KEY (network_id) REFERENCES xen_network(id) ON DELETE CASCADE
);
-- Table KVM Host (Représente un serveur physique sous KVM)
CREATE TABLE kvm_host (
    id SERIAL PRIMARY KEY,
    hostname VARCHAR(100) UNIQUE NOT NULL,  -- Nom de l'hôte KVM
    ip_address VARCHAR(50) UNIQUE NOT NULL, -- Adresse IP de l'hôte
    cpu_cores INT NOT NULL, -- Nombre de cœurs CPU
    ram_size INT NOT NULL, -- RAM en Mo
    storage_size INT NOT NULL, -- Stockage total en Go
    status VARCHAR(20) CHECK (status IN ('active', 'maintenance', 'down')) DEFAULT 'active'
);

-- Table KVM VM (Machines virtuelles tournant sur KVM)
CREATE TABLE kvm_vm (
    id SERIAL PRIMARY KEY,
    host_id INT NOT NULL,  -- L'hôte KVM sur lequel la VM tourne
    name VARCHAR(100) UNIQUE NOT NULL,  -- Nom de la VM
    vcpus INT NOT NULL, -- Nombre de vCPU assignés
    ram_size INT NOT NULL, -- RAM assignée en Mo
    disk_size INT NOT NULL, -- Stockage assigné en Go
    ip_address VARCHAR(50) UNIQUE, -- Adresse IP de la VM
    status VARCHAR(20) CHECK (status IN ('running', 'stopped', 'suspended')) DEFAULT 'running',
    FOREIGN KEY (host_id) REFERENCES kvm_host(id) ON DELETE CASCADE
);

-- Table KVM Network (Réseau pour les VMs KVM)
CREATE TABLE kvm_network (
    id SERIAL PRIMARY KEY,
    name VARCHAR(100) UNIQUE NOT NULL, -- Nom du réseau
    subnet VARCHAR(50) NOT NULL, -- Plage IP (ex: 192.168.1.0/24)
    gateway VARCHAR(50) NOT NULL, -- Passerelle du réseau
    mode VARCHAR(20) CHECK (mode IN ('bridge', 'nat')) NOT NULL -- Type de réseau KVM
);

-- Table KVM VM Network (Associe les VMs à un réseau KVM)
CREATE TABLE kvm_vm_network (
    id SERIAL PRIMARY KEY,
    vm_id INT NOT NULL,
    network_id INT NOT NULL,
    assigned_ip VARCHAR(50) UNIQUE NOT NULL, -- Adresse IP assignée à la VM sur ce réseau
    FOREIGN KEY (vm_id) REFERENCES kvm_vm(id) ON DELETE CASCADE,
    FOREIGN KEY (network_id) REFERENCES kvm_network(id) ON DELETE CASCADE
);
-- Table QEMU Host (Représente un serveur hébergeant des VMs QEMU)
CREATE TABLE qemu_host (
    id SERIAL PRIMARY KEY,
    hostname VARCHAR(100) UNIQUE NOT NULL,  
    ip_address VARCHAR(50) UNIQUE NOT NULL, 
    cpu_cores INT NOT NULL,  
    ram_size INT NOT NULL,  
    storage_size INT NOT NULL,  
    status VARCHAR(20) CHECK (status IN ('active', 'maintenance', 'down')) DEFAULT 'active'
);

-- Table QEMU VM (Machines virtuelles sous QEMU)
CREATE TABLE qemu_vm (
    id SERIAL PRIMARY KEY,
    host_id INT NOT NULL,  
    name VARCHAR(100) UNIQUE NOT NULL,  
    vcpus INT NOT NULL,  
    ram_size INT NOT NULL,  
    disk_size INT NOT NULL,  
    status VARCHAR(20) CHECK (status IN ('running', 'stopped', 'suspended')) DEFAULT 'running',
    FOREIGN KEY (host_id) REFERENCES qemu_host(id) ON DELETE CASCADE
);

-- Table QEMU Disk (Stockage des VMs sous forme de fichiers)
CREATE TABLE qemu_disk (
    id SERIAL PRIMARY KEY,
    vm_id INT NOT NULL,
    path VARCHAR(255) UNIQUE NOT NULL,  
    size INT NOT NULL,  
    format VARCHAR(20) CHECK (format IN ('qcow2', 'raw', 'vmdk')) NOT NULL,  
    FOREIGN KEY (vm_id) REFERENCES qemu_vm(id) ON DELETE CASCADE
);

-- Table QEMU Network (Réseau pour les VMs QEMU)
CREATE TABLE qemu_network (
    id SERIAL PRIMARY KEY,
    name VARCHAR(100) UNIQUE NOT NULL,  
    subnet VARCHAR(50) NOT NULL,  
    gateway VARCHAR(50) NOT NULL,  
    mode VARCHAR(20) CHECK (mode IN ('bridge', 'nat')) NOT NULL 
);

-- Table QEMU VM Network (Associe les VMs à un réseau)
CREATE TABLE qemu_vm_network (
    id SERIAL PRIMARY KEY,
    vm_id INT NOT NULL,
    network_id INT NOT NULL,
    assigned_ip VARCHAR(50) UNIQUE NOT NULL,  
    FOREIGN KEY (vm_id) REFERENCES qemu_vm(id) ON DELETE CASCADE,
    FOREIGN KEY (network_id) REFERENCES qemu_network(id) ON DELETE CASCADE
);

