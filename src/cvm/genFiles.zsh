#!/bin/zsh

create_structure() {
    #base_dir="libvirtXML"
    base_dir="."
    
    # Définir la structure des dossiers
    directories=(
        "$base_dir/kvm"
        "$base_dir/xen"
        "$base_dir/lxc"
        "$base_dir/openvz"
        "$base_dir/docker"
        "$base_dir/proxmox"
        "$base_dir/kubernetes"
        "$base_dir/openstack"
    )
    
    # Définir les fichiers à créer
    declare -A files
    files=(
        "$base_dir/kvm/openwrt.xml" "<domain type='kvm'>\n</domain>"
        "$base_dir/kvm/pfsense.xml" "<domain type='kvm'>\n</domain>"
        "$base_dir/kvm/android.xml" "<domain type='kvm'>\n</domain>"
        "$base_dir/kvm/vps.xml" "<domain type='kvm'>\n</domain>"
        "$base_dir/kvm/proxmox.xml" "<domain type='kvm'>\n</domain>"
        "$base_dir/kvm/windows.xml" "<domain type='kvm'>\n</domain>"
        "$base_dir/kvm/freenas.xml" "<domain type='kvm'>\n</domain>"
        "$base_dir/kvm/unraid.xml" "<domain type='kvm'>\n</domain>"
        "$base_dir/kvm/ipfs.xml" "<domain type='kvm'>\n</domain>"
        "$base_dir/kvm/raspberry-pi.xml" "<domain type='kvm'>\n</domain>"
        "$base_dir/kvm/balena-os.xml" "<domain type='kvm'>\n</domain>"
        "$base_dir/kvm/openwrt-arm.xml" "<domain type='kvm'>\n</domain>"
        "$base_dir/kvm/coreelec.xml" "<domain type='kvm'>\n</domain>"
        "$base_dir/xen/openwrt.cfg" "# Xen configuration file"
        "$base_dir/xen/windows.cfg" "# Xen configuration file"
        "$base_dir/lxc/openwrt.conf" "# LXC configuration file"
        "$base_dir/lxc/freenas.conf" "# LXC configuration file"
        "$base_dir/lxc/ipfs.conf" "# LXC configuration file"
        "$base_dir/openvz/openwrt.conf" "# OpenVZ configuration file"
        "$base_dir/openvz/ipfs.conf" "# OpenVZ configuration file"
        "$base_dir/docker/docker-compose.yml" "version: '3'\nservices:\n  ipfs:\n    image: ipfs/go-ipfs"
        "$base_dir/docker/README.txt" "Instructions pour Docker"
        "$base_dir/proxmox/README.txt" "Instructions pour Proxmox"
        "$base_dir/kubernetes/minikube.xml" "<domain type='kvm'>\n</domain>"
        "$base_dir/openstack/openstack-instance.xml" "<domain type='kvm'>\n</domain>"
    )
    
    # Création des répertoires
    for dir in $directories; do
        mkdir -p "$dir"
    done
    
    # Création des fichiers avec du contenu de base
    for file in ${(k)files}; do
        echo "${files[$file]}" > "$file"
    done
    
    echo "Structure de fichiers et de dossiers générée avec succès."
}

create_structure
