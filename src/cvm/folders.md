❯ tree -L 3 --dirsfirst --noreport
.
├── .infra/ # Configurations globales
│ ├── policy-as-code/ # Régulations automatisées
│ │ ├── sentinel.hcl # HashiCorp Sentinel
│ │ └── opa/ # Open Policy Agent
│ └── compliance/ # Conformité légale
│ ├── gdpr/ # RGPD
│ └── soc2/ # Audit SOC2
├── artifacts/ # Binaires & Releases
│ ├── sbom/ # Software Bill of Materials
│ └── sigstore/ # Signatures cosign
├── atlantis/ # GitOps workflow
├── cluster/ # Déclaration d'état
│ ├── blueprint/ # Modèles réutilisables
│ │ ├── k8s-cluster/ # Cluster K8s standard
│ │ └── hypervisor-pool/ # Pool d'hyperviseurs
│ └── regions/ # Déploiements régionaux
│ ├── eu-west-3a/ # Zone AWS
│ └── proxmox-cluster1/ # On-prem
├── cmd/ # Entrypoints CLI
│ └── cvmctl/ # Controller principal
├── core/ # Noyau Rust sécurisé
│ ├── hypervisor/ # Abstraction hyperviseur
│ │ ├── kvm/ # Implémentation KVM
│ │ └── firecracker/ # Micro-VMs
│ └── scheduler/ # Orchestrateur
├── deployments/ # Déploiements immuables
│ ├── terraform/ # Infrastructure
│ ├── crossplane/ # Kubernetes-native
│ └── pulumi/ # Multi-cloud
├── devsecops/ # Pipeline sécurisé
│ ├── containers/ # Builds Docker
│ │ └── distroless/ # Images minimales
│ ├── tasks/ # Tekton pipelines
│ └── supply-chain/ # Chaîne d'approvisionnement
├── docs/ # Documentation générée
│ ├── threat-model/ # Modèles de menace
│ └── runbooks/ # Procédures opérationnelles
├── hacks/ # Scripts internes
├── observability/ # Télémétrie
│ ├── dashboards/ # Grafana
│ └── fluent-bit/ # Logs unifiés
├── pkg/ # Bibliothèques internes
│ ├── libvirt/ # Bindings Rust
│ └── secureconfig/ # Gestion de configs
├── policy/ # Contrôles d'accès
│ ├── iam/ # RBAC hiérarchique
│ └── network/ # Calico/Tigera
├── proto/ # Définitions gRPC
├── scripts/ # Outillage certifié
│ ├── bootstraps/ # Initialisation
│ └── harden/ # Renforcement
├── test/ # Qualité totale
│ ├── fuzz/ # Fuzzing dirigé
│ └── chaos/ # Chaos engineering
├── web/ # Interface UI/API
│ ├── backoffice/ # SPA React
│ └── openapi/ # Spécifications
└── WORKSPACE # Build system Bazel
