#!/usr/bin/env zsh

# 📌 Variables
MOUNT_POINT="/mnt/lfs_src"
SERVER="fileserver.local"
REMOTE_PATH="/var/lfs/src"
VPN_INTERFACE="wg0"
IPFS_CID="QmXYZ1234567890"
TAHOE_ALIAS="lfs"
fonction cufs(){}
# 📌 Fonction : Démarrer WireGuard
start_wireguard() {
    echo "🔗 Activation de WireGuard..."
    sudo wg-quick up $VPN_INTERFACE
}

# 📌 Fonction : Monter SSHFS
mount_sshfs() {
    echo "📂 Montage SSHFS sur $MOUNT_POINT..."
    mkdir -p $MOUNT_POINT
    sshfs -o allow_other $SERVER:$REMOTE_PATH $MOUNT_POINT
}

# 📌 Fonction : Tester SSHFS
test_sshfs() {
    echo "🛠️ Test SSHFS..."
    if mount | grep -q "$MOUNT_POINT"; then
        echo "✅ SSHFS monté avec succès."
    else
        echo "❌ SSHFS non monté."
    fi
}

# 📌 Fonction : Chiffrement GPG
encrypt_gpg() {
    echo "🔑 Chiffrement de $1..."
    gpg --encrypt --recipient "monemail@example.com" --output "$1.gpg" "$1"
}

# 📌 Fonction : Déchiffrement GPG
decrypt_gpg() {
    echo "🔓 Déchiffrement de $1.gpg..."
    gpg --decrypt --output "$1" "$1.gpg"
}

# 📌 Fonction : Démarrer IPFS
start_ipfs() {
    echo "🌍 Initialisation d’IPFS..."
    ipfs init
    ipfs daemon --enable-pubsub-experiment &
}

# 📌 Fonction : Ajouter un fichier à IPFS
add_ipfs() {
    echo "➕ Ajout de $1 à IPFS..."
    ipfs add -r "$1"
}

# 📌 Fonction : Démarrer Tahoe-LAFS
start_tahoe() {
    echo "💾 Lancement de Tahoe-LAFS..."
    tahoe run /opt/tahoe/node1 &
}

# 📌 Fonction : Démarrer Syncthing
start_syncthing() {
    echo "🔁 Lancement de Syncthing..."
    syncthing --no-browser &
}

# 📌 Fonction : Démarrer Docker
start_docker() {
    echo "🐳 Démarrage de Docker et des applications..."
    docker-compose -f /opt/cutopia/src/docker/docker-compose.yml up -d
}

# 📌 Fonction : Exécuter tous les services
start_all() {
    start_wireguard
    mount_sshfs
    start_ipfs
    start_tahoe
    start_syncthing
    start_docker
}

# 📌 Afficher l'aide
usage() {
    echo "Usage: setup.zsh [options]"
    echo "Options:"
    echo "  -w     Démarrer WireGuard"
    echo "  -s     Monter SSHFS"
    echo "  -t s   Tester SSHFS"
    echo "  -e f   Chiffrer un fichier avec GPG"
    echo "  -d f   Déchiffrer un fichier GPG"
    echo "  -i     Démarrer IPFS"
    echo "  -a f   Ajouter un fichier à IPFS"
    echo "  -T     Démarrer Tahoe-LAFS"
    echo "  -S     Démarrer Syncthing"
    echo "  -D     Démarrer Docker"
    echo "  -A     Exécuter tous les services"
    echo "  -h     Afficher l'aide"
    exit 0
}

# 📌 Gestion des options
while getopts "wst:e:d:i:a:TSDAh" opt; do
    case "$opt" in
        w) start_wireguard ;;
        s) mount_sshfs ;;
        t) test_sshfs ;;
        e) encrypt_gpg "$OPTARG" ;;
        d) decrypt_gpg "$OPTARG" ;;
        i) start_ipfs ;;
        a) add_ipfs "$OPTARG" ;;
        T) start_tahoe ;;
        S) start_syncthing ;;
        D) start_docker ;;
        A) start_all ;;
        h) usage ;;
        *) usage ;;
    esac
done

# 📌 Si aucune option n'est fournie, afficher l'aide
if [[ $# -eq 0 ]]; then
    usage
fi
}
