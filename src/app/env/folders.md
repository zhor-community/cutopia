📂 mon_projet/
├── 📂 envFile/ # Dossier contenant les fichiers d'environnement
│ ├── delibarr.env
│ ├── mariadb.env
│ ├── postgis.env
│ ├── nginx.env
│ ├── gpg.env
│ ├── ipfs.env
│ ├── lxc.env
│ ├── sshfs.env
│ ├── ssl.env
│ ├── syncthing.env
│ ├── taheo.env
│ ├── wireguard.env
│
├── compose.template.yml # Fichier Docker Compose avec les variables d'environnement
