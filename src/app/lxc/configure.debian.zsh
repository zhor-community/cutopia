#!/usr/bin/env zsh

LOG_FILE="/opt/cutopia/src/logs/configure.log"

echo "🔧 Configuration des services..." | tee -a $LOG_FILE

# 📌 Fonction : Configurer WireGuard
configure_wireguard() {
    echo "🔗 Configuration de WireGuard..." | tee -a $LOG_FILE
    read -p "Entrer l'adresse IP du serveur VPN : " VPN_IP
    sed -i "s/Endpoint = .*/Endpoint = $VPN_IP:51820/" /etc/wireguard/wg0.conf
    systemctl restart wg-quick@wg0
    echo "✅ WireGuard configuré." | tee -a $LOG_FILE
}

# 📌 Fonction : Configurer SSHFS
configure_sshfs() {
    echo "📂 Configuration de SSHFS..." | tee -a $LOG_FILE
    read -p "Entrer l’adresse du serveur de fichiers : " SERVER
    read -p "Entrer le chemin distant : " REMOTE_PATH
    echo "$SERVER:$REMOTE_PATH" > /opt/cutopia/src/sshfs/config.txt
    bash /opt/cutopia/src/sshfs/mount.sh
    echo "✅ SSHFS configuré." | tee -a $LOG_FILE
}

# 📌 Fonction : Configurer Syncthing
configure_syncthing() {
    echo "🔁 Configuration de Syncthing..." | tee -a $LOG_FILE
    read -p "Entrer l'ID du serveur Syncthing : " SYNC_ID
    sed -i "s/<device id=.*/<device id=\"$SYNC_ID\" \/>/" /opt/cutopia/src/syncthing/config.xml
    systemctl restart syncthing
    echo "✅ Syncthing configuré." | tee -a $LOG_FILE
}

# 📌 Afficher l’aide
usage() {
    echo "Usage: configure.zsh [options]"
    echo "Options:"
    echo "  -w     Configurer WireGuard"
    echo "  -s     Configurer SSHFS"
    echo "  -S     Configurer Syncthing"
    echo "  -h     Afficher l'aide"
    exit 0
}

# 📌 Gestion des options
while getopts "wsSh" opt; do
    case "$opt" in
        w) configure_wireguard ;;
        s) configure_sshfs ;;
        S) configure_syncthing ;;
        h) usage ;;
        *) usage ;;
    esac
done

# 📌 Si aucune option n'est fournie, afficher l'aide
if [[ $# -eq 0 ]]; then
    usage
fi
