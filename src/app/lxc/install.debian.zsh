
#!/usr/bin/env zsh

LOG_FILE="/opt/cutopia/src/logs/install.log"

echo "🚀 Début de l’installation..." | tee -a $LOG_FILE

# 🔹 Mise à jour et installation des paquets
echo "📦 Installation des paquets nécessaires..." | tee -a $LOG_FILE
sudo apt update && sudo apt install -y wireguard sshfs ipfs tahoe-lafs syncthing docker-compose | tee -a $LOG_FILE

# 🔹 Configuration de WireGuard
echo "🔗 Configuration de WireGuard..." | tee -a $LOG_FILE
sudo cp /opt/cutopia/src/wireguard/wg0.conf /etc/wireguard/wg0.conf
sudo systemctl enable wg-quick@wg0
sudo systemctl start wg-quick@wg0

# 🔹 Configuration de SSHFS
echo "📂 Configuration de SSHFS..." | tee -a $LOG_FILE
mkdir -p /mnt/lfs_src
bash /opt/cutopia/src/sshfs/mount.sh | tee -a $LOG_FILE

# 🔹 Configuration de Syncthing
echo "🔁 Démarrage de Syncthing..." | tee -a $LOG_FILE
bash /opt/cutopia/src/syncthing/start.sh | tee -a $LOG_FILE

# 🔹 Configuration de Docker
echo "🐳 Configuration de Docker..." | tee -a $LOG_FILE
sudo systemctl enable docker
sudo systemctl start docker
bash /opt/cutopia/src/setup.zsh -D | tee -a $LOG_FILE

echo "✅ Installation terminée !" | tee -a $LOG_FILE

