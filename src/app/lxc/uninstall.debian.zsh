#!/usr/bin/env zsh

LOG_FILE="/opt/cutopia/src/logs/uninstall.log"

echo "🚀 Début de la désinstallation..." | tee -a $LOG_FILE

# 🔹 Arrêt des services
echo "🛑 Arrêt des services..." | tee -a $LOG_FILE
sudo systemctl stop wg-quick@wg0
sudo systemctl stop docker
pkill -f syncthing
pkill -f ipfs
pkill -f tahoe

# 🔹 Suppression des fichiers de configuration
echo "🗑️ Suppression des fichiers de configuration..." | tee -a $LOG_FILE
rm -rf /etc/wireguard/wg0.conf
rm -rf /mnt/lfs_src
rm -rf /opt/cutopia/src

# 🔹 Désinstallation des paquets
echo "📦 Désinstallation des paquets..." | tee -a $LOG_FILE
sudo apt remove -y wireguard sshfs ipfs tahoe-lafs syncthing docker-compose | tee -a $LOG_FILE
sudo apt autoremove -y | tee -a $LOG_FILE

echo "✅ Désinstallation terminée !" | tee -a $LOG_FILE

