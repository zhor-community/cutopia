#!/usr/bin/env zsh

LOG_FILE="/opt/cutopia/src/logs/install.log"

echo "🚀 Début de l’installation sur Alpine..." | tee -a $LOG_FILE

# 🔹 Mise à jour et installation des paquets
echo "📦 Installation des paquets nécessaires..." | tee -a $LOG_FILE
apk update && apk add wireguard-tools openssh sshfs ipfs tahoe-lafs syncthing docker docker-compose | tee -a $LOG_FILE

# 🔹 Activation de Docker
echo "🐳 Activation de Docker..." | tee -a $LOG_FILE
rc-update add docker default
service docker start

# 🔹 Configuration de WireGuard
echo "🔗 Configuration de WireGuard..." | tee -a $LOG_FILE
cp /opt/cutopia/src/wireguard/wg0.conf /etc/wireguard/wg0.conf
wg-quick up wg0

# 🔹 Montage SSHFS
echo "📂 Montage SSHFS..." | tee -a $LOG_FILE
mkdir -p /mnt/lfs_src
bash /opt/cutopia/src/sshfs/mount.sh | tee -a $LOG_FILE

# 🔹 Lancement de Syncthing
echo "🔁 Lancement de Syncthing..." | tee -a $LOG_FILE
nohup syncthing > /dev/null 2>&1 &

# 🔹 Lancement de Docker Compose
echo "🐳 Démarrage des applications Docker..." | tee -a $LOG_FILE
bash /opt/cutopia/src/setup.zsh -D | tee -a $LOG_FILE

echo "✅ Installation terminée !" | tee -a $LOG_FILE

