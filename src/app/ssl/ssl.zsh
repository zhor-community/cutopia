#!/usr/bin/env zsh

# === CONFIGURATION ===
LOG_FILE="/opt/cutopia/src/logs/letsencrypt.log"
MYDOMAINNAME=${MYDOMAINNAME:-"zhor-community.zid"}
EMAIL="admin@$MYDOMAINNAME"

# Sous-domaines par défaut
DEFAULT_SUBDOMAINS=(
    "delibarr"
    "mariadb"
    "postgis"
    "nginx"
    "gpg"
    "ipfs"
    "lxc"
    "sshfs"
    "ssl"
    "syncthing"
    "taheo"
    "wireguard"
)

# === FONCTIONS ===

# Gestion des sous-domaines avec options --add et --remove
function subdomainsnames() {
    local subdomains=("${DEFAULT_SUBDOMAINS[@]}")

    # Parsing des arguments
    local opts
    opts=$(getopt -o "a:r:" -l "add:,remove:" -- "$@") || {
        echo "Erreur : options invalides." | tee -a "$LOG_FILE"
        return 1
    }
    eval set -- "$opts"

    while true; do
        case "$1" in
            -a|--add)
                subdomains+=("$2")
                shift 2
                ;;
            -r|--remove)
                subdomains=("${(@)subdomains:#$2}") # Supprime un sous-domaine
                shift 2
                ;;
            --)
                shift
                break
                ;;
            *)
                echo "Erreur : option inconnue $1" | tee -a "$LOG_FILE"
                return 1
                ;;
        esac
    done

    # Générer les arguments Certbot pour les sous-domaines
    local domains="-d $MYDOMAINNAME"
    for sub in "${subdomains[@]}"; do
        domains+=" -d $sub.$MYDOMAINNAME"
    done
    echo "$domains"
}

# Vérifier et installer Certbot si nécessaire
function install_certbot_if_needed() {
    if ! command -v certbot &>/dev/null; then
        echo "Certbot non trouvé. Installation en cours..." | tee -a "$LOG_FILE"
        sudo apt update && sudo apt install -y certbot || {
            echo "Échec de l'installation de Certbot !" | tee -a "$LOG_FILE"
            exit 1
        }
    fi
}

# Obtenir un certificat SSL avec Certbot
function install_certbot() {
    install_certbot_if_needed

    local domains
    domains=$(subdomainsnames "$@") || {
        echo "Erreur dans la génération des sous-domaines." | tee -a "$LOG_FILE"
        exit 1
    }

    echo "Obtention du certificat SSL pour $MYDOMAINNAME et ses sous-domaines..." | tee -a "$LOG_FILE"
    sudo certbot certonly --manual --preferred-challenges=dns \
        --email "$EMAIL" --agree-tos --no-eff-email $domains | tee -a "$LOG_FILE"

    echo "Certificat SSL généré avec succès !" | tee -a "$LOG_FILE"
}

# Gestion des interruptions (CTRL+C)
trap 'echo "Opération annulée par l’utilisateur." | tee -a "$LOG_FILE"; exit 1' INT

# === EXECUTION ===
install_certbot "$@"

