#!/usr/bin/env zsh

LOG_FILE="/opt/cutopia/src/logs/ssl.log"
CERT_DIR="/opt/cutopia/src/ssl/certs"
DOMAIN="cutopia.local"

mkdir -p $CERT_DIR

echo "🔐 Génération d'un certificat SSL auto-signé pour $DOMAIN..." | tee -a $LOG_FILE

openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
    -keyout $CERT_DIR/domain.key \
    -out $CERT_DIR/domain.crt \
    -subj "/C=FR/ST=Paris/L=Paris/O=Cutopia/OU=IT/CN=$DOMAIN"

echo "✅ Certificat généré dans $CERT_DIR" | tee -a $LOG_FILE

