/opt/cutopia/src/app/ # Dossier principal
├── ssl/ # Gestion des certificats SSL
│ ├── generate_ssl.zsh # Génération SSL auto-signé
│ ├── install_certbot.zsh # Installation de Let's Encrypt
│ ├── certs/ # Stockage des certificats
│ │ ├── domain.crt
│ │ ├── domain.key
│ │ ├── fullchain.pem
│ │ ├── privkey.pem
├── install.zsh # Installation principale
├── configure.zsh # Configuration avec SSL
├── setup.zsh # Script principal
