# requirement cuapk nodejs 
cd ~
sudo apt-get purge neovim
sudo apt-get install nodejs npm yarn
curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim-linux64.tar.gz
sudo rm -rf /opt/nvim-linux64
sudo tar -C /opt -xzf nvim-linux64.tar.gz
sudo rm -rf $HOME/.config/nvim
sudo rm -rf ~/.local/share/nvim
mkdir -p ~/.local/share/fonts
curl -fLo "DroidSansMono.zip" https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/DroidSansMono.zip
unzip DroidSansMono.zip -d DroidSansMono
sudo dpkg-reconfigure fontconfig-config
rm -rf /home/hermit/.local/share/nvim/site/pack/packer/start/packer.nvim
python3 -m venv $CALPINE/cupy
source $CALPINE/cupy/bin/activate
pip3 install pynvim
cuapk add plantuml
cuapk add zathura
sudo chown -R $(whoami) /usr/local/lib/node_modules/
sudo chown -R $(whoami) /usr/local/bin
npm install -g prettier #prettierd
npm install -g @fsouza/prettierd
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
source $HOME/.cargo/env
git clone git@gitlab.com:zhor-community/cudit.git ~/.cudit
cargo --version
rustc --version
cargo install stylua
ln -s ~/.cudit ~/.config/nvim
git clone --depth 1 https://github.com/wbthomason/packer.nvim ~/.local/share/nvim/site/pack/packer/start/packer.nvim
nvim -c 'PackerInstall' -c 'PackerSync' -c 'qa'
cuapk add stylua
cd ~/.local/share/nvim/site/pack/packer/opt/markdown-preview.nvim/app
npm install
cd ~
sudo ln -s ~/.cudit /root/.config/nvim
sudo mkdir -p /root/.config
sudo ln -s ~/.cudit /root/.config/nvim
git clone --depth 1 https://github.com/wbthomason/packer.nvim\\n /root/.local/share/nvim/site/pack/packer/start/packer.nvim
sudo git clone --depth 1 https://github.com/wbthomason/packer.nvim\\n /root/.local/share/nvim/site/pack/packer/start/packer.nvim
