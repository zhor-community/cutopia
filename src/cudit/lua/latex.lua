-- Configuration de vimtex

vim.g.vimtex_view_method = "zathura" -- Choisis ton visualiseur PDF préféré

vim.g.vimtex_compiler_latexmk = {
	build_dir = "",
	continuous = 1,
	options = {
		"-shell-escape",
		"-file-line-error",
		"-synctex=1",
		"-interaction=nonstopmode",
	},
}
