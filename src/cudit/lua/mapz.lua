require("Comment").setup({
	padding = true,
	sticky = true,
	ignore = "^$",
	toggler = {
		line = "<leader>c",
		block = "<leader>C",
	},
	opleader = {
		line = "<leader>c",
		block = "<leader>C",
	},
	extra = {
		above = "<leader>cO",
		below = "<leader>co",
		eol = "<leader>cA",
	},
	mappings = {
		basic = true,
		extra = true,
		extended = false,
	},
	pre_hook = nil,
	post_hook = nil,
})
