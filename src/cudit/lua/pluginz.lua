
Plug 'jvirtanen/vim-octave'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'yuki-ycino/fzf-preview.vim', { 'branch': 'release/remote', 'do': ':UpdateRemotePlugins' }
Plug 'lervag/vim-foam'
Plug 'sudar/vim-arduino-syntax'
Plug 'kkvh/vim-docker-tools'
Plug 'xuhdev/vim-latex-live-preview'
Plug 'aklt/plantuml-syntax'             " syntax hl for puml
let mapleader=" "
nnoremap <Leader>ff :CtrlP<CR>
nnoremap <Leader><Leader> :CtrlPBuffer<CR>
nnoremap <Leader>. :bd<CR>
nnoremap <Leader>n :bn<CR>
nnoremap <Leader>b :bN<CR>
nnoremap <Leader>z :RnvimrToggle <CR>
nnoremap <leader>x :rnvimrresize <cr>
nnoremap <leader>h :sp <cr>
nnoremap <leader>v :vs<cr>

" Make adjusing splits size 
noremap <silent> <C-Left> :vertical resize -3<CR>
noremap <silent> <C-Right> :vertical resize +3<CR>
noremap <silent> <C-Up> :resize +3<CR>
noremap <silent> <C-Down> :resize -3<CR>
" Change 2 splits vertical to horiz
map <Leader>th <C-w>t<C-w>H
map <Leader>tk <C-w>t<C-w>K
"Autoformat setting
noremap <F3> :Autoformat<CR>

:imap ;; <Esc>
:map ;; <Esc>

vmap ++ <plug>NERDCommenterToggle
nmap ++ <plug>NERDCommenterToggle
