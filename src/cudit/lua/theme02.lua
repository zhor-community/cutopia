local themeStatus, tokyonight = pcall(require, "tokyonight")

if themeStatus then
    vim.cmd("colorscheme tokyonight")
    vim.g.tokyonight_transparent = true
else
    return
end

-- Activer la transparence pour l'éditeur
vim.cmd [[
    hi Normal guibg=NONE ctermbg=NONE
    hi NormalNC guibg=NONE ctermbg=NONE
    hi SignColumn guibg=NONE ctermbg=NONE
    hi EndOfBuffer guibg=NONE ctermbg=NONE
]]
