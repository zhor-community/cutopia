-- Automatically run: PackerCompile
vim.api.nvim_create_autocmd("BufWritePost", {
	group = vim.api.nvim_create_augroup("PACKER", { clear = true }),
	pattern = "plugins.lua",
	command = "source <afile> | PackerCompile",
})

return require("packer").startup(function(use)
	-- Packer
	use("wbthomason/packer.nvim")

	-- Common utilities
	use("nvim-lua/plenary.nvim")

	-- Icons
	use("nvim-tree/nvim-web-devicons")

	-- Colorschema
	use("rebelot/kanagawa.nvim")
	use("folke/tokyonight.nvim") -- Ajout du thème tokyonight

	-- Statusline
	use({
		"nvim-lualine/lualine.nvim",
		event = "BufEnter",
		config = function()
			require("configs.lualine")
		end,
		requires = { "nvim-web-devicons" },
	})

	-- Treesitter
	use({
		"nvim-treesitter/nvim-treesitter",
		run = function()
			require("nvim-treesitter.install").update({ with_sync = true })
		end,
		config = function()
			require("configs.treesitter")
		end,
	})

	use({ "windwp/nvim-ts-autotag", after = "nvim-treesitter" })

	-- Telescope
	use({
		"nvim-telescope/telescope.nvim",
		tag = "0.1.1",
		requires = { { "nvim-lua/plenary.nvim" } },
	})

	-- LSP
	use({
		"neovim/nvim-lspconfig",
		config = function()
			require("configs.lsp")
		end,
	})

	use("onsails/lspkind-nvim")
	use({
		"L3MON4D3/LuaSnip",
		-- follow latest release.
		tag = "v<CurrentMajor>.*",
	})

	-- cmp: Autocomplete
	use({
		"hrsh7th/nvim-cmp",
		event = "InsertEnter",
		config = function()
			require("configs.cmp")
		end,
	})

	use("Shougo/deoplete.nvim")

	use("hrsh7th/cmp-nvim-lsp")

	use({ "hrsh7th/cmp-path", after = "nvim-cmp" })

	use({ "hrsh7th/cmp-buffer", after = "nvim-cmp" })

	-- LSP diagnostics, code actions, and more via Lua.
	use({
		"jose-elias-alvarez/null-ls.nvim",
		config = function()
			require("configs.null-ls")
		end,
		requires = { "nvim-lua/plenary.nvim" },
	})

	-- Mason: Portable package manager
	use({
		"williamboman/mason.nvim",
		config = function()
			require("mason").setup()
		end,
	})

	use({
		"williamboman/mason-lspconfig.nvim",
		config = function()
			require("configs.mason-lsp")
		end,
		after = "mason.nvim",
	})

	-- File manager
	use({
		"nvim-neo-tree/neo-tree.nvim",
		branch = "v2.x",
		requires = {
			"nvim-lua/plenary.nvim",
			"nvim-tree/nvim-web-devicons",
			"MunifTanjim/nui.nvim",
		},
	})

	-- Show colors
	use({
		"norcalli/nvim-colorizer.lua",
		config = function()
			require("colorizer").setup({ "*" })
		end,
	})

	-- Terminal
	use({
		"akinsho/toggleterm.nvim",
		tag = "*",
		config = function()
			require("configs.toggleterm")
		end,
	})

	-- Git
	use({
		"lewis6991/gitsigns.nvim",
		config = function()
			require("configs.gitsigns")
		end,
	})
	use({
		"iamcco/markdown-preview.nvim",
		run = function()
			vim.fn["mkdp#util#install"]()
		end,
	})

	-- autopairs
	use({
		"windwp/nvim-autopairs",
		config = function()
			require("configs.autopairs")
		end,
	})

	-- Background Transparent
	use({
		"xiyaowong/nvim-transparent",
		config = function()
			require("configs.transparent")
		end,
	})

	use({
		"kevinhwang91/rnvimr",
		config = function()
			-- vim.g.rnvimr_enable_ex = 1
			vim.g.rnvimr_enable_ex = 0
			vim.g.rnvimr_enable_picker = 1
		end,
	})

	use({ "lervag/vim-foam" })
	use({ "sudar/vim-arduino-syntax" })
	use({ "kkvh/vim-docker-tools" })
	use({ "numToStr/Comment.nvim" })
	use({
		"nvimdev/dashboard-nvim",
		event = "VimEnter",
		config = function()
			require("dashboard").setup({
				-- config
				theme = "hyper",
				config = {
					week_header = {
						enable = true,
					},
					shortcut = {
						{ desc = "Update", group = "@property", action = "PackerSync", key = "u" },
						{
							icon = " ",
							icon_hl = "@variable",
							desc = "Files",
							group = "Label",
							action = "Telescope find_files",
							key = "f",
						},
						{
							desc = " Apps",
							group = "DiagnosticHint",
							action = "Telescope app",
							key = "a",
						},
						{
							desc = "ticenergy",
							group = "Number",
							action = "Telescope cutopia",
							key = "t",
						},
						{
							desc = "cutopia",
							group = "Number",
							action = "Telescope cutopia",
							key = "z",
						},
					},
				},
			})
		end,
		requires = { "nvim-tree/nvim-web-devicons" },
	})

	use({ "aklt/plantuml-syntax" })
	use("weirongxu/plantuml-previewer.vim")
	use("tyru/open-browser.vim")

	-- Plugin csv.vim pour le traitement des fichiers CSV
	use({
		"chrisbra/csv.vim",
		config = function()
			-- Configuration avancée pour csv.vim

			-- Remapper les touches et ajouter des commandes spécifiques pour les fichiers CSV
			vim.cmd([[
                " Activer l'alignement automatique des colonnes avec <leader>a
                autocmd FileType csv nnoremap <leader>a :CSVArrangeColumn<CR>

                " Activer le tri des colonnes avec <leader>t
                autocmd FileType csv nnoremap <leader>t :CSVSort<CR>

                " Afficher les options CSV au démarrage
                autocmd FileType csv setlocal formatoptions-=cro

                " Définir les délimiteurs par défaut (par exemple, la virgule)
                let g:csv_default_delimiter = ','
                
                " Définir le mode de citation par défaut (guillemets doubles)
                let g:csv_default_quoting = 'double'

                " Activer la syntaxe de surlignage pour les cellules vides
                let g:csv_highlight_empty_cells = 1

                " Activer la syntaxe de surlignage pour les doublons
                let g:csv_highlight_duplicates = 1

                " Activer le tri automatique en mode de visualisation
                let g:csv_auto_sort = 0
                                " Activer la coloration syntaxique
                syntax enable
                syntax on

                " Appliquer des options spécifiques pour les fichiers CSV
                autocmd FileType csv setlocal formatoptions-=cro

                " Définir les délimiteurs par défaut (par exemple, la virgule)
                let g:csv_default_delimiter = ','
                
                " Définir le mode de citation par défaut (guillemets doubles)
                let g:csv_default_quoting = 'double'

                " Activer la coloration pour les cellules vides et les doublons
                let g:csv_highlight_empty_cells = 1
                let g:csv_highlight_duplicates = 1
                
                " Option pour aligner les colonnes à gauche
                let g:csv_align_left = 1
            ]])
		end,
	}) --use

	-- latex
	use("lervag/vimtex")

	--------------------------------------- END --------------------------------------
end)
