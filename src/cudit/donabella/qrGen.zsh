qrencode "bitcoin:bc1qpdtye4u506fu806459k36wphax33gqxgfm8jmz" -lQ -o img/bitcoinQR.svg
qrencode "solana:5shsoZgU24aShtSVTrWKawCSbBUJLk1oRu3mk1HtjFRd" -lQ -o img/solanaQR.svg
qrencode "ethereum:0x66C4fd42Ea53C8A04f6645c42c2deCaDc7e20579" -lQ -o img/ethereumQR.svg
qrencode "polygon:0x66C4fd42Ea53C8A04f6645c42c2deCaDc7e20579" -lQ -o img/polygonQR.svg
