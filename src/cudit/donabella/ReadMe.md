# Donations

To support this project, you can make a donation to its current maintainer:

### Bitcoin

[![Donate via Bitcoin](img/bitcoin-donate-black.png)](wallet/Bitcoin)
[![Donate via Bitcoin QR Code](img/bitcoinQR.svg)](wallet/Bitcoin)

### Solana

[![Donate via Bitcoin](img/donateSolana.svg)](wallet/Solana)
[![Donate via Solana QR Code](img/solanaQR.svg)](wallet/Solana)

### Ethereum

[![Donate via Bitcoin](img/donateEthereum.svg)](wallet/Ethereum)
[![Donate via Ethereum QR Code](img/ethereumQR.svg)](wallet/Ethereum)

### Polygon

[![Donate via Bitcoin](img/donatePolygon.svg)](wallet/Polygon)
[![Donate via Polygon QR Code](img/polygonQR.svg)](wallet/Polygon)

## QR Code Generation

To generate QR codes for Solana, Ethereum, and Polygon, use the following commands:

```bash

qrencode "bitcoin:bc1qpdtye4u506fu806459k36wphax33gqxgfm8jmz" -lQ -o img/bitcoinQR.svg
qrencode "solana:5shsoZgU24aShtSVTrWKawCSbBUJLk1oRu3mk1HtjFRd" -lQ -o img/solanaQR.svg
qrencode "ethereum:0x66C4fd42Ea53C8A04f6645c42c2deCaDc7e20579" -lQ -o img/ethereumQR.svg
qrencode "polygon:0x66C4fd42Ea53C8A04f6645c42c2deCaDc7e20579" -lQ -o img/polygonQR.svg

```

---
