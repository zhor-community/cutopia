-- 🌑 init.lua
require("settings")
require("maps")
require("plugins")
require("theme02")
require("cucsv")
require("latex")
vim.g.python3_host_prog = "/usr/bin/python3"
