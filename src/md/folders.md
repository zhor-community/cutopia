/opt/cutopia/src/ # Dossier principal des applications
├── setup.zsh # Script principal d’installation
├── wireguard/
│ ├── wg0.conf # Config WireGuard
├── sshfs/
│ ├── mount.sh # Script de montage SSHFS
├── gpg/
│ ├── encrypt.sh # Script de chiffrement GPG
│ ├── decrypt.sh # Script de déchiffrement GPG
├── ipfs/
│ ├── start.sh # Initialisation IPFS
│ ├── add.sh # Ajout des fichiers IPFS
├── tahoe/
│ ├── start.sh # Lancement Tahoe-LAFS
│ ├── tahoe.cfg # Config Tahoe
├── syncthing/
│ ├── start.sh # Lancement Syncthing
│ ├── config.xml # Configuration Syncthing
├── docker/
│ ├── docker-compose.yml # Services Docker
│ ├── app1/
│ │ ├── Dockerfile # Fichier Docker App1
│ ├── app2/
│ │ ├── Dockerfile # Fichier Docker App2
