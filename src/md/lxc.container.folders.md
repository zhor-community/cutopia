/opt/cutopia/src/ # Dossier principal des applications
├── docker-compose.yml # Fichier de configuration Docker Compose
├── envFile # Variables d'environnement (ports, mémoire, etc.)
├── app1/ # Dossier pour App1
│ ├── Dockerfile # Image Docker pour App1
│ ├── config/ # Configuration spécifique
├── app2/ # Dossier pour App2
│ ├── Dockerfile  
│ ├── config/
├── appN/ # Jusqu'à App19
│ ├── Dockerfile  
│ ├── config/

/opt/cutopia/src/etc/docker/ # Configuration Docker
└── daemon.json # Config avec registre privé & optimisation logs

/opt/cutopia/var/lib/lxc/subvps1/ # Dossier LXC du premier SubVPS
└── config # Configuration LXC (RAM, CPU, disque)

/var/lfs/mnt/fileserver/ # Montage du stockage NFS depuis le file server
└── docker-volumes/ # Stockage partagé entre tous les containers
└── docker-images/ # Stockage partagé entre tous les containers
└── lxc-template/ # Stockage partagé entre tous les containers
└── lxc-pool/ # Stockage partagé entre tous les containers
└── qemu-vm/ # Stockage partagé entre tous les containers
└── xen-vm/ # Stockage partagé entre tous les containers

/root/cloud-init.yml # Fichier Cloud-Init pour provisionner le SubVPS
