cutopia() {
    local usage="Usage: cutopia [-a] [-g] [-i] [-s user@serveur --from git|local] [-u] [-h]"
    local archive="cutopia.tar.gz"
    local install_dir="/opt/cutopia"
    local init_script="$install_dir/init.zsh"
    local ssh_key="$HOME/.ssh/id_rsa"
    local log_file="/var/log/cutopia.log"

    # Vérification des prérequis
    for cmd in zsh git tar ssh scp; do
        if ! command -v "$cmd" >/dev/null 2>&1; then
            echo "[❌] Erreur : $cmd n'est pas installé. Veuillez l'installer."
            return 1
        fi
    done

    echo "[ℹ️ $(date +'%Y-%m-%d %H:%M:%S')] Déploiement Cutopia commencé" | sudo tee -a "$log_file"

    if (( $# == 0 )); then
        echo "[❌] Erreur : Aucun argument fourni."
        echo "$usage"
        return 1
    fi

    while (( $# > 0 )); do
        local option="$1"
        shift

        case "$option" in
            -a)
                echo "[📦] Extraction de l'archive locale..."
                [[ -f "$archive" ]] || { echo "[⚠️] Erreur : Archive introuvable."; return 1; }
                sudo tar -xvf "$archive" -C "$install_dir"
                echo "[✅] Extraction terminée."
                ;;
            -g)
                echo "[🔄] Clonage du dépôt Git..."
                command -v git >/dev/null 2>&1 || { echo "[⚠️] Erreur : Git n'est pas installé."; return 1; }
                git clone git@github.com:cutopia/cutopia.git "$install_dir"
                echo "[✅] Clonage terminé."
                ;;
            -i)
                echo "[⚙️] Exécution du script d'initialisation..."
                [[ -x "$init_script" ]] || { echo "[⚠️] Erreur : Script d'initialisation introuvable."; return 1; }
                "$init_script"
                echo "[✅] Initialisation réussie."
                ;;
            -s)
                local server="$1"
                local source="$3"
                shift 2
                
                [[ -z "$server" || "$2" != "--from" || -z "$source" ]] && { echo "[❌] Mauvaise syntaxe."; echo "$usage"; return 1; }

                echo "[🌍] Installation sur $server depuis $source..."

                ssh -i "$ssh_key" -o BatchMode=yes -o ConnectTimeout=5 "$server" "echo Connexion réussie" || {
                    echo "[❌] Erreur : Connexion SSH impossible à $server"
                    return 1
                }

                if [[ "$source" == "git" ]]; then
                    ssh -i "$ssh_key" "$server" "git clone git@github.com:cutopia/cutopia.git $install_dir && $init_script"
                elif [[ "$source" == "local" ]]; then
                    [[ -f "$archive" ]] || { echo "[⚠️] Erreur : Archive locale introuvable."; return 1; }
                    scp -i "$ssh_key" "$archive" "$server:/tmp/" && ssh -i "$ssh_key" "$server" "tar -xvf /tmp/$archive -C $install_dir && $init_script"
                else
                    echo "[❌] Source inconnue ($source)."
                    return 1
                fi
                ;;
            -u)
                echo "[❌] Désinstallation de Cutopia..."
                sudo rm -rf "$install_dir"
                echo "[✅] Désinstallation réussie."
                ;;
            -h)
                echo "$usage"
                ;;
            *)
                echo "[❌] Option inconnue $option"
                echo "$usage"
                return 1
                ;;
        esac
    done
}

