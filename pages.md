frontend/src/pages/
├── Dashboard.vue # Vue d'ensemble des VMs et ressources
├── VirtualMachines.vue # Liste et gestion des machines virtuelles
├── VMDetails.vue # Détails d'une VM spécifique (CPU, RAM, stockage, réseau)
├── Storage.vue # Gestion du stockage (volumes, snapshots)
├── Network.vue # Gestion des réseaux (bridges, NAT, VLANs)
├── Templates.vue # Gestion des templates de VM
├── Users.vue # Gestion des utilisateurs et permissions
├── Logs.vue # Historique et logs du système
├── Settings.vue # Paramètres globaux de la plateforme
├── Login.vue # Page de connexion
├── Register.vue # Page d'inscription (optionnel)
├── NotFound.vue # Page 404 pour les routes inconnues

├── Cluster.vue # Vue globale des nœuds du cluster
├── NodeList.vue # Liste des nœuds disponibles dans le cluster
├── NodeDetails.vue # Détails d’un nœud spécifique (CPU, RAM, stockage, état)
├── NodeNetworking.vue # Gestion du réseau d’un nœud spécifique
├── NodeStorage.vue # Gestion du stockage sur un nœud
├── NodeMetrics.vue # Statistiques et monitoring des ressources du nœud
├── NodeSettings.vue # Configuration spécifique d’un nœud (ajout/suppression, mise à jour)
