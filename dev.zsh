
#!/usr/bin/env zsh

# Définition des variables
FILESERVER="fileserver.local"
MOUNT_POINT="/var/lfs/mnt/fileserver"
DOCKER_COMPOSE_DIR="/opt/cutopia/src"
LXC_CONFIG_DIR="/opt/cutopia/var/lib/lxc/subvps1"
DOCKER_DAEMON_CONFIG="/opt/cutopia/src/etc/docker/daemon.json"

# Fonction pour monter le stockage NFS
mount_nfs() {
    echo "📂 Montage du stockage NFS..."
    mkdir -p $MOUNT_POINT
    mount -t nfs $FILESERVER:/data $MOUNT_POINT
    echo "$FILESERVER:/data $MOUNT_POINT nfs defaults 0 0" >> /etc/fstab
}

# Fonction pour configurer Docker
setup_docker() {
    echo "🐳 Configuration de Docker..."
    mkdir -p $(dirname $DOCKER_DAEMON_CONFIG)
    cat <<EOF > $DOCKER_DAEMON_CONFIG
{
  "insecure-registries": ["$FILESERVER:5000"],
  "log-driver": "json-file",
  "log-opts": { "max-size": "50m", "max-file": "3" },
  "storage-driver": "overlay2"
}
EOF
    service docker restart
}

# Fonction pour configurer LXC
setup_lxc() {
    echo "🖥️ Configuration de LXC..."
    mkdir -p $LXC_CONFIG_DIR
    cat <<EOF > $LXC_CONFIG_DIR/config
lxc.cgroup.memory.limit_in_bytes = 8G
lxc.cgroup.memory.memsw.limit_in_bytes = 12G
lxc.cgroup.cpu.shares = 1024
lxc.rootfs.size = 120G
EOF
}

# Fonction pour créer la structure des dossiers
create_folders() {
    echo "📂 Création des dossiers..."
    mkdir -p $DOCKER_COMPOSE_DIR/app{1..19}/config
    mkdir -p $MOUNT_POINT/{docker-volumes,docker-images,lxc-template,lxc-pool,qemu-vm,xen-vm}
}

# Fonction pour générer le fichier Docker Compose
generate_docker_compose() {
    echo "📝 Génération de docker-compose.yml..."
    cat <<EOF > $DOCKER_COMPOSE_DIR/docker-compose.yml
version: "3.8"
services:
EOF
    for i in {1..19}; do
        cat <<EOF >> $DOCKER_COMPOSE_DIR/docker-compose.yml
  app$i:
    image: $FILESERVER:5000/myapp$i:latest
    restart: always
    mem_limit: 500m
    cpu_shares: 256
    ports:
      - "80$i:80"
    volumes:
      - "$MOUNT_POINT/docker-volumes/app$i:/app/data"
EOF
    done
}

# Fonction pour démarrer Docker Compose
start_docker_compose() {
    echo "🚀 Lancement des applications Docker..."
    cd $DOCKER_COMPOSE_DIR
    docker-compose up -d
}

# Exécution des fonctions
mount_nfs
setup_docker
setup_lxc
create_folders
generate_docker_compose
start_docker_compose

echo "✅ Configuration terminée avec succès !"
