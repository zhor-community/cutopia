# 📘 Manuel d'Utilisation - Script d'Installation Cutopia

## 🔹 Introduction

Le script `cutopia.zsh` est conçu pour automatiser l'installation, la configuration et la désinstallation de Cutopia, aussi bien en local que sur un serveur distant. Il prend en charge l'extraction d'archives, le clonage de dépôts Git, et l'exécution d'un script d'initialisation.

## ⚙️ Prérequis

Avant d'exécuter le script, assurez-vous de disposer des éléments suivants :

- Un système Unix/Linux avec `zsh`
- Accès `sudo` configuré
- Une connexion SSH fonctionnelle pour les installations distantes
- Une clé SSH valide pour accéder au dépôt Git
- `tar` et `git` installés

## 📥 Installation

### 📌 Obtention du script

Vous pouvez obtenir le script `cutopia.zsh` de plusieurs façons :

- En le téléchargeant directement depuis le dépôt officiel Git :
  ```sh
  git clone git@github.com:cutopia/cutopia.git && cd cutopia
  ```
- En téléchargeant une archive contenant le script et en l'extrayant.

### 📌 Permissions d'exécution

Avant d'exécuter le script, assurez-vous qu'il possède les bonnes permissions :

```sh
chmod +x cutopia.zsh
```

## 🚀 Utilisation du Script

### 📌 Syntaxe Générale

```sh
./cutopia.zsh [options]
```

### 📌 Options Disponibles

| Option                            | Description                                                      |
| --------------------------------- | ---------------------------------------------------------------- |
| 🚀 `-a`                           | Extraire l'archive localement                                    |
| 🔄 `-g`                           | Cloner le dépôt Git localement                                   |
| ⚙️ `-i`                           | Exécuter le script d'initialisation (`init.zsh`)                 |
| 🌍 `-s user@serveur --from git`   | Installer Cutopia depuis le dépôt Git sur un serveur distant     |
| 📦 `-s user@serveur --from local` | Installer Cutopia depuis l'archive locale sur un serveur distant |
| ❌ `-u`                           | Désinstaller Cutopia                                             |
| ❓ `-h`                           | Afficher l'aide                                                  |

### 📌 Exemples d'Utilisation

1. **Extraire l'archive localement**
   ```sh
   ./cutopia.zsh -a
   ```
2. **Cloner le dépôt Git**
   ```sh
   ./cutopia.zsh -g
   ```
3. **Exécuter le script d'initialisation**
   ```sh
   ./cutopia.zsh -i
   ```
4. **Installer Cutopia depuis Git sur un serveur distant**
   ```sh
   ./cutopia.zsh -s user@serveur --from git
   ```
5. **Installer Cutopia depuis l'archive locale sur un serveur distant**
   ```sh
   ./cutopia.zsh -s user@serveur --from local
   ```
6. **Désinstaller Cutopia**
   ```sh
   ./cutopia.zsh -u
   ```
7. **Afficher l'aide**
   ```sh
   ./cutopia.zsh -h
   ```

### 🔍 Installation distante - Précisions

- **Remplacez `user@serveur`** par l'utilisateur et le serveur cible, par exemple : `admin@192.168.1.10`.
- **Vérifiez que le serveur distant** dispose de `git`, `tar` et `zsh` installés.
- **Assurez-vous d'avoir les permissions nécessaires** pour exécuter des commandes avec `sudo` sur le serveur distant.

## 🛠️ Fonctionnalités Avancées

- **🔍 Journalisation** : Toutes les opérations sont enregistrées dans `/var/log/cutopia_install.log`.
- **🛑 Sécurisation** : Le script utilise `set -euo pipefail` pour éviter les erreurs et interruptions accidentelles.
- **🔑 Gestion des permissions** : L'installation ajuste automatiquement les droits d'accès.
- **🗑 Désinstallation complète** : Suppression propre des fichiers, groupes et utilisateurs associés.

## ❗ Dépannage

| Problème                       | Solution                                                                             |
| ------------------------------ | ------------------------------------------------------------------------------------ |
| Problème d'accès SSH           | Vérifiez que votre clé SSH est bien ajoutée à votre agent (`ssh-add ~/.ssh/id_rsa`). |
| Permissions insuffisantes      | Exécutez le script avec `sudo` si nécessaire.                                        |
| Archive introuvable            | Assurez-vous qu'elle est placée à l'emplacement défini dans `ARCHIVE`.               |
| Clonage Git échoué             | Vérifiez votre connexion réseau et vos autorisations d'accès au dépôt.               |
| `tar` ou `git` non installés   | Installez-les avec `sudo apt install tar git` ou `sudo yum install tar git`.         |
| Problème d’exécution du script | Essayez avec `zsh ./cutopia.zsh` au lieu de `./cutopia.zsh`.                         |

## 📌 Conclusion

Ce script offre une solution standardisée et automatisée pour le déploiement de Cutopia. Il garantit un processus d'installation fiable et sécurisé, adapté aux environnements de production et de développement.

---

**📅 Version :** 1.0  
**✍️ Auteur :** Équipe Cutopia  
**📌 Dernière mise à jour :** $(date +'%Y-%m-%d')
