frontend/src/layouts/
├── MainLayout.vue # Layout principal (barre latérale, navbar, etc.)
├── AuthLayout.vue # Layout pour les pages d'authentification (Login, Register)
├── DashboardLayout.vue # Layout spécifique pour le tableau de bord
├── ClusterLayout.vue # Layout pour la gestion des nœuds du cluster
├── VMLayout.vue # Layout pour la gestion des machines virtuelles
├── StorageLayout.vue # Layout pour la gestion du stockage
├── NetworkLayout.vue # Layout pour la gestion du réseau
