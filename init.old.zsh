#!/bin/zsh

# Définition des variables
HOSTNAME="subvps1"
DOCKER_APPS_DIR="/opt/docker-apps"
DOCKER_COMPOSE_FILE="$DOCKER_APPS_DIR/docker-compose.yml"

# Configuration de l'hôte
hostnamectl set-hostname $HOSTNAME
echo "127.0.1.1 $HOSTNAME" >> /etc/hosts

echo "Configuration du fuseau horaire..."
timedatectl set-timezone UTC

# Mise à jour du système et installation des paquets
apk update && apk add --no-cache docker docker-compose curl

# Activation et démarrage de Docker
rc-update add docker boot
service docker start

# Configuration de Docker (logs et stockage)
cat <<EOF > /etc/docker/daemon.json
{
  "log-driver": "json-file",
  "log-opts": { "max-size": "50m", "max-file": "3" },
  "storage-driver": "overlay2"
}
EOF
service docker restart

# Création du répertoire des applications Docker
mkdir -p $DOCKER_APPS_DIR
cd $DOCKER_APPS_DIR

# Création du fichier docker-compose.yml
cat <<EOF > $DOCKER_COMPOSE_FILE
version: "3.8"
services:
  app1:
    image: myapp1:latest
    restart: always
    mem_limit: 500m
    cpu_shares: 256
    ports:
      - "8081:80"
  app2:
    image: myapp2:latest
    restart: always
    mem_limit: 500m
    cpu_shares: 256
    ports:
      - "8082:80"
EOF

# Démarrage des services Docker Compose
docker-compose -f $DOCKER_COMPOSE_FILE up -d

# Configuration des ressources pour LXC
echo "Configuration des ressources LXC..."
mkdir -p /var/lib/lxc/$HOSTNAME/
cat <<EOF > /var/lib/lxc/$HOSTNAME/config
lxc.cgroup.memory.limit_in_bytes = 8G
lxc.cgroup.memory.memsw.limit_in_bytes = 12G
lxc.cgroup.cpu.shares = 1024
lxc.rootfs.size = 120G
EOF

echo "Initialisation terminée avec succès."

