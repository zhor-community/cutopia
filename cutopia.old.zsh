#!/bin/zsh
set -euo pipefail  # Sécurise le script : stoppe à la moindre erreur et détecte les variables non définies

# === CONFIGURATION ===
ARCHIVE="/home/hermit/talbine.tar.gz"
DEST_DIR="/opt/cutopia"
USER="hermit"
GROUP="cutopia"
REPO="git@gitlab.com:zhor-community/cutopia.git"
INIT_SCRIPT="$DEST_DIR/init.zsh"

SSH_USER=""
SSH_HOST=""

# === FONCTIONS ===
sudo tar -czvf $ARCHIVE .
## Vérifier et créer le groupe si nécessaire
ensure_group() {
    if ! getent group "$GROUP" >/dev/null; then
        echo "👥 Création du groupe $GROUP..."
        sudo groupadd "$GROUP"
    fi
}

## Extraire l'archive localement
extract_archive() {
    if [[ ! -f "$ARCHIVE" ]]; then
        echo "❌ Erreur : L'archive $ARCHIVE est introuvable."
        exit 1
    fi

    echo "📦 Extraction de l'archive $ARCHIVE..."
    #sudo mkdir -p "$DEST_DIR"
    sudo tar -xvzf "$ARCHIVE" -C /opt/ # "$DEST_DIR"
    echo "✅ Archive extraite avec succès."
}

## Cloner le dépôt Git localement
clone_repo() {
    echo "🌍 Clonage du dépôt Git depuis $REPO..."
    sudo rm -rf "$DEST_DIR"
    sudo git clone "$REPO" "$DEST_DIR"
    echo "✅ Clonage terminé avec succès."
}

## Exécuter le script d'initialisation
run_init_script() {
    if [[ -f "$INIT_SCRIPT" ]]; then
        echo "🚀 Exécution de l'initialisation avec $INIT_SCRIPT..."
        cd $DEST_DIR
        zsh "$INIT_SCRIPT"
        echo "✅ Initialisation terminée."
    else
        echo "⚠️ Fichier init.zsh introuvable dans $DEST_DIR."
    fi
}

## Exécuter une commande SSH
ssh_exec() {
    local cmd="$1"
    ssh -o StrictHostKeyChecking=no "$SSH_USER@$SSH_HOST" "$cmd"
}

## Installation sur un serveur distant via SSH
install_remote() {
  # Vérifier et créer l'utilisateur et le groupe sur le serveur distant
    ssh_exec "getent group $GROUP >/dev/null || sudo groupadd $GROUP"
    ssh_exec "id -u $USER >/dev/null 2>&1 || sudo useradd -m -g $GROUP $USER"

    if [[ -z "$SSH_USER" || -z "$SSH_HOST" ]]; then
        echo "❌ Erreur : Spécifiez un serveur distant avec -s <utilisateur@hôte>"
        exit 1
    fi

    echo "🔗 Connexion à $SSH_USER@$SSH_HOST pour installation..."

    # Vérifier si Cutopia existe déjà sur le serveur distant
    if ssh_exec "[ -d $DEST_DIR ]"; then
        echo "⚠️ Cutopia existe déjà sur le serveur distant. Suppression..."
        ssh_exec "sudo rm -rf $DEST_DIR"
    fi

    # Transfert de l'archive ou clonage Git
    if [[ -f "$ARCHIVE" ]]; then
        echo "📤 Transfert de l'archive vers $SSH_USER@$SSH_HOST..."
        scp "$ARCHIVE" "$SSH_USER@$SSH_HOST:/tmp/talbine.tar.gz"
        ssh_exec "sudo mkdir -p $DEST_DIR && sudo tar -xvzf /tmp/talbine.tar.gz -C $DEST_DIR"
    else
        echo "🌍 Clonage du dépôt Git sur le serveur distant..."
        ssh_exec "sudo git clone $REPO $DEST_DIR"
    fi

    # Mise à jour des permissions
    ssh_exec "sudo chown -R $USER:$GROUP $DEST_DIR"

    # Exécuter l'initialisation sur le serveur distant
    if ssh_exec "[ -f $INIT_SCRIPT ]"; then
        echo "🚀 Exécution de l'initialisation sur le serveur distant..."
        ssh_exec "zsh $INIT_SCRIPT"
    else
        echo "⚠️ Fichier init.zsh introuvable sur le serveur distant."
    fi

    echo "✅ Installation terminée sur $SSH_HOST."
}

## Afficher l'aide
show_help() {
    cat <<EOF
Usage: $0 [options]

Options :
  -a    Extraire l'archive localement
  -g    Cloner le dépôt Git localement
  -i    Exécuter l'initialisation avec init.zsh localement
  -s    Installer Cutopia sur un serveur distant (ex: -s user@serveur)
  -h    Afficher cette aide
EOF
    exit 0
}

# === LOGIQUE PRINCIPALE ===
ensure_group  # Vérifier le groupe

while getopts "agis:h" opt; do
    case $opt in
        a) extract_archive ;;
        g) clone_repo ;;
        i) run_init_script ;;
        s) 
            SSH_USER=$(echo "$OPTARG" | cut -d'@' -f1)
            SSH_HOST=$(echo "$OPTARG" | cut -d'@' -f2)
            install_remote
            ;;
        h) show_help ;;
        *) show_help ;;
    esac
done

# Si aucune option n'est fournie, afficher l'aide
if [[ $OPTIND -eq 1 ]]; then
    show_help
fi

# Appliquer les permissions localement
sudo chown -R "$USER:$GROUP" "$DEST_DIR"
ls -l "$DEST_DIR"

echo "🚀 Installation terminée avec permissions $USER:$GROUP"

